/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 22:34:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:24:18
 */
import { GET_DEVICE_MODELS, GET_DEVICE_MODELS_SUCCESS, GET_DEVICE_MODELS_FAILURE, ADD_DEVICE_MODELS, ADD_DEVICE_MODELS_SUCCESS, ADD_DEVICE_MODELS_FAILURE, UPDATE_DEVICE_MODELS, UPDATE_DEVICE_MODELS_SUCCESS, UPDATE_DEVICE_MODELS_FAILURE, OPEN_FORM_DEVICE_MODELS, CLOSE_FORM_DEVICE_MODELS } from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newDeviceModel: null,
    deviceModels: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE_MODELS:
            return { ...state, loading: true, pagination: action.payload }
        case GET_DEVICE_MODELS_SUCCESS:
            return { ...state, loading: false, deviceModels: action.payload }
        case GET_DEVICE_MODELS_FAILURE:
            return { ...state, loading: false, deviceModels: null }
        case ADD_DEVICE_MODELS:
            return { ...state, loading: true }
        case ADD_DEVICE_MODELS_SUCCESS:
            NotificationManager.success('Successfully create new Device Model');
            return { ...state, loading: false, newDeviceModel: action.payload, modal: false, clearForm: true }
        case ADD_DEVICE_MODELS_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceModel: null }
        case UPDATE_DEVICE_MODELS:
            return { ...state, loading: true }
        case UPDATE_DEVICE_MODELS_SUCCESS:
            NotificationManager.success('Successfully update Device Model');
            return { ...state, loading: false, newDeviceModel: action.payload, modal: false, clearForm: true }
        case UPDATE_DEVICE_MODELS_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceModel: null }
        case OPEN_FORM_DEVICE_MODELS:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_DEVICE_MODELS:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}