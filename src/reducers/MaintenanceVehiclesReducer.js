/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 17:37:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 17:37:41
 */
import {GET_MAINTENANCE_VEHICLES, GET_MAINTENANCE_VEHICLES_SUCCESS, GET_MAINTENANCE_VEHICLES_FAILURE, ADD_MAINTENANCE_VEHICLES, ADD_MAINTENANCE_VEHICLES_SUCCESS, ADD_MAINTENANCE_VEHICLES_FAILURE, UPDATE_MAINTENANCE_VEHICLES, UPDATE_MAINTENANCE_VEHICLES_SUCCESS, UPDATE_MAINTENANCE_VEHICLES_FAILURE, OPEN_FORM_MAINTENANCE_VEHICLES, CLOSE_FORM_MAINTENANCE_VEHICLES} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newMaintenance: null,
    maintenance_vehicles: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MAINTENANCE_VEHICLES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_MAINTENANCE_VEHICLES_SUCCESS:
            return { ...state, maintenance_vehicles: action.payload, loading: false }
        case GET_MAINTENANCE_VEHICLES_FAILURE:
            return { ...state, maintenance_vehicles: null, loading: false }
        case ADD_MAINTENANCE_VEHICLES:
            return { ...state, loading: true }
        case ADD_MAINTENANCE_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully create new Maintenance Vehicle');
            return { ...state, loading: false, newMaintenance: action.payload, modal: false, clearForm: true }
        case ADD_MAINTENANCE_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newMaintenance: null }
        case UPDATE_MAINTENANCE_VEHICLES:
            return { ...state, loading: true }
        case UPDATE_MAINTENANCE_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully update Maintenance Vehicle');
            return { ...state, loading: false, newMaintenance: action.payload, modal: false, clearForm: true }
        case UPDATE_MAINTENANCE_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newMaintenance: null }
        case OPEN_FORM_MAINTENANCE_VEHICLES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_MAINTENANCE_VEHICLES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}