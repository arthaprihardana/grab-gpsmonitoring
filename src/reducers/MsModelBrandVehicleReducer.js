/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:19:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 06:29:40
 */
import {GET_MODEL_BRAND_VEHICLES, GET_MODEL_BRAND_VEHICLES_SUCCESS, GET_MODEL_BRAND_VEHICLES_FAILURE, ADD_MODEL_BRAND_VEHICLES, ADD_MODEL_BRAND_VEHICLES_SUCCESS, ADD_MODEL_BRAND_VEHICLES_FAILURE, UPDATE_MODEL_BRAND_VEHICLES, UPDATE_MODEL_BRAND_VEHICLES_SUCCESS, UPDATE_MODEL_BRAND_VEHICLES_FAILURE, OPEN_FORM_MODAL_BRAND_VEHICLES, CLOSE_FORM_MODAL_BRAND_VEHICLES} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newModel: null,
    model_brand_vehicles: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MODEL_BRAND_VEHICLES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_MODEL_BRAND_VEHICLES_SUCCESS:
            return { ...state, model_brand_vehicles: action.payload, loading: false }
        case GET_MODEL_BRAND_VEHICLES_FAILURE:
            return { ...state, model_brand_vehicles: null, loading: false }
        case ADD_MODEL_BRAND_VEHICLES:
            return { ...state, loading: true }
        case ADD_MODEL_BRAND_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully create new Model Brand Vehicles');
            return { ...state, loading: false, newModel: action.payload, modal: false, clearForm: true }
        case ADD_MODEL_BRAND_VEHICLES_FAILURE:
            NotificationManager.error(action.payload.toString());
            return { ...state, loading: false, newModel: null }
        case UPDATE_MODEL_BRAND_VEHICLES: 
            return { ...state, loading: true }
        case UPDATE_MODEL_BRAND_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully update Model Brand Vehicles');
            return { ...state, loading: false, newModel: action.payload, modal: false, clearForm: true }
        case UPDATE_MODEL_BRAND_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newModel: null }
        case OPEN_FORM_MODAL_BRAND_VEHICLES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_MODAL_BRAND_VEHICLES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}