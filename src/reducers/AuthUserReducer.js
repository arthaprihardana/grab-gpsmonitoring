/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-22 11:05:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-22 11:06:01
 */
/**
 * Auth User Reducers
 */
import { NotificationManager } from 'react-notifications';
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILURE,
    // START TO ACCESS GPS MIDDLEWARE
    GENERATE_PASSWORD_TOKEN,
    GENERATE_PASSWORD_TOKEN_SUCCESS,
    GENERATE_PASSWORD_TOKEN_FAILURE,
    // END
} from 'Actions/types';

/**
 * initial auth user
 */
const INIT_STATE = {
    user: JSON.parse(localStorage.getItem('user_id')),
    token: localStorage.getItem('token'),
    loading: false,
    authMiddleware: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOGIN_USER:
            return { ...state, loading: true };
        case LOGIN_USER_SUCCESS:
            NotificationManager.success('User Logged In');
            return { ...state, loading: false, user: action.payload };
        case LOGIN_USER_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false };
        case LOGOUT_USER:
            return { ...state };
        case LOGOUT_USER_SUCCESS:
            return { ...state, user: null };
        case LOGOUT_USER_FAILURE:
            return { ...state };
        case GENERATE_PASSWORD_TOKEN:
            return { ...state, loading: true }
        case GENERATE_PASSWORD_TOKEN_SUCCESS:
            return { ...state, loading: false, authMiddleware: action.payload }
        case GENERATE_PASSWORD_TOKEN_FAILURE:
            return { ...state, loading: false, authMiddleware: null }
        default: return { ...state };
    }
}
