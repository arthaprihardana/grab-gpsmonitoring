/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 06:59:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 07:05:33
 */
import {NotificationManager} from 'react-notifications';
import { GET_ROLE_PAIR_AREA, GET_ROLE_PAIR_AREA_SUCCESS, GET_ROLE_PAIR_AREA_FAILED, ADD_ROLE_PAIR_AREA, ADD_ROLE_PAIR_AREA_SUCCESS, ADD_ROLE_PAIR_AREA_FAILURE, UPDATE_ROLE_PAIR_AREA, UPDATE_ROLE_PAIR_AREA_SUCCESS, UPDATE_ROLE_PAIR_AREA_FAILURE, OPEN_ROLE_PAIR_AREA, CLOSE_ROLE_PAIR_AREA } from '../actions/types';

const INIT_STATE = {
    newRolePairArea: null,
    rolePairAreas: null,
    loading: false,
    modal: false,
    pagination: false,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ROLE_PAIR_AREA:
            return { ...state, loading: true, pagination: action.payload }
        case GET_ROLE_PAIR_AREA_SUCCESS:
            return { ...state, loading: false, rolePairAreas: action.payload }
        case GET_ROLE_PAIR_AREA_FAILED:
            return { ...state, loading: false, rolePairAreas: null }
        case ADD_ROLE_PAIR_AREA:
            return { ...state, loading: true }
        case ADD_ROLE_PAIR_AREA_SUCCESS:
            NotificationManager.success('Successfully create new Role Pair Area');
            return { ...state, loading: false, newRolePairArea: action.payload, modal: false, clearForm: true }
        case ADD_ROLE_PAIR_AREA_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newRolePairArea: null}
        case UPDATE_ROLE_PAIR_AREA:
            return { ...state, loading: true }
        case UPDATE_ROLE_PAIR_AREA_SUCCESS:
            NotificationManager.success('Successfully update new Role Pair Area');
            return { ...state, loading: false, newRolePairArea: action.payload, modal: false, clearForm: true }
        case UPDATE_ROLE_PAIR_AREA_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newRolePairArea: null }
        case OPEN_ROLE_PAIR_AREA:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_ROLE_PAIR_AREA:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}