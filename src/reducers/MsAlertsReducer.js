/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 13:23:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 13:24:10
 */
import {GET_ALERTS, GET_ALERTS_SUCCESS, GET_ALERTS_FAILURE, ADD_ALERTS, ADD_ALERTS_SUCCESS, ADD_ALERTS_FAILURE, UPDATE_ALERTS, UPDATE_ALERTS_SUCCESS, UPDATE_ALERTS_FAILURE, OPEN_FORM_ALERTS, CLOSE_FORM_ALERTS, GET_DETAIL_ALERT_SUMMARY, GET_DETAIL_ALERT_SUMMARY_SUCCESS, GET_DETAIL_ALERT_SUMMARY_FAILURE} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newAlert: null,
    alerts: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false,
    alertSummary: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ALERTS:
            return { ...state, loading: true, pagination: action.payload }
        case GET_ALERTS_SUCCESS:
            return { ...state, alerts: action.payload, loading: false }
        case GET_ALERTS_FAILURE:
            return { ...state, alerts: null, loading: false }
        case ADD_ALERTS:
            return { ...state, loading: true }
        case ADD_ALERTS_SUCCESS:
            NotificationManager.success('Successfully create new Alert Notification');
            return { ...state, loading: false, newAlert: action.payload, modal: false, clearForm: true }
        case ADD_ALERTS_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newAlert: null }
        case UPDATE_ALERTS:
            return { ...state, loading: true }
        case UPDATE_ALERTS_SUCCESS:
            NotificationManager.success('Successfully update new Alert Notification');
            return { ...state, loading: false, newAlert: action.payload, modal: false, clearForm: true }
        case UPDATE_ALERTS_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newAlert: null }
        case OPEN_FORM_ALERTS:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_ALERTS:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        case GET_DETAIL_ALERT_SUMMARY:
            return { ...state, loading: true }
        case GET_DETAIL_ALERT_SUMMARY_SUCCESS:
            return { ...state, loading: false, alertSummary: action.payload }
        case GET_DETAIL_ALERT_SUMMARY_FAILURE:
            return { ...state, loading: false, alertSummary: null }
        default:
            return { ...state }
    }
}