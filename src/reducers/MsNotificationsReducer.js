/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 12:55:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 08:33:42
 */
import {GET_NOTIFICATIONS, GET_NOTIFICATIONS_SUCCESS, GET_NOTIFICATIONS_FAILURE, ADD_NOTIFICATIONS, ADD_NOTIFICATIONS_SUCCESS, ADD_NOTIFICATIONS_FAILURE, UPDATE_NOTIFICATIONS, UPDATE_NOTIFICATIONS_SUCCESS, UPDATE_NOTIFICATIONS_FAILURE, OPEN_FORM_NOTIFICATIONS, CLOSE_FORM_NOTIFICATIONS} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newNotification: null,
    notifications: null,
    loading: false,
    modal: false,
    pagination: false,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_NOTIFICATIONS:
            return { ...state, loading: true, pagination: action.payload }
        case GET_NOTIFICATIONS_SUCCESS:
            return { ...state, notifications: action.payload, loading: false }
        case GET_NOTIFICATIONS_FAILURE:
            return { ...state, notifications: null, loading: false }
        case ADD_NOTIFICATIONS:
            return { ...state, loading: true }
        case ADD_NOTIFICATIONS_SUCCESS:
            NotificationManager.success('Successfully create new Model Brand Vehicles');
            return { ...state, loading: false, newNotification: action.payload, modal: false, clearForm: true }
        case ADD_NOTIFICATIONS_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newNotification: null }
        case UPDATE_NOTIFICATIONS: 
            return { ...state, loading: true }
        case UPDATE_NOTIFICATIONS_SUCCESS:
            NotificationManager.success('Successfully update Model Brand Vehicles');
            return { ...state, loading: false, newNotification: action.payload, modal: false, clearForm: true }
        case UPDATE_NOTIFICATIONS_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newNotification: null }
        case OPEN_FORM_NOTIFICATIONS:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_NOTIFICATIONS:
            return { ...state, modal: false, formWithData: null, clearForm: true }

        default:
            return { ...state }
    }
}