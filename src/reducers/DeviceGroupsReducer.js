/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 00:58:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 01:00:05
 */
import { GET_DEVICE_GROUPS, GET_DEVICE_GROUPS_SUCCESS, GET_DEVICE_GROUPS_FAILURE, ADD_DEVICE_GROUPS, ADD_DEVICE_GROUPS_SUCCESS, ADD_DEVICE_GROUPS_FAILURE, UPDATE_DEVICE_GROUPS, UPDATE_DEVICE_GROUPS_SUCCESS, UPDATE_DEVICE_GROUPS_FAILURE, OPEN_FORM_DEVICE_GROUPS, CLOSE_FORM_DEVICE_GROUPS } from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newDeviceGroup: null,
    deviceGroups: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithdata: null,
    clearForm: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE_GROUPS:
            return { ...state, loading: true, pagination: action.payload }
        case GET_DEVICE_GROUPS_SUCCESS:
            return { ...state, loading: false, deviceGroups: action.payload }
        case GET_DEVICE_GROUPS_FAILURE:
            return { ...state, loading: false, deviceGroups: null }
        case ADD_DEVICE_GROUPS:
            return { ...state, loading: true }
        case ADD_DEVICE_GROUPS_SUCCESS:
            NotificationManager.success('Successfully create new Device Group');
            return { ...state, loading: false, newDeviceGroup: action.payload, modal: false, clearForm: true }
        case ADD_DEVICE_GROUPS_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceGroup: null }
        case UPDATE_DEVICE_GROUPS:
            return { ...state, loading: true }
        case UPDATE_DEVICE_GROUPS_SUCCESS:
            NotificationManager.success('Successfully update Device Group');
            return { ...state, loading: false, newDeviceGroup: action.payload, modal: false, clearForm: true }
        case UPDATE_DEVICE_GROUPS_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceGroup: null }
        case OPEN_FORM_DEVICE_GROUPS:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_DEVICE_GROUPS:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}