/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 21:50:14 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-09-03 21:50:14 
 */
import { GET_DEVICE, GET_DEVICE_SUCCESS, GET_DEVICE_FAILURE, ADD_DEVICE, ADD_DEVICE_SUCCESS, ADD_DEVICE_FAILURE, UPDATE_DEVICE, UPDATE_DEVICE_SUCCESS, UPDATE_DEVICE_FAILURE, OPEN_FORM_DEVICE, CLOSE_FORM_DEVICE } from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newDevice: null,
    devices: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE:
            return { ...state, loading: true, pagination: action.payload }
        case GET_DEVICE_SUCCESS:
            return { ...state, loading: false, devices: action.payload }
        case GET_DEVICE_FAILURE:
            return { ...state, loading: false, devices: null }
        case ADD_DEVICE:
            return { ...state, loading: true }
        case ADD_DEVICE_SUCCESS:
            NotificationManager.success('Successfully create new Device');
            return { ...state, loading: false, newDevice: action.payload, modal: false, clearForm: true }
        case ADD_DEVICE_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDevice: null }
        case UPDATE_DEVICE:
            return { ...state, loading: true }
        case UPDATE_DEVICE_SUCCESS:
            NotificationManager.success('Successfully update Device');
            return { ...state, loading: false, newDevice: action.payload, modal: false, clearForm: true }
        case UPDATE_DEVICE_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDevice: null }
        case OPEN_FORM_DEVICE:
            return { ...state, modal: true, clearForm: false, formWithData: action.payload }
        case CLOSE_FORM_DEVICE:
            return { ...state, modal: false, clearForm: true, formWithData: null }
        default:
            return { ...state }
    }
}