/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-20 23:25:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-23 15:19:39
 */
import { GET_FILTER_DEVICES_MESSAGES, GET_FILTER_DEVICES_MESSAGES_SUCCESS, GET_FILTER_DEVICES_MESSAGES_FAILURE, CLEAR_FILTER_DEVICE_MESSAGES } from "../actions/types";
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    loading: false,
    filter: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_FILTER_DEVICES_MESSAGES:
            return { ...state, loading: true }
        case GET_FILTER_DEVICES_MESSAGES_SUCCESS:
            if(action.payload.data.length > 0) {
                NotificationManager.success('Successfully load play back vehicle');
            } else {
                NotificationManager.error("Failed to load play back Vehicle");    
            }
            return { ...state, loading: false, filter: action.payload }
        case GET_FILTER_DEVICES_MESSAGES_FAILURE:
            NotificationManager.error("Failed to load play back Vehicle");
            return { ...state, loading: false, filter: null }
        case CLEAR_FILTER_DEVICE_MESSAGES:
            return { ...state, filter: null }
        default:
            return { ...state }
    }
}