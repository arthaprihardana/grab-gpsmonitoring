/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 07:42:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:25:32
 */
import { GET_FIELD_NAMES, GET_FIELD_NAMES_SUCCESS, GET_FIELD_NAMES_FAILURE, ADD_FIELD_NAMES, ADD_FIELD_NAMES_SUCCESS, ADD_FIELD_NAMES_FAILURE, UPDATE_FIELD_NAMES, UPDATE_FIELD_NAMES_SUCCESS, UPDATE_FIELD_NAMES_FAILURE, OPEN_FORM_FIELD_NAMES, CLOSE_FORM_FIELD_NAMES } from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newFieldName: null,
    fieldNames: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_FIELD_NAMES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_FIELD_NAMES_SUCCESS:
            return { ...state, loading: false, fieldNames: action.payload }
        case GET_FIELD_NAMES_FAILURE:
            return { ...state, loading: false, fieldNames: null }
        case ADD_FIELD_NAMES:
            return { ...state, loading: true }
        case ADD_FIELD_NAMES_SUCCESS:
            NotificationManager.success('Successfully create new Field Name');
            return { ...state, loading: false, newFieldName: action.payload, modal: false, clearForm: true }
        case ADD_FIELD_NAMES_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newFieldName: null }
        case UPDATE_FIELD_NAMES:
            return { ...state, loading: true }
        case UPDATE_FIELD_NAMES_SUCCESS:
            NotificationManager.success('Successfully update Field Name');
            return { ...state, loading: false, newFieldName: action.payload, modal: false, clearForm: true }
        case UPDATE_FIELD_NAMES_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newFieldName: null }
        case OPEN_FORM_FIELD_NAMES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_FIELD_NAMES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}