/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 11:07:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 11:11:35
 */
import { OPEN_DETAIL_TRACKING_VEHICLE, CLOSE_DETAIL_TRACKING_VEHICLE, GET_TRACKING_VEHICLE, GET_TRACKING_VEHICLE_SUCCESS, GET_TRACKING_VEHICLE_FAILURE, GET_TRACKING_SUMMARY, GET_TRACKING_SUMMARY_SUCCESS, GET_TRACKING_SUMMARY_FAILURE, SEARCH_NOPOL_TRACKING, SEARCH_NOPOL_TRACKING_SUCCESS, SEARCH_NOPOL_TRACKING_FAILURE, GET_DETAIL_ADDRESS, GET_DETAIL_ADDRESS_SUCCESS, GET_DETAIL_ADDRESS_FAILURE} from "../actions/types";

const INIT_STATE = {
    loading: false,
    vehicle: null,
    summary: null,
    modal: false,
    search_nopol: null,
    detail_vehicle: null,
    detail_address: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_TRACKING_VEHICLE:
            return { ...state, loading: true }
        case GET_TRACKING_VEHICLE_SUCCESS:
            return { ...state, loading: false, vehicle: action.payload }
        case GET_TRACKING_VEHICLE_FAILURE:
            return { ...state, loading: false, vehicle: null }
        case GET_TRACKING_SUMMARY:
            return { ...state, loading: true }
        case GET_TRACKING_SUMMARY_SUCCESS:
            return { ...state, loading: false, summary: action.payload }
        case GET_TRACKING_SUMMARY_FAILURE:
            return { ...state, loading: false, summary: null }
        case SEARCH_NOPOL_TRACKING:
            return { ...state, loading: true, search_nopol: action.payload }
        case SEARCH_NOPOL_TRACKING_SUCCESS:
            return { ...state, loading: false, vehicle: action.payload, search_nopol: null }
        case SEARCH_NOPOL_TRACKING_FAILURE:
            return { ...state, loading: false, vehicle: null, search_nopol: null }
        case OPEN_DETAIL_TRACKING_VEHICLE:
            return { ...state, detail_vehicle: action.payload }
        case CLOSE_DETAIL_TRACKING_VEHICLE:
            return { ...state, detail_vehicle: null }
        case GET_DETAIL_ADDRESS:
            return { ...state, loading: true }
        case GET_DETAIL_ADDRESS_SUCCESS:
            return { ...state, loading: false, detail_address: action.payload }
        case GET_DETAIL_ADDRESS_FAILURE: 
            return { ...state, loading: false, detail_address: null }
        default:
            return { ...state }
    }
}