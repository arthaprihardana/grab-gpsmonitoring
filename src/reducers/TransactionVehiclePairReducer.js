/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 19:39:01 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 19:39:01 
 */
import {GET_VEHICLES_DRIVER_PAIR, GET_VEHICLES_DRIVER_PAIR_SUCCESS, GET_VEHICLES_DRIVER_PAIR_FAILURE, ADD_VEHICLES_DRIVER_PAIR, ADD_VEHICLES_DRIVER_PAIR_SUCCESS, ADD_VEHICLES_DRIVER_PAIR_FAILURE, UPDATE_VEHICLES_DRIVER_PAIR, UPDATE_VEHICLES_DRIVER_PAIR_SUCCESS, UPDATE_VEHICLES_DRIVER_PAIR_FAILURE, OPEN_FORM_VEHICLES_DRIVER_PAIR, CLOSE_FORM_VEHICLES_DRIVER_PAIR} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newPairing: null,
    vehicle_pair: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VEHICLES_DRIVER_PAIR:
            return { ...state, loading: true, payload: action.payload }
        case GET_VEHICLES_DRIVER_PAIR_SUCCESS:
            return { ...state, vehicle_pair: action.payload, loading: false }
        case GET_VEHICLES_DRIVER_PAIR_FAILURE:
            return { ...state, vehicle_pair: null, loading: false }
        case ADD_VEHICLES_DRIVER_PAIR:
            return { ...state, loading: true }
        case ADD_VEHICLES_DRIVER_PAIR_SUCCESS:
            NotificationManager.success('Successfully create new Pairing')
            return { ...state, loading: false, newPairing: action.payload, modal: false, clearForm: true }
        case ADD_VEHICLES_DRIVER_PAIR_FAILURE:
            NotificationManager.error(action.payload)
            return { ...state, loading: false, newPairing: null }
        case UPDATE_VEHICLES_DRIVER_PAIR:
            return { ...state, loading: true }
        case UPDATE_VEHICLES_DRIVER_PAIR_SUCCESS:
            return { ...state, loading: false, newPairing: action.payload, modal: false, clearForm: true }
        case UPDATE_VEHICLES_DRIVER_PAIR_FAILURE:
            NotificationManager.error('Opps! something went wrong')
            return { ...state, loading: false, newPairing: null}
        case OPEN_FORM_VEHICLES_DRIVER_PAIR:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_VEHICLES_DRIVER_PAIR:
            return { ...state, modal: false, formWithData: null, clearForm: true}
        default:
            return { ...state }
    }
}