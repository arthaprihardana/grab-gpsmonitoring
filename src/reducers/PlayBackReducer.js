/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-22 17:25:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-23 07:14:57
 */
import { PLAYBACK_PLAY, PLAYBACK_STOP } from "../actions/types";

const INIT_STATE = {
    // start: Date.now()
    start: null,
    elapsed: 0
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case PLAYBACK_PLAY:
            let a = setTimeout(() => {
                return new Date() - action.payload
            }, 3000)
            return { ...state, elapsed: a }
        case PLAYBACK_STOP:
            return { ...state, start: "tost" }
        default:
            return { ...state }
    }
}