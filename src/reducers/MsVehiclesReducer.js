/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 17:17:16 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 17:17:16 
 */
import {GET_VEHICLES, GET_VEHICLES_SUCCESS, GET_VEHICLES_FAILURE, ADD_VEHICLES, ADD_VEHICLES_SUCCESS, ADD_VEHICLES_FAILURE, UPDATE_VEHICLES, UPDATE_VEHICLES_SUCCESS, UPDATE_VEHICLES_FAILURE, OPEN_FORM_VEHICLES, CLOSE_FORM_VEHICLES, GET_VEHICLES_BY_CODE, GET_VEHICLES_BY_CODE_SUCCESS, GET_VEHICLES_BY_CODE_FAILURE} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newVehicles: null,
    vehicles: null,
    searchVehicle: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_VEHICLES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_VEHICLES_SUCCESS:
            return { ...state, vehicles: action.payload, loading: false }
        case GET_VEHICLES_FAILURE:
            return { ...state, vehicles: null, loading: false }
        case GET_VEHICLES_BY_CODE:
            return { ...state, loading: true, code: action.payload }
        case GET_VEHICLES_BY_CODE_SUCCESS:
            let response = action.payload;
            if(response === null) {
                NotificationManager.error('Vehicle Code Tidak Ditemukan');
            }
            return { ...state, loading: false, searchVehicle: action.payload }
        case GET_VEHICLES_BY_CODE_FAILURE:
            return { ...state, loading: false, searchVehicle: null }
        case ADD_VEHICLES:
            return { ...state, loading: true }
        case ADD_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully create new Vehicles');
            return { ...state, loading: false, newVehicles: action.payload, modal: false }
        case ADD_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newVehicles: null }
        case UPDATE_VEHICLES:
            return { ...state, loading: true }
        case UPDATE_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully update new Vehicles');
            return { ...state, loading: false, newVehicles: action.payload, modal: false }
        case UPDATE_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newVehicles: null }
        case OPEN_FORM_VEHICLES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_VEHICLES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}