/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 09:51:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 09:54:28
 */
import {GET_ROLES, GET_ROLES_FAILURE, GET_ROLES_SUCCESS, OPEN_FORM_ROLES, CLOSE_FORM_ROLES, ADD_ROLES, ADD_ROLES_SUCCESS, ADD_ROLES_FAILURE, UPDATE_ROLES, UPDATE_ROLES_SUCCESS, UPDATE_ROLES_FAILURE} from '../actions/types';
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newRole: null,
    roles: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ROLES:
            return { ...state, loading: true, pagination: action.payload}
        case GET_ROLES_SUCCESS:
            return { ...state, roles: action.payload, loading: false}
        case GET_ROLES_FAILURE:
            return { ...state, loading: false, roles: null }
        case ADD_ROLES:
            return { ...state, loading: true }
        case ADD_ROLES_SUCCESS:
            NotificationManager.success('Successfully create new Role');
            return { ...state, loading: false, newRole: action.payload, modal: false, clearForm: true}
        case ADD_ROLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newRole: null}
        case UPDATE_ROLES:
            return { ...state, loading: true }
        case UPDATE_ROLES_SUCCESS:
            NotificationManager.success('Successfully update Role');
            return { ...state, loading: false, newRole: action.payload, modal: false, clearForm: true}
        case UPDATE_ROLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newRole: null}
        case OPEN_FORM_ROLES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_ROLES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}