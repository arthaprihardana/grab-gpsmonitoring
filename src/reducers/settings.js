/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 17:35:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-20 07:18:40
 */
/**
 * App Settings Reducers
 */
import {
	COLLAPSED_SIDEBAR,
	DARK_MODE,
	BOXED_LAYOUT,
	RTL_LAYOUT,
	MINI_SIDEBAR,
	TOGGLE_DARK_SIDENAV,
} from 'Actions/types';

// app config
import AppConfig from 'Constants/AppConfig';

/**
 * initial app settings
 */
const INIT_STATE = {
	navCollapsed: AppConfig.navCollapsed,
	darkMode: AppConfig.darkMode,
	boxLayout: AppConfig.boxLayout,
	rtlLayout: AppConfig.rtlLayout,
	miniSidebar: AppConfig.miniSidebar,
	startUserTour: false,
	isDarkSidenav: AppConfig.isDarkSidenav,
	themes: [
		{
			id: 1,
			name: 'primary'
		},
		{
			id: 2,
			name: 'secondary'
		},
		{
			id: 3,
			name: 'warning'
		},
		{
			id: 4,
			name: 'info'
		},
		{
			id: 5,
			name: 'danger'
		},
		{
			id: 6,
			name: 'success'
		}
	],
	activeTheme: {
		id: 1,
		name: 'primary'
	},
	locale: AppConfig.locale,
	languages: [
		{
			languageId: 'english',
			locale: 'en',
			name: 'English',
			icon: 'en',
		},
	],
};

export default (state = INIT_STATE, action) => {
	switch (action.type) {

		// collapse sidebar
		case COLLAPSED_SIDEBAR:
			return { ...state, navCollapsed: action.isCollapsed };

		// dark mode
		case DARK_MODE:
			return { ...state, darkMode: action.payload };

		// boxed layout
		case BOXED_LAYOUT:
			return { ...state, boxLayout: action.payload };

		// rtl layout
		case RTL_LAYOUT:
			return { ...state, rtlLayout: action.payload };

		// mini sidebar
		case MINI_SIDEBAR:
			return { ...state, miniSidebar: action.payload };

		// dark sidenav
		case TOGGLE_DARK_SIDENAV:
			return { ...state, isDarkSidenav: !state.isDarkSidenav };

		default: return { ...state };
	}
}
