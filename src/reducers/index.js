/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-20 07:12:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-24 09:15:25
 */
/**
 * App Reducers
 */
import { combineReducers } from 'redux';
import settings from './settings';
import sidebarReducer from './SidebarReducer';
import authUserReducer from './AuthUserReducer';
// import feedbacksReducer from './FeedbacksReducer';

import areasReducer from './MsAreasReducer';
import rolesReducer from './MsRolesReducer';
import rolePairAreareducer from './MsRolePairAreaReducer';
import brandVehiclesReducer from './MsBrandVehiclesReducer';
import modelBrandVehicleReducer from './MsModelBrandVehicleReducer';
import statusVehiclesReducer from './MsStatusVehiclesReducer';
import notificationsReducer from './MsNotificationsReducer';
import alertsReducer from './MsAlertsReducer';
import driversReducer from './MsDriversReducer';
import vehiclesReducer from './MsVehiclesReducer';
import maintenanceVehicleReducer from './MaintenanceVehiclesReducer';
import transactionVehiclePair from './TransactionVehiclePairReducer';
import userProfile from './UserProfileReducer';
import zoneReducer from './MsZoneReducer';
import trackingReducer from './TrackingReducer';
import zoneKoordinatReducer from './ZoneKoordinatReducer';
// import PlayBackReducer from './PlayBackReducer';
import reportReducer from './ReportReducer';

// GPS MIDDLEWARE
import DeviceReducer from './DeviceReducer';
import DeviceTypesReducer from './DeviceTypesReducer';
import DeviceModelsReducer from './DeviceModelsReducer';
import DeviceGroupsReducer from './DeviceGroupsReducer';
import DeviceSchemeReducer from './DeviceSchemeReducer';
import EventTypesReducer from './EventTypesReducer';
import FieldNamesReducer from './FieldNamesReducer';
import DeviceSchemeFieldNameReducer from './DeviceSchemeFieldNameReducer';
import DeviceSchemeEventTypeReducer from './DeviceSchemeEventTypesReducer';
import EventTypeConditionReducer from './EventTypeConditionReducer';
import FilterDeviceMessageReducer from './FilterDeviceMessageReducer';

const reducers = combineReducers({
    settings,
    sidebar: sidebarReducer,
    authUser: authUserReducer,
    // feedback: feedbacksReducer,
    areas: areasReducer,
    roles: rolesReducer,
	brand_vehicles: brandVehiclesReducer,
	model_brand_vehicles: modelBrandVehicleReducer,
	status_vehicles: statusVehiclesReducer,
	notifications: notificationsReducer,
	alerts: alertsReducer,
	drivers: driversReducer,
	vehicles: vehiclesReducer,
	maintenance_vehicles: maintenanceVehicleReducer,
	vehicle_pair: transactionVehiclePair,
	user_profile: userProfile,
	zone: zoneReducer,
	rolePairArea: rolePairAreareducer,
	tracking: trackingReducer,
	zoneKoordinat: zoneKoordinatReducer,
	report: reportReducer,
	// GPS MIDDLEWARE
	devices: DeviceReducer,
	deviceTypes: DeviceTypesReducer,
	deviceModels: DeviceModelsReducer,
	deviceGroups:DeviceGroupsReducer,
	deviceSchemes: DeviceSchemeReducer,
	eventTypes: EventTypesReducer,
	fieldNames: FieldNamesReducer,
	deviceSchemeFieldName: DeviceSchemeFieldNameReducer,
	deviceSchemeEventType: DeviceSchemeEventTypeReducer,
	eventTypeCondition: EventTypeConditionReducer,
	filterDeviceMessage: FilterDeviceMessageReducer
});

export default reducers;
