/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 20:04:43 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 20:04:43 
 */
import {GET_USER_PROFILE, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE, ADD_USER_PROFILE, ADD_USER_PROFILE_SUCCESS, ADD_USER_PROFILE_FAILURE, UPDATE_USER_PROFILE, UPDATE_USER_PROFILE_SUCCESS, UPDATE_USER_PROFILE_FAILURE, OPEN_FORM_USER_PROFILE, CLOSE_FORM_USER_PROFILE} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newUser: null,
    user_profile: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_USER_PROFILE:
            return { ...state, loading: true, pagination: action.payload }
        case GET_USER_PROFILE_SUCCESS:
            return { ...state, user_profile: action.payload, loading: false }
        case GET_USER_PROFILE_FAILURE:
            return { ...state, user_profile: null, loading: false }
        case ADD_USER_PROFILE:
            return { ...state, loading: true }
        case ADD_USER_PROFILE_SUCCESS:
            NotificationManager.success('Successfully create new Driver');
            return { ...state, loading: false, newUser: action.payload, modal: false, clearForm: true }
        case ADD_USER_PROFILE_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newUser: null }
        case UPDATE_USER_PROFILE:
            return { ...state, loading: true }
        case UPDATE_USER_PROFILE_SUCCESS:
            NotificationManager.success('Successfully update new Driver');
            return { ...state, loading: false, newUser: action.payload, modal: false, clearForm: true }
        case UPDATE_USER_PROFILE_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newUser: null }
        case OPEN_FORM_USER_PROFILE:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_USER_PROFILE:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}