/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 21:51:49 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:09:13
 */
import { GET_DEVICE_TYPES, GET_DEVICE_TYPES_SUCCESS, GET_DEVICE_TYPES_FAILURE, ADD_DEVICE_TYPES, ADD_DEVICE_TYPES_SUCCESS, ADD_DEVICE_TYPES_FAILURE, UPDATE_DEVICE_TYPES, UPDATE_DEVICE_TYPES_SUCCESS, UPDATE_DEVICE_TYPES_FAILURE, OPEN_FORM_DEVICE_TYPES, CLOSE_FORM_DEVICE_TYPES } from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newDeviceTypes: null,
    deviceTypes: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE_TYPES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_DEVICE_TYPES_SUCCESS:
            return { ...state, loading: false, deviceTypes: action.payload }
        case GET_DEVICE_TYPES_FAILURE:
            return { ...state, loading: false, deviceTypes: null }
        case ADD_DEVICE_TYPES:
            return { ...state, loading: true }
        case ADD_DEVICE_TYPES_SUCCESS:
            NotificationManager.success('Successfully create new Device Types');
            return { ...state, loading: false, newDeviceTypes: action.payload, modal: false, clearForm: true }
        case ADD_DEVICE_TYPES_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceTypes: null }
        case UPDATE_DEVICE_TYPES:
            return { ...state, loading: true }
        case UPDATE_DEVICE_TYPES_SUCCESS:
            NotificationManager.success('Successfully update Device Types');
            return { ...state, loading: false, newDeviceTypes: action.payload, modal: false, clearForm: true }
        case UPDATE_DEVICE_TYPES_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceTypes: null }
        case OPEN_FORM_DEVICE_TYPES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_DEVICE_TYPES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}