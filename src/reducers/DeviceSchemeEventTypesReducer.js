/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 15:31:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:32:57
 */
import { GET_DEVICE_SCHEME_EVENT_TYPE, GET_DEVICE_SCHEME_EVENT_TYPE_SUCCESS, GET_DEVICE_SCHEME_EVENT_TYPE_FAILURE } from "../actions/types";

const INIT_STATE = {
    deviceSchemeEventType: null,
    loading: false,
    pagination: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE_SCHEME_EVENT_TYPE:
            return { ...state, loading: true }
        case GET_DEVICE_SCHEME_EVENT_TYPE_SUCCESS:
            return { ...state, loading: false, deviceSchemeEventType: action.payload }
        case GET_DEVICE_SCHEME_EVENT_TYPE_FAILURE:
            return { ...state, loading: false, deviceSchemeEventType: null }
        default:
            return { ...state }
    }
}