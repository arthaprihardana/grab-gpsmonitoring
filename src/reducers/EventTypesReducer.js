/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 06:58:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 22:44:10
 */
import { GET_EVENT_TYPES, GET_EVENT_TYPES_SUCCESS, GET_EVENT_TYPES_FAILURE, ADD_EVENT_TYPES, ADD_EVENT_TYPES_SUCCESS, ADD_EVENT_TYPES_FAILURE, UPDATE_EVENT_TYPES, UPDATE_EVENT_TYPES_SUCCESS, UPDATE_EVENT_TYPES_FAILURE, OPEN_FORM_EVENT_TYPES, CLOSE_FORM_EVENT_TYPES } from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newEventType: null,
    eventTypes: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_EVENT_TYPES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_EVENT_TYPES_SUCCESS:
            return { ...state, loading: false, eventTypes: action.payload }
        case GET_EVENT_TYPES_FAILURE:
            return { ...state, loading: false, eventTypes: null }
        case ADD_EVENT_TYPES:
            return { ...state, loading: true }
        case ADD_EVENT_TYPES_SUCCESS:
            NotificationManager.success('Successfully create new Event Types');
            return { ...state, loading: false, newEventType: action.payload, modal: false, clearForm: true }
        case ADD_EVENT_TYPES_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newEventType: null }
        case UPDATE_EVENT_TYPES:
            return { ...state, loading: true }
        case UPDATE_EVENT_TYPES_SUCCESS:
            NotificationManager.success('Successfully update Event Types');
            return { ...state, loading: false, newEventType: action.payload, modal: false, clearForm: true }
        case UPDATE_EVENT_TYPES_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newEventType: null }
        case OPEN_FORM_EVENT_TYPES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_EVENT_TYPES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}