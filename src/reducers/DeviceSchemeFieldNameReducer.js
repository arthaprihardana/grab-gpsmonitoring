/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 14:59:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:01:12
 */
import { GET_DEVICE_SCHEME_FIELD_NAME, GET_DEVICE_SCHEME_FIELD_NAME_SUCCESS, GET_DEVICE_SCHEME_FIELD_NAME_FAILURE } from "../actions/types";

const INIT_STATE = {
    deviceSchemeFieldName: null,
    loading: false,
    pagination: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE_SCHEME_FIELD_NAME:
            return { ...state, loading: true }
        case GET_DEVICE_SCHEME_FIELD_NAME_SUCCESS:
            return { ...state, loading: false, deviceSchemeFieldName: action.payload }
        case GET_DEVICE_SCHEME_FIELD_NAME_FAILURE:
            return { ...state, loading: false, deviceSchemeFieldName: null }
        default:
            return { ...state }
    }
}