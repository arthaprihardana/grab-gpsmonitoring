/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 06:34:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 12:26:25
 */
import { GET_DEVICE_SCHEME, GET_DEVICE_SCHEME_SUCCESS, GET_DEVICE_SCHEME_FAILURE, ADD_DEVICE_SCHEME , ADD_DEVICE_SCHEME_SUCCESS, ADD_DEVICE_SCHEME_FAILURE, UPDATE_DEVICE_SCHEME, UPDATE_DEVICE_SCHEME_SUCCESS, UPDATE_DEVICE_SCHEME_FAILURE, OPEN_FORM_DEVICE_SCHEME, CLOSE_FORM_DEVICE_SCHEME} from "../actions/types";
import { NotificationManager } from 'react-notifications';

const INIT_STATE = {
    newDeviceScheme: null,
    deviceScheme: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DEVICE_SCHEME:
            return { ...state, loading: true, pagination: action.payload }
        case GET_DEVICE_SCHEME_SUCCESS:
            return { ...state, loading: false, deviceScheme: action.payload }
        case GET_DEVICE_SCHEME_FAILURE:
            return { ...state, loading: false, deviceScheme: null }
        case ADD_DEVICE_SCHEME:
            return { ...state, loading: true }
        case ADD_DEVICE_SCHEME_SUCCESS:
            NotificationManager.success('Successfully create new Device Scheme');
            return { ...state, loading: false, newDeviceScheme: action.payload, modal: false, clearForm: true }
        case ADD_DEVICE_SCHEME_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceScheme: null }
        case UPDATE_DEVICE_SCHEME:
            return { ...state, loading: true }
        case UPDATE_DEVICE_SCHEME_SUCCESS:
            NotificationManager.success('Successfully update Device Scheme');
            return { ...state, loading: false, newDeviceScheme: action.payload, modal: false, clearForm: true }
        case UPDATE_DEVICE_SCHEME_FAILURE:
            NotificationManager.error(action.payload);
            return { ...state, loading: false, newDeviceScheme: null }
        case OPEN_FORM_DEVICE_SCHEME:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_DEVICE_SCHEME:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}