/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 10:24:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 10:26:46
 */
import { GET_BRAND_VEHICLES, GET_BRAND_VEHICLES_SUCCESS, GET_BRAND_VEHICLES_FAILURE, ADD_BRAND_VEHICLES, ADD_BRAND_VEHICLES_SUCCESS, ADD_BRAND_VEHICLES_FAILURE, UPDATE_BRAND_VEHICLES, UPDATE_BRAND_VEHICLES_SUCCESS, UPDATE_BRAND_VEHICLES_FAILURE, OPEN_FORM_BRAND_VEHICLES, CLOSE_FORM_BRAND_VEHICLES } from "../actions/types";
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newBrand: null,
    brand_vehicles: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_BRAND_VEHICLES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_BRAND_VEHICLES_SUCCESS:
            return { ...state, brand_vehicles: action.payload, loading: false }
        case GET_BRAND_VEHICLES_FAILURE:
            return { ...state, brand_vehicles: null, loading: false }
        case ADD_BRAND_VEHICLES:
            return { ...state, loading: true }
        case ADD_BRAND_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully create new Brand Vehicles');
            return { ...state, loading: false, newBrand: action.payload, modal: false, clearForm: true }
        case ADD_BRAND_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newBrand: null }
        case UPDATE_BRAND_VEHICLES:
            return { ...state, loading: true }
        case UPDATE_BRAND_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully update Brand Vehicles');
            return { ...state, loading: false, newBrand: action.payload, modal: false, clearForm: true }
        case UPDATE_BRAND_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newBrand: null }
        case OPEN_FORM_BRAND_VEHICLES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: true }
        case CLOSE_FORM_BRAND_VEHICLES:
            return { ...state, modal: false, formWithData: null, clearForm: false }
        default:
            return { ...state }
    }
}