/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 16:32:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 16:34:13
 */
import { GET_ZONE_KOORDINAT, GET_ZONE_KOORDINAT_SUCCESS, GET_ZONE_KOORDINAT_FAILURE } from "../actions/types";

const INIT_STATE = {
    loading: false,
    koordinat: null
}

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ZONE_KOORDINAT:
            return { ...state, loading: true }
        case GET_ZONE_KOORDINAT_SUCCESS:
            return { ...state, loading: false, koordinat: action.payload }
        case GET_ZONE_KOORDINAT_FAILURE:
            return { ...state, loading: false, koordinat: null }
        default:
            return { ...state }
    }
}