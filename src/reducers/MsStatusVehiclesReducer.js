/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:41:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 06:39:01
 */
import {GET_STATUS_VEHICLES, GET_STATUS_VEHICLES_SUCCESS, GET_STATUS_VEHICLES_FAILURE, ADD_STATUS_VEHICLES, ADD_STATUS_VEHICLES_SUCCESS, ADD_STATUS_VEHICLES_FAILURE, UPDATE_STATUS_VEHICLES, UPDATE_STATUS_VEHICLES_SUCCESS, UPDATE_STATUS_VEHICLES_FAILURE, OPEN_FORM_STATUS_VEHICLES, CLOSE_FORM_STATUS_VEHICLES} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newStatus: null,
    status_vehicles: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_STATUS_VEHICLES:
            return { ...state, loading: true, pagination: action.payload }
        case GET_STATUS_VEHICLES_SUCCESS:
            return { ...state, status_vehicles: action.payload, loading: false }
        case GET_STATUS_VEHICLES_FAILURE:
            return { ...state, status_vehicles: null, loading: false }
        case ADD_STATUS_VEHICLES:
            return { ...state, loading: true }
        case ADD_STATUS_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully create new Status Vehicle');
            return { ...state, loading: false, newStatus: action.payload, modal: false, clearForm: true }
        case ADD_STATUS_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newStatus: null }
        case UPDATE_STATUS_VEHICLES: 
            return { ...state, loading: true }
        case UPDATE_STATUS_VEHICLES_SUCCESS:
            NotificationManager.success('Successfully update Status Vehicle');
            return { ...state, loading: false, newStatus: action.payload, modal: false, clearForm: true }
        case UPDATE_STATUS_VEHICLES_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newStatus: null }
        case OPEN_FORM_STATUS_VEHICLES:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_STATUS_VEHICLES:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}