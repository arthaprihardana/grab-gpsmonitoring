/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-10 00:21:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:07:02
 */
import { GET_EVENT_TYPE_CONDITION, GET_EVENT_TYPE_CONDITION_SUCCESS, GET_EVENT_TYPE_CONDITION_FAILURE } from "../actions/types";

const INIT_STATE = {
    eventTypeCondition: null,
    loading: false,
    pagination: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_EVENT_TYPE_CONDITION:
            return { ...state, loading: true }
        case GET_EVENT_TYPE_CONDITION_SUCCESS:
            return { ...state, loading: false, eventTypeCondition: action.payload }
        case GET_EVENT_TYPE_CONDITION_FAILURE:
            return { ...state, loading: false, eventTypeCondition: null }
        default:
            return { ...state }
    }
}