/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-27 06:24:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-07 08:59:02
 */
import {GET_MASTER_ZONE, GET_MASTER_ZONE_SUCCESS, GET_MASTER_ZONE_FAILURE, ADD_MASTER_ZONE, ADD_MASTER_ZONE_SUCCESS, ADD_MASTER_ZONE_FAILURE, UPDATE_MASTER_ZONE, UPDATE_MASTER_ZONE_SUCCESS, UPDATE_MASTER_ZONE_FAILURE, OPEN_FORM_MASTER_ZONE, CLOSE_FORM_MASTER_ZONE, OPEN_DRAW_ZONE, CLOSE_DRAW_ZONE, ADD_DRAW_ZONE, ADD_DRAW_ZONE_SUCCESS, ADD_DRAW_ZONE_FAILURE, UPDATE_DRAW_ZONE, UPDATE_DRAW_ZONE_SUCCESS, UPDATE_DRAW_ZONE_FAILURE} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newZone: null,
    newDrawZone: null,
    zone: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: null,
    modalDraw: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_MASTER_ZONE:
            return { ...state, loading: true, pagination: action.payload}
        case GET_MASTER_ZONE_SUCCESS:
            return { ...state, zone: action.payload, loading: false }
        case GET_MASTER_ZONE_FAILURE:
            return { ...state, zone: null, loading: false }
        case ADD_MASTER_ZONE:
            return { ...state, loading: true }
        case ADD_MASTER_ZONE_SUCCESS:
            NotificationManager.success('Successfully create new Zone');
            return { ...state, loading: false, newZone: action.payload, modal: false, clearForm: true}
        case ADD_MASTER_ZONE_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newZone: null }
        case UPDATE_MASTER_ZONE:
            return { ...state, loading: true }
        case UPDATE_MASTER_ZONE_SUCCESS:
            NotificationManager.success('Successfully update Zone');
            return { ...state, loading: false, newZone: action.payload, modal: false, clearForm: true}
        case UPDATE_MASTER_ZONE_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newZone: null }
        case OPEN_FORM_MASTER_ZONE:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false}
        case CLOSE_FORM_MASTER_ZONE:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        case OPEN_DRAW_ZONE:
            return { ...state, modalDraw: true }
        case CLOSE_DRAW_ZONE:
            return { ...state, modalDraw: false }
        case ADD_DRAW_ZONE:
            return { ...state, loading: true }
        case ADD_DRAW_ZONE_SUCCESS:
            NotificationManager.success('Successfully Add New Draw Zone');
            return { ...state, loading: false, newDrawZone: action.payload, modalDraw: false }
        case ADD_DRAW_ZONE_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newDrawZone: null }
        case UPDATE_DRAW_ZONE:
            return { ...state, loading: true }
        case UPDATE_DRAW_ZONE_SUCCESS:
            NotificationManager.success('Successfully Update New Draw Zone');
            return { ...state, loading: false, newDrawZone: action.payload, modalDraw: false }
        case UPDATE_DRAW_ZONE_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newDrawZone: null }
        default:
            return { ...state }
    }
}