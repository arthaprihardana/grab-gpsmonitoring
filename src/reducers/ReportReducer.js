/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 08:49:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-24 09:14:34
 */
import {NotificationManager} from 'react-notifications';
import { REPORT_HISTORICAL_REQUEST, REPORT_HISTORICAL_REQUEST_SUCCESS, REPORT_HISTORICAL_REQUEST_FAILURE, REPORT_FLEET_UTILIZATION_REQUEST, REPORT_FLEET_UTILIZATION_REQUEST_SUCCESS, REPORT_FLEET_UTILIZATION_REQUEST_FAILURE, REPORT_OVERSPEED_REQUEST, REPORT_OVERSPEED_REQUEST_SUCCESS, REPORT_OVERSPEED_REQUEST_FAILURE, REPORT_KM_DRIVEN_REQUEST, REPORT_KM_DRIVEN_REQUEST_SUCCESS, REPORT_KM_DRIVEN_REQUEST_FAILURE, RESET_ALL_REPORT, REPORT_DRIVER_SCORE_REQUEST, REPORT_DRIVER_SCORE_REQUEST_FAILURE , REPORT_DRIVER_SCORE_REQUEST_SUCCESS, REPORT_OUT_OF_GEOFENCE_REQUEST, REPORT_OUT_OF_GEOFENCE_REQUEST_SUCCESS, REPORT_OUT_OF_GEOFENCE_REQUEST_FAILURE, REPORT_GPS_NOT_UPDATE_REQUEST, REPORT_GPS_NOT_UPDATE_REQUEST_SUCCESS, REPORT_GPS_NOT_UPDATE_REQUEST_FAILURE, REPORT_NOTIFICATION_REQUEST, REPORT_NOTIFICATION_REQUEST_SUCCESS, REPORT_NOTIFICATION_REQUEST_FAILURE, REPORT_UNPLUGGED, REPORT_UNPLUGGED_SUCCESS, REPORT_UNPLUGGED_FAILURE, REPORT_UNPLUGGED_REQUEST, REPORT_UNPLUGGED_REQUEST_SUCCESS, REPORT_UNPLUGGED_REQUEST_FAILURE} from '../actions/types';

const INIT_STATE = {
    loading: false,
    reportHistorical: null,
    reportFleetUtil: null,
    reportOverspeed: null,
    reportKmDriven: null,
    reportDriverScore: null,
    reportOutOfGeofence: null,
    reportGpsNotUpdate: null,
    reportNotification: null,
    reportUnplugged: null
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case REPORT_HISTORICAL_REQUEST:
            return { ...state, loading: true }
        case REPORT_HISTORICAL_REQUEST_SUCCESS:
            return { ...state, loading: false, reportHistorical: action.payload }
        case REPORT_HISTORICAL_REQUEST_FAILURE:
            NotificationManager.error('Report data tidak ditemukan');
            return { ...state, loading: false, reportHistorical: null }
        case REPORT_FLEET_UTILIZATION_REQUEST:
            return { ...state, loading: true }
        case REPORT_FLEET_UTILIZATION_REQUEST_SUCCESS:
            return { ...state, loading: false, reportFleetUtil: action.payload }
        case REPORT_FLEET_UTILIZATION_REQUEST_FAILURE:
            NotificationManager.error('Report data tidak ditemukan');
            return { ...state, loading: false, reportFleetUtil: null }
        case REPORT_OVERSPEED_REQUEST:
            return { ...state, loading: true }
        case REPORT_OVERSPEED_REQUEST_SUCCESS:
            return { ...state, loading: false, reportOverspeed: action.payload }
        case REPORT_OVERSPEED_REQUEST_FAILURE:
            NotificationManager.error('Report data tidak ditemukan');
            return { ...state, loading: false, reportOverspeed: null }
        case REPORT_KM_DRIVEN_REQUEST:
            return { ...state, loading: true }
        case REPORT_KM_DRIVEN_REQUEST_SUCCESS:
            return { ...state, loading: false, reportKmDriven: action.payload }
        case REPORT_KM_DRIVEN_REQUEST_FAILURE:
            NotificationManager.error('Report data tidak ditemukan');
            return { ...state, loading: false, reportKmDriven: null }
        case REPORT_DRIVER_SCORE_REQUEST:
            return { ...state, loading: true }
        case REPORT_DRIVER_SCORE_REQUEST_SUCCESS:
            return { ...state, loading: false, reportDriverScore: action.payload }
        case REPORT_DRIVER_SCORE_REQUEST_FAILURE:
            NotificationManager.error('Report data tidak ditemukan');
            return { ...state, loading: false, reportDriverScore: null }
        case REPORT_OUT_OF_GEOFENCE_REQUEST:
            return { ...state, loading: true }
        case REPORT_OUT_OF_GEOFENCE_REQUEST_SUCCESS:
            return { ...state, loading: false, reportOutOfGeofence: action.payload }
        case REPORT_OUT_OF_GEOFENCE_REQUEST_FAILURE:
            NotificationManager.error('Report data tidak ditemukan');
            return { ...state, loading: false, reportOutOfGeofence: null }
        case REPORT_GPS_NOT_UPDATE_REQUEST:
            return { ...state, loading: true }
        case REPORT_GPS_NOT_UPDATE_REQUEST_SUCCESS:
            return { ...state, loading: false, reportGpsNotUpdate: action.payload }
        case REPORT_GPS_NOT_UPDATE_REQUEST_FAILURE:
            return { ...state, loading: false, reportGpsNotUpdate: null }
        case REPORT_NOTIFICATION_REQUEST:
            return { ...state, loading: true }
        case REPORT_NOTIFICATION_REQUEST_SUCCESS:
            return { ...state, loading: false, reportNotification: action.payload }
        case REPORT_NOTIFICATION_REQUEST_FAILURE:
            return { ...state, loading: false, reportNotification: null }
        case REPORT_UNPLUGGED_REQUEST:
            return { ...state, loading: true }
        case REPORT_UNPLUGGED_REQUEST_SUCCESS:
            return { ...state, loading: false, reportUnplugged: action.payload }
        case REPORT_UNPLUGGED_REQUEST_FAILURE:
            return { ...state, loading: false, reportUnplugged: null }
        case RESET_ALL_REPORT:
            return { 
                reportHistorical: null,
                reportFleetUtil: null,
                reportOverspeed: null,
                reportKmDriven: null,
                reportDriverScore: null,
                reportOutOfGeofence: null,
                reportGpsNotUpdate: null,
                reportNotification: null,
                reportUnplugged: null
            }
        default:
            return { ...state }
    }
}