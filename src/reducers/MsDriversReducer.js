/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 13:56:41 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 13:56:41 
 */
import {GET_DRIVERS, GET_DRIVERS_SUCCESS, GET_DRIVERS_FAILURE, ADD_DRIVERS, ADD_DRIVERS_SUCCESS, ADD_DRIVERS_FAILURE, UPDATE_DRIVERS, UPDATE_DRIVERS_SUCCESS, UPDATE_DRIVERS_FAILURE, OPEN_FORM_DRIVERS, CLOSE_FORM_DRIVERS} from '../actions/types';
import {NotificationManager} from 'react-notifications';

const INIT_STATE = {
    newDriver: null,
    drivers: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DRIVERS:
            return { ...state, loading: true, pagination: action.payload }
        case GET_DRIVERS_SUCCESS:
            return { ...state, drivers: action.payload, loading: false }
        case GET_DRIVERS_FAILURE:
            return { ...state, drivers: null, loading: false }
        case ADD_DRIVERS:
            return { ...state, loading: true }
        case ADD_DRIVERS_SUCCESS:
            NotificationManager.success('Successfully create new Driver');
            return { ...state, loading: false, newDriver: action.payload, modal: false, clearForm: true }
        case ADD_DRIVERS_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newDriver: null }
        case UPDATE_DRIVERS:
            return { ...state, loading: true }
        case UPDATE_DRIVERS_SUCCESS:
            NotificationManager.success('Successfully update Driver');
            return { ...state, loading: false, newDriver: action.payload, modal: false, clearForm: true }
        case UPDATE_DRIVERS_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newDriver: null }
        case OPEN_FORM_DRIVERS:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_DRIVERS:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}