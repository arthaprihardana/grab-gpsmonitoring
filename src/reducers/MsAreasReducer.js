/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-24 16:51:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-29 05:08:45
 */
import {
    GET_AREAS,
    GET_AREAS_SUCCESS,
    GET_AREAS_FAILURE,
    ADD_AREA,
    ADD_AREA_SUCCESS,
    ADD_AREA_FAILURE,
    OPEN_FORM_AREA,
    CLOSE_FORM_AREA,
    UPDATE_AREA,
    UPDATE_AREA_SUCCESS,
    UPDATE_AREA_FAILURE
} from '../actions/types';
import {NotificationManager} from 'react-notifications';
/**
 * @const INIT_STATE Area Reducer
 */
const INIT_STATE = {
    newArea: null,
    areas: null,
    loading: false,
    modal: false,
    pagination: null,
    formWithData: null,
    clearForm: false
}
/**
 * @exports Master Area Reducer
 */
export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_AREAS:
            return { ...state, loading: true, pagination: action.payload }
        case GET_AREAS_SUCCESS:
            return { ...state, areas: action.payload, loading: false}
        case GET_AREAS_FAILURE:
            return { ...state, loading: false, areas: action.payload}
        case ADD_AREA:
            return { ...state, loading: true}
        case ADD_AREA_SUCCESS:
            NotificationManager.success('Successfully create new Area');
            return { ...state, loading: false, newArea: action.payload, modal: false, clearForm: true }
        case ADD_AREA_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newArea: null}
        case UPDATE_AREA:
            return { ...state, loading: true }
        case UPDATE_AREA_SUCCESS:
            NotificationManager.success('Successfully update Area');
            return { ...state, loading: false, newArea: action.payload, modal: false, clearForm: true}
        case UPDATE_AREA_FAILURE:
            NotificationManager.error('Opps! something went wrong');
            return { ...state, loading: false, newArea: null}
        case OPEN_FORM_AREA:
            return { ...state, modal: true, formWithData: action.payload, clearForm: false }
        case CLOSE_FORM_AREA:
            return { ...state, modal: false, formWithData: null, clearForm: true }
        default:
            return { ...state }
    }
}