/*======= Include All App Css and Js Files =======*/
// Use Themify Icon
import 'Assets/themify-icons/themify-icons.css';
// notifications
import 'react-notifications/lib/notifications.css';
// Custom Style File
import 'Assets/scss/_style.scss';