/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-27 06:40:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-11 14:46:37
 */
const styles = theme => ({
    root: {
      width: '100%',
      marginTop: theme.spacing.unit * 3,
    },
    table: {
      minWidth: 1020,
    },
    tableWrapper: {
      overflowX: 'auto',
    },
    progress: {
      margin: theme.spacing.unit * 2,
    },
    // TextInput Bottstrap
    bootstrapRoot: {
      padding: 0,
      width: '100%',
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    bootstrapRootPassword: {
      padding: 0,
      width: '100%',
      border: '1px solid #ced4da',
      borderRadius: '4px',
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    bootstrapRootSearchVehicle: {
      padding: 0,
      width: '100%',
      borderTop: '1px solid #CCCCCC',
      borderBottom: '1px solid #CCCCCC',
      borderRadius: '0px',
      'label + &': {
        marginTop: theme.spacing.unit * 3,
      },
    },
    bootstrapInput: {
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
      border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 12px',
      height: '22px',
      // width: '100%',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      '&:focus': {
        borderColor: '#80bdff',
        boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      },
    },
    bootstrapInputPassword: {
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
      // border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 12px',
      height: '22px',
      // width: '100%',
      // transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      // '&:focus': {
      //   borderColor: '#80bdff',
      //   boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      // },
    },
    bootstrapInputSearchVehicle: {
      borderRadius: 0,
      backgroundColor: theme.palette.common.white,
      // border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 12px',
      height: '22px',
      // width: '100%',
      // transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      // '&:focus': {
      //   borderColor: '#80bdff',
      //   boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
      // },
    },
    bootstrapFormLabel: {
      fontSize: 20
    },
    // Select Bootstrap
    input: {
      display: "flex",
      padding: "0px 0px 0px 10px",
      marginTop: 10,
      borderRadius: 4,
      backgroundColor: theme.palette.common.white,
      border: "1px solid #ced4da",
      fontSize: 16,
      width: "100%",
      transition: theme.transitions.create(["border-color", "box-shadow"]),
      fontFamily: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"'
      ].join(","),
      "&:focus": {
        borderColor: "#80bdff",
        boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
      }
    },
    valueContainer: {
      display: "flex",
      flexWrap: "wrap",
      flex: 1,
      alignItems: "center"
    },
    noOptionsMessage: {
      padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
    },
    singleValue: {
      fontSize: 16
    },
    placeholder: {
      position: "absolute",
      left: 12,
      fontSize: 16
    },
    paper: {
      position: "absolute",
      zIndex: 1,
      marginTop: theme.spacing.unit,
      left: 0,
      right: 0
    },
    divider: {
      height: theme.spacing.unit * 2
    },
    // test
    chip: {
      margin: theme.spacing.unit / 4,
    },
    // We had to use a lot of global selectors in order to style react-select.
    // We are waiting on https://github.com/JedWatson/react-select/issues/1679
    // to provide a better implementation.
    // Also, we had to reset the default style injected by the library.
    '@global': {
      '.Select-control': {
        display: 'flex',
        alignItems: 'center',
        borderRadius: 4,
        backgroundColor: theme.palette.common.white,
        // border: 0,
        border: '1px solid #ced4da',
        height: 'auto',
        background: 'transparent',
        padding: '10px 12px',
        '&:hover': {
          // boxShadow: 'none',
          borderColor: "#80bdff",
          boxShadow: "0 0 0 0.2rem rgba(0,123,255,.25)"
        },
      },
      '.Select-multi-value-wrapper': {
        flexGrow: 1,
        display: 'flex',
        flexWrap: 'wrap',
      },
      '.Select--multi .Select-input': {
        margin: 0,
      },
      '.Select.has-value.is-clearable.Select--single > .Select-control .Select-value': {
        // padding: 0,
        padding: '10px 12px'
      },
      '.Select-noresults': {
        padding: theme.spacing.unit * 2,
      },
      '.Select-input': {
        display: 'inline-flex !important',
        padding: 0,
        height: 'auto',
      },
      '.Select-input input': {
        background: 'transparent',
        border: 0,
        padding: 0,
        cursor: 'default',
        display: 'inline-block',
        fontFamily: 'inherit',
        fontSize: 'inherit',
        margin: 0,
        outline: 0,
      },
      '.Select-placeholder, .Select--single .Select-value': {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        display: 'flex',
        alignItems: 'center',
        fontFamily: theme.typography.fontFamily,
        fontSize: theme.typography.pxToRem(16),
        padding: 12,
      },
      '.Select-placeholder': {
        opacity: 0.42,
        color: theme.palette.common.black,
      },
      '.Select-menu-outer': {
        backgroundColor: theme.palette.background.paper,
        boxShadow: theme.shadows[2],
        // position: 'absolute',
        // left: 0,
        // top: `calc(100% + ${theme.spacing.unit}px)`,
        position: 'relative',
        marginTop:'10px',
        width: '100%',
        zIndex: 2,
        maxHeight: 48 * 4.5,
      },
      '.Select.is-focused:not(.is-open) > .Select-control': {
        boxShadow: 'none',
      },
      '.Select-menu': {
        maxHeight: 48 * 4.5,
        overflowY: 'auto',
      },
      '.Select-menu div': {
        boxSizing: 'content-box',
      },
      '.Select-arrow-zone, .Select-clear-zone': {
        color: theme.palette.action.active,
        cursor: 'pointer',
        textAlign: 'center',
        height: 21,
        width: 21,
        zIndex: 1,
      },
      '.Select-clear-zone': {
        position: 'absolute',
        right: '30px',
        top: '19px'
      },
      // Only for screen readers. We can't use display none.
      '.Select-aria-only': {
        position: 'absolute',
        overflow: 'hidden',
        clip: 'rect(0 0 0 0)',
        height: 1,
        width: 1,
        margin: -1,
      },
    },
});

export default styles;