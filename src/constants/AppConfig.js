/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-20 06:24:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-20 06:26:09
 */
/**
 * App Config File
 */
const AppConfig = {
    appLogo: require('Assets/img/logo.png'),                // App Logo
    // appLogo: require('Assets/img/hello-world.png'),                // App Logo
    brandName: 'Grab Monitoring',                           // Brand Name
    navCollapsed: false,                                    // Sidebar collapse
    darkMode: false,                                        // Dark Mode
    boxLayout: false,                                       // Box Layout
    rtlLayout: false,                                       // RTL Layout
    miniSidebar: false,                                     // Mini Sidebar
    isDarkSidenav: true,                                   // Set true to dark sidebar
    locale: {
        languageId: 'english',
        locale: 'en',
        name: 'English',
        icon: 'en',
    },
    // enableUserTour: process.env.NODE_ENV === 'production' ? true : false,  // Enable / Disable User Tour
    enableUserTour: false,  // Enable / Disable User Tour
    copyRightText: 'Grab © 2018 All Rights Reserved.',      // Copy Right Text
    // light theme colors
    themeColors: {
        // 'primary': '#5D92F4',
        'primary': '#009d3b',
        'secondary': '#677080',
        'success': '#00D014',
        'danger': '#FF3739',
        'warning': '#FFB70F',
        'info': '#00D0BD',
        'dark': '#464D69',
        'default': '#FAFAFA',
        'greyLighten': '#A5A7B2',
        'grey': '#677080',
        'white': '#FFFFFF',
        'purple': '#896BD6',
        'yellow': '#D46B08'
    },
    // dark theme colors
    darkThemeColors: {
        darkBgColor: '#424242'
    }
}

export default AppConfig;
