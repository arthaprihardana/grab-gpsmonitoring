/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-22 10:31:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-24 06:16:22
 */
import axios from 'axios';
// // DEVELOPMENT
// const BASE_URI = "http://178.128.49.206/api";
// export const WS = 'ws://178.128.49.206:8900';
// export const GOOGLE_MAP_API = "AIzaSyC_F2Xqon90td5CzgdpDHT4elkQyqUxqXA";
// // SETTING GPS MIDDLEWARE START
// const GPS_MIDDLEWARE_API = "http://188.166.182.12:8081";
// const GPS_MIDDLEWARE_CLIENT_ID = "1";
// const GPS_MIDDLEWARE_CLIENT_SECRET = "f0DR3Tp9Unxo3NPSKLdPEVeRciwXsMPY8Ofa3GtF";
// const GPS_MIDDLEWARE_USERNAME = "rakishmu@gmail.com";
// const GPS_MIDDLEWARE_PASSWORD = "p@ssword123456";
// const GPS_MIDDLEWARE_SCOPE = "backend message-stream";

// PRODUCTION
const BASE_URI = "http://178.128.114.171/api";
export const WS = 'ws://178.128.114.171:8900';
export const GOOGLE_MAP_API = "AIzaSyC_F2Xqon90td5CzgdpDHT4elkQyqUxqXA";
// SETTING GPS MIDDLEWARE START
const GPS_MIDDLEWARE_API = "http://178.128.221.107";
const GPS_MIDDLEWARE_CLIENT_ID = "1";
const GPS_MIDDLEWARE_CLIENT_SECRET = "f0DR3Tp9Unxo3NPSKLdPEVeRciwXsMPY8Ofa3GtF";
const GPS_MIDDLEWARE_USERNAME = "adisadono@gmail.com";
const GPS_MIDDLEWARE_PASSWORD = "act-n3k4t";
const GPS_MIDDLEWARE_SCOPE = "backend message-stream";

export const GPS_MIDDLEWARE_OAUTH_REQUEST = {
    client_id: GPS_MIDDLEWARE_CLIENT_ID,
    client_secret: GPS_MIDDLEWARE_CLIENT_SECRET,
    username: GPS_MIDDLEWARE_USERNAME,
    password: GPS_MIDDLEWARE_PASSWORD,
    scope: GPS_MIDDLEWARE_SCOPE
};
// SETTING GPS MIDDLEWARE END

axios.defaults.headers.post['Content-Type'] = 'application/json';
// axios.defaults.headers.common['Access-Control-Request-Headers'] = null;
// axios.defaults.headers.common['Access-Control-Request-Method'] = null;

export const LOGIN = `${BASE_URI}/auth/login`;
export const REFRESH_TOKEN = async () => {
    let token = localStorage.getItem('token');
    return await axios.get(`${BASE_URI}/auth/refresh`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error)
}

// ALERT
export const ALERT = `${BASE_URI}/alert`;
// AREAS
export const AREAS = `${BASE_URI}/areas`;
// DRIVER
export const DRIVER = `${BASE_URI}/driver`;
// VEHICLE MODEL
export const VEHICLE_MODEL = `${BASE_URI}/vehicle-model`;
// NOTIFICATION
export const NOTIFICATION = `${BASE_URI}/notification`;
// ROLES
export const ROLES = `${BASE_URI}/role`;
// VEHICLE STATUS
export const VEHICLE_STATUS = `${BASE_URI}/vehicle-status`;
// VEHICLE
export const VEHICLE = `${BASE_URI}/vehicle`;
// ZONE
export const ZONE = `${BASE_URI}/zone`;
// ZONE DETAIL KOORDINAT
export const ZONE_KOORDINAT = `${BASE_URI}/zone-detail-coordinate`;
// REGISTER
export const REGISTER_USER = `${BASE_URI}/register`;
// USER PROFILE
export const USER_PROFILE = `${BASE_URI}/user-profile`;
// VEHICLE MAINTENANCE
export const VEHICLE_MAINTENANCE = `${BASE_URI}/vehicle-maintenance`;
// TRANSACTION VEHICLE PAIR
export const TRANSACTION_VEHICLE_PAIR = `${BASE_URI}/transaction-vehicle-pair`;
// TRANSACTION VEHICLE BRAND
export const VEHICLE_BRAND = `${BASE_URI}/vehicle-brand`;
// ROLE PAIR AREA
export const ROLE_PAIR_AREA = `${BASE_URI}/role-pair-area`;
// MW MAPPING
export const MW_MAPPING = `${BASE_URI}/mw-mapping`;
// MW MAPPING TOTAL
export const MW_MAPPING_TOTAL = `${BASE_URI}/mw-mapping-total`;
// REPORT HISTORICAL
export const REPORT_HISTORICAL = `${BASE_URI}/rpt-historycal`;
// REPORT FLEET UTILIZATION
export const REPORT_FLEET_UTILIZATION = `${BASE_URI}/rpt-fleetUtilisation`;
// REPORT OVERSPEED
export const REPORT_OVERSPEED = `${BASE_URI}/rpt-overSpeed`;
// REPORT KM DRIVEN
export const REPORT_KM_DRIVEN = `${BASE_URI}/rpt-KMdriven`;
// REPORT DRIVER SCORE
export const REPORT_DRIVER_SCORE = `${BASE_URI}/rpt-driverScore`;
// REPORT OUT OF GEOFENCE
export const REPORT_OUT_OF_GEOFENCE = `${BASE_URI}/rpt-outOfGeofence`;
// REPORT GPS NOT UPDATE
export const REPORT_GPS_NOT_UPDATE = `${BASE_URI}/rpt-gpsNotUpdate`;
// REPORT NOTIFICATION
export const REPORT_NOTIFICATION = `${BASE_URI}/rpt-notification`;
// REPORT UNPLUGGED
export const REPORT_UNPLUGGED = `${BASE_URI}/rpt-unplugged`;
// DETAIL ADDRESS
export const API_GET_DETAIL_ADDRESS = `${BASE_URI}/address-detail`;
// DETAIL ALERT SUMMARY
export const API_GET_DETAIL_ALERT_SUMMARY = `${BASE_URI}/show-alert-status-detail`;

/** 
 * GPS MIDDLEWARE
 */
export const API_GENERATE_PASSWORD_TOKEN = `${GPS_MIDDLEWARE_API}/oauth/token`;
export const API_GENERATE_REFRESH_TOKEN = async () => {
    let refresh_token = localStorage.getItem('refresh_token');
    return await axios.post(API_GENERATE_PASSWORD_TOKEN, {
        grant_type: 'refresh_token',
        client_id: GPS_MIDDLEWARE_OAUTH_REQUEST.client_id,
        client_secret: GPS_MIDDLEWARE_OAUTH_REQUEST.client_secret,
        refresh_token: refresh_token,
        scope: GPS_MIDDLEWARE_OAUTH_REQUEST.scope
    }, {
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response)
    .catch(error => error);
}
export const API_DEVICE = `${GPS_MIDDLEWARE_API}/api/v1/backend/devices`;
export const API_DEVICE_TYPES = `${GPS_MIDDLEWARE_API}/api/v1/backend/device-types`;
export const API_DEVICE_MODELS = `${GPS_MIDDLEWARE_API}/api/v1/backend/device-models`;
export const API_DEVICE_GROUPS = `${GPS_MIDDLEWARE_API}/api/v1/backend/device-groups`;
export const API_DEVICE_SCHEMES = `${GPS_MIDDLEWARE_API}/api/v1/backend/device-schemes`;
export const API_EVENT_TYPES = `${GPS_MIDDLEWARE_API}/api/v1/backend/event-types`;
export const API_FIELD_NAMES = `${GPS_MIDDLEWARE_API}/api/v1/backend/field-names`;
export const API_SCHEME_FIELD_NAME = `${GPS_MIDDLEWARE_API}/api/v1/backend/device-scheme-field-names`;
export const API_SCHEME_EVENT_TYPE = `${GPS_MIDDLEWARE_API}/api/v1/backend/device-scheme-event-types`;
export const API_EVENT_TYPE_CONDITION = `${GPS_MIDDLEWARE_API}/api/v1/backend/event-type-conditions`;
export const API_FILTER_DEVICE_MESSAGES = `${GPS_MIDDLEWARE_API}/api/v1/backend/filter-device-messages`;