/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 21:54:36 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-16 08:27:32
 */
/**
 * Routes Monitoring
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { 
    AsyncPlayBack,
    AsyncTracking
} from "Components/AsyncComponent/AsyncComponent";

const Monitoring = ({ match }) => (
    <Switch>
        <Redirect exact from={`${match.url}/`} to={`${match.url}/monitoring`} />
        <Route path={`${match.url}/play-back`} component={AsyncPlayBack} />
        <Route path={`${match.url}/tracking`} component={AsyncTracking} />
    </Switch>
);

export default Monitoring;