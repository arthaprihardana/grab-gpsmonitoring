/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 08:08:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-15 16:51:50
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Drawer, Hidden, Button, Typography, Grid, LinearProgress } from '@material-ui/core';
import { PlayArrow as IconPlay, Pause as IconPause, AccessTime as IconTime, Stop as IconStop, FastForward as IconFastForward } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import GoogleMap from 'google-map-react';
import _ from 'lodash';

import gmaps from '../../../lib/gmap';
import PlayBackSideBar from './SideBar';
import { GOOGLE_MAP_API } from '../../../constants/LibraryAPI';
import moment from 'moment';
import {getFilterDeviceMessage, getZone, clearFilterDeviceMessage} from '../../../actions';

const styles = theme => ({
	root: {
		flexGrow: 1,
		zIndex: 1,
		overflow: 'hidden',
		position: 'relative',
		display: 'flex',
		width: '100%',
	},
	toolbar: theme.mixins.toolbar,
	drawerPaper: {
		width: 230,
		[theme.breakpoints.up('md')]: {
			position: 'relative',
			width: drawerWidth
		},
		backgroundColor:'#fff'
	},
	content: {
		flexGrow: 1
	},
});

const drawerWidth = 310;

class PlayBack extends Component {

    state = {
		mobileOpen: false,
		zone: [],
		play: false,
		path: [],
		pathInfo: [],
		duration: 0,
		point: 0,
		// tick: [2000, 1500, 1000],	// milisecond
		tick: [1200, 800, 300],	// milisecond
		tickChoose: 0
	}
	
	// point = 0;
	cnt = 0;
	map = null;
	directionsService = null;
	position = null;
	marker = null;
	polyline = null;
	speed = 0.000005;
	wait = 1;
	timerHandle = null;
	steps = [];
	step = 1; // default 50; // metres
	eol = null;
	k = 0;
	stepnum = 0;
	speed = "";
	lastVertex = 1;
	car = "M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z";
	icon = null;
	markers = [];

	constructor(props) {
		super(props);
		this.animate = this.animate.bind(this);
	}
	

	componentDidMount() {
		this.props.getZone({ page: 1 });
	}

	componentWillUnmount() {
		clearTimeout(this.timerHandle);
		this.props.clearFilterDeviceMessage();
	}

    handleDrawerToggle = () => {
		this.setState({ mobileOpen: !this.state.mobileOpen });
	}

	calcRoute = (map, maps) => {
		if (this.timerHandle) {
			clearTimeout(this.timerHandle);
		}
		if (this.marker) {
			this.marker.setMap(null);
		}
		this.polyline = new maps.Polyline({
			path: this.state.path,
			geodesic: true,
			strokeColor: '#2196F3',
			strokeOpacity: 1.0,
			strokeWeight: 2
		});
		this.icon = {
			path: this.car,
			scale: .7,
			strokeColor: 'white',
			strokeWeight: .10,
			fillOpacity: 1,
			fillColor: '#404040',
			offset: '5%',
			anchor: new maps.Point(10, 25) // orig 10,50 back of car, 10,0 front of car, 10,25 center of car
		};
		this.marker =  new maps.Marker({
			position: this.polyline.getPath().getAt(this.state.point),
			map: map,
			icon: this.icon
		});
		var rendererOptions = {
			map: map
		};
		let directionsDisplay = new maps.DirectionsRenderer(rendererOptions);
		var bounds = new maps.LatLngBounds();
		this.polyline.setMap(map);
	}

	animate = (d) => {
		let self = this;
		let maps = this._map.maps_;
		let map = this._map.map_;

		if(this.state.point > this.polyline.getPath().getLength()) {
			map.panTo(_.last(this.state.path));
			this.marker.setPosition(_.last(this.state.path));
			this.setState({ play: !this.state.play, point: 0 });
			clearTimeout(this.timerHandle)
			return;
		}
		// Metode 1 ( get data per point tetapi heading mobil ga sesuai)
		// let p = this.polyline.GetPointAtDistance(this.state.point);
		// map.panTo(this.polyline.getPath().getAt(this.state.point));
		// var lastPosn = this.marker.getPosition();
		// this.marker.setPosition(this.polyline.getPath().getAt(this.state.point));

		// Metode 2 ( heading mobil sesuai tetapi get data per point ga sesuai )
		let p = this.polyline.GetPointAtDistance(this.state.duration);
		map.panTo(p);
		var lastPosn = this.marker.getPosition();

		this.marker.setPosition(p);
		var heading = maps.geometry.spherical.computeHeading(lastPosn, p);
		this.icon.rotation = heading;
		this.marker.setIcon(this.icon);
		this.timerHandle = setTimeout(function() {
			self.setState({
				duration: self.state.duration + self.step,
				point: self.state.point + 1
			}, self.animate())
		}, this.state.tick[this.state.tickChoose]);
	}

	startAnimation = () => {
		let map = this._map.map_;
		this.eol = this.polyline.Distance();
		map.setCenter(this.polyline.getPath().getAt(this.state.point));
		this.setState({
			duration: this.state.duration !== 0 ? this.state.duration : 0,
			point: this.state.point
		}, this.animate())
	}

	getSnapshotBeforeUpdate(prevProps, prevState) {
		if(this.props.detailZone !== prevProps.detailZone) {
			return { zone: this.props.detailZone }
		}
		if(this.props.filter !== prevProps.filter) {
			return { filter: this.props.filter }
		}
		return null;
	}
	
	
	componentDidUpdate(prevProps, prevState, snapshot) {
		if(snapshot !== null) {
			if(snapshot.zone !== undefined) {
				this.setState(prevState => {
					return { zone: snapshot.zone }
				})
			}
			if(snapshot.filter !== undefined) {
				let maps = this._map.maps_;
				let map = this._map.map_;
				if(snapshot.filter.data.length > 0) {
					this.setState(prevState => {
						return { 
							path: snapshot.filter.data.map((value, key) => {
								return { lat:value.latitude, lng: value.longitude }
							}),
							pathInfo: snapshot.filter.data.map((value, key) => {
								return { 
									created_at: value.created_at,
									device_time: value.device_time,
									server_time: value.server_time,
									speed: value.speed,
									trip_odometer: value.trip_odometer,
									total_odometer: value.total_odometer,
									gsm_signal_level: value.gsm_signal_level
								}
							})
						}
					}, () => this.calcRoute(map, maps))
				}
			}
		}
		if(this.state.play !== prevState.play) {
			if(this.state.play) {
				this.startAnimation();
			} else {
				clearTimeout(this.timerHandle);
			}
		}
	}

	renderZonePolygon = (map, maps) => {
		const { zone } = this.state;
		if(zone.length > 0) {
			var infoWindow = new maps.InfoWindow();
			var detailZone = new Array();
			zone.map((value, key) => {
				if(value.zone_detail.length > 0) {
					const latlng = value.zone_detail.map((val, idx) => {
						return new maps.LatLng(val.latitude, val.longitude)
					});
					detailZone[key] = new maps.Polygon({
						path: latlng,
						strokeColor: value.type_zone === "OUT" ? "#FF0000" : "#2196F3",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: value.type_zone === "OUT" ? "#FF0000" : "#2196F3",
						fillOpacity: 0.35
					});
					detailZone[key].setMap(map);
					maps.event.addListener(detailZone[key], 'click', event => {
						var contentString = "<b>"+value.zone_name+"</b><br />Type : "+value.type_zone;
						infoWindow.setContent(contentString);
						infoWindow.setPosition(event.latLng);
						infoWindow.open(map); 
					})
				}
			});
		}
	}
    
    render() {
		const { classes, loading, filter } = this.props;
		const { pathInfo, point } = this.state;
		// console.log('info ==>', pathInfo[point]);
        return (
            <div className="chat-wrapper">
				{loading && <LinearProgress /> }
                <div className={classes.root}>
                    <Hidden mdUp className="user-list-wrap">
                        <Drawer
							variant="temporary"
							anchor={'left'}
							open={this.state.mobileOpen}
							onClose={this.handleDrawerToggle}
							classes={{
								paper: classes.drawerPaper,
							}}
							ModalProps={{
								keepMounted: true,
							}}>
							<PlayBackSideBar />
						</Drawer>
                    </Hidden>
                    <Hidden smDown implementation="css" className="user-list-wrap">
						<Drawer
							variant="permanent"
							open
							classes={{
								paper: classes.drawerPaper,
							}}>
                            <PlayBackSideBar />
						</Drawer>
					</Hidden>
                    <div className={`chat-content ${classes.content}`}>
                        <GoogleMap
							ref={ref => this._map = ref}
                            bootstrapURLKeys={{ key: GOOGLE_MAP_API }}
                            yesIWantToUseGoogleMapApiInternals={true}
							resetBoundsOnResize={true}
                            center={ filter !== null && filter.data.length > 0 ? [filter.data[0].latitude, filter.data[0].longitude] : [-1.212843, 119.046166]}
                            zoom={filter !== null && filter.data.length > 0 ? 18 : 5} style={{ position: 'relative', width: '100%', height: '93vh' }}
							onGoogleApiLoaded={({ map, maps }) => {
								this.renderZonePolygon(map, maps);
								gmaps(map, maps);
							}}
							options={maps => {
								return {
									styles: [{
										featureType: 'all',
										elementType: 'labels',
										stylers: [{
											visibility: 'on'
										}]
									}],
									mapTypeControl: true,
									mapTypeId: maps.MapTypeId.ROADMAP,
									mapTypeControlOptions: {
										style: maps.MapTypeControlStyle.HORIZONTAL_BAR,
										position: maps.ControlPosition.TOP_LEFT,
										mapTypeIds: [
											maps.MapTypeId.ROADMAP,
											maps.MapTypeId.SATELLITE,
											maps.MapTypeId.HYBRID,
											maps.MapTypeId.TERRAIN
										]
									},
									streetViewControl: true,
								}
							}}
                        />
						{ filter !== null && filter.data.length > 0 && <Grid container spacing={0} style={{
							width: 500,
							position: 'absolute', 
							bottom: 100,
							left: 0, 
							right: 0, 
							marginLeft: 'auto', 
							marginRight: 'auto', 
							backgroundColor: 'rgba(0,0,0,0.5)',
							borderRadius: 4
						}}>
							<Grid item xs={12}>
								<div style={{ flexDirection: 'row', justifyContent:'center', alignItems: 'center', textAlign: 'center'}}>
									<Button
										onClick={() => {
											let map = this._map.map_;
											this.marker.setPosition(_.first(this.state.path));
											map.setCenter(_.first(this.state.path));
											this.setState({ 
												play: false, 
												point: 0,
												duration: 0
											});
											clearTimeout(this.timerHandle)
										}} 
										style={{ 
											width: 50, 
											minWidth: 50,
											height: 50, 
											textAlign: 'center'
										}}>
										<IconStop style={{ color: '#FFF', fontSize: '28px' }} />
									</Button>
									<Button
										onClick={() => this.setState({ play: !this.state.play })} 
										style={{ 
											width: 50, 
											minWidth: 50,
											height: 50, 
											textAlign: 'center'
										}}>
										{this.state.play ? <IconPause style={{ color: '#FFF', fontSize: '28px' }} /> : <IconPlay style={{ color: '#FFF', fontSize: '28px' }} /> }
									</Button>
									<Button
										onClick={() => {
											this.setState({ tickChoose: (this.state.tickChoose + 1 >= this.state.tick.length) ? 0 : this.state.tickChoose + 1 })
										}} 
										style={{ 
											width: 50, 
											minWidth: 50,
											height: 50, 
											textAlign: 'center'
										}}>
										<IconFastForward style={{ color: '#FFF', fontSize: '28px' }} /><br />
									</Button>
									<Typography variant="caption" style={{ position: 'absolute', right: 130, top: 18, color: '#FFF'}}>
										{`Speed ${this.state.tickChoose}x`}
									</Typography>
								</div>
							</Grid>
							<Grid item xs={4} style={{ height: 40 }}>
								<Typography variant="body2" style={{ color: '#FFF' }} align="center">
									{ pathInfo[point] !== undefined ? `${pathInfo[point].speed} km/h` : '0 km/h' }
								</Typography>
							</Grid>
							<Grid item xs={4} style={{ height: 40 }}>
								<Typography variant="body2" style={{ color: '#FFF' }} align="center">
									{ pathInfo[point] !== undefined ? moment(pathInfo[point].device_time).format('lll') : moment().format('lll') }
								</Typography>
							</Grid>
							<Grid item xs={4} style={{ height: 40 }}>
								<Typography variant="body2" style={{ color: '#FFF' }} align="center">
									{ pathInfo[point] !== undefined ? `${_.round(pathInfo[point].trip_odometer, 4)} km` : '0 km' }
								</Typography>
							</Grid>
						</Grid> }
                    </div>
                </div>
            </div>
        );
    }
}

const classPlayBack = withStyles(styles, { withTheme: true })(PlayBack);

const mapStateToProps = ({ zone, filterDeviceMessage }) => {
	const detailZone = zone.zone !== null && zone.zone.data;
	const { loading, filter } = filterDeviceMessage;
	return { detailZone: detailZone, loading, filter };
}

export default connect(mapStateToProps, {
	getFilterDeviceMessage,
	getZone,
	clearFilterDeviceMessage
})(classPlayBack);