/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-11 20:47:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-21 01:15:11
 */
import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import {IconButton, Paper, TextField, InputAdornment, CircularProgress, FormGroup, MenuList, MenuItem, ListItemIcon, ListItemText, LinearProgress} from '@material-ui/core';
import { UnfoldMore as IconUnfoldMore, LocalTaxi as IconTaxi } from '@material-ui/icons';
import { Button } from 'reactstrap';
import { withStyles } from '@material-ui/core/styles';
import { Scrollbars } from 'react-custom-scrollbars';
// Date Picker
import { DateTimePicker } from 'material-ui-pickers';
import styles from '../../../lib/styles';
import moment from 'moment';
import 'moment/locale/id';
import {getTrackingVehicle, getFilterDeviceMessage} from '../../../actions';

class PlayBackSideBar extends Component {

    state = {
        vehicleList: [],
        searchVehicle: "",
        vehicle: null,
        imei: null,
        from: moment().subtract(3, 'days').format('YYYY-MM-DD HH:mm:ss'),
        to: moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss')
    }
    
    onSearchVehicle = (searchVehicle) => {
        this.props.getTrackingVehicle({ search: searchVehicle })
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.searchVehicle !== prevProps.searchVehicle) {
            if(this.props.searchVehicle.length > 0) {
                this.setState({ vehicleList: this.props.searchVehicle });
            }
        }
    }

    render() {
        const { classes } = this.props;
        const { vehicleList, vehicle } = this.state;
        return (
            <div className="chat-sidebar">
                <Scrollbars
                    className="rct-scroll"
                    autoHide
                    style={{ height: 'calc(100vh - 64px)' }}>
                    <div className="media p-0">
                        <div className="media-body pl-20 pr-20">
                            <span className="mb-20 text-indigo font-2x d-block pl-0 pt-20">Play Back</span>
                            <div className="form-group">
                                <TextField
                                    fullWidth
                                    type={'text'}
                                    id="search-vehicle"
                                    label="Search Vehicle"
                                    // value={this.state.vehicle ? this.state.vehicle : ""}
                                    margin="none"
                                    InputProps={{
                                        disableUnderline: true,
                                        classes: {
                                            root: classes.bootstrapRootPassword,
                                            input: classes.bootstrapInputPassword,
                                        },
                                        endAdornment: (
                                            <InputAdornment position="end" style={{ marginTop: '6px'}}>
                                                <IconButton onClick={() => {
                                                    if(this.state.searchVehicle.length >= 3) {
                                                        this.onSearchVehicle(this.state.searchVehicle);
                                                    }
                                                    }}>
                                                    <i className="zmdi zmdi-search"></i>
                                                </IconButton>
                                                {this.props.loading &&
                                                    <CircularProgress style={{ position: 'absolute', right: 7, top: 2 }} size={36} /> }
                                            </InputAdornment>
                                        )
                                    }}
                                    value={this.state.searchVehicle !== null ? this.state.searchVehicle : ""}
                                    InputLabelProps={{
                                        shrink: true,
                                        className: classes.bootstrapFormLabel,
                                    }}
                                    onChange={(event) => {
                                        let self = this;
                                        this.setState({ searchVehicle: event.target.value }, () => {
                                            if(self.state.searchVehicle.length >= 3) {
                                                self.onSearchVehicle(self.state.searchVehicle);
                                            }
                                        })
                                    }} 
                                    />
                                {vehicleList && vehicleList.length > 0 && 
                                <Paper className={classes.paper} style={{ marginLeft: 20, marginRight: 20 }}>
                                    <MenuList>
                                        {vehicleList.map((value, key) => (
                                            <MenuItem key={key} onClick={() => this.setState({ 
                                                imei: value.imei,
                                                vehicle: value.license_plate,
                                                searchVehicle: value.license_plate,
                                                vehicleList: []
                                                }) }>
                                                <ListItemIcon className={classes.icon}>
                                                    {/* <IconTaxi /> */}
                                                    <i className="zmdi zmdi-directions-car" style={{ fontSize: '18px' }}></i>
                                                </ListItemIcon>
                                                <ListItemText inset primary={value.license_plate} />
                                            </MenuItem>
                                        ))}
                                    </MenuList>
                                </Paper> }
                            </div>
                            <div className="form-group">
                                <Fragment>
                                    <div className="rct-picker">
                                        <DateTimePicker
                                            ref={"from"}
                                            label="Periode"
                                            value={this.state.from}
                                            onChange={(date) => this.setState({ from: moment(date).format('YYYY-MM-DD HH:mm:ss') }) }
                                            format={'DD/MM/YYYY HH:mm:ss'}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.bootstrapRootPassword,
                                                    input: classes.bootstrapInputPassword,
                                                },
                                                endAdornment: (
                                                    <InputAdornment position="end" style={{
                                                        position: 'absolute',
                                                        right: '16px',
                                                        bottom: '7px',
                                                        color: '#999'
                                                    }}>
                                                        <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.from.open() }>
                                                            <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                        </IconButton>
                                                    </InputAdornment>
                                                ),
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: classes.bootstrapFormLabel,
                                            }}
                                            animateYearScrolling={false}
                                            leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                            rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                            fullWidth
                                            keyboard
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/,' ', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                                            ampm={false}
                                            minDate={moment().subtract(1, 'month').format('YYYY-MM-DD')}
                                            maxDate={moment().format('YYYY-MM-DD')}
                                        />
                                    </div>
                                </Fragment>
                                <div style={{ width: '100%', textAlign: 'center', marginBottom: 10, marginTop: 15 }}>
                                    <IconUnfoldMore style={{ color: '#999999' }} />
                                </div>
                                <Fragment>
                                    <div className="rct-picker">
                                        <DateTimePicker
                                            ref={"to"}
                                            label=""
                                            value={this.state.to}
                                            onChange={(date) => this.setState({ to: moment(date).format('YYYY-MM-DD HH:mm:ss') })}
                                            format={'DD/MM/YYYY HH:mm:ss'}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.bootstrapRootPassword,
                                                    input: classes.bootstrapInputPassword,
                                                },
                                                endAdornment: (
                                                    <InputAdornment position="end" style={{
                                                        position: 'absolute',
                                                        right: '16px',
                                                        bottom: '7px',
                                                        color: '#999'
                                                    }}>
                                                        <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.to.open() }>
                                                            <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                        </IconButton>
                                                    </InputAdornment>
                                                ),
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: classes.bootstrapFormLabel,
                                            }}
                                            animateYearScrolling={false}
                                            leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                            rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                            fullWidth
                                            keyboard
                                            mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/,' ', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                                            ampm={false}
                                            minDate={moment(this.state.from).format('YYYY-MM-DD')}
                                            maxDate={moment(this.state.from).add(1, 'week').format('YYYY-MM-DD')}
                                        />
                                    </div>
                                </Fragment>
                            </div>

                            <FormGroup className="mb-15">
                                <Button outline color="success" style={{ height: 45 }} onClick={() => this.props.getFilterDeviceMessage({
                                    FilterData: {
                                        imei: this.state.imei,
                                        start_time: this.state.from,
                                        end_time: this.state.to
                                    }
                                }) }>
                                    Search
                                </Button>
                            </FormGroup>
                        </div>
                    </div>
                </Scrollbars>
			</div>
        );
    }
}

const classPlayBackSidebar = withStyles(styles)(PlayBackSideBar);

const mapStateToProps = ({ tracking, filterDeviceMessage }) => {
    const { loading, vehicle } = tracking;
    const searchVehicle = vehicle !== null ? vehicle.data.Data : null;
    return { loading, searchVehicle, filterDeviceMessage }
}

export default connect(mapStateToProps, {
    getTrackingVehicle,
    getFilterDeviceMessage
})(classPlayBackSidebar);