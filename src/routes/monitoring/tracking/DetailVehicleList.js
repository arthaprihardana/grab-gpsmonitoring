/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-13 23:22:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 00:12:14
 */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {IconButton, Grid, Paper, Typography, Divider} from '@material-ui/core';
import _ from 'lodash';
import { Place as IconPlace } from '@material-ui/icons';
import { Close } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import { Scrollbars } from 'react-custom-scrollbars';
import {connect} from 'react-redux';
import {closeDetailTrackingVehicle, closeDetailTrackingVehicleSuccess, getDetailAddress} from '../../../actions';
import moment from 'moment';
import 'moment/locale/id'
import ab from '../../../lib/autobahn';
import {WS, GOOGLE_MAP_API} from '../../../constants/LibraryAPI';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        height: 140,
        width: 100,
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
});

class DetailVehicleList extends Component {

    state = {
        detail_address: null
    }

    componentDidMount() {
        this.props.getDetailAddress({
            longitude: this.props.data.location.split(',')[1].trim(),
            latitude: this.props.data.location.split(',')[0].trim()
        })
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.detail_address !== prevProps.detail_address) {
            return { detail_address: this.props.detail_address.data }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.detail_address !== undefined) {
                let dt = snapshot.detail_address.Data;
                this.setState(prevState => {
                    return { detail_address: dt !== null ? snapshot.detail_address.Data.address : null }
                })
            }
        }
    }

    render() {
        const { classes, data } = this.props;
        return (
            <div className="chat-sidebar">
                <Scrollbars
                    className="rct-scroll"
                    autoHide
                    style={{ height: 'calc(100vh - 64px)' }}>
                    <IconButton onClick={() => this.props.closeDetailTrackingVehicle() } className={classes.button} aria-label="Close" style={{ position: 'absolute', right: '10px' }}>
                        <Close />
                    </IconButton>
                    <div className="media pt-10 pr-20 pl-20">
                        <div className="media-body">
                            <span className="mb-10 text-indigo font-2x d-block">Details</span>
                            <Grid container>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Vehicle Description</h4>
                                    <span className="text-muted fs-14">{data ? data.vehicle_description : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Vehicle Machine Number</h4>
                                    <span className="text-muted fs-14">{data ? data.machine_number : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">SIM Card Number</h4>
                                    <span className="text-muted fs-14">{data ? data.simcard_number : "-"}</span>
                                </Grid>
                            </Grid>
                            <Grid container className="mb-20">
                                <Grid item xs={6}>
                                    <h4 className="mb-5">License Plate</h4>
                                    <span className="text-muted fs-14">{data ? data.license_plate : "-"}</span>
                                </Grid>
                                <Grid item xs={6}>
                                    <h4 className="mb-5">Driver Name</h4>
                                    <span className="text-muted fs-14">{data ? data.driver_name : "-"}</span>
                                </Grid>
                            </Grid>
                            <Divider className="mb-20" />
                            <Grid container>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Time</h4>
                                    <span className="text-muted fs-14">{data ? moment(data.device_time).format('HH:mm') : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Vehicle Status</h4>
                                    <span className="text-muted fs-14">{data ? data.vehicle_status : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Position</h4>
                                    <a href={`https://www.google.com/maps/search/?api=1&query=${data.location}`} target="_blank">
                                        <span className="text-muted fs-14">{data ? data.location : "-"}</span>
                                    </a>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Address</h4>
                                    <span className="text-muted fs-14">{this.state.detail_address !== null ? this.state.detail_address : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Total Distance</h4>
                                    <span className="text-muted fs-14">{data ? `${data.total_odometer} KM` : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Voltage Accu</h4>
                                    <span className="text-muted fs-14">{data ? `${data.external_power_voltage} V` : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Immo Status</h4>
                                    <span className="text-muted fs-14">{data ? (data.digital_output_1 === 0 ? "OFF" : "ON") : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Sleep Mode</h4>
                                    <span className="text-muted fs-14">{data ? (data.deep_sleep === 0 ? "OFF" : "ON") : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">GPS Battery</h4>
                                    <span className="text-muted fs-14">{data ? `${_.round((parseFloat(data.internal_battery_voltage) / 4) * 100, 2)}%` : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">GSM Signal</h4>
                                    <span className="text-muted fs-14">{data ? `${data.gsm_signal_level * 20}%` : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Fuel Tank</h4>
                                    <span className="text-muted fs-14">0.00%</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Altitude</h4>
                                    <span className="text-muted fs-14">{data ? data.altitude : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Engine RPM</h4>
                                    <span className="text-muted fs-14">{data ? data.engine_speed_x : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Speed</h4>
                                    <span className="text-muted fs-14">{data ? data.speed : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">DTC</h4>
                                    <span className="text-muted fs-14">null</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Engine Coolant Temperature</h4>
                                    <span className="text-muted fs-14">{data && data.engine_coolant_temperature_x !== null ? data.engine_coolant_temperature_x : "-"}</span>
                                </Grid>
                                <Grid item xs={12} className="mb-10">
                                    <h4 className="mb-5">Direction</h4>
                                    <span className="text-muted fs-14">{data ? data.direction : "-"}</span>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </Scrollbars>
			</div>
        );
    }
}

const classDetailVehicleList = withStyles(styles)(DetailVehicleList);

const mapStateToProps = ({ tracking }) => {
    const { detail_vehicle, detail_address, loading } = tracking;
    return { detail_vehicle, detail_address, loading };
}

export default connect(mapStateToProps, {
    closeDetailTrackingVehicle,
    closeDetailTrackingVehicleSuccess,
    getDetailAddress
})(classDetailVehicleList);