/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-11 12:52:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 00:13:22
 */
import React, { Component } from 'react';
import { TextField, InputAdornment, IconButton, CircularProgress, ListItem , Button, List, Typography, Chip} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import { Input } from 'reactstrap';
import { connect } from 'react-redux';
import { Scrollbars } from 'react-custom-scrollbars';
import { withRouter } from 'react-router-dom';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';

// styles
import styles from '../../../lib/styles';
import {openDetailTrackingVehicle, getTrackingVehicle} from '../../../actions/TrackingActions';

class VehicleList extends Component {

    state = {
        search_vehicle: '',
        data: [],
        loading: false,
        loadingDetail: false
    }

    componentDidMount() {
        this.props.getTrackingVehicle()
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.vehicle !== prevProps.vehicle) {
            if(this.props.vehicle !== null) {
                let data = this.props.vehicle.data.Data;
                this.setState({ data: data, loading: false });
            }
        }
    }

    onSearchVehicles = () => {
        this.setState({ loading: true })
        this.props.getTrackingVehicle({ search: this.state.search_vehicle });
    }

    render() {
        const { classes } = this.props;
        const { data } = this.state;
        return (
            <div>
                <div className="search-wrapper mb-0 position-relative">
                    <TextField
                        fullWidth
                        type={'text'}
                        id="search-vehicle"
                        margin="none"
                        placeholder="Search Vehicle (NOPOL) ..."
                        InputProps={{
                            disableUnderline: true,
                            classes: {
                                root: classes.bootstrapRootSearchVehicle,
                                input: classes.bootstrapInputSearchVehicle,
                            },
                            endAdornment: (
                                <InputAdornment position="end" style={{ marginTop: '6px'}}>
                                    <IconButton onClick={() => this.onSearchVehicles()}>
                                        <i className="zmdi zmdi-search" style={{color: "#02a84a"}}></i>
                                    </IconButton>
                                    {this.state.loading &&
                                        <CircularProgress style={{ position: 'absolute', right: 7, top: 2 }} size={36} /> }
                                </InputAdornment>
                            ),
                        }}
                        onChange={(event) => this.setState({ search_vehicle: event.target.value }) }
                        />
                </div>
                <div className="chat-list">
                    <Scrollbars
                        className="rct-scroll"
                        autoHide
                        style={{ height: 'calc(100vh - 188px)' }}
                        >
                        {/* List Vehicle */}
                        <List className="p-0 mb-0">
                            {data && data.length > 0 ? data.map((value, key) => (
                                <ListItem
                                    key={key}
                                    onClick={() => {
                                        this.setState(prevState => {
                                            return { loadingDetail: true }
                                        })
                                        this.props.openDetailTrackingVehicle(value) 
                                    }}
                                    className={'user-list-item'}>
                                    <div className="d-flex justify-content-between w-100 align-items-center">
                                        <div className="media align-items-center w-90">
                                            <div className="media-left position-relative mr-10">
                                                <Button variant="fab" mini disabled className="text-white mr-15 mb-10 mt-1" style={{ backgroundColor: value.vehicle_status_color}}>
                                                    <i className="zmdi zmdi-directions-car" style={{ fontSize: '18px' }}></i>
                                                </Button>
                                            </div>
                                            <div className="media-body pt-5">
                                                <h5 className="mb-0">No Polisi</h5>
                                                <Typography variant="subheading">{value.license_plate}</Typography>
                                            </div>
                                        </div>
                                        <div className="text-right msg-count">
                                            <Chip label={value.vehicle_status} className={classes.chip} style={{ backgroundColor: value.vehicle_status_color, color: '#FFFFFF' }} />
                                        </div>
                                    </div>
                                </ListItem>
                            )) : <ListItem style={{ justifyContent: 'center' }}>
                                    <Typography variant="caption">Vehicle Not Found</Typography>
                                </ListItem> }
                            {this.props.loading && <div style={{ width: '100%', textAlign: 'center' }}><CircularProgress className={classes.progress} size={20} /></div> }
                        </List>
                        {this.state.loadingDetail && <RctSectionLoader /> }
                        {/*  */}
                    </Scrollbars>
                </div>
            </div>
        );
    }
}


const classVehicleList = withStyles(styles)(VehicleList);

const mapStateToProps = ({ tracking }) => {
    const { loading, vehicle, detail_vehicle } = tracking;
    return { loading, vehicle, detail_vehicle }
}

export default connect(mapStateToProps, {
    openDetailTrackingVehicle,
    getTrackingVehicle
})(classVehicleList);