/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-11 12:48:09 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 13:43:49
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Grid, Typography, CircularProgress} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import VehicleList from './VehicleList';
import DetailVehicleList from './DetailVehicleList';
import {getTrackingSummary} from '../../../actions/TrackingActions';
import 'moment/locale/id';

const styles = theme => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      height: 140,
      width: 100,
    },
    control: {
      padding: theme.spacing.unit * 2,
    },
    progress: {
        margin: theme.spacing.unit * 2,
    },
});

class TrackingSideBar extends Component {

    state = {
        detail: null,
        summary: []
    }

    componentDidMount() {
        this.props.getTrackingSummary();
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.summary !== prevProps.summary) {
            if(this.props.summary !== null) {
                let data = this.props.summary.data.Data;
                this.setState({
                    summary: data
                })
            }
        }
    }

    render() {
        const { classes, detailVehicle } = this.props;
        const { summary } = this.state;
        return (
            detailVehicle && detailVehicle !== null ? 
                <div className="chat-sidebar">
                    <DetailVehicleList data={detailVehicle} />
                </div>
                : 
                <div className="chat-sidebar">
                    <div className="user-wrap d-flex justify-content-between" style={{ background: '#eee', minHeight: 70 }}>
                        <Grid container className={classes.root} justify="space-between" spacing={16} style={{ padding: 16 }}>
                            {summary && summary.length > 0 ? 
                                summary && summary.slice(0, 3).map((value, key) => (
                                    <Grid key={key} item style={{ textAlign: 'center' }}>
                                        <Typography variant="title" gutterBottom style={{ color: "#363A45" }}>{value.total}</Typography>
                                        <Typography variant="caption" style={{ color: "#363A45" }}>{value.statusVehicle}</Typography>
                                    </Grid>
                                ))
                            : 
                                ["Moving","Stop","Offline"].map((value, key) => (
                                    <Grid key={key} item style={{ textAlign: 'center' }}>
                                        <Typography variant="title" gutterBottom style={{ color: "#363A45" }}>0</Typography>
                                        <Typography variant="caption" style={{ color: "#363A45" }}>{value}</Typography>
                                    </Grid>
                                ))
                            }
                        </Grid>
                    </div>
                    <VehicleList />
                </div>
        );
    }
}

const classTrackingSideBar = withStyles(styles)(TrackingSideBar);

const mapStateToProps = ({ tracking }) => {
    const { summary, detail_vehicle, loading } = tracking;
    return { summary, detail_vehicle, loading };
}

export default connect(mapStateToProps, {
    getTrackingSummary
})(classTrackingSideBar);