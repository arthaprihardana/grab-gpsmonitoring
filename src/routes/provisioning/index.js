import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { AsyncDevice, AsyncDeviceIOMapper } from '../../components/AsyncComponent/AsyncComponent';

const Provisioning = ({ match }) => (
    <div className="content-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}`} to={`${match.url}/device`} />
            <Route path={`${match.url}/device`} component={AsyncDevice} />
            <Route path={`${match.url}/device-io-mapper`} component={AsyncDeviceIOMapper} />
        </Switch>
    </div>
);

export default Provisioning;