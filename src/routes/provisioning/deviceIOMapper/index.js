/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-02 21:20:08 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-04 06:50:05
 */
// React
import React, { Component } from 'react';
// Page Title Bar
import PageTitleBar from '../../../components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from '../../../components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from '../../../util/IntlMessages';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Material Core
import { Tabs, Tab, Typography } from '@material-ui/core';
// Styles
import styles from './styles';
// Part Of Main IO Mapper
import DeviceSchemes from './device-schemes';
import EventTypes from './event-types';
import FieldNames from './field-names';


class MainDeviceIOMapper extends Component {
    
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.deviceIOMapper" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className={classes.root}>
                        <Tabs
                        value={value}
                        onChange={this.handleChange}
                        classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                        >
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Device Schemes"
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Event Types"
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Field Names"
                            />
                        </Tabs>
                        {value === 0 && <DeviceSchemes />}
                        {value === 1 && <EventTypes />}
                        {value === 2 && <FieldNames />}
                    </div>
                </RctCollapsibleCard>
            </div>
        )
    }
}

export default withStyles(styles)(MainDeviceIOMapper);