/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 14:15:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:48:43
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, Tabs, Tab, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// Component
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addDeviceScheme, closeFormDeviceScheme, updateDeviceScheme } from "../../../../actions";
// Validator
import FormValidator from '../../../../helpers/FormValidation';
// Detail Device SCheme
import FieldName from './field-name';
import EventType from './event-type';

class FormDeviceScheme extends Component {

    validator = new FormValidator([
        {
            field: 'code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Code tidak boleh kososng'
        },
        {
            field: 'event_type_input',
            method: 'isEmpty',
            validWhen: false,
            message: 'Event Type Input tidak boleh kososng'
        }
    ])

    submitted = false;

    state = {
        code: '',
        event_type_input: '',
        tab_value: 0,
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    code: '',
                    event_type_input: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    code: this.props.data.attributes.code,
                    event_type_input: this.props.data.attributes.event_type_input
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateDeviceScheme({
                id: this.props.data.id,
                FormData: {
                    code: this.state.code,
                    event_type_input: this.state.event_type_input
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addDeviceScheme({
                    FormData: {
                        code: this.state.code,
                        event_type_input: this.state.event_type_input
                    }
                })
            }
        }
    }

    handleTabChange = (event, value) => {
        this.setState({ tab_value: value });
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { tab_value } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}
                maxWidth={data !== null ? 'md' : 'sm'}
                fullWidth={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormDeviceScheme()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan device scheme. silahkan tambahkan device scheme pada form dibawah ini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.code : ""}
                                label="Code"
                                id="code"
                                error={validation.code.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ code: event.target.value }) } 
                                helperText={validation.code.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.event_type_input : ""}
                                label="Event Input Type"
                                id="event-type-input"
                                error={validation.event_type_input.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ event_type_input: event.target.value }) } 
                                helperText={validation.event_type_input.message}
                                margin="none"
                            />
                        </div>
                    </form>
                    {data !== null &&
                    <RctCollapsibleCard fullBlock>
                        <div className={classes.root}>
                            <Tabs
                                value={tab_value}
                                onChange={this.handleTabChange}
                                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                                >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                    label="Device Schemes Field Name"
                                />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                    label="Device Scheme Event Type"
                                />
                            </Tabs>
                            {tab_value === 0 && <FieldName />}
                            {tab_value === 1 && <EventType />}
                        </div>
                    </RctCollapsibleCard>
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormDeviceScheme() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormDeviceScheme = withStyles(styles)(FormDeviceScheme);

const mapStateToProps = ({ newDeviceScheme }) => {
    return {newDeviceScheme};
}

export default connect(mapStateToProps, {
    addDeviceScheme,
    updateDeviceScheme,
    closeFormDeviceScheme
})(classFormDeviceScheme);