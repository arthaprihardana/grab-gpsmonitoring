/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 15:42:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:45:53
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, FormGroup, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Actions
import { getDeviceSchemeEventTypes } from '../../../../../actions';
// Styles
import styles from '../../../../../lib/styles';

class DeviceSchemeEventType extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: ''
    }

    componentDidMount() {
        
    }
    
    componentDidUpdate(prevProps, prevState) {
        
    }
    
    render() {
        const { loading, className } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type="text"
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        {/* <Button outline color="success" onClick={() => this.props.openFormDeviceScheme(null) }>
                            Add Device Schemes <i className="zmdi zmdi-plus"></i>
                        </Button> */}
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>Event Type</th>
                            <th>Description</th>
                            <th>Include Telemetry</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>value</td>
                            <td>value</td>
                            <td>value</td>
                        </tr>
                        <tr>
                            <td>value</td>
                            <td>value</td>
                            <td>value</td>
                        </tr>
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                {/* <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => ( */}
                                    <Pagination size="sm" className="justify-content-center" aria-label="Page navigation example">
                                        <PaginationItem>
                                            <PaginationLink previous href="#" />
                                        </PaginationItem>
                                        <PaginationItem>
                                            <PaginationLink href="#">
                                                1
                                            </PaginationLink>
                                        </PaginationItem>
                                        <PaginationItem>
                                            <PaginationLink href="#">
                                                2
                                            </PaginationLink>
                                        </PaginationItem>
                                        <PaginationItem>
                                            <PaginationLink href="#">
                                                3
                                            </PaginationLink>
                                        </PaginationItem>
                                        <PaginationItem>
                                            <PaginationLink href="#">
                                                4
                                            </PaginationLink>
                                        </PaginationItem>
                                        <PaginationItem>
                                            <PaginationLink href="#">
                                                5
                                            </PaginationLink>
                                        </PaginationItem>
                                        <PaginationItem>
                                            <PaginationLink next href="#" />
                                        </PaginationItem>
                                    </Pagination>
                                {/* )}
                                </Paginating> */}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        );
    }
}

const classDeviceSchemeEventType = withStyles(styles)(DeviceSchemeEventType);

const mapStateToProps = ({ deviceSchemeEventType }) => {
    return {deviceSchemeEventType};
}

export default connect(mapStateToProps, {
    getDeviceSchemeEventTypes
})(classDeviceSchemeEventType);