/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-02 22:22:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:47:42
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, FormGroup, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Actions
import { getFieldNames, openFormFieldNames } from '../../../../actions';
// Styles
import styles from '../../../../lib/styles';
// Form
import FormFieldName from './form';

class FieldNames extends Component {
    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: ''
    }

    componentDidMount() {
        this.props.getFieldNames({ page: this.state.current_page })
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.fieldNames !== prevProps.fieldNames) {
            this.setState({
                data: this.props.fieldNames.data,
                rowsPerPage: this.props.fieldNames.meta.per_page,
                current_page: this.props.fieldNames.meta.current_page,
                last_page: this.props.fieldNames.meta.last_page,
                total: this.props.fieldNames.meta.total
            })
        }
        if(this.props.newFieldName !== prevProps.newFieldName) {
            this.props.getFieldNames({ page: this.state.current_page });
        }
    }
    
    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type="text"
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        <Button outline color="success" onClick={() => this.props.openFormFieldNames(null) }>
                            Add Field Names <i className="zmdi zmdi-plus"></i>
                        </Button>
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data && data.map((value, key) => (
                            <tr key={key}>
                                <td>{value.attributes.code}</td>
                                <td>{value.attributes.description}</td>
                                <td className="list-action">
                                    <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                    <a href="javascript:void(0)" onClick={() => this.props.openFormFieldNames(value)}><i className="ti-pencil"></i></a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage,
                                        previousPage,
                                        nextPage
                                    }) => (
                                        <Pagination className="mb-0 py-10 px-10">
                                            <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                    this.setState(prevState => {
                                                        return { current_page: previousPage }
                                                    }, () => this.props.getFieldNames({ page: this.state.current_page }))
                                                }} />
                                            </PaginationItem>
                                            {pages.map(page => {
                                                return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                    <PaginationLink href="javascript:void(0)" onClick={() => {
                                                        if(page !== current_page) {
                                                            this.setState(prevState => {
                                                                return { current_page: page }
                                                            }, () => this.props.getFieldNames({ page: this.state.current_page }));
                                                        }
                                                    }} >{page}</PaginationLink>
                                                </PaginationItem>
                                            })}
                                            <PaginationItem disabled={hasNextPage ? false : true}>
                                                <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                    this.setState(prevState => {
                                                        return { current_page: nextPage }
                                                    }, () => this.props.getFieldNames({ page: this.state.current_page }))    
                                                }} />
                                            </PaginationItem>
                                        </Pagination>
                                    )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {loading && !modal &&
						<RctSectionLoader />
					}
                {/* Modal */}
                <FormFieldName 
                    title={'Add Field Name'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classFieldNames = withStyles(styles)(FieldNames);

const mapStateToProps = ({ fieldNames }) => {
    return fieldNames;
}

export default connect(mapStateToProps, {
    getFieldNames,
    openFormFieldNames
})(classFieldNames);