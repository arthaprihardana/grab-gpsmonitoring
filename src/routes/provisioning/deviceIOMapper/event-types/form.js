/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 22:52:24 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:01:13
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, Tabs, Tab, Radio, RadioGroup, FormLabel, FormControlLabel, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// Component
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addEventTypes, closeFormEventTypes, updateEventTypes } from "../../../../actions";
// Validator
import FormValidator from '../../../../helpers/FormValidation';
// Form Eventt Type Condition
import EventTypeCondition from './event-type-condition'
import {getEventTypeCondition} from '../../../../actions/EventTypeConditionActions';

class FormEventTypes extends Component {

    validator = new FormValidator([
        {
            field: 'code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Code tidak boleh kososng'
        },
        {
            field: 'description',
            method: 'isEmpty',
            validWhen: false,
            message: 'Description tidak boleh kososng'
        },
        {
            field: 'use_alias',
            method: 'isEmpty',
            validWhen: false,
            message: 'Use Alias tidak boleh kososng'
        },
        {
            field: 'alias',
            method: 'isEmpty',
            validWhen: false,
            message: 'Alias tidak boleh kososng'
        },
        {
            field: 'has_condition',
            method: 'isEmpty',
            validWhen: false,
            message: 'Has Condition tidak boleh kososng'
        },
    ])

    submitted = false;

    state = {
        code: '',
        description: '',
        alias: '',
        use_alias: "true",
        has_condition: "true",
        tab_value: 0,
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    code: '',
                    description: '',
                    alias: '',
                    use_alias: "true",
                    has_condition: "true",
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    code: this.props.data.attributes.code,
                    description: this.props.data.attributes.description,
                    alias: this.props.data.attributes.alias,
                    use_alias: this.props.data.attributes.use_alias ? "true" : "false",
                    has_condition: this.props.data.attributes.has_condition ? "true" : "false",
                })
            }
        }
    }
    
    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateEventTypes({
                id: this.props.data.id,
                FormData: {
                    code: this.state.code,
                    description: this.state.description,
                    alias: this.state.alias,
                    use_alias: this.state.use_alias === "true" ? true : false,
                    has_condition: this.state.has_condition === "true" ? true : false,
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addEventTypes({
                    FormData: {
                        code: this.state.code,
                        description: this.state.description,
                        alias: this.state.alias,
                        use_alias: this.state.use_alias === "true" ? true : false,
                        has_condition: this.state.has_condition === "true" ? true : false,
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { tab_value } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}
                maxWidth={data !== null ? 'md' : 'sm'}
                fullWidth={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormEventTypes()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan event types. silahkan tambahkan event types pada form dibawah ini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.code : ""}
                                label="Code"
                                id="code"
                                error={validation.code.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ code: event.target.value }) } 
                                helperText={validation.code.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.description : ""}
                                label="Description"
                                id="description"
                                error={validation.description.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ description: event.target.value }) } 
                                helperText={validation.description.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <FormLabel>Use Alias</FormLabel>
                            <RadioGroup
                                row
                                name="use-alias"
                                aria-label="use-alias"
                                value={this.state.use_alias}
                                onChange={(event) => this.setState({ use_alias: event.target.value })}
                                >
                                <FormControlLabel value="true" control={<Radio />} label="Yes" />
                                <FormControlLabel value="false" control={<Radio />} label="No" />
                            </RadioGroup>
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.alias : ""}
                                label="Alias"
                                id="alias"
                                error={validation.alias.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ alias: event.target.value }) } 
                                helperText={validation.alias.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <FormLabel>Has Condition</FormLabel>
                            <RadioGroup
                                row
                                name="has-condition"
                                aria-label="has-condition"
                                value={this.state.has_condition}
                                onChange={(event) => this.setState({ has_condition: event.target.value })}
                                >
                                <FormControlLabel value="true" control={<Radio />} label="Yes" />
                                <FormControlLabel value="false" control={<Radio />} label="No" />
                            </RadioGroup>
                        </div>
                    </form>
                    {data !== null &&
                    <RctCollapsibleCard fullBlock>
                        <div className={classes.root}>
                            <Tabs
                                value={tab_value}
                                onChange={this.handleTabChange}
                                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                                >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                    label="Event Type Condition"
                                />
                            </Tabs>
                            {tab_value === 0 && <EventTypeCondition />}
                        </div>
                    </RctCollapsibleCard> }
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormEventTypes() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormEventTypes = withStyles(styles)(FormEventTypes);

const mapStateToProps = ({ eventTypes }) => {
    return { eventTypes }
}

export default connect(mapStateToProps, {
    addEventTypes,
    updateEventTypes,
    closeFormEventTypes
})(classFormEventTypes);