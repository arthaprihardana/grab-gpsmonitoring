/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-02 22:22:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 00:13:44
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, FormGroup, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Actions 
import { getEventTypes, openFormEventTypes } from '../../../../actions';
// Styles
import styles from '../../../../lib/styles';
// Form
import FormEventType from './form';

class EventTypes extends Component {
    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: ''
    }

    componentDidMount() {
        this.props.getEventTypes({ page: this.state.current_page })
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.eventTypes !== prevProps.eventTypes) {
            this.setState({
                data: this.props.eventTypes.data,
                rowsPerPage: this.props.eventTypes.meta.per_page,
                current_page: this.props.eventTypes.meta.current_page,
                last_page: this.props.eventTypes.meta.last_page,
                total: this.props.eventTypes.meta.total
            })
        }
        if(this.props.newEventType !== prevProps.newEventType) {
            this.props.getEventTypes({ page: this.state.current_page });
        }
    }
    
    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type="text"
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        <Button outline color="success" onClick={() => this.props.openFormEventTypes(null) }>
                            Add Event Types <i className="zmdi zmdi-plus"></i>
                        </Button>
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Use Alias</th>
                            <th>Alias</th>
                            <th>Has Condition</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data && data.map((value, key) => (
                            <tr key={key}>
                                <td>{value.attributes.code}</td>
                                <td>{value.attributes.description}</td>
                                <td>{value.attributes.use_alias ? "true" : "false"}</td>
                                <td>{value.attributes.alias}</td>
                                <td>{value.attributes.has_condition ? "true" : "false"}</td>
                                <td className="list-action">
                                    <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                    <a href="javascript:void(0)" onClick={() => this.props.openFormEventTypes(value)}><i className="ti-pencil"></i></a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => (
                                        <Pagination className="mb-0 py-10 px-10">
                                            <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                    if(hasPreviousPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page - 1 }
                                                        }, () => this.props.getEventTypes({page: this.state.current_page}))
                                                    }
                                                }} />
                                            </PaginationItem>
                                            {pages.map(page => {
                                                return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                    <PaginationLink href="javascript:void(0)" onClick={() => {
                                                        if(page !== current_page) {
                                                            this.setState(prevState => {
                                                                return { current_page: page }
                                                            }, () => this.props.getEventTypes({page: this.state.current_page}));
                                                        }
                                                    }} >{page}</PaginationLink>
                                                </PaginationItem>
                                            })}
                                            <PaginationItem disabled={hasNextPage ? false : true}>
                                                <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                    if(hasNextPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page + 1 }
                                                        }, () => this.props.getEventTypes({page: this.state.current_page}))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                        </Pagination>
                                    )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {loading && !modal &&
						<RctSectionLoader />
					}
                <FormEventType 
                    title={'Add Event Types'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm}/>
            </div>
        );
    }
}

const classEventTypes = withStyles(styles)(EventTypes);

const mapStateToProps = ({ eventTypes }) => {
    return eventTypes;
}

export default connect(mapStateToProps, {
    getEventTypes,
    openFormEventTypes
})(classEventTypes);