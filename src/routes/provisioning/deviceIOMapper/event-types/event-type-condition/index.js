/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-10 00:15:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:19:59
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, FormGroup, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Actions
import { getEventTypeCondition } from '../../../../../actions';
// Styles
import styles from '../../../../../lib/styles';

class EventTypeCondition extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: ''
    }

    componentDidMount() {
        this.props.getEventTypeCondition({ page: this.state.current_page });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.eventTypeCondition !== prevProps.eventTypeCondition) {
            this.setState({
                data: this.props.eventTypeCondition.data,
                rowsPerPage: this.props.eventTypeCondition.meta.per_page,
                current_page: this.props.eventTypeCondition.meta.current_page,
                last_page: this.props.eventTypeCondition.meta.last_page,
                total: this.props.eventTypeCondition.meta.total
            })
        }
    }
    
    render() {
        const { loading, className } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type="text"
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        {/* <Button outline color="success" onClick={() => this.props.openFormDeviceScheme(null) }>
                            Add Device Schemes <i className="zmdi zmdi-plus"></i>
                        </Button> */}
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>Condition</th>
                            <th>Condition Type</th>
                            <th>Value</th>
                            <th>Alias</th>
                            <th>Order Number</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data && data.map((value, key) => (
                            <tr key={key}>
                                <td>{value.attributes.condition}</td>
                                <td>{value.attributes.condition_type}</td>
                                <td>{value.attributes.value}</td>
                                <td>{value.attributes.alias}</td>
                                <td>{value.attributes.order_number}</td>
                            </tr>  
                        ))}
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => (
                                    <Pagination size="sm" className="justify-content-center">
                                        <PaginationItem disabled={hasPreviousPage ? false : true}>
                                            <PaginationLink previous href="#" onClick={() => {
                                                if(hasPreviousPage) {
                                                    this.setState(prevState => {
                                                        return { current_page: prevState.current_page - 1 }
                                                    }, () => this.props.getEventTypeCondition({page: this.state.current_page}))
                                                }}} />
                                        </PaginationItem>
                                        {pages.map(page => {
                                            return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                <PaginationLink href="#" onClick={() => {
                                                    if(page !== current_page) {
                                                        this.setState(prevState => {
                                                            return { current_page: page }
                                                        }, () => this.props.getEventTypeCondition({page: this.state.current_page}))
                                                    }}}>
                                                    {page}
                                                </PaginationLink>
                                            </PaginationItem>
                                        })}
                                        <PaginationItem disabled={hasNextPage ? false : true}>
                                            <PaginationLink next href="#" onClick={() => {
                                                if(hasNextPage) {
                                                    this.setState(prevState => {
                                                        return { current_page: prevState.current_page + 1 }
                                                    }, () => this.props.getEventTypeCondition({page: this.state.current_page}))
                                                }}} />
                                        </PaginationItem>
                                    </Pagination>
                                )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        );
    }

}

const classEventTypeCondition = withStyles(styles)(EventTypeCondition);

const mapStateTpProps = ({ eventTypeCondition }) => {
    return eventTypeCondition;
}

export default connect(mapStateTpProps, {
    getEventTypeCondition
})(classEventTypeCondition);