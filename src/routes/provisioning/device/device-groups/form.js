/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 01:10:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 01:42:09
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, InputLabel, Input, FormHelperText, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// Component
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addDeviceGroups, closeFormDeviceGroups, updateDeviceGroups, getDeviceShemes } from "../../../../actions";
// Validator
import FormValidator from '../../../../helpers/FormValidation';

class FormDeviceGroups extends Component {

    validator = new FormValidator([
        {
            field: 'code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Code tidak boleh kososng'
        },
        {
            field: 'device_scheme_id',
            method: 'isEmpty',
            validWhen: false,
            message: 'Device Schema tidak boleh kososng'
        }
    ])

    submitted = false;

    state = {
        code: '',
        device_scheme_id: '',
        dataScehemes: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getDeviceShemes({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    code: '',
                    device_scheme_id: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    code: this.props.data.attributes.code,
                    device_scheme_id: this.props.data.attributes.device_scheme_id,
                })
            }
        }
        if(this.props.deviceScheme !== prevProps.deviceScheme) {
            let master_scheme = this.props.deviceScheme.data;
            let dataSelect = this.state.dataScehemes;
            master_scheme.map((value, key) => {
                dataSelect.push({ value: value.id, label: value.attributes.code});
            })
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateDeviceGroups({
                id: this.props.data.id,
                FormData: {
                    code: this.state.code,
                    device_scheme_id: this.state.device_scheme_id
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addDeviceGroups({
                    FormData: {
                        code: this.state.code,
                        device_scheme_id: this.state.device_scheme_id
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataScehemes } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormDeviceGroups()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan device group. silahkan tambahkan device group pada form dibawah ini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.code : ""}
                                label="Code"
                                id="code"
                                error={validation.code.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ code: event.target.value }) } 
                                helperText={validation.code.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Device Schemes</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.device_scheme_id,
                                    onChange: val => this.setState({ device_scheme_id: val }),
                                    placeholder: 'Select Role',
                                    instanceId: 'select-role',
                                    id: 'select-role',
                                    name: 'select-role',
                                    simpleValue: true,
                                    options: dataScehemes.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.device_scheme_id.isInvalid && <FormHelperText error={true}>{validation.device_scheme_id.message}</FormHelperText> }
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormDeviceGroups() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormDeviceGroups = withStyles(styles)(FormDeviceGroups);

const mapStateToProps = ({ newDeviceGroup, deviceSchemes }) => {
    const { deviceScheme } = deviceSchemes;
    return { newDeviceGroup, deviceScheme }
}

export default connect(mapStateToProps, {
    addDeviceGroups,
    updateDeviceGroups,
    closeFormDeviceGroups,
    getDeviceShemes
})(classFormDeviceGroups);