/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-02 22:16:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-02 22:20:25
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Pagination, PaginationItem, PaginationLink, Button, FormGroup, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// Component
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';

class DeviceUploads extends Component {
    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: ''
    }

    componentDidMount() {
        
    }
    
    componentDidUpdate(prevProps, prevState) {
        
    }
    
    render() {
        const { loading, className, modal, formWithData } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type="text"
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        <Button outline color="success" className="mr-5" onClick={() => {} }>
                            Choose File <i className="zmdi zmdi-cloud-upload"></i>
                        </Button>
                        <Button outline color="success" onClick={() => {} }>
                            Proses Upload <i className="zmdi zmdi-refresh-alt"></i>
                        </Button>
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>Code</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>value</td>
                            <td>value</td>
                            <td className="list-action">
                                <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                <a href="javascript:void(0)" onClick={() => {}}><i className="ti-pencil"></i></a>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => (
                                        <Pagination className="mb-0 py-10 px-10">
                                            {hasPreviousPage && (
                                                <PaginationItem>
                                                    <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page - 1 }
                                                        }, () => {})
                                                    }} />
                                                </PaginationItem>
                                            )}
                                            {pages.map(page => {
                                                return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                    <PaginationLink href="javascript:void(0)" onClick={() => {
                                                        if(page !== current_page) {
                                                            this.setState(prevState => {
                                                                return { current_page: page }
                                                            }, () => {});
                                                        }
                                                    }} >{page}</PaginationLink>
                                                </PaginationItem>
                                            })}
                                            {hasNextPage && (
                                                <PaginationItem>
                                                    <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page + 1 }
                                                        }, () => {})    
                                                    }} />
                                                </PaginationItem>
                                            )}
                                        </Pagination>
                                    )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {loading && !modal &&
						<RctSectionLoader />
					}
            </div>
        );
    }
}

export default DeviceUploads;