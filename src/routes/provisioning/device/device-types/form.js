/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-08 23:21:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:16:33
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addDeviceTypes, closeFormDeviceTypes, updateDeviceTypes } from "../../../../actions";
// Validator
import FormValidator from '../../../../helpers/FormValidation';

class FormDeviceTypes extends Component {

    validator = new FormValidator([
        {
            field: 'code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Code tidak boleh kososng'
        },
        {
            field: 'description',
            method: 'isEmpty',
            validWhen: false,
            message: 'Description tidak boleh kososng'
        },
        {
            field: 'input_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Input Name tidak boleh kososng'
        }
    ])

    submitted = false;

    state = {
        code: '',
        description: '',
        input_name: '',
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    code: '',
                    description: '',
                    input_name: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    code: this.props.data.attributes.code,
                    description: this.props.data.attributes.description,
                    input_name: this.props.data.attributes.input_name,
                })
            }
        }
    }
    
    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateDeviceTypes({
                id: this.props.data.id,
                FormData: {
                    code: this.state.code,
                    description: this.state.description,
                    input_name: this.state.input_name,
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addDeviceTypes({
                    FormData: {
                        code: this.state.code,
                        description: this.state.description,
                        input_name: this.state.input_name,
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormDeviceTypes()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan device type. silahkan tambahkan device type pada form dibawah ini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.code : ""}
                                label="Code"
                                id="code"
                                error={validation.code.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ code: event.target.value }) } 
                                helperText={validation.code.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.description : ""}
                                label="Description"
                                id="description"
                                error={validation.description.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ description: event.target.value }) } 
                                helperText={validation.description.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.input_name : ""}
                                label="Input Name"
                                id="input-name"
                                error={validation.input_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ input_name: event.target.value }) } 
                                helperText={validation.input_name.message}
                                margin="none"
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormDeviceTypes() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormDeviceTypes = withStyles(styles)(FormDeviceTypes);

const mapStateToProps = ({ newDeviceTypes }) => {
    return { newDeviceTypes };
}

export default connect(mapStateToProps, {
    addDeviceTypes,
    updateDeviceTypes,
    closeFormDeviceTypes
})(classFormDeviceTypes);