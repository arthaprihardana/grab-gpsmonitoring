/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 09:31:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 09:59:22
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, InputLabel, Input, FormHelperText, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// Component
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addDevice, closeFormDevice, updateDevice, getDeviceTypes, getDeviceModels, getDeviceGroups } from "../../../../actions";
// Validator
import FormValidator from '../../../../helpers/FormValidation';

class FormDevice extends Component {

    validator = new FormValidator([
        {
            field: 'code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Code tidak boleh kososng'
        },
        {
            field: 'description',
            method: 'isEmpty',
            validWhen: false,
            message: 'Description tidak boleh kososng'
        },
        {
            field: 'vehicle_number',
            method: 'isEmpty',
            validWhen: false,
            message: 'Vehicle Number tidak boleh kososng'
        },
        {
            field: 'device_type_id',
            method: 'isEmpty',
            validWhen: false,
            message: 'Device Type tidak boleh kososng'
        },
        {
            field: 'device_model_id',
            method: 'isEmpty',
            validWhen: false,
            message: 'Device Model tidak boleh kososng'
        },
        {
            field: 'device_group_id',
            method: 'isEmpty',
            validWhen: false,
            message: 'Device Group tidak boleh kososng'
        },
    ])

    submitted = false;

    state = {
        code: '',
        description: '',
        vehicle_number: '',
        device_type_id: '',
        device_model_id: '',
        device_group_id:  '',
        data_device_type: [],
        data_device_model: [],
        data_device_group: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getDeviceTypes({ page: 1 });
        this.props.getDeviceModels({ page: 1 });
        this.props.getDeviceGroups({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    code: '',
                    description: '',
                    vehicle_number: '',
                    device_type_id: '',
                    device_model_id: '',
                    device_group_id:  '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    code: this.props.data.attributes.code,
                    description: this.props.data.attributes.description,
                    vehicle_number: this.props.data.attributes.vehicle_number,
                    device_type_id: this.props.data.attributes.device_type_id,
                    device_model_id: this.props.data.attributes.device_model_id,
                    device_group_id:  this.props.data.attributes.device_group_id,
                })
            }
        }
        if(this.props.deviceType !== prevProps.deviceType) {
            let master_device_type = this.props.deviceType.data;
            let dataSelect = this.state.data_device_type;
            master_device_type.map((value, key) => {
                dataSelect.push({ value: value.id, label: value.attributes.code });
            })
        }
        if(this.props.deviceGroup !== prevProps.deviceGroup) {
            let master_device_group = this.props.deviceGroup.data;
            let dataSelect = this.state.data_device_group;
            master_device_group.map((value, key) => {
                dataSelect.push({ value: value.id, label: value.attributes.code });
            })
        }
        if(this.props.deviceModel !== prevProps.deviceModel) {
            let master_device_model = this.props.deviceModel.data;
            let dataSelect = this.state.data_device_model;
            master_device_model.map((value, key) => {
                dataSelect.push({ value: value.id, label: value.attributes.code });
            })
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateDevice({
                id: this.props.data.id,
                FormData: {
                    code: this.state.code,
                    description: this.state.description,
                    vehicle_number: this.state.vehicle_number,
                    device_type_id: this.state.device_type_id,
                    device_model_id: this.state.device_model_id,
                    device_group_id:  this.state.device_group_id,
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addDevice({
                    FormData: {
                        code: this.state.code,
                        description: this.state.description,
                        vehicle_number: this.state.vehicle_number,
                        device_type_id: this.state.device_type_id,
                        device_model_id: this.state.device_model_id,
                        device_group_id:  this.state.device_group_id,
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { data_device_type, data_device_model, data_device_group } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormDevice()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan device. silahkan tambahkan device pada form dibawah ini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.code : ""}
                                label="Code"
                                id="code"
                                error={validation.code.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ code: event.target.value }) } 
                                helperText={validation.code.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.description : ""}
                                label="Description"
                                id="description"
                                error={validation.description.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ description: event.target.value }) } 
                                helperText={validation.description.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? this.state.vehicle_number : ""}
                                label="Vehicle Number"
                                id="vehicle-number"
                                error={validation.vehicle_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                onChange={(event) => this.setState({ vehicle_number: event.target.value }) } 
                                helperText={validation.vehicle_number.message}
                                margin="none"
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Device Types</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.device_type_id,
                                    onChange: val => this.setState({ device_type_id: val }),
                                    placeholder: 'Select Device Types',
                                    instanceId: 'select-device-types',
                                    id: 'select-device-types',
                                    name: 'select-device-types',
                                    simpleValue: true,
                                    options: data_device_type.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.device_type_id.isInvalid && <FormHelperText error={true}>{validation.device_type_id.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Device Models</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.device_model_id,
                                    onChange: val => this.setState({ device_model_id: val }),
                                    placeholder: 'Select Device Models',
                                    instanceId: 'select-device-models',
                                    id: 'select-device-models',
                                    name: 'select-device-models',
                                    simpleValue: true,
                                    options: data_device_model.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.device_model_id.isInvalid && <FormHelperText error={true}>{validation.device_model_id.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Device Groups</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.device_group_id,
                                    onChange: val => this.setState({ device_group_id: val }),
                                    placeholder: 'Select Device Groups',
                                    instanceId: 'select-device-groups',
                                    id: 'select-device-groups',
                                    name: 'select-device-groups',
                                    simpleValue: true,
                                    options: data_device_group.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.device_group_id.isInvalid && <FormHelperText error={true}>{validation.device_group_id.message}</FormHelperText> }
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormDevice() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormDevice = withStyles(styles)(FormDevice);

const mapStateToProps = ({ newDevice, deviceTypes, deviceGroups, deviceModels }) => {
    const deviceType = deviceTypes.deviceTypes;
    const deviceGroup = deviceGroups.deviceGroups;
    const deviceModel = deviceModels.deviceModels;
    return { newDevice, deviceType, deviceGroup, deviceModel };
}

export default connect(mapStateToProps, {
    addDevice,
    updateDevice,
    closeFormDevice,
    getDeviceTypes,
    getDeviceModels,
    getDeviceGroups
})(classFormDevice)