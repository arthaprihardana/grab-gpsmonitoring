/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-02 21:12:05 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-08 23:21:18
 */
// React
import React, { Component } from 'react';
// Page Title Bar
import PageTitleBar from '../../../components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from '../../../components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from '../../../components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from '../../../util/IntlMessages';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Material Core
import { Tabs, Tab, Typography } from '@material-ui/core';
// Styles
import styles from './styles';
// Part Of Main Device
import Device from './device';
import DeviceTypes from './device-types';
import DeviceModels from './device-models';
import DeviceGroups from './device-groups';
import DeviceUploads from './device-upload';

class MainDevice extends Component {
    
    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { classes } = this.props;
        const { value } = this.state;
        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.device" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className={classes.root}>
                        <Tabs
                        value={value}
                        onChange={this.handleChange}
                        classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                        >
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Device"
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Device Types"
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Device Models"
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Device Groups"
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                label="Device Upload"
                            />
                        </Tabs>
                        {value === 0 && <Device />}
                        {value === 1 && <DeviceTypes />}
                        {value === 2 && <DeviceModels />}
                        {value === 3 && <DeviceGroups />}
                        {value === 4 && <DeviceUploads />}
                    </div>
                </RctCollapsibleCard>
            </div>
        )
    }
}

export default withStyles(styles)(MainDevice);