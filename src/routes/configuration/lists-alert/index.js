/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:47:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-16 16:47:44
 */
/** 
 * Configuration Lists Alert Routes
 */
import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

export default class ListsAlert extends Component {
    render() {
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.listsAlert" />} match={this.props.match} />
            </div>
        );
    }
}