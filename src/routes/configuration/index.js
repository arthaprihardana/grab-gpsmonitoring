/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:48:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-16 16:54:55
 */
/**
 * Routes Configuration
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import {
    AsyncAlertNotification,
    AsyncListsAlert
} from 'Components/AsyncComponent/AsyncComponent';

const Configuration = ({ match }) => (
    <div className="content-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/alert-notification`} />
            <Route path={`${match.url}/alert-notification`} component={AsyncAlertNotification} />
            <Route path={`${match.url}/lists-alert`} component={AsyncListsAlert} />
        </Switch>
    </div>
);

export default Configuration;
