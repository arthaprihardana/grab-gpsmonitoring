/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 23:37:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 23:43:43
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, InputLabel, Input, FormHelperText, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import { addAlert, closeFormAlert, updateAlert, getNotifications} from '../../../actions';
// Validation
import FormValidator from '../../../helpers/FormValidation';

class FormAlertNotification extends Component {

    validator = new FormValidator([
        {
            field: "alert_name",
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama alert tidak boleh kosong'
        },
        {
            field: "notification_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Notification code tidak boleh kosong'
        }
    ])

    submitted = false
    
    state = {
        alert_name: '',
        notification_code: '',
        score: '',
        alert_priority_code: '',
        dataNotification: [],
        dataAlertPriority: [
            {
                "code":"APR-0001",
                "name":"Critical"
            },
            {
                "code":"APR-0002",
                "name":"Warning"
            },
            {
                "code":"APR-0003",
                "name":"Information"
            }
        ],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getNotifications({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.notifications.notifications !== prevProps.notifications.notifications) {
            let master_notifications = this.props.notifications.notifications.data;
            let dataSelect = this.state.dataNotification;
            master_notifications.map((value, key) => {
                dataSelect.push({value: value.notification_code, label: value.notification_name})
            });
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    alert_name: '',
                    notification_code: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    alert_name: this.props.data.alert_name,
                    notification_code: this.props.data.notification_code,
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateAlert({
                id: this.props.data.alert_code,
                FormData: {
                    alert_name: this.state.alert_name,
                    notification_code: this.state.notification_code,
                    score: this.state.score,
                    alert_priority_code: this.state.alert_priority_code
                }
            });
        } else {
            if(validation.isValid) {
                this.props.addAlert({
                    FormData: {
                        alert_name: this.state.alert_name,
                        notification_code: this.state.notification_code,
                        score: this.state.score,
                        alert_priority_code: this.state.alert_priority_code
                    }
                });
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataNotification, dataAlertPriority } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormAlert()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan alert. silahkan daftarkan alert baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.alert_name : ""}
                                label="Alert Name"
                                id="alert-name"
                                error={validation.alert_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ alert_name: event.target.value }) } 
                                helperText={validation.alert_name.message}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Notification</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.notification_code,
                                    onChange: val => this.setState({ notification_code: val }),
                                    placeholder: 'Select Notification',
                                    instanceId: 'select-notification',
                                    id: 'select-notification',
                                    name: 'select-notification',
                                    simpleValue: true,
                                    options: dataNotification.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.notification_code.isInvalid && <FormHelperText error={true}>{validation.notification_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.score : ""}
                                label="Score"
                                id="score"
                                // error={validation.alert_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ score: event.target.value }) } 
                                // helperText={validation.alert_name.message}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Alert Priority Code</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.alert_priority_code,
                                    onChange: val => this.setState({ alert_priority_code: val }),
                                    placeholder: 'Select Priority Code',
                                    instanceId: 'select-priority-code',
                                    id: 'select-priority-code',
                                    name: 'select-priority-code',
                                    simpleValue: true,
                                    options: dataAlertPriority.map(value => ({
                                        label: value.name,
                                        value: value.code
                                    })),
                                }}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormAlert()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormAlertNotification = withStyles(styles)(FormAlertNotification);

const mapStateToProps = ({ newAlert, notifications }) => {
    return { newAlert, notifications };
}

export default connect(mapStateToProps, {
    addAlert,
    closeFormAlert,
    updateAlert,
    getNotifications
})(classFormAlertNotification);