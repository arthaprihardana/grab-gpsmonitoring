/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:46:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-27 06:44:35
 */
// React
import React, { Component } from 'react';
// Redux
import {connect} from 'react-redux';
// Material UI
import {withStyles} from '@material-ui/core';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// form
import FormAlertNotification from './form';
// Actions
import { getAlerts, openFormAlert } from '../../../actions';
// Styles
import styles from '../../../lib/styles';

/**
 * @description Index data Master Alerts Notification
 *
 * @class AlertNotification
 * @extends {Component}
 */
class AlertNotification extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25
    }

    componentDidMount() {
        this.props.getAlerts({ page: this.state.current_page });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.alerts !== prevProps.alerts) {
            this.setState({
                data: this.props.alerts.data,
                rowsPerPage: this.props.alerts.per_page,
                current_page: this.props.alerts.current_page,
                last_page: this.props.alerts.last_page,
                total: this.props.alerts.total
            });
        }
        if(this.props.newAlert !== prevProps.newAlert) {
            this.props.getAlerts({ page: this.state.current_page });
        }
    }

    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;

        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.alertNotification" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="table-responsive">
						<div className="d-flex justify-content-between py-20 px-10 border-bottom">
                            <div className="col-md-4">
                                <InputGroup>
                                    <Input
                                        value={this.state.search}
                                        type={"text"}
                                        name="search"
                                        id="search"
                                        className="input-lg"
                                        placeholder="Search"
                                        onChange={(event) => this.setState({ search: event.target.value })}
                                    />
                                    <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                        <span className="input-group-text">
                                            <i className="ti-search"></i>
                                        </span>
                                    </InputGroupAddon>
                                </InputGroup>
							</div>
							<div>
                                <Button outline color="success" onClick={() => this.props.openFormAlert(null) }>
                                    Add New Alert <i className="zmdi zmdi-plus"></i>
                                </Button>
							</div>
						</div>
                        <table className="table table-middle table-hover mb-0">
							<thead>
								<tr style={{ height: 60 }}>
									<th>Alert Code</th>
									<th>Alert Name</th>
									<th>Notification Name</th>
                                    <th>Score</th>
                                    <th>Alert Priority Code</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
                                {data && data.map((value, key) => (
                                    <tr key={key}>
                                        <td>{value.alert_code}</td>
                                        <td>{value.alert_name}</td>
                                        <td>{value.notification && value.notification.length > 0 ? value.notification.map((val, key) => { return val.notification_name }).join(",") : ""}</td>
                                        <td>{value.score}</td>
                                        <td>{value.status_alert_priority_code}</td>
                                        <td className="list-action">
											<a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
											<a href="javascript:void(0)" onClick={() => this.props.openFormAlert(value)}><i className="ti-pencil"></i></a>
										</td>
                                    </tr>
                                ))}
                            </tbody>
                            <tfoot className="border-top">
								<tr>
									<td colSpan="100%">
                                        <Paginating
                                            total={total}
                                            limit={rowsPerPage}
                                            pageCount={5}
                                            currentPage={current_page}>
                                            {({
                                                pages,
                                                hasNextPage,
                                                hasPreviousPage
                                            }) => (
                                                <Pagination className="mb-0 py-10 px-10">
                                                    <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                        <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                            if(hasPreviousPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page - 1 }
                                                                }, () => this.props.getAlerts({ page: this.state.current_page }))
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                    {pages.map(page => {
                                                        return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                            <PaginationLink href="javascript:void(0)" onClick={() => {
                                                                if(page !== current_page) {
                                                                    this.setState(prevState => {
                                                                        return { current_page: page }
                                                                    }, () => this.props.getAlerts({ page: this.state.current_page }));
                                                                }
                                                            }} >{page}</PaginationLink>
                                                        </PaginationItem>
                                                    })}
                                                    <PaginationItem disabled={hasNextPage ? false : true}>
                                                        <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                            if(hasNextPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page + 1 }
                                                                }, () => this.props.getAlerts({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                </Pagination>
                                            )}
                                        </Paginating>
									</td>
								</tr>
							</tfoot>
                        </table>
                    </div>
                    {loading && !modal &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
                {/* Modal */}
                <FormAlertNotification 
                    title={'Add Alert Notification'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classAlertNotification = withStyles(styles)(AlertNotification);

const mapStateToProps = ({ alerts }) => {
    return alerts;
}

export default connect(mapStateToProps, {
    getAlerts,
    openFormAlert
})(classAlertNotification)