/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-08 22:29:29 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 00:20:37
 */
import React from 'react';
import { Typography } from '@material-ui/core';
// Component
import { RctCardFooter } from 'Components/RctCard';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import moment from 'moment';
import 'moment/locale/id';
import {hhmmss} from '../../helpers/helpers';


const GPSNotUpdated = props => (
    <RctCollapsibleCard 
        heading="GPS Not Updated in 24 Hours" 
        customClasses="overflow-hidden" 
        collapsible
        reloadable
        fullBlock>
        <div style={{ height: 338 }}>
            <table className="table table-middle table-hover mb-0">
                <thead>
                    <tr style={{ height: 60 }}>
                        <th>No Polisi</th>
                        <th>Duration</th>
                        <th>Last Update</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data && props.data.length > 0 ? props.data.map((value, key) => (
                        <tr key={key}>
                            <td><Typography variant="body1">{value.license_plate}</Typography></td>
                            <td><Typography variant="body1">{hhmmss(value.duration)}</Typography></td>
                            <td><Typography variant="body1">{moment(value.last_updated).format('lll')}</Typography></td>
                        </tr>
                    )) : <tr>
                            <td colSpan={3} style={{ textAlign: 'center' }}>
                                <Typography variant="caption">Data Not Found</Typography>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
        <RctCardFooter>
            <span className="fs-12 text-base">
                <i className="mr-5 zmdi zmdi-refresh"></i>
                Last update on {moment().format('LLL')}
            </span>
        </RctCardFooter>
    </RctCollapsibleCard>
)

export default GPSNotUpdated;