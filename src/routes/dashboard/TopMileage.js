/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-08 22:03:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 00:19:00
 */
import React from 'react';
// Material UI
import { Typography } from '@material-ui/core';
// Redux
import { connect } from 'react-redux';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';
// Component
import { RctCardFooter } from 'Components/RctCard';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import moment from 'moment';
import 'moment/locale/id';

const TopMileage = props => (
    <RctCollapsibleCard 
        heading="Top 10 Mileage" 
        customClasses="overflow-hidden" 
        collapsible
        reloadable
        fullBlock>
        <div style={{ height: 338, overflowY: 'auto' }}>
            <table className="table table-middle table-hover mb-0">
                <thead>
                    <tr style={{ height: 60 }}>
                        <th>No Polisi</th>
                        <th>Driver Name</th>
                        <th>Total Mileage</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data && props.data.length > 0 ? props.data.map((value, key) => (
                        <tr key={key}>
                            <td><Typography variant="body1">{value.license_plate}</Typography></td>
                            <td><Typography variant="body1">{value.driver_name}</Typography></td>
                            <td><Typography variant="body1">{value.total_odometer}</Typography></td>
                        </tr>
                    )) : <tr>
                            <td colSpan={3} style={{ textAlign: 'center' }}>
                                <Typography variant="caption">Data Not Found</Typography>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
        <RctCardFooter>
            <span className="fs-12 text-base">
                <i className="mr-5 zmdi zmdi-refresh"></i>
                Last update on {moment().format('LLL')}
            </span>
        </RctCardFooter>
    </RctCollapsibleCard>
)

export default TopMileage;