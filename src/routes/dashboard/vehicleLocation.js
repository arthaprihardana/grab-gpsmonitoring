/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-21 14:08:25 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 00:18:48
 */
import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { Typography } from '@material-ui/core';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';
// Component
import { RctCardFooter } from 'Components/RctCard';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import moment from 'moment';
import 'moment/locale/id';

const VehicleLocation = props => (
    <RctCollapsibleCard 
        heading="Vehicle Location" 
        customClasses="overflow-hidden" 
        collapsible
        reloadable
        fullBlock>
        <div style={{ height: 338, overflowY: 'auto' }}>
            <table className="table table-middle table-hover mb-0">
                <thead>
                    <tr style={{ height: 60 }}>
                        <th style={{ width: '30%' }}>No Polisi</th>
                        <th style={{ width: '70%' }}>Location</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data && props.data.length > 0 ? props.data.map((value, key) => (
                        <tr key={key}>
                            <td><Typography variant="body1">{value.license_plate}</Typography></td>
                            <td><Typography variant="body1">{value.last_location}</Typography></td>
                        </tr>
                    )) : <tr>
                            <td colSpan={2} style={{ textAlign: 'center' }}>
                                <Typography variant="caption">Data Not Found</Typography>
                            </td>
                        </tr>}
                </tbody>
            </table>
        </div>
        <RctCardFooter>
            <span className="fs-12 text-base">
                <i className="mr-5 zmdi zmdi-refresh"></i>
                Last update on {moment().subtract(2, 'days').format('LLL')}
            </span>
        </RctCardFooter>
    </RctCollapsibleCard>
)

export default VehicleLocation;