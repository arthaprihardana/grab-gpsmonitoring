/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-08 22:26:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 00:18:08
 */
import React from 'react';
import { connect } from 'react-redux';
// Material UI
import { Typography } from '@material-ui/core';
// Scrollbars
import { Scrollbars } from 'react-custom-scrollbars';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';
// Component
import { RctCardFooter } from 'Components/RctCard';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';

import moment from 'moment';
import 'moment/locale/id';
import { hhmmss } from '../../helpers/helpers';

const GeoFenceSummary = props => (
    <RctCollapsibleCard 
        heading="Out Of Geofence" 
        customClasses="overflow-hidden" 
        collapsible
        reloadable
        fullBlock>
        <div style={{ height: 338, overflowY: 'auto' }}>
            <table className="table table-middle table-hover mb-0">
                <thead>
                    <tr style={{ height: 60 }}>
                        <th>No Polisi</th>
                        <th>Duration</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data && props.data.length > 0 ? props.data.map((value, key) => (
                        <tr key={key}>
                            <td><Typography variant="body1">{value.license_plate}</Typography></td>
                            {/* <td><Typography variant="body1">{moment.duration(value.duration_out_zone, 'minutes').humanize(true)}</Typography></td> */}
                            <td><Typography variant="body1">{hhmmss(value.duration_out_zone)}</Typography></td>
                        </tr>
                    )) : <tr>
                            <td colSpan={2} style={{ textAlign: 'center' }}>
                                <Typography variant="caption">Data Not Found</Typography>
                            </td>
                        </tr>}
                </tbody>
            </table>
        </div>
        <RctCardFooter>
            <span className="fs-12 text-base">
                <i className="mr-5 zmdi zmdi-refresh"></i>
                Last update on {moment().format('LLL')}
            </span>
        </RctCardFooter>
    </RctCollapsibleCard>
)

export default GeoFenceSummary;