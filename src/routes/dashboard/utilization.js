/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-21 14:38:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-11 16:40:57
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { PieChart, Pie, Cell, Legend, ResponsiveContainer, Tooltip, Label } from 'recharts';

// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// card component
import { RctCardFooter } from 'Components/RctCard';
// Lodash
import _ from 'lodash';

import moment from 'moment';
import 'moment/locale/id';

class Utilization extends Component {

    render() {
        let data = this.props.data && Object.keys(this.props.data).map((value, key, arr) => {
            return {
                name: _.capitalize(_.lowerCase(value)),
                value: this.props.data[value]
            }
        });
        const dataNull = this.props.data && Object.keys(this.props.data).map((value, key) => {
            if(key === 0) {
                return {
                    name: _.capitalize(_.lowerCase(value)), 
                    value: 100
                }
            } else {
                return {
                    name: _.capitalize(_.lowerCase(value)), 
                    value: 0
                }
            }
        });
        let total = data && _.reduce(data, (sum, n) => { return sum.value + n.value; });
        const COLORS = [
            total !== 0 ? '#0088FE' : '#CCCCCC', 
            total !== 0 ? '#00C49F' : '#CCCCCC'
        ];
        const RADIAN = Math.PI / 180;
        const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index, value }) => {
            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
            const x  = cx + radius * Math.cos(-midAngle * RADIAN);
            const y = cy  + radius * Math.sin(-midAngle * RADIAN);
            return (
                <text x={x} y={y} fill="white" textAnchor={'middle'} dominantBaseline="middle" style={{ fontSize: 10 }}>
                    {percent !== 0 && `${_.round(percent * 100, 2)}%`}
                </text>
            );
        };
        return (
            <RctCollapsibleCard 
                heading="Utilization %" 
                customClasses="overflow-hidden" 
                collapsible
                reloadable
                fullBlock>

                <div>
                    <div className="p-20">
                        <ResponsiveContainer height={300} width="100%">
                            <PieChart>
                                <Pie
                                    data={ data && total !== 0 ? data : dataNull }
                                    dataKey='value'
                                    cx={"50%"} 
                                    cy={"50%"} 
                                    innerRadius={65}
                                    outerRadius={100}
                                    startAngle={90} 
                                    endAngle={450}
                                    label={total !== 0 ? renderCustomizedLabel : null}
                                    labelLine={false}
                                    >
                                    {data && data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]} /> )}
                                    <Label value={`Total ${_.round(total, 2)}`} offset={0} position="center" />
                                </Pie>
                                { total !== 0 && <Tooltip /> }
                                <Legend align="right" layout="vertical" verticalAlign="top" height={36} iconType="circle" />
                            </PieChart>
                        </ResponsiveContainer>
                    </div>
                </div>

                {/*  */}

                <RctCardFooter>
                    <span className="fs-12 text-base">
                        <i className="mr-5 zmdi zmdi-refresh"></i>
                        Last update on {moment().subtract(2, 'days').format('LLL')}
                    </span>
                </RctCardFooter>
            </RctCollapsibleCard>
        )
    }
}

export default Utilization;