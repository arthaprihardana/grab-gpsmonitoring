/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-21 14:16:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-11 16:52:35
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Chip , withStyles, Grid, Typography, Dialog, DialogTitle, IconButton, Table, TableBody, TableRow, TableCell, CircularProgress} from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { Container, Row, Col } from 'reactstrap';
import { PieChart, Pie, Cell, Legend, ResponsiveContainer, Tooltip, Label } from 'recharts';
// Lodash
import _ from 'lodash';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// card component
import { RctCardFooter } from 'Components/RctCard';
import styles from '../../lib/styles';

import { getDetailAlertSummary } from '../../actions';

import moment from 'moment';
import 'moment/locale/id';

class AlertsSummary extends Component {

    state = {
        infoDetailAlertSummary: false,
        listDetailAlertSummary: []
    }

    openData = (alertPriority) => {
        this.props.getDetailAlertSummary(alertPriority)
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.alertSummary !== prevProps.alertSummary) {
            return this.props.alertSummary.data;
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.Status || snapshot.Status === 1) {
                this.setState(prevState => {
                    return { 
                        infoDetailAlertSummary: true,
                        listDetailAlertSummary: snapshot.Data 
                    }
                })
            }
        }
    }

    render() {
        return (
            <RctCollapsibleCard 
                heading="Alert Summary" 
                customClasses="overflow-hidden" 
                collapsible
                reloadable
                fullBlock>
                <div style={{ height: 338, overflowY: 'auto' }}>
                    <table className="table table-middle table-hover mb-0">
                        <thead>
                            <tr style={{ height: 60 }}>
                                <th>Alert Category</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.data && this.props.data.length > 0 ? this.props.data.map((value, key) => (
                                <tr key={key} onClick={() => this.openData(value.alert_priority)} style={{ cursor: 'pointer' }}>
                                    <td><Typography variant="body1">{value.alert_priority}</Typography></td>
                                    <td>
                                        <Typography variant="body1">{value.total}</Typography>
                                    </td>
                                </tr>
                            )) : <tr>
                                    <td colSpan={3} style={{ textAlign: 'center' }}>
                                        <Typography variant="caption">Data Not Found</Typography>
                                    </td>
                                </tr>
                            }
                        </tbody>
                    </table>
                </div>
                <RctCardFooter>
                    <span className="fs-12 text-base">
                        <i className="mr-5 zmdi zmdi-refresh"></i>
                        Last update on {moment().format('LLL')}
                    </span>
                </RctCardFooter>
                <Dialog
                    maxWidth={"sm"}
                    fullWidth
                    open={this.state.infoDetailAlertSummary}
                    onClose={() => {}}
                    aria-labelledby="alert-dialog-title"
					aria-describedby="alert-dialog-description"
                    >
                    <DialogTitle id="alert-dialog-title">Info Detail Summary</DialogTitle>
                    <IconButton color="inherit" onClick={() => this.setState({ infoDetailAlertSummary: false })} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
						<CloseIcon style={{ color: '#999999'}} />
					</IconButton>
                    <div>
                        <Table>
                            <TableBody>
                                {this.state.listDetailAlertSummary.length > 0 ? 
                                    this.state.listDetailAlertSummary.map(row => {
                                        return (
                                            <TableRow key={row.id}>
                                                <TableCell component="th" scope="row">{row.alert_status}</TableCell>
                                                <TableCell numeric>{row.total}</TableCell>
                                            </TableRow>
                                        );
                                    })
                                    :
                                    <TableRow key={0}>
                                        <TableCell component="th" scope="row">Data Not Found</TableCell>
                                    </TableRow>
                                }
                            </TableBody>
                        </Table>
                    </div>
                </Dialog>
            </RctCollapsibleCard>
        );
    }
}

// const AlertsSummary = props => (
//     <RctCollapsibleCard 
//         heading="Alert Summary" 
//         customClasses="overflow-hidden" 
//         collapsible
//         reloadable
//         fullBlock>
//         <div style={{ height: 338, overflowY: 'auto' }}>
//             <table className="table table-middle table-hover mb-0">
//                 <thead>
//                     <tr style={{ height: 60 }}>
//                         <th>Alert Category</th>
//                         <th>Total</th>
//                     </tr>
//                 </thead>
//                 <tbody>
//                     {props.data && props.data.length > 0 ? props.data.map((value, key) => (
//                         <tr key={key} onClick={() => props.getDetailAlertSummary(value.alert_priority) } style={{ cursor: 'pointer' }}>
//                             <td><Typography variant="body1">{value.alert_priority}</Typography></td>
//                             <td><Typography variant="body1">{value.total}</Typography></td>
//                         </tr>
//                     )) : <tr>
//                             <td colSpan={3} style={{ textAlign: 'center' }}>
//                                 <Typography variant="caption">Data Not Found</Typography>
//                             </td>
//                         </tr>
//                     }
//                 </tbody>
//             </table>
//         </div>
//         <RctCardFooter>
//             <span className="fs-12 text-base">
//                 <i className="mr-5 zmdi zmdi-refresh"></i>
//                 Last update on {moment().subtract(2, 'days').format('LLL')}
//             </span>
//         </RctCardFooter>
//         <Dialog
//             maxWidth={"sm"}
// 			fullWidth
//             >

//         </Dialog>
//     </RctCollapsibleCard>
// )

const mapStateToProps = ({ alerts }) => {
    const { loading, alertSummary } = alerts;
    return { loading, alertSummary }
}

export default connect(mapStateToProps, {
    getDetailAlertSummary
})(AlertsSummary);

// class AlertsSummary extends Component {

//     render() {
//         // const { classes } = this.props;
//         const data = this.props.data && this.props.data.slice(3, 7).map((value, key) => {
//             return {
//                 name: value.alert_status, 
//                 value: value.percentage
//             }
//         });
//         const dataNull = this.props.data && this.props.data.slice(3, 7).map((value, key) => {
//             if(key === 0) {
//                 return {
//                     name: value.alert_status, 
//                     value: 100
//                 }
//             } else {
//                 return {
//                     name: value.alert_status, 
//                     value: 0
//                 }
//             }
//         });
//         let total = data && _.reduce(data, (sum, n) => { return typeof sum === "number" ? sum+n.value : sum.value + n.value; });
//         const COLORS = this.props.data && this.props.data.slice(3, 7).map((value, key) => {
//             if(total !== 0) {
//                 return value.status_alert_color_hex
//             } else {
//                 return '#CCCCCC'
//             }
//         })
//         const RADIAN = Math.PI / 180;
//         const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
//             const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
//             const x  = cx + radius * Math.cos(-midAngle * RADIAN);
//             const y = cy  + radius * Math.sin(-midAngle * RADIAN);
//             return (
//                 <text x={x} y={y} fill="white" textAnchor={'middle'} dominantBaseline="middle" style={{ fontSize: 10 }}>
//                     {percent !== 0 && `${_.round((percent * 100), 2)}%`}
//                 </text>
//             );
//         };
//         return (
//             <RctCollapsibleCard 
//                 heading="Alerts Summary" 
//                 customClasses="overflow-hidden" 
//                 collapsible
//                 reloadable
//                 fullBlock>
//                 <div>
//                     <div className="p-20">
//                         <ResponsiveContainer height={250} width="100%">
//                             <PieChart>
//                                 <Pie
//                                     data={data && total !== 0 ? data : dataNull}
//                                     dataKey='value'
//                                     cx={"50%"} 
//                                     cy={"40%"} 
//                                     innerRadius={60}
//                                     outerRadius={90}
//                                     startAngle={90} 
//                                     endAngle={450}
//                                     label={total !== 0 ? renderCustomizedLabel : null}
//                                     labelLine={false}
//                                     >
//                                     {data && data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]} /> )}
//                                     <Label value={`Total ${_.round(total, 2)}%`} offset={0} position="center" />
//                                 </Pie>
//                                 { total !== 0 && <Tooltip /> }
//                                 <Legend align="right" layout="vertical" verticalAlign="top" height={36} iconType="circle" wrapperStyle={{ fontSize: 12 }} />
//                             </PieChart>
//                         </ResponsiveContainer>
//                         <Container>
//                             <Row>
//                                 {this.props.data && this.props.data.slice(0,3).map((value, key) => (
//                                     <Col key={key} xs="4" style={{ backgroundColor: value.alert_priority_color, color: '#FFFFFF', textAlign: 'center', alignItems: 'center', height: 50 }}>
//                                         {value.alert_priority}<br />
//                                         {_.ceil(value.percentage, 2) || 0}
//                                     </Col>
//                                 ))}
//                             </Row>
//                         </Container>
//                     </div>
//                 </div>

//                 <RctCardFooter>
//                     <span className="fs-12 text-base">
//                         <i className="mr-5 zmdi zmdi-refresh"></i>
//                         Last update on {moment().subtract(2, 'days').format('LLL')}
//                     </span>
//                 </RctCardFooter>
//             </RctCollapsibleCard>
//         );
//     }

// }

// export default withStyles(styles)(AlertsSummary);