/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 21:35:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-23 15:37:20
 */
/**
 * Dasboard Routes
 * TODO: Ada data unplugged (base on data tracking) tapi di dashboard vehicle status tidak muncul
 */
import React, { Component } from 'react';
import {NotificationManager} from 'react-notifications';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import VehicleStatus from './vehicleStatus';
// import VehicleLocation from './vehicleLocation';
import AlertsSummary from './alertsSummary';
// import Utilization from './utilization';
// import AssetsUsage from './assetsUsage';
import BestDriver from './BestDriver';
import WorstDriver from './WorstDriver';
import GeoFenceSummary from './geofenceSummary';
import GPSNotUpdates from './gpsNotUpdates';
import GPSNotUpdatesMoreThanThreeDays from './gpsNotUpdatesMoreThanThreeDays';
import TopMileage from './TopMileage';

import ab from '../../lib/autobahn';
import {WS} from '../../constants/LibraryAPI';

export default class Dashboard extends Component {

    state = {
        data: null,
        showAlertSummary: null,
        showBestDriver: null,
        showGPSnotUpdatedOneDay: null,
        showGPSnotUpdatedThreeDay: null,
        showGeofence: null,
        showTopMileage: null,
        showVehicleLocation: null,
        showVehicleStatus: null,
        showWorstDriver: null,
    }

    constructor(props) {
        super(props);
        let self = this;
        this.conn = new ab.Session(WS,
            () => {
                this.conn.subscribe('dashboard', function(topic, data) {
                    let dashboard = data.data;
                    self.setState({
                        data: dashboard,
                        showAlertSummary: dashboard.showAlertSummary,
                        showBestDriver: dashboard.showBestDriver,
                        showGPSnotUpdatedOneDay: dashboard.showGPSnotUpdatedOneDay,
                        showGPSnotUpdatedThreeDay: dashboard.showGPSnotUpdatedThreeDay,
                        showGeofence: dashboard.showGeofence,
                        showTopMileage: dashboard.showTopMileage,
                        showVehicleLocation: dashboard.showVehicleLocation,
                        showVehicleStatus: dashboard.showVehicleStatus,
                        showWorstDriver: dashboard.showWorstDriver
                    });
                });
                this.conn.subscribe('notification', function(topic, data) {
                    // console.log('notification ==>', data)
                    if(data.data !== "") {
                        NotificationManager.success(data.data);
                    }
                });
            },
            () => {
                console.warn('WebSocket connection closed');
            },
            {'skipSubprotocolCheck': true}
        );
    }

    async componentWillUnmount() {
        if(this.conn.status()) {
            this.conn.close();
        }
    }

    render() {
        const { showBestDriver, showVehicleStatus, showGeofence, showAlertSummary, showWorstDriver, showTopMileage, showGPSnotUpdatedOneDay, showGPSnotUpdatedThreeDay } = this.state;
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.dashboard" />} match={this.props.match} />
                <div className="row">
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <VehicleStatus data={showVehicleStatus} />
                    </div>
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <AlertsSummary data={showAlertSummary} />
                    </div>
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <BestDriver data={showBestDriver} />
                    </div>
                    
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <WorstDriver data={showWorstDriver} />
                    </div>
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <TopMileage data={showTopMileage} />
                    </div>
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <GeoFenceSummary data={showGeofence} />
                    </div>

                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <GPSNotUpdates data={showGPSnotUpdatedOneDay} />
                    </div>
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        <GPSNotUpdatesMoreThanThreeDays data={showGPSnotUpdatedThreeDay} />
                    </div>
                    <div className="col-sm-12 col-md-4 col-xl-4">
                        
                    </div>
                </div>
            </div>
        );
    }
}
