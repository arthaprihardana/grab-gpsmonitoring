/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-21 14:05:41 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-23 15:36:46
 */
import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Typography } from '@material-ui/core';
import { PieChart, Pie, Cell, Legend, ResponsiveContainer, Tooltip, Label } from 'recharts';
// Lodash
import _ from 'lodash';
// rct card box
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
// card component
import { RctCardFooter } from 'Components/RctCard';

import moment from 'moment';
import 'moment/locale/id';

const VehicleStatus = props => (
    <RctCollapsibleCard 
        heading="Vehicle Status" 
        customClasses="overflow-hidden" 
        collapsible
        reloadable
        fullBlock>
        <div style={{ height: 338, overflowY: 'auto' }}>
            <table className="table table-middle table-hover mb-0">
                <thead>
                    <tr style={{ height: 60 }}>
                        <th>Vehicle Status</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    {props.data && props.data.length > 0 ? props.data.map((value, key) => (
                        <tr key={key}>
                            <td><Typography variant="body1">{value.vehicle_status}</Typography></td>
                            <td><Typography variant="body1">{value.total !== null ? value.total : 0}</Typography></td>
                        </tr>
                    )) : <tr>
                            <td colSpan={3} style={{ textAlign: 'center' }}>
                                <Typography variant="caption">Data Not Found</Typography>
                            </td>
                        </tr>
                    }
                </tbody>
            </table>
        </div>
        <RctCardFooter>
            <span className="fs-12 text-base">
                <i className="mr-5 zmdi zmdi-refresh"></i>
                Last update on {moment().format('LLL')}
            </span>
        </RctCardFooter>
    </RctCollapsibleCard>
)

export default VehicleStatus;

// class VehicleStatus extends Component {

//     render() {
//         const data = this.props.data && this.props.data.slice(0, 4).map((value, key) => {
//             return {
//                 name: value.vehicle_status, 
//                 value: value.percentage
//             }
//         });
//         const dataNull = this.props.data && this.props.data.slice(0, 4).map((value, key) => {
//             if(key === 0) {
//                 return {
//                     name: value.vehicle_status, 
//                     value: 100
//                 }
//             } else {
//                 return {
//                     name: value.vehicle_status, 
//                     value: 0
//                 }
//             }
//         });
//         let total = data && _.reduce(data, (sum, n) => { return typeof sum === "number" ? sum+n.value : sum.value + n.value; });
//         const COLORS = this.props.data && this.props.data.slice(0, 4).map((value, key) => {
//             if(total !== 0) {
//                 return value.vehicle_status_color
//             } else {
//                 return '#CCCCCC'
//             }
//         })
//         const RADIAN = Math.PI / 180;
//         const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
//             const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
//             const x  = cx + radius * Math.cos(-midAngle * RADIAN);
//             const y = cy  + radius * Math.sin(-midAngle * RADIAN);
//             // console.log('==>', percent !== 0 && `${(percent * 100)}%`);
//             return (
//                 <text x={x} y={y} fill="white" textAnchor={'middle'} dominantBaseline="middle" style={{ fontSize: 10 }}>
//                     {percent !== 0 && `${_.round(percent * 100, 2)}%`}
//                 </text>
//             );
//         };
//         return (
//             <RctCollapsibleCard 
//                 heading="Vehicle Status" 
//                 customClasses="overflow-hidden" 
//                 collapsible
//                 reloadable
//                 fullBlock>
//                 <div>
//                     <div className="p-20">
//                         <ResponsiveContainer height={300} width="100%">
//                             <PieChart>
//                                 <Pie
//                                     data={data && total !== 0 ? data : dataNull}
//                                     dataKey='value'
//                                     cx={"50%"} 
//                                     cy={"50%"} 
//                                     innerRadius={65}
//                                     outerRadius={100}
//                                     startAngle={90} 
//                                     endAngle={450}
//                                     label={total !== 0 ? renderCustomizedLabel : null}
//                                     labelLine={false}
//                                     >
//                                     {data && data.map((entry, index) => <Cell key={index} fill={COLORS[index % COLORS.length]} /> )}
//                                     <Label value={`Total ${_.round(total, 2)}%`} offset={0} position="center" />
//                                 </Pie>
//                                 {/* { total !== 0 && <Tooltip /> } */}
//                                 <Legend align="right" layout="vertical" verticalAlign="top" height={36} iconType="circle" />
//                             </PieChart>
//                         </ResponsiveContainer>
//                     </div>
//                 </div>

//                 <RctCardFooter>
//                     <span className="fs-12 text-base">
//                         <i className="mr-5 zmdi zmdi-refresh"></i>
//                         Last update on {moment().subtract(2, 'days').format('LLL')}
//                     </span>
//                 </RctCardFooter>
//             </RctCollapsibleCard>
//         );
//     }

// }

// export default VehicleStatus;