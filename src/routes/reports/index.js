/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 08:14:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-15 16:43:07
 */
/**
 * Report Routes
 */
import React, { Component, Fragment } from 'react';
import { FormGroup, Button } from 'reactstrap';
import { InputLabel, Paper, Input, IconButton, withStyles, TextField, InputAdornment, CircularProgress, MenuList, MenuItem, ListItemIcon, ListItemText } from '@material-ui/core';
import {connect} from 'react-redux';
import {NotificationManager} from 'react-notifications';
import _ from 'lodash';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
import RctSelect from 'Components/RctSelect/RctSelect';

import { DateTimePicker } from 'material-ui-pickers';

// styles
import styles from '../../lib/styles';
import {getTrackingVehicle, getReportHistorical, getReportFleetUtil, getReportOverspeed, getReportKmDriven, getReportDriverScore, getReportOutOfGeofence, getReportGpsNotUpdate,getReportNotification, getReportUnplugged, resetAllReport} from '../../actions';

import ListReport from './listReport';

import moment from 'moment';
import 'moment/locale/id';

class Report extends Component {

    state = {
        reportType: [{
            label: "Report Historical",
            value: 0
        }, {
            label: "Fleet Utilisasi",
            value: 1
        }, {
            label: "Overspeed",
            value: 2
        }, {
            label: "KM Driven",
            value: 3
        }, {
            label: "Driver Score",
            value: 4
        }, {
            label: "Out Of Geofence",
            value: 5
        }, {
            label: "GPS Not Update Between 1 - 3 Days",
            value: 6
        }, {
            label: "GPS Not Update >= 3 Days",
            value: 7
        }, {
            label: "Notification",
            value: 8
        }, {
            label: "Unplugged",
            value: 9
        }],
        reportSelected: null,
        vehicleList: [],
        searchVehicle: "",
        vehicle: "",
        startDate: moment().subtract(6, 'days').format('YYYY-MM-DD HH:mm:ss'),
        endDate: moment().subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss'),
        preview: [],
        hasError: false
    }

    onSearchVehicle = (searchVehicle) => {
        this.props.getTrackingVehicle({ search: searchVehicle })
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    componentDidCatch(error, info) {
        // console.log('error ==>', error);
        // console.log('info ==>', info)
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.state.reportSelected !== prevState.reportSelected) {
            this.props.resetAllReport();
            return { reset: true }
        }
        if(this.props.searchVehicle !== prevProps.searchVehicle) {
            return { searchVehicle: this.props.searchVehicle };
        }
        if(this.props.reportHistorical !== prevProps.reportHistorical) {
            if(this.props.reportHistorical !== null) {
                return { reportHistorical: this.props.reportHistorical.data }
            }
        }
        if(this.props.reportFleetUtil !== prevProps.reportFleetUtil) {
            if(this.props.reportFleetUtil !== null) {
                return { reportFleetUtil: this.props.reportFleetUtil.data }
            }
        }
        if(this.props.reportOverspeed !== prevProps.reportOverspeed) {
            if(this.props.reportOverspeed !== null) {
                return { reportOverspeed: this.props.reportOverspeed.data }
            }
        }
        if(this.props.reportKmDriven !== prevProps.reportKmDriven) {
            if(this.props.reportKmDriven !== null) {
                return { reportKmDriven: this.props.reportKmDriven.data }
            }
        }
        if(this.props.reportDriverScore !== prevProps.reportDriverScore) {
            if(this.props.reportDriverScore !== null) {
                return { reportDriverScore: this.props.reportDriverScore.data }
            }
        }
        if(this.props.reportOutOfGeofence !== prevProps.reportOutOfGeofence) {
            if(this.props.reportOutOfGeofence !== null) {
                return { reportOutOfGeofence: this.props.reportOutOfGeofence.data }
            }
        }
        if(this.props.reportGpsNotUpdate !== prevProps.reportGpsNotUpdate) {
            if(this.props.reportGpsNotUpdate !== null) {
                return { reportGpsNotUpdate: this.props.reportGpsNotUpdate.data }
            }
        }
        if(this.props.reportNotification !== prevProps.reportNotification) {
            if(this.props.reportNotification !== null) {
                return { reportNotification: this.props.reportNotification.data }
            }
        }
        if(this.props.reportUnplugged !== prevProps.reportUnplugged) {
            if(this.props.reportUnplugged !== null) {
                return { reportUnplugged: this.props.reportUnplugged.data }
            }
        }
        return null;
    }
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.reset !== undefined) {
                this.setState({ preview: [] });
            }
            if(snapshot.searchVehicle !== undefined ) {
                this.setState({ vehicleList: snapshot.searchVehicle });
            }
            if(snapshot.reportHistorical !== undefined) {
                if(snapshot.reportHistorical.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportHistorical.Data })
            }
            if(snapshot.reportFleetUtil !== undefined) {
                if(snapshot.reportFleetUtil.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportFleetUtil.Data })
            }
            if(snapshot.reportOverspeed !== undefined) {
                if(snapshot.reportOverspeed.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportOverspeed.Data })
            }
            if(snapshot.reportKmDriven !== undefined) {
                if(snapshot.reportKmDriven.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportKmDriven.Data })
            }
            if(snapshot.reportDriverScore !== undefined) {
                if(snapshot.reportDriverScore.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportDriverScore.Data })
            }
            if(snapshot.reportOutOfGeofence !== undefined) {
                if(snapshot.reportOutOfGeofence.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportOutOfGeofence.Data })
            }
            if(snapshot.reportGpsNotUpdate !== undefined) {
                if(snapshot.reportGpsNotUpdate.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportGpsNotUpdate.Data })
            }
            if(snapshot.reportNotification !== undefined) {
                if(snapshot.reportNotification.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportNotification.Data });
            }
            if(snapshot.reportUnplugged !== undefined) {
                if(snapshot.reportUnplugged.Data.length === 0) {
                    NotificationManager.error('Report data tidak ditemukan');
                }
                this.setState({ preview: snapshot.reportUnplugged.Data });
            }
        }
    }

    generateReport = () => {
        const { reportSelected } = this.state;
        switch (reportSelected) {
            case 0:
                this.props.getReportHistorical({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    license_plate: this.state.searchVehicle
                });
                break;
            case 1:
                this.props.getReportFleetUtil({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate
                });
                break;
            case 2:
                this.props.getReportOverspeed({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate
                });
                break;
            case 3:
                this.props.getReportKmDriven({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate
                });
                break;
            case 4:
                this.props.getReportDriverScore({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate
                });
                break;
            case 5:
                this.props.getReportOutOfGeofence({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate
                });
                break;
            case 6:
                this.props.getReportGpsNotUpdate({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    category: 1
                });
                break;
            case 7:
                this.props.getReportGpsNotUpdate({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    category: 2
                });
                break;
            case 8:
                this.props.getReportNotification({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                    license_plate: this.state.searchVehicle
                });
                break;
            case 9:
                this.props.getReportUnplugged({
                    startDate: this.state.startDate,
                    endDate: this.state.endDate,
                })
                break;
            default:
                this.setState({ preview: [] })
                break;
        }
    }

    render() {
        const { classes } = this.props;
        const { vehicleList } = this.state;
        let maxDate = moment(this.state.startDate).add(6, 'days') > moment() ? moment().format('YYYY-MM-DD') : moment(this.state.startDate).add(7, 'days').format('YYYY-MM-DD');
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.report" />} match={this.props.match} />
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-xl-12">
                        <RctCollapsibleCard
                            heading="Report Area" 
                            customClasses="overflow-hidden" 
                            collapsible
                            fullBlock>
                            <div className="p-20">
                                <div className="row">
                                    <div className="col-md-3">
                                        <FormGroup>
                                            <InputLabel htmlFor="reportType">Report Type</InputLabel>
                                            <Input
                                                fullWidth
                                                inputComponent={RctSelect}
                                                disableUnderline={true}
                                                inputProps={{
                                                    classes,
                                                    value: this.state.reportSelected,
                                                    onChange: val => this.setState({ 
                                                        reportSelected: val,
                                                        searchVehicle: ""
                                                    }),
                                                    placeholder: 'Select Report',
                                                    instanceId: 'select-report',
                                                    id: 'select-report',
                                                    name: 'select-report',
                                                    simpleValue: true,
                                                    options: this.state.reportType.map(value => ({
                                                        label: value.label,
                                                        value: value.value
                                                    })),
                                                }}
                                            />
                                        </FormGroup>
                                    </div>
                                    <div className="col-md-3">
                                        <FormGroup style={{ marginTop: 4 }}>
                                            <TextField
                                                disabled={this.state.reportSelected === 0 || this.state.reportSelected === 8 ? false : true}
                                                fullWidth
                                                type={'text'}
                                                id="search-vehicle"
                                                label="Search Vehicle"
                                                margin="none"
                                                InputProps={{
                                                    disableUnderline: true,
                                                    classes: {
                                                        root: classes.bootstrapRootPassword,
                                                        input: classes.bootstrapInputPassword,
                                                    },
                                                    endAdornment: (
                                                        <InputAdornment position="end" style={{ marginTop: '6px'}}>
                                                            <IconButton onClick={() => {
                                                                if(this.state.searchVehicle.length >= 3) {
                                                                    this.onSearchVehicle(this.state.searchVehicle);
                                                                }
                                                                }}>
                                                                <i className="zmdi zmdi-search"></i>
                                                            </IconButton>
                                                            {this.props.loading &&
                                                                <CircularProgress style={{ position: 'absolute', right: 7, top: 2 }} size={36} /> }
                                                        </InputAdornment>
                                                    )
                                                }}
                                                value={this.state.searchVehicle !== null ? this.state.searchVehicle : ""}
                                                InputLabelProps={{
                                                    shrink: true,
                                                    className: classes.bootstrapFormLabel,
                                                }}
                                                onChange={(event) => {
                                                    let self = this;
                                                    this.setState({ searchVehicle: event.target.value }, () => {
                                                        if(self.state.searchVehicle.length >= 3) {
                                                            self.onSearchVehicle(self.state.searchVehicle);
                                                        }
                                                    })
                                                }} 
                                                />
                                                {vehicleList && vehicleList.length > 0 && 
                                                    <Paper className={classes.root} style={{ marginTop: 5 }} elevation={1}>
                                                        <MenuList>
                                                            {vehicleList.map((value, key) => (
                                                                <MenuItem key={key} onClick={() => this.setState({ 
                                                                    imei: value.imei,
                                                                    vehicle: value.license_plate,
                                                                    searchVehicle: value.license_plate,
                                                                    vehicleList: []
                                                                    }) }>
                                                                    <ListItemIcon className={classes.icon}>
                                                                        <i className="zmdi zmdi-directions-car" style={{ fontSize: '18px' }}></i>
                                                                    </ListItemIcon>
                                                                    <ListItemText inset primary={value.license_plate} />
                                                                </MenuItem>
                                                            ))}
                                                        </MenuList>
                                                    </Paper> }
                                        </FormGroup>
                                    </div>
                                    <div className="col-md-2">
                                        <Fragment>
                                            <div className="rct-picker" style={{ marginTop: 4 }}>
                                                <DateTimePicker
                                                    ref={"startDate"}
                                                    label="Start Date"
                                                    value={this.state.startDate}
                                                    onChange={(date) => this.setState({ startDate: moment(date).format('YYYY-MM-DD HH:mm:ss') }) }
                                                    format={'DD/MM/YYYY HH:mm:ss'}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            root: classes.bootstrapRootPassword,
                                                            input: classes.bootstrapInputPassword,
                                                        },
                                                        endAdornment: (
                                                            <InputAdornment position="end" style={{
                                                                position: 'absolute',
                                                                right: '16px',
                                                                bottom: '7px',
                                                                color: '#999'
                                                            }}>
                                                                <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.startDate.open() }>
                                                                    <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                                </IconButton>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                        className: classes.bootstrapFormLabel,
                                                    }}
                                                    animateYearScrolling={false}
                                                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                                    fullWidth
                                                    keyboard
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/,' ', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                                                    ampm={false}
                                                    maxDate={moment().format('YYYY-MM-DD HH:mm:ss')}
                                                />
                                            </div>
                                        </Fragment>
                                    </div>
                                    <div className="col-md-2">
                                        <Fragment>
                                            <div className="rct-picker" style={{ marginTop: 4 }}>
                                                <DateTimePicker
                                                    ref={"endDate"}
                                                    label="End Date"
                                                    value={this.state.endDate}
                                                    onChange={(date) => this.setState({ endDate: moment(date).format('YYYY-MM-DD HH:mm:ss') }) }
                                                    format={'DD/MM/YYYY HH:mm:ss'}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            root: classes.bootstrapRootPassword,
                                                            input: classes.bootstrapInputPassword,
                                                        },
                                                        endAdornment: (
                                                            <InputAdornment position="end" style={{
                                                                position: 'absolute',
                                                                right: '16px',
                                                                bottom: '7px',
                                                                color: '#999'
                                                            }}>
                                                                <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.endDate.open() }>
                                                                    <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                                </IconButton>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                        className: classes.bootstrapFormLabel,
                                                    }}
                                                    animateYearScrolling={false}
                                                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                                    fullWidth
                                                    keyboard
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/,' ', /\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/]}
                                                    ampm={false}
                                                    minDate={moment(this.state.startDate).format('YYYY-MM-DD HH:mm:ss')}
                                                    maxDate={moment(this.state.startDate).add(6, 'days') > moment() ? moment().format('YYYY-MM-DD HH:mm:ss') : moment(this.state.startDate).add(7, 'days').format('YYYY-MM-DD HH:mm:ss')}
                                                    maxDateMessage={`Maksimum tanggal untuk penarikan report adalah ${maxDate}`}
                                                    minDateMessage={`Minimum tanggal untuk penarikan report adalah ${moment(this.state.startDate).add(30, 'minutes').format('lll')}`}
                                                />
                                            </div>
                                        </Fragment>
                                    </div>
                                    <div className="col-md-2">
                                        <Button outline color="success" onClick={() => {
                                            if(moment(this.state.endDate).isBetween(this.state.startDate, moment(this.state.startDate).add(6, 'days').format('YYYY-MM-DD HH:mm:ss'))) {
                                                this.generateReport()
                                            }
                                        }} style={{ height: 45, marginTop: 27 }}>
                                            Search
                                        </Button>
                                    </div>
                                </div>
                            </div>
                        </RctCollapsibleCard>
                    </div>
                </div>
                { this.state.preview.length > 0 && 
                <div className="row">
                    <div className="col-sm-12 col-md-12 col-xl-12">
                        <ListReport tipe={this.state.reportSelected} data={this.state.preview} />
                    </div>
                </div> }
                { this.props.loadingReport && <RctSectionLoader /> }
            </div>
        );
    }
}

const classReport = withStyles(styles, { withTheme: true })(Report);

const mapStateToProps = ({ tracking, report }) => {
    const { loading, vehicle } = tracking;
    const searchVehicle = vehicle !== null ? vehicle.data.Data : null;
    const { reportHistorical, reportFleetUtil, reportOverspeed, reportKmDriven, reportDriverScore, reportOutOfGeofence, reportGpsNotUpdate, reportNotification, reportUnplugged } = report;
    const loadingReport = report.loading;
    return { loading, searchVehicle, loadingReport, reportHistorical, reportFleetUtil, reportOverspeed, reportKmDriven, reportDriverScore, reportOutOfGeofence, reportGpsNotUpdate, reportNotification, reportUnplugged };
}

export default connect(mapStateToProps, {
    getTrackingVehicle,
    getReportHistorical,
    getReportFleetUtil,
    getReportOverspeed,
    getReportKmDriven,
    getReportDriverScore,
    getReportOutOfGeofence,
    getReportGpsNotUpdate,
    getReportNotification,
    getReportUnplugged,
    resetAllReport
})(classReport)