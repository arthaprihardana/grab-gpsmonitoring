/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import { hhmmss } from '../../../helpers/helpers'
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class OutOfGeofence extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 2500px";
    }

    render() {
        const columns = [{
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Time",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Rangka",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Mesin",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Duration",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Address",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Geofence Area",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                item.license_plate,
                moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                item.vin,
                item.machine_number,
                hhmmss(item.duration),
                item.address,
                item.geofence_area
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                LicensePlate: item.license_plate,
                Time: moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                NoRangka: item.vin,
                NoMesin: item.machine_number,
                Duration: hhmmss(item.duration),
                Address: item.address,
                GeofenceArea: item.geofence_area
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'Out_Of_Geofence_Report.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton  onClick={this.toExcel}>
                                    <DescriptionIcon  />
                                </IconButton>
                            </Tooltip>
                        } filename="Out_Of_Geofence_Report">
                            <ExcelSheet data={excelData} name="Out Of Geofence Report">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Time" value={col => moment(col.device_time).format('YYYY-MM-DD HH:mm:ss')}/>
                                <ExcelColumn label="No Rangka" value="vin"/>
                                <ExcelColumn label="No Mesin" value="machine_number" />
                                <ExcelColumn label="Duration" value={ item => hhmmss(item.duration) } />
                                <ExcelColumn label="Address" value="address" />
                                <ExcelColumn label="Geofence Area" value="geofence_area" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton  onClick={() => printJS({printable: printData, properties: ['LicensePlate', 'Time', 'NoRangka', 'NoMesin', 'Duration', 'Address', 'GeofenceArea'], type: 'json'})}>
                                <PrintIcon  />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"Out Of Geofence Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

export default OutOfGeofence;