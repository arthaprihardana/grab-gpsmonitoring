/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
// import { withStyles } from "@material-ui/core/styles";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import { hhmmss } from '../../../helpers/helpers'
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const defaultToolbarStyles = {
    iconButton: {}
};

class Overspeed extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 2500px";
    }

    render() {
        // const { classes } = this.props;
        const columns = [{
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Time",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Rangka",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Mesin",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Duration",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Kategori Overspeed",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Speed (km/h)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Address",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                item.license_plate,
                moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                item.vin,
                item.machine_number,
                hhmmss(item.over_speed_time),
                item.category_over_speed,
                item.speed,
                item.address
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                LicensePlate: item.license_plate,
                time: moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                NoRangka: item.vin,
                MachineNumber: item.machine_number,
                Duration: hhmmss(item.over_speed_time),
                KategoriOverspeed: item.category_over_speed,
                Speed: item.speed,
                Address: item.address
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'Overspeed.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton onClick={this.toExcel}>
                                    <DescriptionIcon />
                                </IconButton>
                            </Tooltip>
                        } filename="Overspeed_Report">
                            <ExcelSheet data={excelData} name="Overspeed Report">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Time" value={col => moment(col.device_time).format('YYYY-MM-DD HH:mm:ss')}/>
                                <ExcelColumn label="No Rangka" value="vin"/>
                                <ExcelColumn label="No Mesin" value="machine_number" />
                                <ExcelColumn label="Duration" value={ item => hhmmss(item.over_speed_time) } />
                                <ExcelColumn label="Kategori Overspeed" value="category_over_speed" />
                                <ExcelColumn label="Speed (km/h)" value="speed" />
                                <ExcelColumn label="Address" value="address" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton onClick={() => printJS({printable: printData, properties: ['LicensePlate', 'time', 'NoRangka', 'MachineNumber', 'Duration', 'KategoriOverspeed', 'Speed', 'Address'], type: 'json'})}>
                                <PrintIcon />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"Overspeed Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

// export default withStyles(defaultToolbarStyles, { name: "Overspeed" })(Overspeed);
export default Overspeed;