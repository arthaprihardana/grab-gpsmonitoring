/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import _ from 'lodash';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
// import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import moment from 'moment';
import 'moment/locale/id';
import { hhmmss } from '../../../helpers/helpers';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const defaultToolbarStyles = {
    iconButton: {}
};

class FleetUtilisasi extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 3500px";
    }

    render() {
        const { classes } = this.props;
        const columns = [{
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Rangka",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Mesin",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Total Data",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Park Time",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Moving Time",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Engine On",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Total Mileage (km)",
            options: {
                filter: false,
                sort: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return _.floor(value / 1000, 2);
                }
            }
        }, {
            name: "Average Speed (km/h)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Rasio Engine On (%)",
            options: {
                filter: false,
                sort: true
            }
        }/*, {
            name: "Out Of Zone Duration",
            options: {
                filter: false,
                sort: true
            }
        }*/, {
            name: "BBM Consumption (L)",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data && this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                item.license_plate,
                item.vin,
                item.machine_number,
                item.total_data,
                hhmmss(item.park_time),
                hhmmss(item.moving_time),
                hhmmss(item.engine_on_time),
                `${_.floor(item.total_mileage / 1000, 2)}`,
                `${_.floor(item.average_speed, 2)}`,
                `${_.floor(item.rasio_engine_on, 2)}`,
                // hhmmss(item.duration_out_zone),
                item.fuel
            ]
        }) : [];

        const excelData = this.props.data && this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data && this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                LicensePlate: item.license_plate,
                NoRangka: item.vin,
                NoMesin: item.machine_number,
                TotalData: item.total_data,
                ParkTime: hhmmss(item.park_time),
                MovingTime: hhmmss(item.moving_time),
                EngineOn: hhmmss(item.engine_on_time),
                TotalMileage: `${_.floor(item.total_mileage / 1000, 2)}`,
                AverageSpeed: `${_.floor(item.average_speed, 2)}`,
                RasioEngineOn: `${_.floor(item.rasio_engine_on, 2)}`,
                // OutOfZoneDuration: hhmmss(item.duration_out_zone),
                BbmConsumption: item.fuel
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'Fleet_Utilization.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton  onClick={this.toExcel}>
                                    <DescriptionIcon />
                                </IconButton>
                            </Tooltip>
                        } filename="Fleet_Utilization">
                            <ExcelSheet data={excelData} name="Fleet Utilization Report">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="No Rangka" value="vin"/>
                                <ExcelColumn label="No Mesin" value="machine_number"/>
                                <ExcelColumn label="Total Data" value="total_data" />
                                <ExcelColumn label="Park Time" value={ item => hhmmss(item.park_time)} />
                                <ExcelColumn label="Moving Time" value={ item => hhmmss(item.moving_time)} />
                                <ExcelColumn label="Engine On" value={ item => hhmmss(item.engine_on_time)} />
                                <ExcelColumn label="Total Mileage (km)" value={ item => _.floor(item.total_mileage / 1000, 2) } />
                                <ExcelColumn label="Average Speed (km/h)" value={ item => _.floor(item.average_speed, 2) } />
                                <ExcelColumn label="Rasio Engine On (%)" value={ item => _.floor(item.rasio_engine_on, 2) } />
                                {/* <ExcelColumn label="Out Of Zone Duration" value={ item => hhmmss(item.duration_out_zone)} /> */}
                                <ExcelColumn label="BBM Consumption (L)" value="fuel" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton  onClick={() => printJS({printable: printData, properties: ['LicensePlate', 'NoRangka', 'NoMesin', 'TotalData', 'ParkTime', 'MovingTime', 'EngineOn', 'TotalMileage', 'AverageSpeed', 'RasioEngineOn'/*, 'OutOfZoneDuration'*/, 'BbmConsumption'], type: 'json'})}>
                                <PrintIcon />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"Fleet Utilization Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

// export default withStyles(defaultToolbarStyles, { name: "FleetUtilisasi" })(FleetUtilisasi);
export default FleetUtilisasi;