/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import _ from 'lodash';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
// import { withStyles } from "@material-ui/core/styles";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

const defaultToolbarStyles = {
    iconButton: {}
};

class KmDriven extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 3500px";
    }

    render() {
        // const { classes } = this.props;
        const columns = [{
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Driver",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Rangka",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "No Mesin",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Alamat Awal",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Alamat Akhir",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "KM Awal (km)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "KM Akhir (km)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "KM Driven (km)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Total Odometer (km)",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                item.license_plate,
                item.driver,
                item.vin,
                item.machine_number,
                item.start_address,
                item.end_address,
                `${_.floor(item.km_start / 1000, 2)}`,
                `${_.floor(item.km_end / 1000, 2)}`,
                `${_.floor(item.km_driven / 1000, 2)}`,
                `${_.floor(item.total_odometer / 1000, 2)}`
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                LicensePlate: item.license_plate,
                Driver: item.driver,
                NoRangka: item.vin,
                NoMesin: item.machine_number,
                AlamatAwal: item.start_address,
                AlamatAkhir: item.end_address,
                KmAwal: _.floor(item.km_start / 1000, 2),
                KmAkhir: _.floor(item.km_end / 1000, 2),
                KmDriven: _.floor(item.km_driven / 1000, 2),
                TotalOdometer: _.floor(item.total_odometer / 1000, 2)
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'KM_Driven.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton onClick={this.toExcel}>
                                    <DescriptionIcon />
                                </IconButton>
                            </Tooltip>
                        } filename="KM_Driven">
                            <ExcelSheet data={excelData} name="KM Driven Report">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Driver" value="driver"/>
                                <ExcelColumn label="No Rangka" value="vin"/>
                                <ExcelColumn label="No Mesin" value="machine_number" />
                                <ExcelColumn label="Alamat Awal" value="start_address" />
                                <ExcelColumn label="Alamat Akhir" value="end_address" />
                                <ExcelColumn label="KM Awal" value={ item => _.floor(item.km_start / 1000, 2)} />
                                <ExcelColumn label="KM Akhir" value={ item => _.floor(item.km_end / 1000, 2)} />
                                <ExcelColumn label="KM Driven" value={ item => _.floor(item.km_driven / 1000, 2) } />
                                <ExcelColumn label="Total Odometer" value={ item => _.floor(item.total_odometer / 1000, 2)} />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton onClick={() => printJS({printable: printData, properties: ['LicensePlate', 'Driver', 'NoRangka', 'NoMesin', 'AlamatAwal', 'AlamatAkhir', 'KmAwal', 'KmAkhir', 'KmDriven', 'TotalOdometer'], type: 'json'})}>
                                <PrintIcon />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"KM Driven Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

// export default withStyles(defaultToolbarStyles, { name: "KmDriven" })(KmDriven);
export default KmDriven;