/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class DriverScore extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 2000px";
    }

    render() {
        const columns = [{
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Driver",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Overspeed",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Harsh Acceleration",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Harsh Braking",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Harsh Cornering",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Main Power Cut",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Signal Jamming",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Bump Detection",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Out Of Zone",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Total",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                item.license_plate,
                item.driver,
                item.over_speed,
                item.harsh_acceleration,
                item.harsh_braking,
                item.harsh_cornering,
                item.main_power_cut,
                item.signal_jamming,
                item.bump_detection,
                item.out_of_zone,
                item.total
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                LicensePlate: item.license_plate,
                Driver: item.driver,
                OverSpeed: item.over_speed,
                HarshAcceleration: item.harsh_acceleration,
                HarshBreaking: item.harsh_braking,
                HarshCornering: item.harsh_cornering,
                MainPowerCut: item.main_power_cut,
                SignalJamming: item.signal_jamming,
                BumpDetection: item.bump_detection,
                OutOfZone: item.out_of_zone,
                Total: item.total
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'Driver_Score_Report.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton  onClick={this.toExcel}>
                                    <DescriptionIcon  />
                                </IconButton>
                            </Tooltip>
                        } filename="Driver_Score_Report">
                            <ExcelSheet data={excelData} name="Driver Score Report">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Driver" value="driver"/>
                                <ExcelColumn label="Overspeed" value="over_speed"/>
                                <ExcelColumn label="Harsh Acceleration" value="harsh_acceleration" />
                                <ExcelColumn label="Harsh Braking" value="harsh_braking" />
                                <ExcelColumn label="Harsh Cornering" value="harsh_cornering" />
                                <ExcelColumn label="Main Power Cut" value="main_power_cut" />
                                <ExcelColumn label="Signal Jamming" value="signal_jamming" />
                                <ExcelColumn label="Bump Detection" value="bump_detection" />
                                <ExcelColumn label="Out Of Zone" value="out_of_zone" />
                                <ExcelColumn label="Total" value="total" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton  onClick={() => printJS({printable: printData, properties: ['LicensePlate', 'Driver', 'OverSpeed', 'HarshAcceleration', 'HarshBreaking', 'HarshCornering', 'MainPowerCut', 'SignalJamming', 'BumpDetection', 'OutOfZone', 'Total'], type: 'json'})}>
                                <PrintIcon  />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"Driver Score Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

export default DriverScore;