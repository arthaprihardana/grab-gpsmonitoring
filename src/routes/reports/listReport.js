/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:19:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-15 16:41:42
 */
import React, { Component } from 'react';
import HistoricalReport from './HistoricalReport';
import FleetUtilReport from './FleetUtilisasi';
import OverspeedReport from './Overspeed';
import KmDrivenReport from './KMDriven';
import DriverScoreReport from './DriverScore';
import OutOfGeofenceReport from './OutOfGeofence';
import GpsNotUpdateReport from './GpsNotUpdate';
import NotificationReport from './Notification';
import UnpluggedReport from './Unplugged';

export default class ListReport extends Component {

    render() {
        const { tipe, data } = this.props;
        switch (tipe) {
            case 0:
                return <HistoricalReport key={0} data={data} />
            case 1:
                return <FleetUtilReport key={1} data={data} />
            case 2:
                return <OverspeedReport key={2} data={data} />
            case 3:
                return <KmDrivenReport key={3} data={data} />
            case 4:
                return <DriverScoreReport key={4} data={data} />
            case 5:
                return <OutOfGeofenceReport key={5} data={data} />
            case 6:
                return <GpsNotUpdateReport key={6} data={data} />
            case 7:
                return <GpsNotUpdateReport key={7} data={data} />
            case 8:
                return <NotificationReport key={8} data={data} />
            case 9:
                return <UnpluggedReport key={9} data={data} />
            default:
                return <div />
        }
    }

}