/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import _ from 'lodash';
import ReactExport from "react-data-export";
import printJS from 'print-js';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Notification extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 2500px";
    }

    render() {
        const columns = [{
            name: "Date Time",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Status Engine",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Heading",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Koordinat (longitude)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Koordinat (latitude)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Speed",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Mileage (km)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Alert",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Address",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                item.license_plate,
                item.engine_status,
                item.heading,
                item.longitude,
                item.latitude,
                item.speed,
                _.floor(item.mileage / 1000, 2),
                item.alert,
                item.address
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                DateTime: moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                LicensePlate: item.license_plate,
                StatusEngine: item.engine_status,
                Heading: item.heading,
                Longitude: item.longitude,
                Latitude: item.latitude,
                Speed: item.speed,
                Mileage: _.floor(item.mileage / 1000, 2),
                Alert: item.alert,
                Address: item.address
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'Notification_Report.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton  onClick={this.toExcel}>
                                    <DescriptionIcon  />
                                </IconButton>
                            </Tooltip>
                        } filename="Notification_Report">
                            <ExcelSheet data={excelData} name="Driver Score Report">
                                <ExcelColumn label="Date Time" value={item => moment(item.device_time).format('YYYY-MM-DD HH:mm:ss')}/>
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Status Engine" value="engine_status"/>
                                <ExcelColumn label="Heading" value="heading" />
                                <ExcelColumn label="Koordinat (longitude)" value="longitude" />
                                <ExcelColumn label="Koordinat (latitude)" value="latitude" />
                                <ExcelColumn label="Speed" value="speed" />
                                <ExcelColumn label="Mileage (km)" value={ item => _.floor(item.mileage / 1000, 2) } />
                                <ExcelColumn label="Alert" value="alert" />
                                <ExcelColumn label="Address" value="address" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton  onClick={() => printJS({printable: printData, properties: ['DateTime', 'LicensePlate', 'StatusEngine', 'Heading', 'Longitude', 'Latitude', 'Speed', 'Mileage', 'Alert', 'Address'], type: 'json'})}>
                                <PrintIcon  />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"Notification Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

export default Notification;