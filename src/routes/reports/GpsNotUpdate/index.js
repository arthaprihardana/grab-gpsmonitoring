/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class GpsNotUpdate extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 2500px";
    }

    render() {
        const columns = [{
            name: "Kategori",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "GPS Supplier",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Branch (Location)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Imei",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "VIN",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Install Date",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Last Update",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Last Location",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "GPS Satelite",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "GSM Signal (%)",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                item.category,
                item.gps_supplier,
                item.branch,
                item.imei,
                item.license_plate,
                item.vin,
                moment(item.date_installation).format('YYYY-MM-DD'),
                moment(item.last_update).format('YYYY-MM-DD HH:mm:ss'),
                item.last_location,
                item.gps_satellite,
                item.gsm_signal * 20
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                Kategori: item.category,
                GpsSupplier: item.gps_supplier,
                Branch: item.branch,
                Imei: item.imei,
                LicensePlate: item.license_plate,
                VIN: item.vin,
                InstallDate: moment(item.date_installation).format('YYYY-MM-DD'),
                LastUpdate: moment(item.last_update).format('YYYY-MM-DD HH:mm:ss'),
                LastLocation: item.last_location,
                GpsSatelite: item.gps_satellite,
                GsmSignal: item.gsm_signal * 20
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'GPS_Not_Update_Report.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton  onClick={this.toExcel}>
                                    <DescriptionIcon  />
                                </IconButton>
                            </Tooltip>
                        } filename="GPS_Not_Update_Report">
                            <ExcelSheet data={excelData} name="GPS Not Update Report">
                                <ExcelColumn label="Kategori" value="category"/>
                                <ExcelColumn label="GPS Supplier" value="gps_supplier"/>
                                <ExcelColumn label="Branch (Location)" value="branch"/>
                                <ExcelColumn label="Imei" value="imei" />
                                <ExcelColumn label="License Plate" value="license_plate" />
                                <ExcelColumn label="VIN" value="vin" />
                                <ExcelColumn label="Install Date" value={item => moment(item.date_installation).format('YYYY-MM-DD')} />
                                <ExcelColumn label="Last Update" value={item => moment(item.last_update).format('YYYY-MM-DD HH:mm:ss')} />
                                <ExcelColumn label="Last Location" value="last_location" />
                                <ExcelColumn label="GPS Satelite" value="gps_satellite" />
                                <ExcelColumn label="GSM Signal (%)" value={col => col.gsm_signal * 20} />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton  onClick={() => printJS({printable: printData, properties: ['Kategori', 'GpsSupplier', 'Branch', 'Imei', 'LicensePlate', 'VIN', 'InstallDate', 'LastUpdate', 'LastLocation', 'GpsSatelite', 'GsmSignal'], type: 'json'})}>
                                <PrintIcon  />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"GPS Not Update Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

export default GpsNotUpdate;