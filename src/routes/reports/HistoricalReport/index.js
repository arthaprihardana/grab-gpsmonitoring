/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 16:13:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-25 01:15:59
 */
import React, { Component, Fragment } from 'react';
import MUIDataTable from "mui-datatables";
import ReactExport from "react-data-export";
import printJS from 'print-js';
import _ from 'lodash';
import { IconButton, Tooltip, Typography } from "@material-ui/core";
// import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import { Print as PrintIcon, Description as DescriptionIcon } from "@material-ui/icons";
import moment from 'moment';
import 'moment/locale/id';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

// const defaultToolbarStyles = {
//     iconButton: {}
// };

class HistoricalReport extends Component {

    componentDidMount() {
        document.querySelector('table[role="grid"]').style="width: 3500px";
    }

    render() {
        // const { classes } = this.props;
        const columns = [{
            name: "Date Time",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "License Plate",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Engine Status",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Heading",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Koordinat (longitude)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Koordinat (latitude)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Speed (km/h)",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Mileage (km)",
            options: {
                filter: false,
                sort: true,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return _.floor(value / 1000, 2);
                }
            }
        }, {
            name: "Alert",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Immo",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Out Of Zone",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Sleep Mode",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Satelite",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Accu",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "GSM Signal %",
            options: {
                filter: false,
                sort: true
            }
        }, {
            name: "Address",
            options: {
                filter: false,
                sort: true
            }
        }];

        const data = this.props.data.length > 0 ? this.props.data.map(item => {
            return [
                moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                item.license_plate,
                item.engine_status,
                item.heading,
                item.longitude,
                item.latitude,
                item.speed,
                item.mileage,
                item.alert,
                item.immo,
                item.out_of_zone,
                item.sleep_mode,
                item.satellite,
                item.accu,
                item.gsm_signal * 20,
                item.address
            ]
        }) : [];

        const excelData = this.props.data.length > 0 ? this.props.data : [];
        const printData = this.props.data.length > 0 ? this.props.data.map(item => {
            return {
                DateTime: moment(item.device_time).format('YYYY-MM-DD HH:mm:ss'),
                LicensePlate: item.license_plate,
                EngineStatus: item.engine_status,
                Heading: item.heading,
                Longitude: item.longitude,
                Latitude: item.latitude,
                Speed: item.speed,
                Mileage: item.mileage,
                Alert: item.alert,
                Immo: item.immo,
                OutOfZone: item.out_of_zone,
                SleepMode: item.sleep_mode,
                Satelite: item.satellite,
                Accu: item.accu,
                GsmSignal: item.gsm_signal * 20,
                Address: item.address
            }
        }) : [];

        const options = {
            filter: false,
            filterType: 'dropdown',
            responsive: "scroll",
            selectableRows: false,
            search: false,
            viewColumns: false,
            print: false,
            downloadOptions: {filename: 'Historical_Report.csv', separator: ','},
            customToolbar: () => {
                return (
                    <Fragment>
                        <ExcelFile element={
                            <Tooltip title={"Download Excel"}>
                                <IconButton  onClick={this.toExcel}>
                                    <DescriptionIcon  />
                                </IconButton>
                            </Tooltip>
                        } filename="Historical_Report">
                            <ExcelSheet data={excelData} name="Historical Report">
                                <ExcelColumn label="Date Time" value={(col) => moment(col.device_time).format('YYYY-MM-DD HH:mm:ss')} />
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Engine Status" value="engine_status"/>
                                <ExcelColumn label="Heading" value="heading"/>
                                <ExcelColumn label="Koordinat (longitude)" value="longitude" />
                                <ExcelColumn label="Koordinat (latitude)" value="latitude" />
                                <ExcelColumn label="Speed (km/h)" value="speed" />
                                <ExcelColumn label="Mileage (km)" value="mileage"/>
                                <ExcelColumn label="Alert" value="alert" />
                                <ExcelColumn label="Immo" value="immo" />
                                <ExcelColumn label="Out of Zone" value="out_of_zone" />
                                <ExcelColumn label="Sleep Mode" value="sleep_mode" />
                                <ExcelColumn label="Satellite" value="satellite" />
                                <ExcelColumn label="Accu" value="accu" />
                                <ExcelColumn label="GSM Signal %" value={col => (col.gsm_signal * 20)} />
                                <ExcelColumn label="Address" value="address" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Tooltip title={"Print"}>
                            <IconButton  onClick={() => printJS({printable: printData, properties: ['DateTime', 'LicensePlate', 'EngineStatus', 'Heading', 'Longitude', 'Latitude', 'Speed', 'Mileage', 'Alert', 'Immo', 'OutOfZone', 'SleepMode', 'Satelite', 'Accu', 'GsmSignal', 'Address'], type: 'json'})}>
                                <PrintIcon  />
                            </IconButton>
                        </Tooltip>
                    </Fragment>
                );
            }
        };

        return (
            <MUIDataTable 
                title={"Historical Report"} 
                data={data} 
                columns={columns} 
                options={options}
                />
        );
    }

}

// export default withStyles(defaultToolbarStyles, { name: "HistoricalReport" })(HistoricalReport);
export default HistoricalReport;