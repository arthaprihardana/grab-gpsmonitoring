/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 23:13:47 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 23:20:43
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, List, ListItem, ListItemText, Divider, AppBar, Toolbar, IconButton, Slide, TextField, FormControl, InputLabel, Input, InputAdornment, Select, MenuItem, FormHelperText } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../lib/styles';
// Transition
import Transition from '../../components/Transition';
// Actions
import { addDriver, closeFormDriver, updateDriver, getAreas } from '../../actions';
// Validation
import FormValidator from '../../helpers/FormValidation';

class FormMasterDriver extends Component {

    validator = new FormValidator([
        {
            field: "name",
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama driver tidak boleh kosong'
        },
        {
            field: "spk_number",
            method: 'isEmpty',
            validWhen: false,
            message: 'SPK number tidak boleh kosong'
        },
        {
            field: "area_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Area dode tidak boleh kosong'
        },
    ])

    submitted = false

    state = {
        name: '',
        spk_number: '',
        area_code: '',
        dataArea: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getAreas({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.areas.areas !== prevProps.areas.areas) {
            let master_areas = this.props.areas.areas.data;
            let dataSelect = this.state.dataArea;
            master_areas.map((value, key) => {
                dataSelect.push({ value: value.area_code, label: value.area_name })
            });
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    name: '',
                    spk_number: '',
                    area_code: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    name: this.props.data.name,
                    spk_number: this.props.data.spk_number,
                    area_code: this.props.data.area_code
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateDriver({
                id: this.props.data.driver_code,
                FormData: {
                    name: this.state.name,
                    spk_number: this.state.spk_number,
                    area_code: this.state.area_code,
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addDriver({
                    FormData: {
                        name: this.state.name,
                        spk_number: this.state.spk_number,
                        area_code: this.state.area_code
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataArea } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormDriver()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan master driver. silahkan daftarkan driver baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.name : ""}
                                label="Driver Name"
                                id="driver-name"
                                error={validation.name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ name: event.target.value }) } 
                                helperText={validation.name.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.spk_number : ""}
                                label="SPK Number"
                                id="spk-number"
                                error={validation.spk_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ spk_number: event.target.value }) } 
                                helperText={validation.spk_number.message}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Area</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.area_code,
                                    onChange: val => this.setState({ area_code: val }),
                                    placeholder: 'Select Area',
                                    instanceId: 'select-area',
                                    id: 'select-area',
                                    name: 'select-area',
                                    simpleValue: true,
                                    options: dataArea.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.area_code.isInvalid && <FormHelperText error={true}>{validation.area_code.message}</FormHelperText> }
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormDriver()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormMasterDriver = withStyles(styles)(FormMasterDriver);

const mapStateToProps = ({ newDriver, areas }) => {
    return { newDriver, areas };
}

export default connect(mapStateToProps, {
    addDriver,
    closeFormDriver,
    updateDriver,
    getAreas
})(classFormMasterDriver);