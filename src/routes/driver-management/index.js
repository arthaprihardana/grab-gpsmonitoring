/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 09:09:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-27 06:45:36
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
// Material UI
import {withStyles} from '@material-ui/core';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// form
import FormMasterDriver from './form';
// Actions
import { getDrivers, openFormDriver } from '../../actions';
// Styles
import styles from '../../lib/styles';

/**
 * @description Index data Driver Management
 *
 * @class DriverManagement
 * @extends {Component}
 * 
 * FIXME: driver code di take out, area code ganti area name
 */
class DriverManagement extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25
    }

    componentDidMount() {
        this.props.getDrivers({ page: this.state.current_page });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.drivers !== prevProps.drivers) {
            this.setState({
                data: this.props.drivers.data,
                rowsPerPage: this.props.drivers.per_page,
                current_page: this.props.drivers.current_page,
                last_page: this.props.drivers.last_page,
                total: this.props.drivers.total
            });
        }
        if(this.props.newDriver !== prevProps.newDriver) {
            this.props.getDrivers({ page: this.state.current_page });
        }
    }

    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        
        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.driverManagement" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="table-responsive">
						<div className="d-flex justify-content-between py-20 px-10 border-bottom">
                            <div className="col-md-4">
                                <InputGroup>
                                    <Input
                                        value={this.state.search}
                                        type={"text"}
                                        name="search"
                                        id="search"
                                        className="input-lg"
                                        placeholder="Search"
                                        onChange={(event) => this.setState({ search: event.target.value })}
                                    />
                                    <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                        <span className="input-group-text">
                                            <i className="ti-search"></i>
                                        </span>
                                    </InputGroupAddon>
                                </InputGroup>
							</div>
							<div>
                                <Button outline color="success" onClick={() => this.props.openFormDriver(null) }>
                                    Add New Driver <i className="zmdi zmdi-plus"></i>
                                </Button>
							</div>
						</div>
                        <table className="table table-middle table-hover mb-0">
							<thead>
								<tr style={{ height: 60 }}>
									{/* <th>Driver Code</th> */}
									<th>Driver Name</th>
									<th>Agreement</th>
									<th>Area Name</th>
									{/* <th>Status</th> */}
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
                                {data && data.map((value, key) => (
                                    <tr key={key}>
                                        {/* <td>{value.driver_code}</td> */}
                                        <td>{value.name}</td>
                                        <td>{value.spk_number}</td>
                                        <td>{value.area_code}</td>
                                        {/* <td>{value.status}</td> */}
                                        <td className="list-action">
											<a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
											<a href="javascript:void(0)" onClick={() => this.props.openFormDriver(value)}><i className="ti-pencil"></i></a>
										</td>
                                    </tr>
                                ))}
                            </tbody>
                            <tfoot>
                                <tr>
									<td colSpan="100%">
                                        <Paginating
                                            total={total}
                                            limit={rowsPerPage}
                                            pageCount={5}
                                            currentPage={current_page}>
                                            {({
                                                pages,
                                                hasNextPage,
                                                hasPreviousPage
                                            }) => (
                                                <Pagination className="mb-0 py-10 px-10">
                                                    <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                        <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                            if(hasPreviousPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page - 1 }
                                                                }, () => this.props.getDrivers({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                    {pages.map(page => {
                                                        return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                            <PaginationLink href="javascript:void(0)" onClick={() => {
                                                                if(page !== current_page) {
                                                                    this.setState(prevState => {
                                                                        return { current_page: page }
                                                                    }, () => this.props.getDrivers({ page: this.state.current_page }));
                                                                }
                                                            }} >{page}</PaginationLink>
                                                        </PaginationItem>
                                                    })}
                                                    <PaginationItem disabled={hasNextPage ? false : true}>
                                                        <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                            if(hasNextPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page + 1 }
                                                                }, () => this.props.getDrivers({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                </Pagination>
                                            )}
                                        </Paginating>
									</td>
								</tr>
                            </tfoot>
                        </table>
                    </div>
                    {loading && !modal &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
                {/* Modal */}
                <FormMasterDriver 
                    title={'Add Driver'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classDriverManagement = withStyles(styles)(DriverManagement);

const mapStateToProps = ({ drivers }) => {
    return drivers;
}

export default connect(mapStateToProps, {
    getDrivers,
    openFormDriver
})(classDriverManagement)