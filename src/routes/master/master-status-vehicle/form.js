/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:56:35 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 07:12:55
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../lib/styles';
// Transistion
import Transition from '../../../components/Transition';
// Actions
import { addStatusVehicle, closeFormStatusVehicle, updateStatusVehicle } from '../../../actions';
// Validation
import FormValidator from '../../../helpers/FormValidation';

class FormMasterStatusVehicles extends Component {

    validator = new FormValidator([
        {
            field: "status_vehicle_name",
            method: 'isEmpty',
            validWhen: false,
            message: 'Status tidak boleh kosong'
        },
        {
            field: "color_hex",
            method: 'isEmpty',
            validWhen: false,
            message: 'Color Hex tidak boleh kosong'
        }
    ])

    submitted = false
    
    state = {
        status_vehicle_name: '',
        color_hex: '',
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    status_vehicle_name: '',
                    color_hex: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate())
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    status_vehicle_name: this.props.data.status_vehicle_name,
                    color_hex: this.props.data.color_hex
                })
            }
        }
    }
    
    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateStatusVehicle({
                id: this.props.data.status_vehicle_code,
                FormData: {
                    status_vehicle_name: this.state.status_vehicle_name,
                    color_hex: this.state.color_hex
                }
            });
        } else {
            if (validation.isValid) {
                this.props.addStatusVehicle({
                    FormData: {
                        status_vehicle_name: this.state.status_vehicle_name,
                        color_hex: this.state.color_hex
                    }
                });
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormStatusVehicle()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan status vehicle. silahkan daftarkan status vehicle baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.status_vehicle_name : ""}
                                label="Status Vehicle Name"
                                id="status-vehicle-name"
                                error={validation.status_vehicle_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ status_vehicle_name: event.target.value }) } 
                                helperText={validation.status_vehicle_name.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                type="color"
                                defaultValue={data !== null ? data.color_hex : ""}
                                label="Color Hex"
                                id="color-hex"
                                error={validation.color_hex.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ color_hex: event.target.value }) } 
                                helperText={validation.color_hex.message}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormStatusVehicle()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormMasterStatusVehicles = withStyles(styles)(FormMasterStatusVehicles);

const mapStateToProps = ({ newStatus }) => {
    return { newStatus };
}

export default connect(mapStateToProps, {
    addStatusVehicle,
    closeFormStatusVehicle,
    updateStatusVehicle
})(classFormMasterStatusVehicles)