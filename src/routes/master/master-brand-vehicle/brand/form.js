/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 20:04:21 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 20:13:05
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addBrandVehicles, closeFormBrandVehicles, updateBrandVehicles } from '../../../../actions';
// Validator
import FormValidator from '../../../../helpers/FormValidation';

class FormMasterBrand extends Component {

    validator = new FormValidator([
        {
            field: 'brand_vehicle_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama Brand tidak boleh kososng'
        }
    ])
    
    submitted = false

    state = {
        brand_vehicle_name: '',
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    brand_vehicle_name: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    brand_vehicle_name: this.props.data.brand_vehicle_name
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateBrandVehicles({
                id: this.props.data.brand_vehicle_code,
                FormData: {
                    brand_vehicle_name: this.state.brand_vehicle_name
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addBrandVehicles({
                    FormData: {
                        brand_vehicle_name: this.state.brand_vehicle_name
                    }
                })
            }
        }
        // if(this.state.brand_vehicle_name !== null) {
        //     if(this.props.data !== null) {
        //         this.props.updateBrandVehicles({ id: this.props.data.brand_vehicle_code, brand_vehicle_name: this.state.brand_vehicle_name })
        //         this.setState({ brand_vehicle_name: null });
        //     } else {
        //         this.props.addBrandVehicles(this.state);
        //         this.setState({ brand_vehicle_name: null });
        //     }
        // } else {
        //     this.setState({ errorMessage: "Nama brand tidak boleh kosong"});
        // }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormBrandVehicles()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText>
                        Untuk menambahkan brand vehicle. silahkan daftarkan brand vehicle baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.brand_vehicle_name : ""}
                                label="Brand Vehicle Name"
                                id="brand-name"
                                error={validation.brand_vehicle_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ brand_vehicle_name: event.target.value.toUpperCase() }) } 
                                helperText={validation.brand_vehicle_name.message}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormBrandVehicles()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }
    
}

const classFormMasterBrand = withStyles(styles)(FormMasterBrand);

const mapStateToProps = ({ newBrand }) => {
    return {newBrand};
}

export default connect(mapStateToProps, {
    addBrandVehicles,
    updateBrandVehicles,
    closeFormBrandVehicles
})(classFormMasterBrand);