/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:18:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 20:01:21
 */
/**
 * Master Brand Vehicles Routes
 */
import React, { Component } from 'react';

// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';

// intl messages
import IntlMessages from 'Util/IntlMessages';

import Brand from './brand';
import TypeOfBrand from './typeOfBrand';

/**
 * FIXME: model vehicle code di take out, brand vehicle code ganti brand vehicle name
 */
export default class MasterBrandVehicle extends Component {

    render() {
        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.masterBrandVehicle" />} match={this.props.match} />
                <Brand />
                <TypeOfBrand />
            </div>
        );
    }

}