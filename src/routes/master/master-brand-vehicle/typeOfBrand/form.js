/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 20:14:01 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 06:30:48
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, InputLabel, Input, FormHelperText, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../../lib/styles';
// Transition
import Transition from '../../../../components/Transition';
// Actions
import { addModelBrandVehicle, closeFormModelBrandVehicle, updateModelBrandVehicle, getBrandVehicles } from '../../../../actions'
// Validation
import FormValidator from '../../../../helpers/FormValidation';

class FormTypeOfBrand extends Component {

    validator = new FormValidator([
        {
            field: "model_vehicle_name",
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama model tidak boleh kosong'
        },
        {
            field: "brand_vehicle_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama brand tidak boleh kosong'
        },
        {
            field: "fuel_ratio",
            method: 'isEmpty',
            validWhen: false,
            message: 'Fuel ratio tidak boleh kosong'
        }
    ])

    submitted = false

    state = {
        model_vehicle_name: '',
        brand_vehicle_code: '',
        fuel_ratio: '',
        dataBrand: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getBrandVehicles({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.brand_vehicles.brand_vehicles !== prevProps.brand_vehicles.brand_vehicles) {
            if(prevProps.brand_vehicles.brand_vehicles !== null) {
                let master_brand = this.props.brand_vehicles.brand_vehicles.data;
                let dataSelect = this.state.dataBrand;
                master_brand.map((value, key) => {
                    dataSelect.push({value: value.brand_vehicle_code, label: value.brand_vehicle_name})
                });
            }
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    model_vehicle_name: '',
                    brand_vehicle_code: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate())
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    model_vehicle_name: this.props.data.model_vehicle_name,
                    brand_vehicle_code: this.props.data.brand_vehicle_code,
                    fuel_ratio: this.props.data.fuel_ratio
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateModelBrandVehicle({
                id: this.props.data.model_vehicle_code,
                FormData: {
                    "model_vehicle_name" : this.state.model_vehicle_name,
                    "brand_vehicle_code" : this.state.brand_vehicle_code,
                    "fuel_ratio" : this.state.fuel_ratio,
                }
            });
        } else {
            if (validation.isValid) {
                this.props.addModelBrandVehicle({
                    FormData: {
                        "model_vehicle_name" : this.state.model_vehicle_name,
                        "brand_vehicle_code" : this.state.brand_vehicle_code,
                        "fuel_ratio" : this.state.fuel_ratio,
                        "status" : "1"
                    }
                });
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataBrand } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormModelBrandVehicle()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan modal vehicle brand. silahkan daftarkan model baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div>
                            <InputLabel htmlFor="role" error={validation.brand_vehicle_code.isInvalid ? true : false}>Choose Role</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.brand_vehicle_code,
                                    onChange: val => this.setState({ brand_vehicle_code: val }),
                                    placeholder: 'Select Brand Vehicle Name',
                                    instanceId: 'select-brand',
                                    id: 'select-brand',
                                    name: 'select-brand',
                                    simpleValue: true,
                                    options: dataBrand.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.brand_vehicle_code.isInvalid ? <FormHelperText error={true}>{validation.brand_vehicle_code.message}</FormHelperText> : null }
                        </div>
                        <div>
                            {/* <TextField 
                                id="model-name" 
                                error={validation.model_vehicle_name.isInvalid ? true : false}
                                fullWidth 
                                label="Model Name" 
                                placeholder="Model Name" 
                                value={this.state.model_vehicle_name}
                                onChange={(event) => this.setState({ model_vehicle_name: event.target.value }) } 
                                helperText={validation.model_vehicle_name.message}
                                margin="none" /> */}
                            <TextField
                                defaultValue={data !== null ? data.model_vehicle_name : ""}
                                label="Model Vehicle Name"
                                id="model-name"
                                error={validation.model_vehicle_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ model_vehicle_name: event.target.value.toUpperCase() }) } 
                                helperText={validation.model_vehicle_name.message}
                            />
                        </div>
                        <div>
                            <TextField
                                defaultValue={data !== null ? data.fuel_ratio : ""}
                                label="Fuel Ratio"
                                id="fuel-ratio"
                                error={validation.fuel_ratio.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ fuel_ratio: event.target.value.toUpperCase() }) } 
                                helperText={validation.fuel_ratio.message}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormModelBrandVehicle()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormTypeOfBrand = withStyles(styles)(FormTypeOfBrand);

const mapStateToProps = ({ newModel, brand_vehicles }) => {
    return {newModel, brand_vehicles};
}

export default connect(mapStateToProps, {
    addModelBrandVehicle,
    closeFormModelBrandVehicle,
    updateModelBrandVehicle,
    getBrandVehicles
})(classFormTypeOfBrand);