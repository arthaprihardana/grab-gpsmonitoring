/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 19:59:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 05:57:12
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
// Material UI
import {withStyles} from '@material-ui/core';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, InputGroup, InputGroupAddon, Input, Alert} from 'reactstrap';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// form
import FormTypeOfBrand from './form';
// Actions
import { getModelBrandVehicle, openFormModelBrandVehicle } from '../../../../actions';
// Styles
import styles from '../../../../lib/styles';

/**
 * @description Index data Model Brand Vehicle
 *
 * @class TypeOfBrand
 * @extends {Component}
 */
class TypeOfBrand extends Component {
    
    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25
    }

    componentDidMount() {
        this.props.getModelBrandVehicle({ page: this.state.current_page });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.model_brand_vehicles !== prevProps.model_brand_vehicles) {
            this.setState({
                data: this.props.model_brand_vehicles.data,
                rowsPerPage: this.props.model_brand_vehicles.per_page,
                current_page: this.props.model_brand_vehicles.current_page,
                last_page: this.props.model_brand_vehicles.last_page,
                total: this.props.model_brand_vehicles.total
            });
        }
        if(this.props.newModel !== prevProps.newModel) {
            this.props.getModelBrandVehicle({ page: this.state.current_page });
        }
    }

    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        
        return (
            <div>
                <RctCollapsibleCard fullBlock>
                    <div className="table-responsive">
						<div className="d-flex justify-content-between py-20 px-10 border-bottom">
                            <div className="col-md-4">
                                <InputGroup>
                                    <Input
                                        value={this.state.search}
                                        type={"text"}
                                        name="search"
                                        id="search"
                                        className="input-lg"
                                        placeholder="Search"
                                        onChange={(event) => this.setState({ search: event.target.value })}
                                    />
                                    <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                        <span className="input-group-text">
                                            <i className="ti-search"></i>
                                        </span>
                                    </InputGroupAddon>
                                </InputGroup>
							</div>
							<div>
                                <Button outline color="success" onClick={() => this.props.openFormModelBrandVehicle(null) }>
                                    Add New Model <i className="zmdi zmdi-plus"></i>
                                </Button>
							</div>
						</div>
                        <table className="table table-middle table-hover mb-0">
							<thead>
								<tr style={{ height: 60 }}>
									<th>Model Vehicle Code</th>
									<th>Vehicle Type</th>
									<th>Brand Vehicle Name</th>
                                    <th>Fuel Ratio</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
                                {data && data.map((value, key) => (
                                    <tr key={key}>
                                        <td>{value.model_vehicle_code}</td>
                                        <td>{value.model_vehicle_name}</td>
                                        <td>{value.brand_vehicle_code}</td>
                                        <td>{value.fuel_ratio}</td>
                                        <td className="list-action">
											<a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
											<a href="javascript:void(0)" onClick={() => this.props.openFormModelBrandVehicle(value)}><i className="ti-pencil"></i></a>
										</td>
                                    </tr>
                                ))}
                            </tbody>
                            <tfoot className="border-top">
								<tr>
									<td colSpan="100%">
                                        <Paginating
                                            total={total}
                                            limit={rowsPerPage}
                                            pageCount={5}
                                            currentPage={current_page}>
                                            {({
                                                pages,
                                                hasNextPage,
                                                hasPreviousPage
                                            }) => (
                                                <Pagination className="mb-0 py-10 px-10">
                                                    <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                        <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                            if(hasPreviousPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page - 1 }
                                                                }, () => this.props.getModelBrandVehicle({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                    {pages.map(page => {
                                                        return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                            <PaginationLink href="javascript:void(0)" onClick={() => {
                                                                if(page !== current_page) {
                                                                    this.setState(prevState => {
                                                                        return { current_page: page }
                                                                    }, () => this.props.getModelBrandVehicle({ page: this.state.current_page }));
                                                                }
                                                            }} >{page}</PaginationLink>
                                                        </PaginationItem>
                                                    })}
                                                    <PaginationItem disabled={hasNextPage ? false : true}>
                                                        <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                            if(hasNextPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page + 1 }
                                                                }, () => this.props.getModelBrandVehicle({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                </Pagination>
                                            )}
                                        </Paginating>
									</td>
								</tr>
							</tfoot>
                        </table>
                    </div>
                    {loading && !modal &&
						<RctSectionLoader />
					}
                </RctCollapsibleCard>
                {/* Modal */}
                <FormTypeOfBrand 
                    title={'Add Master Type Of Brand'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }

}

const classTypeOfBrand = withStyles(styles)(TypeOfBrand);

const mapStateToProps = ({ model_brand_vehicles }) => {
    return model_brand_vehicles;
}

export default connect(mapStateToProps, {
    getModelBrandVehicle,
    openFormModelBrandVehicle
})(classTypeOfBrand);