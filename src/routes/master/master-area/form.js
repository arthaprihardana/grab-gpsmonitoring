/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 19:20:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-07 09:09:43
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons'
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import {addAreas, closeFormArea, updateAreas} from '../../../actions';
// Validator
import FormValidator from '../../../helpers/FormValidation';

/**
 * @description Form Input dan Update Master Area
 *
 * @class FormMasterArea
 * @extends {Component}
 */
class FormMasterArea extends Component {
    
    validator = new FormValidator([
        {
            field: 'area_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama area tidak boleh kososng'
        }
    ])

    submitted = false;

    state = {
        area_name: '',
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    area_name: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    area_name: this.props.data.area_name
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateAreas({
                id: this.props.data.area_code,
                FormData: {
                    area_name: this.state.area_name
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addAreas({
                    FormData: {
                        area_name: this.state.area_name
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        // const { area_name, errorMessage } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormArea()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText>
                        Untuk menambahkan area. silahkan daftarkan area baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.area_name : ""}
                                label="Area Name"
                                id="area-name"
                                error={validation.area_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ area_name: event.target.value.toUpperCase() }) } 
                                helperText={validation.area_name.message}
                                margin="none"
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormArea() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormMasterArea = withStyles(styles)(FormMasterArea);

const mapStateToProps = ({ newArea }) => {
    return {newArea};
}

export default connect(mapStateToProps, {
    addAreas,
    updateAreas,
    closeFormArea,
})(classFormMasterArea)