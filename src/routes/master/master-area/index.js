/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:10:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-29 04:55:21
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, InputGroup, InputGroupAddon, Input} from 'reactstrap';
// Component
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// Form
import FormMasterArea from './form';
// Actions
import { getAreas, addAreas, openFormArea } from '../../../actions';
// Styles
import styles from '../../../lib/styles';

/**
 * @description Routes Class Master Area
 *
 * @class MasterArea
 * @extends {Component}
 */
class MasterArea extends Component {

    /**
     * @type state
     * @memberof MasterArea
     */
    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25
    }
    
    /**
     * @description get Data Area pada saat Component di Load
     * @method componentDidMount
     * @memberof MasterArea
     */
    componentDidMount() {
        this.props.getAreas({ page: this.state.current_page });
    }

    /**
     * @description update state data Area jika terjadi perubahan data pada Component
     * @method componentDidUpdate
     *
     * @param {*} prevProps
     * @param {*} prevState
     * @memberof MasterArea
     */
    componentDidUpdate(prevProps, prevState) {
        if(this.props.areas !== prevProps.areas) {
            this.setState({
                data: this.props.areas.data,
                rowsPerPage: this.props.areas.per_page,
                current_page: this.props.areas.current_page,
                last_page: this.props.areas.last_page,
                total: this.props.areas.total
            });
        }
        if(this.props.newArea !== prevProps.newArea) {
            this.props.getAreas({ page: this.state.current_page });
        }
    }

    /**
     * @method render
     *
     * @returns
     * @memberof MasterArea
     */
    render() {    
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        
        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.masterArea" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="table-responsive">
						<div className="d-flex justify-content-between py-20 px-10 border-bottom">
                            <div className="col-md-4">
                                <InputGroup>
                                    <Input
                                        value={this.state.search}
                                        type={"text"}
                                        name="search"
                                        id="search"
                                        className="input-lg"
                                        placeholder="Search"
                                        onChange={(event) => this.setState({ search: event.target.value })}
                                    />
                                    <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                        <span className="input-group-text">
                                            <i className="ti-search"></i>
                                        </span>
                                    </InputGroupAddon>
                                </InputGroup>
							</div>
							<div>
                                <Button outline color="success" onClick={() => this.props.openFormArea(null) }>
                                    Add New Area <i className="zmdi zmdi-plus"></i>
                                </Button>
							</div>
						</div>
                        <table className="table table-middle table-hover mb-0">
							<thead>
								<tr style={{ height: 60 }}>
									<th>Area Code</th>
									<th>Area Name</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
                                {data && data.map((value, key) => (
                                    <tr key={key}>
                                        <td>{value.area_code}</td>
                                        <td>{value.area_name}</td>
                                        <td className="list-action">
											<a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
											<a href="javascript:void(0)" onClick={() => this.props.openFormArea(value) }><i className="ti-pencil"></i></a>
										</td>
                                    </tr>
                                ))}
                            </tbody>
                            <tfoot className="border-top">
								<tr>
									<td colSpan="100%">
                                        <Paginating
                                            total={total}
                                            limit={rowsPerPage}
                                            pageCount={5}
                                            currentPage={current_page}>
                                            {({
                                                pages,
                                                hasNextPage,
                                                hasPreviousPage
                                            }) => (
                                                <Pagination className="mb-0 py-10 px-10">
                                                    <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                        <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                            if(hasPreviousPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page - 1 }
                                                                }, () => this.props.getAreas({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                    {pages.map(page => {
                                                        return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                            <PaginationLink href="javascript:void(0)" onClick={() => {
                                                                if(page !== current_page) {
                                                                    this.setState(prevState => {
                                                                        return { current_page: page }
                                                                    }, () => this.props.getAreas({ page: this.state.current_page }));
                                                                }
                                                            }} >{page}</PaginationLink>
                                                        </PaginationItem>
                                                    })}
                                                    <PaginationItem disabled={hasNextPage ? false : true}>
                                                        <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                            if(hasNextPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page + 1 }
                                                                }, () => this.props.getAreas({ page: this.state.current_page }))
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                </Pagination>
                                            )}
                                        </Paginating>
									</td>
								</tr>
							</tfoot>
                        </table>
                    </div>
                    {loading && !modal &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
                {/* Modal */}
                <FormMasterArea 
                    title={'Add Master Area'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

/**
 * @const classMasterArea
 * include component Master Area with global styles
 */
const classMasterArea = withStyles(styles)(MasterArea);

/**
 * @const mapStateToProps
 */
const mapStateToProps = ({ areas }) => {
    return areas;
};

/**
 * @export MasterArea
 * @property {function} getAreas        - Get data area from server
 * @property {function} addAreas        - Send form data to Server
 * @property {boolean}  openFormArea    - Change true/false form dialog to open/close
 */
export default connect(mapStateToProps, {
    getAreas,
    addAreas,
    openFormArea
})(classMasterArea)