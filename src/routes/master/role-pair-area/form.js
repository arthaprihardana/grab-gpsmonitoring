/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 08:08:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-05 07:35:05
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, InputLabel, Input, FormHelperText, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import { addRolePairArea, closeFormRolePairArea, updateRolePairArea, getRoles, getAreas } from '../../../actions';
// Validation
import FormValidator from '../../../helpers/FormValidation';

class FormRolePairArea extends Component {

    validator = new FormValidator([
        {
            field: "role_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Role Code tidak boleh kosong'
        },
        {
            field: "area_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Area Code tidak boleh kosong'
        }
    ])

    submitted = false

    state = {
        role_code: '',
        area_code: '',
        dataRole: [],
        dataArea: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getAreas({ page: 1 });
        this.props.getRoles({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.areas.areas !== prevProps.areas.areas) {
            let master_areas = this.props.areas.areas.data;
            let dataSelect = this.state.dataArea;
            master_areas.map((value, key) => {
                dataSelect.push({ value: value.area_code, label: value.area_name });
            })
        }
        if(this.props.roles.roles !== prevProps.roles.roles) {
            let master_roles = this.props.roles.roles.data;
            let dataSelect = this.state.dataRole;
            master_roles.map((value, key) => {
                dataSelect.push({ value: value.role_code, label: value.role_name })
            });
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    role_code: this.props.data.role_code,
                    area_code: this.props.data.area_code
                })
            }
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    role_code: '',
                    area_code: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateRolePairArea({
                id: this.props.data.role_area_code,
                FormData: {
                    role_code: this.state.role_code,
                    area_code: this.state.area_code
                }
            });
        } else {
            if(validation.isValid) {
                this.props.addRolePairArea({
                    FormData: {
                        role_code: this.state.role_code,
                        area_code: this.state.area_code
                    }
                });
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes, theme } = this.props;
        const { dataArea, dataRole } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormRolePairArea()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan Role Pair Area. silahkan daftarkan di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <InputLabel htmlFor="area">Choose Area</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.area_code,
                                    onChange: val => this.setState({ area_code: val }),
                                    placeholder: 'Select Area',
                                    instanceId: 'select-area',
                                    id: 'select-area',
                                    name: 'select-area',
                                    simpleValue: true,
                                    options: dataArea.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.area_code.isInvalid && <FormHelperText error={true}>{validation.area_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Role</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.role_code,
                                    onChange: val => this.setState({ role_code: val }),
                                    placeholder: 'Select Role',
                                    instanceId: 'select-role',
                                    id: 'select-role',
                                    name: 'select-role',
                                    simpleValue: true,
                                    options: dataRole.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.role_code.isInvalid && <FormHelperText error={true}>{validation.role_code.message}</FormHelperText> }
                        </div>
                        {/* <div className="form-group">
                            <InputLabel htmlFor="area">Choose Area</InputLabel>
                            <Select
                                fullWidth
                                value={this.state.area_code}
                                onChange={event  => this.setState({area_code: event.target.value})}
                                disableUnderline={true}
                                inputProps={{
                                    classes: {
                                        select: classes.bootstrapInput,
                                    },
                                }}
                                >
                                <MenuItem value=""><em>None</em></MenuItem>
                                {dataArea && dataArea.map((value, key) => (
                                    <MenuItem key={key} value={value.value}>{value.label}</MenuItem>
                                ) )}
                            </Select>
                            <FormHelperText error={validation.area_code.isInvalid ? true : false}>{validation.area_code.message}</FormHelperText>
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Role</InputLabel>
                            <Select
                                fullWidth
                                value={this.state.role_code}
                                onChange={event  => this.setState({role_code: event.target.value})}
                                disableUnderline={true}
                                inputProps={{
                                    classes: {
                                        select: classes.bootstrapInput,
                                    },
                                }}
                                >
                                <MenuItem value=""><em>None</em></MenuItem>
                                {dataRole && dataRole.map((value, key) => (
                                    <MenuItem key={key} value={value.value}>{value.label}</MenuItem>
                                ) )}
                            </Select>
                            <FormHelperText error={validation.role_code.isInvalid ? true : false}>{validation.role_code.message}</FormHelperText>
                        </div> */}
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormRolePairArea()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormRolePairArea = withStyles(styles, { withTheme: true })(FormRolePairArea);

const mapStateToProps = ({ newRolePairArea, areas, roles }) => {
    return { newRolePairArea, areas, roles }
}

export default connect(mapStateToProps, {
    addRolePairArea,
    closeFormRolePairArea,
    updateRolePairArea,
    getAreas,
    getRoles
})(classFormRolePairArea);