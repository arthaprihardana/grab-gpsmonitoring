/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:06:19 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 07:53:38
 */
/**
 * Master Routes
 */
import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import {
    AsyncMasterArea,
    AsyncMasterBrandVehicle,
    AsyncMasterRoles,
    AsyncMasterNotification,
    AsyncMasterStatusVehicles,
    AsyncRolePairArea,
    AsyncZoneManagement
} from 'Components/AsyncComponent/AsyncComponent';

const Master = ({ match }) => (
    <div className="content-wrapper">
        <Switch>
            <Redirect exact from={`${match.url}/`} to={`${match.url}/master-roles`} />
            <Route path={`${match.url}/master-roles`} component={AsyncMasterRoles} />
            <Route path={`${match.url}/master-area`} component={AsyncMasterArea} />
            <Route path={`${match.url}/master-role-pair-area`} component={AsyncRolePairArea} />
            <Route path={`${match.url}/master-brand-vehicle`} component={AsyncMasterBrandVehicle} />
            <Route path={`${match.url}/master-status-vehicle`} component={AsyncMasterStatusVehicles} />
            <Route path={`${match.url}/master-notification`} component={AsyncMasterNotification} />
            <Route path={`${match.url}/zone-management`} component={AsyncZoneManagement} />
        </Switch>
    </div>
)

export default Master;