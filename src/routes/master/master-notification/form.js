/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 20:29:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 08:32:05
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import { addNotification, closeFormNotification, updateNotification } from '../../../actions';
// Validator
import FormValidator from '../../../helpers/FormValidation';

class FormMasterNotification extends Component {

    validator = new FormValidator([
        {
            field: 'notification_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama notifikasi tidak boleh kososng'
        }
    ]);

    submitted = false

    state = {
        notification_name: '',
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    notification_name: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate())
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    notification_name: this.props.data.notification_name,
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateNotification({
                id: this.props.data.notification_code,
                FormData: {
                    notification_name: this.state.notification_name
                }
            });
        } else {
            if (validation.isValid) {
                this.props.addNotification({
                    FormData: {
                        notification_name: this.state.notification_name
                    }
                });
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormNotification()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan notifikasi. silahkan daftarkan notifikasi baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.notification_name : ""}
                                label="Notification Name"
                                id="notification-name"
                                error={validation.notification_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ notification_name: event.target.value }) } 
                                helperText={validation.notification_name.message}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormNotification()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }
}

const classFormMasterNotification = withStyles(styles)(FormMasterNotification);

const mapStateToProps = ({ newNotification }) => {
    return { newNotification };
}

export default connect(mapStateToProps, {
    addNotification,
    closeFormNotification,
    updateNotification
})(classFormMasterNotification);