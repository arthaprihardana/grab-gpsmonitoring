/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-16 11:43:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-16 12:26:12
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton, InputLabel, Input } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
// Google Map
import GoogleMap from 'google-map-react';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import {closeDrawZone, addDrawZone, updateDrawZone, getDraw} from '../../../actions/MsZoneActions';
import { GOOGLE_MAP_API } from '../../../constants/LibraryAPI';

class DrawZone extends Component {

    state = {
        dataZone: [],
        all_overlays: [],
        zone_code: '',
        status: '',
        latitude: '',
        longitude: ''
    }

    componentDidMount() {
        // this.props.getZone({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.ListZone.length !== prevProps.ListZone.length) {
            let master_zone = this.props.ListZone;
            let dataSelect = this.state.dataZone;
            master_zone.map((value, key) => {
                dataSelect.push({ value: value.zone_code, label: value.zone_name });
            });
        }
    }

    CenterControl(controlDiv, map) {
        let self = this;
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.style.backgroundColor = '#fff';
        controlUI.style.border = '2px solid #fff';
        controlUI.style.borderRadius = '3px';
        controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
        controlUI.style.cursor = 'pointer';
        controlUI.style.marginTop = '5px';
        controlUI.style.marginLeft = '5px';
        controlUI.style.textAlign = 'center';
        controlUI.title = 'Click to recenter the map';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.style.color = 'rgb(25,25,25)';
        controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
        controlText.style.fontSize = '12px';
        controlText.style.lineHeight = '30px';
        controlText.style.paddingLeft = '10px';
        controlText.style.paddingRight = '10px';
        controlText.innerHTML = 'Remove Draw';
        controlUI.appendChild(controlText);

        controlUI.addEventListener('click', () => {
            self.deleteOverlays();
            // this.deleteOverlays();
        });
    }

    deleteOverlays() {
        let overlays = this.state.all_overlays;
        for (var i=0; i < overlays.length; i++) {
            overlays[i].overlay.setMap(null);
        }
        this.setState({ all_overlays: [] })
    }

    renderDrawZone(map, maps) {
        let self = this;
        var drawingManager = new maps.drawing.DrawingManager({
            drawingMode: maps.drawing.OverlayType.POLYGON,
            drawingControl: true,
            drawingControlOptions: {
                position: maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon', 'polyline', 'rectangle']
            },
            // markerOptions: {icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png'},
            polygonOptions: {
                fillColor: '#009f2e',
                strokeColor: '#009f2e',
                fillOpacity: 0.5,
                strokeWeight: 5,
                clickable: false
            },
            polylineOptions: {
                fillColor: '#009f2e',
                strokeColor: '#009f2e',
                fillOpacity: 0.5,
                strokeWeight: 5,
                clickable: false
            },
            rectangleOptions: {
                fillColor: '#009f2e',
                strokeColor: '#009f2e',
                fillOpacity: 0.5,
                strokeWeight: 5,
                clickable: false
            }
        });
        drawingManager.setMap(map);
        maps.event.addListener(drawingManager, 'overlaycomplete', (event) => {
            let overlays = self.state.all_overlays;
            if(overlays.length > 0) {
                overlays[0].overlay.setMap(null);
                overlays = _.drop(self.state.all_overlays);
                self.setState({ all_overlays: overlays })
            }
            overlays.push(event);
            self.setState({ all_overlays: overlays })
        })
    }
    
    onSubmit = () => {
        const overlays = this.state.all_overlays[0];
        const maps = this._map.maps_;
        var coords = null
        switch (overlays.type) {
            case 'rectangle':
                var bounds = overlays.overlay.getBounds();
                var NE = bounds.getNorthEast();
                var SW = bounds.getSouthWest();
                var NW = new maps.LatLng(NE.lat(), SW.lng());
                var SE = new maps.LatLng(SW.lat(), NE.lng());
                let FormData = [{
                    "zone_code" : this.state.zone_code,
                    "latitude" : NE.lat(),
                    "longitude" : NE.lng(),
                    "status" : "1"
                },{
                    "zone_code" : this.state.zone_code,
                    "latitude" : SW.lat(),
                    "longitude" : SW.lng(),
                    "status" : "1"
                },{
                    "zone_code" : this.state.zone_code,
                    "latitude" : NW.lat(),
                    "longitude" : NW.lng(),
                    "status" : "1"
                },{
                    "zone_code" : this.state.zone_code,
                    "latitude" : SE.lat(),
                    "longitude" : SE.lng(),
                    "status" : "1"
                }];
                this.props.addDrawZone({ FormData: FormData });
                break;
            default:
                coords = overlays.overlay.getPath();
                var arr = [];
                for (var i =0; i < coords.getLength(); i++) {
                    var xy = coords.getAt(i);
                    arr.push({
                        "zone_code" : this.state.zone_code,
                        "latitude" : xy.lat(),
                        "longitude" : xy.lng(),
                        "status" : "1"
                    });
                }
                this.props.addDrawZone({ FormData: arr })
                break;
        }
    }

    render() {
        const { isOpen, title, isLoading, classes } = this.props;
        const { dataZone } = this.state;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}
                fullWidth={true}
                maxWidth={'md'}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeDrawZone()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20}}>
                        Untuk menambahkan Zone. silahkan gambar area zona baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <InputLabel htmlFor="area">Choose Zone</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.zone_code,
                                    onChange: val => this.setState({ zone_code: val }),
                                    placeholder: 'Select Zone',
                                    instanceId: 'select-zone',
                                    id: 'select-zone',
                                    name: 'select-zone',
                                    simpleValue: true,
                                    options: dataZone.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="area">Draw Zone On Map</InputLabel>
                            <GoogleMap
                                ref={ref => this._map = ref}
                                bootstrapURLKeys={{ 
                                    key: GOOGLE_MAP_API,
                                    region: 'id',
                                    libraries: ['drawing']
                                }}
                                yesIWantToUseGoogleMapApiInternals={true}
                                center={[-1.212843, 119.046166]}
                                zoom={5} style={{ position: 'relative', width: '100%', height: '73vh' }}
                                heatmapLibrary={true}
                                onGoogleApiLoaded={({ map, maps }) => {
                                    this.renderDrawZone(map, maps)
                                    var centerControlDiv = document.createElement('div');
                                    this.CenterControl(centerControlDiv, map);
                                    centerControlDiv.index = 1;
                                    map.controls[maps.ControlPosition.TOP_LEFT].push(centerControlDiv);
                                }}
                                options={maps => {
								return {
									styles: [{
										featureType: 'all',
										elementType: 'labels',
										stylers: [{
											visibility: 'on'
										}]
									}]
								}
							}}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeDrawZone() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">Save</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classDrawZone = withStyles(styles)(DrawZone);

const mapStateToProps = ({ zone }) => {
    const { newDrawZone, loading } = zone;
    const ListZone = zone.zone !== null ? zone.zone.data : [];
    return { ListZone: ListZone, newDrawZone, loading };
}

export default connect(mapStateToProps, {
    getDraw,
    closeDrawZone,
    addDrawZone
})(classDrawZone)