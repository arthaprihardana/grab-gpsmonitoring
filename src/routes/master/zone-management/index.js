/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 21:50:19 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-16 11:49:10
 */
/** 
 * Monitoring Zone Management Routes
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
// Material UI
import {withStyles} from '@material-ui/core';
// React Paginating
import Paginating from 'react-paginating';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, InputGroup, Input, InputGroupAddon} from 'reactstrap';
// Google Map
import GoogleMap from 'google-map-react';
// Component
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// Form
import FormMasterZone from './form';
// Draw
import DrawMasterZone from './draw';
// Actions
import { getZone, addZone, openFormZone, openDrawZone } from '../../../actions';
// Styles
import styles from '../../../lib/styles';
import { GOOGLE_MAP_API } from '../../../constants/LibraryAPI';

class ZoneManagement extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25
    }

    componentDidMount() {
        this.props.getZone({ page: this.state.current_page });
    }

    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.zone !== prevProps.zone) {
            return { zone: this.props.zone }
        }
        if(this.props.newZone !== prevProps.newZone) {
            return { newZone: this.props.newZone }
        }
        if(this.props.newDrawZone !== prevProps.newDrawZone) { 
            return { newDrawZone: this.props.newDrawZone }
        }
        return null;
    }
    
    
    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.zone !== undefined) {
                this.setState(prevState => {
                    return {
                        data: snapshot.zone.data,
                        rowsPerPage: snapshot.zone.per_page,
                        current_page: snapshot.zone.current_page,
                        last_page: snapshot.zone.last_page,
                        total: snapshot.zone.total
                    }
                })
            }
            if(snapshot.newZone !== undefined) {
                this.props.getZone({ page: this.state.current_page });
            }
            if(snapshot.newDrawZone !== undefined) {
                let maps = this._map.maps_;
                let map = this._map.map_;
                this.props.getZone({ page: this.state.current_page });
                this.renderZonePolygon(map, maps)
            }
        }
    }

    renderZonePolygon(map, maps) {
		const { data } = this.state;
		if(data.length > 0) {
			var infoWindow = new maps.InfoWindow();
			var detailZone = new Array();
			data.map((value, key) => {
				if(value.zone_detail.length > 0) {
					const latlng = value.zone_detail.map((val, idx) => {
						return new maps.LatLng(val.latitude, val.longitude)
					});
					detailZone[key] = new maps.Polygon({
						path: latlng,
						strokeColor: value.type_zone === "OUT" ? "#FF0000" : "#2196F3",
						strokeOpacity: 0.8,
						strokeWeight: 2,
						fillColor: value.type_zone === "OUT" ? "#FF0000" : "#2196F3",
						fillOpacity: 0.35
					});
					detailZone[key].setMap(map);
					maps.event.addListener(detailZone[key], 'click', event => {
						var contentString = "<b>"+value.zone_name+"</b><br />Type : "+value.type_zone;
						infoWindow.setContent(contentString);
						infoWindow.setPosition(event.latLng);
						infoWindow.open(map); 
					})
				}
			});
		}
	}

    render() {
        const { loading, className, modal, modalDraw, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        return (
            <div className="general-widgets-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.zoneManagement" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                        <div className="col-md-4" />
                        <div className="mr-10">
                            <Button outline color="success" onClick={() => this.props.openDrawZone() }>
                                Draw New Zone <i className="zmdi zmdi-border-color"></i>
                            </Button>
                        </div>
                    </div>
                    <div className="mb-10">
                        <GoogleMap
                            ref={ref => this._map = ref}
                            bootstrapURLKeys={{ 
                                key: GOOGLE_MAP_API,
                                region: 'id',
                                libraries: ['drawing']
                            }}
                            yesIWantToUseGoogleMapApiInternals={true}
                            center={[-1.212843, 119.046166]}
                            zoom={5} style={{ position: 'relative', width: '100%', height: '73vh' }}
                            onGoogleApiLoaded={({ map, maps }) => this.renderZonePolygon(map, maps)}
                            options={maps => {
								return {
									styles: [{
										featureType: 'all',
										elementType: 'labels',
										stylers: [{
											visibility: 'on'
										}]
									}]
								}
							}}
                        />
                    </div>
                </RctCollapsibleCard>
                <RctCollapsibleCard fullBlock>
                    <div className="table-responsive">
						<div className="d-flex justify-content-between py-20 px-10 border-bottom">
                            <div className="col-md-4">
                                <InputGroup>
                                    <Input
                                        value={this.state.search}
                                        type={"text"}
                                        name="search"
                                        id="search"
                                        className="input-lg"
                                        placeholder="Search"
                                        onChange={(event) => this.setState({ search: event.target.value })}
                                    />
                                    <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                        <span className="input-group-text">
                                            <i className="ti-search"></i>
                                        </span>
                                    </InputGroupAddon>
                                </InputGroup>
							</div>
							<div>
                                <Button outline color="success" onClick={() => this.props.openFormZone(null) }>
                                    Add New Zone <i className="zmdi zmdi-plus"></i>
                                </Button>
							</div>
						</div>
                        <table className="table table-middle table-hover mb-0">
							<thead>
								<tr style={{ height: 60 }}>
									<th>Type Zone</th>
									<th>Zone Name</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
                                {data && data.length > 0 ?
                                    data && data.map((value, key) => (
                                        <tr key={key}>
                                            <td>{value.type_zone}</td>
                                            <td>{value.zone_name}</td>
                                            <td className="list-action">
                                                <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                                <a href="javascript:void(0)" onClick={() => this.props.openFormZone(value)}><i className="ti-pencil"></i></a>
                                            </td>
                                        </tr>
                                    )) : 
                                    <tr>
                                        <td colSpan={3} style={{ textAlign: 'center' }}>Data Not Found!</td>
                                    </tr>
                                }
                            </tbody>
                            <tfoot className="border-top">
								<tr>
									<td colSpan="100%">
                                        <Paginating
                                            total={total}
                                            limit={rowsPerPage}
                                            pageCount={5}
                                            currentPage={current_page}>
                                            {({
                                                pages,
                                                hasNextPage,
                                                hasPreviousPage
                                            }) => (
                                                <Pagination className="mb-0 py-10 px-10">
                                                    <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                        <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                            if(hasPreviousPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page - 1 }
                                                                }, () => this.props.getZone({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                    {pages.map(page => {
                                                        return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                            <PaginationLink href="javascript:void(0)" onClick={() => {
                                                                if(page !== current_page) {
                                                                    this.setState(prevState => {
                                                                        return { current_page: page }
                                                                    }, () => this.props.getZone({ page: this.state.current_page }));
                                                                }
                                                            }} >{page}</PaginationLink>
                                                        </PaginationItem>
                                                    })}
                                                    <PaginationItem disabled={hasNextPage ? false : true}>
                                                        <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                            if(hasNextPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page + 1 }
                                                                }, () => this.props.getZone({ page: this.state.current_page }))
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                </Pagination>
                                            )}
                                        </Paginating>
									</td>
								</tr>
							</tfoot>
                        </table>
                    </div>
                    {loading && !modal &&
						<RctSectionLoader />
					}
                </RctCollapsibleCard>
                <FormMasterZone 
                    title={'Add Master Zone'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
                <DrawMasterZone 
                    title={'Draw Zone'}
                    isOpen={modalDraw}
                    className={className}
                    isLoading={loading} />
            </div>
        );
    }
}

const classZoneManagement = withStyles(styles)(ZoneManagement);

const mapStateToProps = ({ zone }) => {
    return zone;
}

export default connect(mapStateToProps, {
    getZone,
    addZone,
    openFormZone,
    openDrawZone
})(classZoneManagement);