/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-07 08:50:45 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-07 09:10:21
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton, InputLabel, Input, FormHelperText } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import {addZone, closeFormZone, updateZone, getAreas} from '../../../actions';
// Validator
import FormValidator from '../../../helpers/FormValidation';

class FormZone extends Component {
    validator = new FormValidator([
        {
            field: 'type_zone',
            method: 'isEmpty',
            validWhen: false,
            message: 'Tipe Zone tidak boleh kososng'
        },
        {
            field: "area_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Area Code tidak boleh kosong'
        },
        {
            field: 'zone_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Zone Name tidak boleh kososng'
        }
    ])

    submitted = false;

    state = {
        dataArea: [],
        type_zone: '',
        zone_name: '',
        area_code: '',
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getAreas({ page: 1 });
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.areas.areas !== prevProps.areas.areas) {
            let master_areas = this.props.areas.areas.data;
            let dataSelect = this.state.dataArea;
            master_areas.map((value, key) => {
                dataSelect.push({ value: value.area_code, label: value.area_name });
            })
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    type_zone: '',
                    zone_name: '',
                    area_code: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    type_zone: this.props.data.type_zone,
                    zone_name: this.props.data.zone_name,
                    area_code: this.props.data.area_code,
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateZone({
                id: this.props.data.area_code,
                FormData: {
                    type_zone: this.state.type_zone,
                    zone_name: this.state.zone_name,
                    area_code: this.state.area_code,
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addZone({
                    FormData: {
                        type_zone: this.state.type_zone,
                        zone_name: this.state.zone_name,
                        area_code: this.state.area_code,
                        status: "1"
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataArea } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormZone()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20}}>
                        Untuk menambahkan Zone. silahkan daftarkan area baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <InputLabel htmlFor="type-zone">Type Zone</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.type_zone,
                                    onChange: val => this.setState({ type_zone: val }),
                                    placeholder: 'Type Zone',
                                    instanceId: 'type-zone',
                                    id: 'type-zone',
                                    name: 'type-zone',
                                    simpleValue: true,
                                    options: ["POOL", "OUT"].map(value => ({
                                        label: value,
                                        value: value
                                    })),
                                }}
                            />
                            {validation.type_zone.isInvalid && <FormHelperText error={true}>{validation.type_zone.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="area">Choose Area</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.area_code,
                                    onChange: val => this.setState({ area_code: val }),
                                    placeholder: 'Select Area',
                                    instanceId: 'select-area',
                                    id: 'select-area',
                                    name: 'select-area',
                                    simpleValue: true,
                                    options: dataArea.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.area_code.isInvalid && <FormHelperText error={true}>{validation.area_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.zone_name : ""}
                                label="Zone Name"
                                id="zone-name"
                                error={validation.zone_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ zone_name: event.target.value.toUpperCase() }) } 
                                helperText={validation.zone_name.message}
                                margin="none"
                            />
                            {validation.zone_name.isInvalid && <FormHelperText error={true}>{validation.zone_name.message}</FormHelperText> }
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormZone() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }
    
}

const classFormMasterZone = withStyles(styles)(FormZone);

const mapStateToProps = ({ newZone, areas }) => {
    return {newZone, areas};
}

export default connect(mapStateToProps, {
    addZone,
    updateZone,
    closeFormZone,
    getAreas
})(classFormMasterZone)