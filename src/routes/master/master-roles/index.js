/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 16:17:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-11 14:13:06
 */
/**
 * Master Roles Routes
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import { Pagination, PaginationItem, PaginationLink, Button, FormGroup, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// Lodash
import _ from 'lodash';
// React Paginating
import Paginating from 'react-paginating';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// Component
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// Form
import FormMasterRoles from './form';
// Actions
import { getRoles, openFormRoles } from '../../../actions';
// Styles
import styles from '../../../lib/styles';

/**
 * @description Index data Master Roles
 *
 * @class MasterRoles
 * @extends {Component}
 */
class MasterRoles extends Component {

    /**
     * @constant state
     *
     * @memberof MasterRoles
     */
    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: ''
    }

    componentDidMount() {
        this.props.getRoles({ page: this.state.current_page });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.roles !== prevProps.roles) {
            this.setState({
                data: this.props.roles.data,
                rowsPerPage: this.props.roles.per_page,
                current_page: this.props.roles.current_page,
                last_page: this.props.roles.last_page,
                total: this.props.roles.total
            });
        }
        if(this.props.newRole !== prevProps.newRole) {
            this.props.getRoles({ page: this.state.current_page });
        }
    }
    
    /**
     * @method render
     * @description Render view of class MasterRoles
     *
     * @returns
     * @memberof MasterRoles
     */
    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;

        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.masterRoles" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className="table-responsive">
						<div className="d-flex justify-content-between py-20 px-10 border-bottom">
                            <div className="col-md-4">
                                <InputGroup>
                                    <Input
                                        value={this.state.search}
                                        type="text"
                                        name="search"
                                        id="search"
                                        className="input-lg"
                                        placeholder="Search"
                                        onChange={(event) => this.setState({ search: event.target.value })}
                                    />
                                    <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={() => console.log('click')}>
                                        <span className="input-group-text">
                                            <i className="ti-search"></i>
                                        </span>
                                    </InputGroupAddon>
                                </InputGroup>
							</div>
							<div>
                                <Button outline color="success" onClick={() => this.props.openFormRoles(null) }>
                                    Add New Roles <i className="zmdi zmdi-plus"></i>
                                </Button>
							</div>
						</div>
                        <table className="table table-middle table-hover mb-0">
							<thead>
								<tr style={{ height: 60 }}>
									<th>Role Code</th>
									<th>Role Name</th>
									<th>Action</th>
								</tr>
							</thead>
                            <tbody>
                                {data && data.map((value, key) => (
                                    <tr key={key}>
                                        <td>{value.role_code}</td>
                                        <td>{value.role_name}</td>
                                        <td className="list-action">
											<a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
											<a href="javascript:void(0)" onClick={() => this.props.openFormRoles(value)}><i className="ti-pencil"></i></a>
										</td>
                                    </tr>
                                ))}
                            </tbody>
                            <tfoot className="border-top">
								<tr>
									<td colSpan="100%">
                                        <Paginating
                                            total={total}
                                            limit={rowsPerPage}
                                            pageCount={5}
                                            currentPage={current_page}>
                                            {({
                                                pages,
                                                hasNextPage,
                                                hasPreviousPage
                                            }) => (
                                                <Pagination className="mb-0 py-10 px-10">
                                                    <PaginationItem disabled={hasPreviousPage ? false : true }>
                                                        <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                            if(hasPreviousPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page - 1 }
                                                                }, () => this.props.getRoles({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                    {pages.map(page => {
                                                        return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                            <PaginationLink href="javascript:void(0)" onClick={() => {
                                                                if(page !== current_page) {
                                                                    this.setState(prevState => {
                                                                        return { current_page: page }
                                                                    }, () => this.props.getRoles({ page: this.state.current_page }));
                                                                }
                                                            }} >{page}</PaginationLink>
                                                        </PaginationItem>
                                                    })}
                                                    <PaginationItem disabled={hasNextPage ? false : true}>
                                                        <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                            if(hasNextPage) {
                                                                this.setState(prevState => {
                                                                    return { current_page: prevState.current_page + 1 }
                                                                }, () => this.props.getRoles({ page: this.state.current_page }))    
                                                            }
                                                        }} />
                                                    </PaginationItem>
                                                </Pagination>
                                            )}
                                        </Paginating>
									</td>
								</tr>
							</tfoot>
                        </table>
                    </div>
                    {loading && !modal &&
						<RctSectionLoader />
					}
				</RctCollapsibleCard>
                {/* Modal */}
                <FormMasterRoles 
                    title={'Add Master Role'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classMasterRoles = withStyles(styles)(MasterRoles);

const mapStateToProps = ({ roles }) => {
    return roles;
};

export default connect(mapStateToProps, {
    getRoles,
    openFormRoles
})(classMasterRoles);