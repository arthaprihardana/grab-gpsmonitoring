/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-17 19:12:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-11 14:13:36
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, TextField, IconButton } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { FormGroup, Label, Input, Form, FormFeedback } from 'reactstrap';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import {closeFormRoles, addRoles, updateRoles} from '../../../actions';
// Validator
import FormValidator from '../../../helpers/FormValidation';

/**
 * @description Form input master roles
 *
 * @class FormMasterRoles
 * @extends {Component}
 */
class FormMasterRoles extends Component {

    validator = new FormValidator([
        {
            field: 'role_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Nama role tidak boleh kososng'
        }
    ])

    submitted = false;

    /**
     * @constant state
     *
     * @memberof FormMasterRoles
     */
    state = {
        role_name: '',
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    role_name: '',
                    validation: this.validator.valid()
                }, () => this.forceUpdate())
                this.submitted = false
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    role_name: this.props.data.role_name
                });
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateRoles({
                id: this.props.data.role_code,
                FormData: {
                    role_name: this.state.role_name
                }
            });
        } else {
            if(validation.isValid) {
                this.props.addRoles({
                    FormData: {
                        role_name: this.state.role_name
                    }
                })
            }
        }
    }

    /**
     * @method render
     * @description Render view of class FormMasterRoles
     *
     * @returns
     * @memberof FormMasterRoles
     */
    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        // const { roleName, errorMessage } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormRoles()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText>
                        Untuk menambahkan role/hak akses pengguna aplikasi. silahkan daftarkan role baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.role_name : ""}
                                label="Role Name"
                                id="role-name"
                                error={validation.role_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="normal"
                                onChange={(event) => this.setState({ role_name: event.target.value.toUpperCase() }) } 
                                helperText={validation.role_name.message}
                            />
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormRoles() } color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }
}

// export default FormMasterRoles;
const classFormMasterRoles = withStyles(styles)(FormMasterRoles);

const mapStateToProps = ({ newRole }) => {
    return {newRole};
}

export default connect(mapStateToProps, {
    closeFormRoles,
    addRoles,
    updateRoles
})(classFormMasterRoles)