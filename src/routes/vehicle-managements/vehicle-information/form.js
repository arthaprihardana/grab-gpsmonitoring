/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 21:29:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-02 23:15:51
 */
import React, { Fragment, Component } from 'react';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, List, ListItem, ListItemText, Divider, AppBar, Toolbar, IconButton, Slide, TextField, FormControl, InputLabel, Input, InputAdornment, Select, MenuItem, FormHelperText } from '@material-ui/core';
import { Close as CloseIcon } from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
import {connect} from 'react-redux';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// DatePicker
import { DatePicker } from 'material-ui-pickers';
// Styles
import styles from '../../../lib/styles';
// Transition
import Transition from '../../../components/Transition';
// Actions
import { addVehicles, closeFormVehicles, updateVehicles, getBrandVehicles, getModelBrandVehicle, getAreas } from '../../../actions';
// Validation
import FormValidator from '../../../helpers/FormValidation';
// Moment
import moment from 'moment';
import 'moment/locale/id';

class FormMasterVehicle extends Component {

    validator = new FormValidator([
        {
            field: 'license_plate',
            method: 'isEmpty',
            validWhen: false,
            message: 'License plate tidak boleh kososng'
        },
        {
            field: 'imei_obd_number',
            method: 'isEmpty',
            validWhen: false,
            message: 'IMEI OBD number tidak boleh kososng'
        },
        {
            field: 'simcard_number',
            method: 'isEmpty',
            validWhen: false,
            message: 'Sim card tidak boleh kososng'
        },
        {
            field: 'year_of_vehicle',
            method: 'isEmpty',
            validWhen: false,
            message: 'Year vehicle tidak boleh kososng'
        },
        {
            field: 'color_vehicle',
            method: 'isEmpty',
            validWhen: false,
            message: 'Color vehicle tidak boleh kososng'
        },
        {
            field: 'brand_vehicle_code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Brand vehicle tidak boleh kososng'
        },
        {
            field: 'model_vehicle_code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Model vehicle tidak boleh kososng'
        },
        {
            field: 'chassis_number',
            method: 'isEmpty',
            validWhen: false,
            message: 'Chassis number tidak boleh kososng'
        },
        {
            field: 'machine_number',
            method: 'isEmpty',
            validWhen: false,
            message: 'Machine number tidak boleh kososng'
        },
        // {
        //     field: 'date_stnk',
        //     method: 'isEmpty',
        //     validWhen: false,
        //     message: 'Date STNK tidak boleh kososng'
        // },
        // {
        //     field: 'date_installation',
        //     method: 'isEmpty',
        //     validWhen: false,
        //     message: 'Date Installation tidak boleh kososng'
        // },
        {
            field: 'speed_limit',
            method: 'isEmpty',
            validWhen: false,
            message: 'Speed limit tidak boleh kososng'
        },
        {
            field: 'odometer',
            method: 'isEmpty',
            validWhen: false,
            message: 'Odometer tidak boleh kososng'
        },
        {
            field: 'area_code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Area tidak boleh kososng'
        },
    ])

    submitted = false

    state = {
        "license_plate" : "",
        "imei_obd_number" : "",
        "simcard_number" : "",
        "year_of_vehicle" : "",
        "color_vehicle" : "",
        "brand_vehicle_code" : "",
        "model_vehicle_code" : "",
        "chassis_number" : "",
        "machine_number" : "",
        "date_stnk" : moment(),
        "date_installation" : moment(),
        "speed_limit" : "",
        "odometer" : "",
        "area_code": "",
        "type_vehicle": "",

        "dataBrand": [],
        "dataModel": [],
        "dataAreas": [],
        "validation": this.validator.valid()
    };

    componentDidMount() {
        this.props.getBrandVehicles({ page: 1 });
        this.props.getModelBrandVehicle({ page: 1 });
        this.props.getAreas({ page: 1});
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.brand_vehicles.brand_vehicles !== prevProps.brand_vehicles.brand_vehicles) {
            let master_brand_vehicles = this.props.brand_vehicles.brand_vehicles.data;
            let dataSelect = this.state.dataBrand;
            master_brand_vehicles.map((value, key) => {
                dataSelect.push({ value: value.brand_vehicle_code, label: value.brand_vehicle_name })
            })
        }

        if(this.props.model_brand_vehicles.model_brand_vehicles !== prevProps.model_brand_vehicles.model_brand_vehicles) {
            let master_model_brand_vehicles = this.props.model_brand_vehicles.model_brand_vehicles.data;
            let dataSelect = this.state.dataModel;
            master_model_brand_vehicles.map((value, key) => {
                dataSelect.push({ value: value.model_vehicle_code, label: value.model_vehicle_name })
            })
        }

        if(this.props.areas.areas !== prevProps.areas.areas) {
            let master_areas = this.props.areas.areas.data;
            let dataSelect = this.state.dataAreas;
            master_areas.map((value, key) => {
                dataSelect.push({ value: value.area_code, label: value.area_name })
            })
        }

        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    "license_plate" : "",
                    "imei_obd_number" : "",
                    "simcard_number" : "",
                    "year_of_vehicle" : "",
                    "color_vehicle" : "",
                    "brand_vehicle_code" : "",
                    "model_vehicle_code" : "",
                    "chassis_number" : "",
                    "machine_number" : "",
                    "date_stnk" : "",
                    "date_installation" : "",
                    "speed_limit" : "",
                    "odometer" : "",
                    "area_code": "",
                    // validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    "license_plate" : this.props.data.license_plate,
                    "imei_obd_number" : this.props.data.imei_obd_number,
                    "simcard_number" : this.props.data.simcard_number,
                    "year_of_vehicle" : this.props.data.year_of_vehicle,
                    "color_vehicle" : this.props.data.color_vehicle,
                    "brand_vehicle_code" : this.props.data.brand_vehicle_code,
                    "model_vehicle_code" : this.props.data.model_vehicle_code,
                    "chassis_number" : this.props.data.chassis_number,
                    "machine_number" : this.props.data.machine_number,
                    "date_stnk" : this.props.data.date_stnk,
                    "date_installation" : this.props.data.date_installation,
                    "speed_limit" : this.props.data.speed_limit,
                    "odometer" : this.props.data.odometer,
                    "area_code": this.props.data.area_code
                })
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateVehicles({
                id: this.props.data.vehicle_code,
                FormData: {
                    "license_plate" : this.state.license_plate,
                    "imei_obd_number" : this.state.imei_obd_number,
                    "simcard_number" : this.state.simcard_number,
                    "year_of_vehicle" : this.state.year_of_vehicle,
                    "color_vehicle" : this.state.color_vehicle,
                    "brand_vehicle_code" : this.state.brand_vehicle_code,
                    "type_vehicle": this.state.type_vehicle,
                    "model_vehicle_code" : this.state.model_vehicle_code,
                    "chassis_number" : this.state.chassis_number,
                    "machine_number" : this.state.machine_number,
                    "date_stnk" : moment(this.state.date_stnk).format('YYYY-MM-DD'),
                    "date_installation" : moment(this.state.date_installation).format('YYYY-MM-DD'),
                    "speed_limit" : this.state.speed_limit,
                    "odometer" : this.state.odometer,
                    "area_code": this.state.area_code,
                    "status": 1
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addVehicles({
                    FormData: {
                        "license_plate" : this.state.license_plate,
                        "imei_obd_number" : this.state.imei_obd_number,
                        "simcard_number" : this.state.simcard_number,
                        "year_of_vehicle" : this.state.year_of_vehicle,
                        "color_vehicle" : this.state.color_vehicle,
                        "brand_vehicle_code" : this.state.brand_vehicle_code,
                        "type_vehicle": this.state.type_vehicle,
                        "model_vehicle_code" : this.state.model_vehicle_code,
                        "chassis_number" : this.state.chassis_number,
                        "machine_number" : this.state.machine_number,
                        "date_stnk" : moment(this.state.date_stnk).format('YYYY-MM-DD'),
                        "date_installation" : moment(this.state.date_installation).format('YYYY-MM-DD'),
                        "speed_limit" : this.state.speed_limit,
                        "odometer" : this.state.odometer,
                        "area_code": this.state.area_code,
                        "status": 1
                    }
                })
            } else {
                console.log('tidak valid');
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataBrand, dataModel, dataAreas } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}
                // maxWidth={'md'}
                //fullWidth={true}
                >
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormVehicles()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan data master vehicle. silahkan input data vehicle baru di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.license_plate : ""}
                                label="License Plate"
                                id="license-plat"
                                error={validation.license_plate.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ license_plate: event.target.value }) } 
                                helperText={validation.license_plate.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.imei_obd_number : ""}
                                label="Imei OBD Number"
                                id="imei-obd-number"
                                error={validation.imei_obd_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ imei_obd_number: event.target.value }) } 
                                helperText={validation.imei_obd_number.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.simcard_number : ""}
                                label="Sim Card Number"
                                id="simcard-number"
                                error={validation.simcard_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ simcard_number: event.target.value }) } 
                                helperText={validation.simcard_number.message}
                            />
                        </div>
                        <div className="form-group">
                            <Fragment>
                                <div className="rct-picker">
                                    <DatePicker
                                        ref={"stnk_date"}
                                        label="Choose STNK Date"
                                        value={this.state.date_stnk}
                                        onChange={(date) => {
                                            this.setState({ date_stnk: date });
                                        }}
                                        format={'DD/MM/YYYY'}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.bootstrapRootPassword,
                                                input: classes.bootstrapInputPassword,
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end" style={{
                                                    position: 'absolute',
                                                    right: '16px',
                                                    bottom: '7px',
                                                    color: '#999'
                                                }}>
                                                    <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.stnk_date.open() }>
                                                        <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999'}}></i>
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                        InputLabelProps={{
                                            shrink: true,
                                            className: classes.bootstrapFormLabel,
                                        }}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                        keyboard
                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                    />
                                </div>
                            </Fragment>
                        </div>
                        <div className="form-group">
                            <Fragment>
                                <div className="rct-picker">
                                    <DatePicker
                                        ref={"installation_date"}
                                        label="Choose Installation Date"
                                        value={this.state.date_installation}
                                        onChange={(date) => {
                                            this.setState({ date_installation: date });
                                        }}
                                        format={'DD/MM/YYYY'}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.bootstrapRootPassword,
                                                input: classes.bootstrapInputPassword,
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end" style={{
                                                    position: 'absolute',
                                                    right: '16px',
                                                    bottom: '7px',
                                                    color: '#999'
                                                }}>
                                                    <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.installation_date.open() }>
                                                        <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                        InputLabelProps={{
                                            shrink: true,
                                            className: classes.bootstrapFormLabel,
                                        }}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                        keyboard
                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                    />
                                </div>
                            </Fragment>
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Brand</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.brand_vehicle_code,
                                    onChange: val => this.setState({ brand_vehicle_code: val }),
                                    placeholder: 'Select Brand',
                                    instanceId: 'select-brand',
                                    id: 'select-brand',
                                    name: 'select-brand',
                                    simpleValue: true,
                                    options: dataBrand.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.brand_vehicle_code.isInvalid && <FormHelperText error={true}>{validation.brand_vehicle_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.type_vehicle : ""}
                                label="Vehicle Type"
                                id="vehicle-type"
                                // error={validation.simcard_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ type_vehicle: event.target.value }) } 
                                // helperText={validation.simcard_number.message}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Model</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.model_vehicle_code,
                                    onChange: val => this.setState({ model_vehicle_code: val }),
                                    placeholder: 'Select Model',
                                    instanceId: 'select-model',
                                    id: 'select-model',
                                    name: 'select-model',
                                    simpleValue: true,
                                    options: dataModel.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.model_vehicle_code.isInvalid && <FormHelperText error={true}>{validation.model_vehicle_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.chassis_number : ""}
                                label="Chassis Number"
                                id="chassis-number"
                                error={validation.chassis_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ chassis_number: event.target.value }) } 
                                helperText={validation.chassis_number.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.machine_number : ""}
                                label="Machine Number"
                                id="machine-number"
                                error={validation.machine_number.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ machine_number: event.target.value }) } 
                                helperText={validation.machine_number.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.color_vehicle : ""}
                                label="Color Vehicle"
                                id="color-vehicle"
                                error={validation.color_vehicle.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ color_vehicle: event.target.value }) } 
                                helperText={validation.color_vehicle.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.odometer : ""}
                                label="Odometer"
                                id="odometer"
                                error={validation.odometer.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ odometer: event.target.value }) } 
                                helperText={validation.odometer.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.speed_limit : ""}
                                label="Speed Limit"
                                id="speed-limit"
                                error={validation.speed_limit.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ speed_limit: event.target.value }) } 
                                helperText={validation.speed_limit.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.year_of_vehicle : ""}
                                label="Year Of Vehicle"
                                id="year-of-vehicle"
                                error={validation.year_of_vehicle.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ year_of_vehicle: event.target.value }) } 
                                helperText={validation.year_of_vehicle.message}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Area</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.area_code,
                                    onChange: val => this.setState({ area_code: val }),
                                    placeholder: 'Select Area',
                                    instanceId: 'select-area',
                                    id: 'select-area',
                                    name: 'select-area',
                                    simpleValue: true,
                                    options: dataAreas.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.area_code.isInvalid && <FormHelperText error={true}>{validation.area_code.message}</FormHelperText> }
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormVehicles()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormMasterVehicle = withStyles(styles)(FormMasterVehicle);

const mapStateToProps = ({ newVehicles, brand_vehicles, model_brand_vehicles, areas }) => {
    return { newVehicles, brand_vehicles, model_brand_vehicles, areas };
}

export default connect(mapStateToProps, {
    addVehicles,
    closeFormVehicles,
    updateVehicles,
    getBrandVehicles,
    getModelBrandVehicle,
    getAreas
})(classFormMasterVehicle);