/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 08:57:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 11:37:33
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
// Material UI
import {withStyles} from '@material-ui/core/styles';
// Material Code
import { Tabs, Tab, Typography } from '@material-ui/core';
// React Paginating
import Paginating from 'react-paginating';
// Export Data
import ReactExport from "react-data-export";
// Reactstrap
import { Button, Pagination, PaginationItem, PaginationLink, Input, InputGroup, InputGroupAddon, Alert } from 'reactstrap';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// Moment
import moment from 'moment';
import 'moment/locale/id';
// Form
import FormMasterVehicle from './form';
// Actions
import { getVehicles, openFormVehicles } from '../../../actions';
// Styles
import styles from '../../../lib/styles';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

/**
 * @description Index Data Master Vehicles
 *
 * @class MasterVehicle
 * @extends {Component}
 */
class VehicleInformation extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: this.props.onSearch
    }

    componentDidMount() {
        this.props.getVehicles({ 
            page: this.state.current_page,
            license_plate: this.state.search
        });
    }
    
    getSnapshotBeforeUpdate(prevProps, prevState) {
        if(this.props.vehicles !== prevProps.vehicles) {
            return { vehicles: this.props.vehicles }
        }
        if(this.props.newVehicles !== prevProps.newVehicles) {
            return { newVehicles: true }
        }
        return null;
    }
    

    componentDidUpdate(prevProps, prevState, snapshot) {
        if(snapshot !== null) {
            if(snapshot.vehicles !== undefined) {
                this.setState(prevState => {
                    return {
                        data: snapshot.vehicles.data,
                        rowsPerPage: snapshot.vehicles.per_page,
                        current_page: snapshot.vehicles.current_page,
                        last_page: snapshot.vehicles.last_page,
                        total: snapshot.vehicles.total
                    }
                })
            }
            if(snapshot.newVehicles !== undefined) {
                this.props.getVehicles({ page: this.state.current_page });
            }
        }
    }

    handleChange = (event, value) => {
        this.setState({ value });
    };

    onSearch = () => {
        this.props.callbackValue(this.state.search)
        this.props.getVehicles({ page: this.state.current_page, license_plate: this.state.search })
    }

    render() {
        const { loading, className, modal, formWithData, clearForm, classes } = this.props;
        const { data, total, current_page, rowsPerPage, last_page, value } = this.state;
        const excelData = data.length > 0 ? data : [];

        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type={"text"}
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={this.onSearch}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        <ExcelFile element={
                            <Button outline color="success" onClick={this.toExcel}>
                                Export Excel <i className="zmdi zmdi-cloud-download"></i>
                            </Button>
                        } filename="Vehicle_Information">
                            <ExcelSheet data={excelData} name="Vehicle Information">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="Brand Vehicle" value={ value => value.brand !== null && value.brand.brand_vehicle_name } />
                                <ExcelColumn label="Model Vehicle" value={ value => value.model !== null && value.model.model_vehicle_code} />
                                <ExcelColumn label="Year of Vehicle" value="year_of_vehicle" />
                                <ExcelColumn label="Color Vehicle" value="color_vehicle" />
                            </ExcelSheet>
                        </ExcelFile>
                        <Button outline color="success" onClick={() => this.props.openFormVehicles(null) } style={{ marginLeft: 10 }}>
                            Add New Vehicle <i className="zmdi zmdi-plus"></i>
                        </Button>
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            {/* <th>Vehicle Code</th> */}
                            <th>License Plate</th>
                            <th>Brand Vehicle</th>
                            <th>Type Vehicle</th>
                            <th>Model Vehicle</th>
                            <th>Year of Vehicle</th>
                            <th>Color Vehicle</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data && data.map((value, key) => (
                            <tr key={key}>
                                {/* <td>{value.vehicle_code}</td> */}
                                <td>{value.license_plate}</td>
                                <td>{value.brand !== null && value.brand.brand_vehicle_name}</td>
                                <td>{value.type_vehicle !== null && value.type_vehicle.toUpperCase()}</td>
                                <td>{value.model !== null && value.model.model_vehicle_name}</td>
                                <td>{value.year_of_vehicle}</td>
                                <td>{value.color_vehicle}</td>
                                <td className="list-action">
                                    <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                    <a href="javascript:void(0)" onClick={() => this.props.openFormVehicles(value)}><i className="ti-pencil"></i></a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => (
                                        <Pagination className="mb-0 py-10 px-10">
                                            <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                    if(hasPreviousPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page - 1 }
                                                        }, () => this.props.getVehicles({ page: this.state.current_page }))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                            {pages.map(page => {
                                                return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                    <PaginationLink href="javascript:void(0)" onClick={() => {
                                                        if(page !== current_page) {
                                                            this.setState(prevState => {
                                                                return { current_page: page }
                                                            }, () => this.props.getVehicles({ page: this.state.current_page }));
                                                        }
                                                    }} >{page}</PaginationLink>
                                                </PaginationItem>
                                            })}
                                            <PaginationItem disabled={hasNextPage ? false : true}>
                                                <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                    if(hasNextPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page + 1 }
                                                        }, () => this.props.getVehicles({ page: this.state.current_page }))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                        </Pagination>
                                    )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {loading &&
                    <RctSectionLoader />
                }
                {/* Modal */}
                <FormMasterVehicle 
                    title={'Add Master Vehicle'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classVehicleInformation = withStyles(styles)(VehicleInformation);

const mapStateToProps = ({ vehicles }) => {
    return vehicles;
}

export default connect(mapStateToProps, {
    getVehicles,
    openFormVehicles
})(classVehicleInformation)