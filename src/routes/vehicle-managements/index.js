/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 08:57:48 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 13:53:17
 */
/** 
 * TODO: cek maintenance vehicle
 * TODO: konfirmasi flow maintenance vehicle (skenario switch device)
 * TODO: add checklist ganti imei di halaman maintenance vehicle
 */
// React
import React, { Component } from 'react';
// Page Title Bar
import PageTitleBar from '../../components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from '../../components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from '../../components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from '../../util/IntlMessages';
// Material UI
import { withStyles } from '@material-ui/core/styles';
// Material Core
import { Tabs, Tab, Typography } from '@material-ui/core';
// Styles
import styles from './styles';
// Part Of Maintenance Vehicle
import VehicleInformation from './vehicle-information';
import HistoryDriver from './driver-history';
import HistoryMaintenance from './history-maintenance';

class VehicleManagaments extends Component {

    state = {
        value: 0,
        search: ""
    }

    handleChange = (event, value) => {
        this.setState({ value });
    }

    render() {
        const { classes } = this.props;
        const { value } = this.state;

        return (
            <div className="general-wrapper">
                <PageTitleBar title={<IntlMessages id="sidebar.vehicleManagements" />} match={this.props.match} />
                <RctCollapsibleCard fullBlock>
                    <div className={classes.root}>
                        <Tabs
                            value={value}
                            onChange={this.handleChange}
                            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator }}
                            >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                    label="Vehicle Information"
                                />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                    label="History Driver"
                                />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot, selected: classes.tabSelected }}
                                    label="History Maintenance"
                                />
                            </Tabs>
                            {value === 0 && <VehicleInformation callbackValue={value => this.setState(prevState => {
                                return { search: value }    
                            })} onSearch={this.state.search} />}
                            {value === 1 && <HistoryDriver callbackValue={value => this.setState(prevState => {
                                return { search: value }    
                            })} onSearch={this.state.search} />}
                            {value === 2 && <HistoryMaintenance callbackValue={value => this.setState(prevState => {
                                return { search: value }    
                            })} onSearch={this.state.search} />}
                    </div>
                </RctCollapsibleCard>
            </div>
        );
    }

}

export default withStyles(styles)(VehicleManagaments);