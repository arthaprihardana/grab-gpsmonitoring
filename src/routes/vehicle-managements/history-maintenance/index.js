/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 08:30:12 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 13:49:48
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
// Material UI
import {withStyles} from '@material-ui/core';
// React Paginating
import Paginating from 'react-paginating';
// ReactStrap
import { Pagination, PaginationItem, PaginationLink, Button, Input, InputGroup, InputGroupAddon} from 'reactstrap';
// Export Data
import ReactExport from "react-data-export";
// Moment
import moment from 'moment';
import 'moment/locale/id';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// Component RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// Form
import FormMaintenanceVehicle from './form';
// Actions
import { getMaintenanceVehicles, openFormMaintenanceVehicle } from '../../../actions';
// Styles
import styles from '../../../lib/styles';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

/**
 * @description Index Data Maintenance Vehicle
 *
 * @class MaintenanceVehicle
 * @extends {Component}
 */
class HistoryMaintenance extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: this.props.onSearch
    }

    componentDidMount() {
        this.props.getMaintenanceVehicles({ 
            page: this.state.current_page,
            license_plate: this.state.search
        })
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.maintenance_vehicles !== prevProps.maintenance_vehicles) {
            this.setState({
                data: this.props.maintenance_vehicles.data,
                rowsPerPage: this.props.maintenance_vehicles.per_page,
                current_page: this.props.maintenance_vehicles.current_page,
                last_page: this.props.maintenance_vehicles.last_page,
                total: this.props.maintenance_vehicles.total
            });
        }
        if(this.props.newMaintenance !== prevProps.newMaintenance) {
            this.props.getMaintenanceVehicles({ page: this.state.current_page });
        }
    }

    onSearch = () => {
        this.props.callbackValue(this.state.search)
        this.props.getMaintenanceVehicles({ page: this.state.current_page, license_plate: this.state.search })
    }

    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        const excelData = data.length > 0 ? data : [];

        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type={"text"}
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={this.onSearch}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    <div>
                        <ExcelFile element={
                            <Button outline color="success" onClick={this.toExcel}>
                                Export Excel <i className="zmdi zmdi-cloud-download"></i>
                            </Button>
                        } filename="History_Maintenance">
                            <ExcelSheet data={excelData} name="History Maintenance">
                                <ExcelColumn label="License Plate" value="license_plate"/>
                                <ExcelColumn label="IMEI OBD Number (OLD)" value="imei_obd_number_old" />
                                <ExcelColumn label="IMEI OBD Number (NEW)" value="imei_obd_number_new" />
                                <ExcelColumn label="Maintenance Date" value={ item => moment(item.start_date_maintenance).format('YYYY-MM-DD') } />
                            </ExcelSheet>
                        </ExcelFile>
                        <Button outline color="success" onClick={() => this.props.openFormMaintenanceVehicle(null) } style={{ marginLeft: 10 }}>
                            Add New Maintenance <i className="zmdi zmdi-plus"></i>
                        </Button>
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>License Plate</th>
                            <th>IMEI OBD Number (OLD)</th>
                            <th>IMEI OBD Number (NEW)</th>
                            <th>Maintenance Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data && data.map((value, key) => (
                            <tr key={key}>
                                <td>{value.license_plate}</td>
                                <td>{value.imei_obd_number_old}</td>
                                <td>{value.imei_obd_number_new}</td>
                                <td>{moment(value.start_date_maintenance).format('LL')}</td>
                                <td className="list-action">
                                    <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                    <a href="javascript:void(0)" onClick={() => this.props.openFormMaintenanceVehicle(value)}><i className="ti-pencil"></i></a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => (
                                        <Pagination className="mb-0 py-10 px-10">
                                            <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                    if(hasPreviousPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page - 1 }
                                                        }, () => this.props.getMaintenanceVehicles({ page: this.state.current_page }))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                            {pages.map(page => {
                                                return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                    <PaginationLink href="javascript:void(0)" onClick={() => {
                                                        if(page !== current_page) {
                                                            this.setState(prevState => {
                                                                return { current_page: page }
                                                            }, () => this.props.getMaintenanceVehicles({ page: this.state.current_page }));
                                                        }
                                                    }} >{page}</PaginationLink>
                                                </PaginationItem>
                                            })}
                                            <PaginationItem disabled={hasNextPage ? false : true}>
                                                <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                    if(hasNextPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page + 1 }
                                                        }, () => this.props.getMaintenanceVehicles({ page: this.state.current_page }))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                        </Pagination>
                                    )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {loading && !modal &&
                    <RctSectionLoader />
                }
                {/* Modal */}
                <FormMaintenanceVehicle 
                    title={'Add Maintenance Vehicle'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading}
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classHistoryMaintenance = withStyles(styles)(HistoryMaintenance);

const mapStateToProps = ({ maintenance_vehicles }) => {
    return maintenance_vehicles;
}

export default connect(mapStateToProps, {
    getMaintenanceVehicles,
    openFormMaintenanceVehicle
})(classHistoryMaintenance)