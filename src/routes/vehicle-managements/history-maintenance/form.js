/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 22:39:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 23:00:36
 */
import React, { Fragment, Component } from 'react';
import {connect} from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, IconButton, TextField, FormControl, InputLabel, Input, InputAdornment, Grid, CircularProgress } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// styles
import styles from '../../../lib/styles';
// Moment
import moment from 'moment';
import 'moment/locale/id';
// DatePicker
import { DatePicker } from 'material-ui-pickers';
// Transition
import Transition from '../../../components/Transition';
// Actions
import { addMaintenanceVehicles, closeFormMaintenanceVehicle, updateMaintenanceVehicles, getVehiclesByCode } from '../../../actions';
// Validation
import FormValidator from '../../../helpers/FormValidation';

class FormMaintenanceVehicle extends Component {

    validator = new FormValidator([
        {
            field: "imei_obd_number_old",
            method: 'isEmpty',
            validWhen: false,
            message: 'IMEI OBD Number lama tidak boleh kosong'
        },
        {
            field: "imei_obd_number_new",
            method: 'isEmpty',
            validWhen: false,
            message: 'IMEI OBD Number baru tidak boleh kosong'
        },
        {
            field: "simcard_number_old",
            method: 'isEmpty',
            validWhen: false,
            message: 'Simcard number lama tidak boleh kosong'
        },
        {
            field: "simcard_number_new",
            method: 'isEmpty',
            validWhen: false,
            message: 'Simcard number baru tidak boleh kosong'
        },
        {
            field: "start_date_maintenance",
            method: 'isEmpty',
            validWhen: false,
            message: 'Start Date Maintenance tidak boleh kosong'
        },
        {
            field: "end_date_maintenance",
            method: 'isEmpty',
            validWhen: false,
            message: 'End Date Maintenance tidak boleh kosong'
        },
    ])

    submitted = false

    state = {
        search_vehicle: "",
        maintenance_vehicle_code: "",
        imei_obd_number_old : "",
        imei_obd_number_new : "",
        simcard_number_old : "",
        simcard_number_new : "",
        start_date_maintenance : moment(),
        end_date_maintenance : moment(),
        validation: this.validator.valid()
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    search_vehicle: "",
                    maintenance_vehicle_code: "",
                    imei_obd_number_old : "",
                    imei_obd_number_new : "",
                    simcard_number_old : "",
                    simcard_number_new : "",
                    start_date_maintenance : moment(),
                    end_date_maintenance : moment()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    maintenance_vehicle_code: this.props.data.maintenance_vehicle_code,
                    imei_obd_number_old : this.props.data.imei_obd_number_old,
                    imei_obd_number_new : this.props.data.imei_obd_number_new,
                    simcard_number_old : this.props.data.simcard_number_old,
                    simcard_number_new : this.props.data.simcard_number_new,
                    start_date_maintenance : moment(this.props.data.start_date_maintenance),
                    end_date_maintenance : moment(this.props.data.end_date_maintenance)
                })
            }
        }
        if(this.props.searchVehicle !== prevProps.searchVehicle) {
            if(this.props.searchVehicle !== null) {
                this.setState({
                    imei_obd_number_old: this.props.searchVehicle.imei_obd_number,
                    simcard_number_old: this.props.searchVehicle.simcard_number
                });
            } else {
                this.setState({
                    imei_obd_number_old: '',
                    simcard_number_old: ''
                });
            }
        }
    }

    onSearchVehicles = () => {
        this.props.getVehiclesByCode(this.state.search_vehicle)
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateMaintenanceVehicles({
                id: this.props.data.maintenance_vehicle_code,
                FormData: {
                    imei_obd_number_old : this.state.imei_obd_number_old,
                    imei_obd_number_new : this.state.imei_obd_number_new,
                    simcard_number_old : this.state.simcard_number_old,
                    simcard_number_new : this.state.simcard_number_new,
                    start_date_maintenance : moment(this.state.start_date_maintenance).format('YYYY-MM-DD'),
                    end_date_maintenance : moment(this.state.end_date_maintenance).format('YYYY-MM-DD')
                }
            })
        } else {
            if(validation.isValid) {
                this.props.addMaintenanceVehicles({
                    FormData: {
                        // maintenance_vehicle_code: this.state.maintenance_vehicle_code,
                        imei_obd_number_old : this.state.imei_obd_number_old,
                        imei_obd_number_new : this.state.imei_obd_number_new,
                        simcard_number_old : this.state.simcard_number_old,
                        simcard_number_new : this.state.simcard_number_new,
                        start_date_maintenance : moment(this.state.start_date_maintenance).format('YYYY-MM-DD'),
                        end_date_maintenance : moment(this.state.end_date_maintenance).format('YYYY-MM-DD'),
                        status: 1
                    }
                })
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}
                maxWidth={'md'}
                fullWidth={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        {data !== null ? "Untuk memperbaharui data silahkan input IMEI OBD Number (NEW) dan Simcard Number (NEW) didalam form ini." : "Untuk menambahkan maintenance vehicle. Silahkan input No Polisi terlebih dahulu untuk menampilkan IMEI OBD Number (OLD) dan Simcard Number (OLD)."}
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        {data === null &&
                        <Grid container spacing={16}>
                            <Grid item xs={12} sm={12}>
                                <div className="form-group">
                                    <TextField
                                        fullWidth
                                        type={'text'}
                                        id="search-vehicle"
                                        label="Search Vehicle"
                                        margin="none"
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.bootstrapRootPassword,
                                                input: classes.bootstrapInputPassword,
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end" style={{ marginTop: '6px'}}>
                                                    <IconButton onClick={() => this.onSearchVehicles()}>
                                                        <i className="zmdi zmdi-search"></i>
                                                    </IconButton>
                                                    {this.props.loading &&
                                                        <CircularProgress style={{ position: 'absolute', right: 7, top: 2 }} size={36} /> }
                                                </InputAdornment>
                                            ),
                                        }}
                                        InputLabelProps={{
                                            shrink: true,
                                            className: classes.bootstrapFormLabel,
                                        }}
                                        onChange={(event) => this.setState({ search_vehicle: event.target.value }) } 
                                        // helperText={validation.search_vehicle.message}
                                        />
                                </div>
                            </Grid>
                        </Grid>
                        }
                        {this.props.searchVehicle !== null || data !== null ?
                            <Grid container spacing={16}>
                                <Grid item xs={12} sm={6}>
                                    <div className="form-group">
                                        <TextField
                                            value={this.state.imei_obd_number_old}
                                            label="IMEI OBD Number (OLD)"
                                            id="imei_obd_number_oldy"
                                            error={validation.imei_obd_number_old.isInvalid ? true : false}
                                            fullWidth={true}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.bootstrapRoot,
                                                    input: classes.bootstrapInput,
                                                },
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: classes.bootstrapFormLabel,
                                            }}
                                            // style={{marginTop: 20}}
                                            margin="none"
                                            onChange={(event) => this.setState({ imei_obd_number_old: event.target.value }) } 
                                            helperText={validation.imei_obd_number_old.message}
                                            disabled
                                        />
                                    </div>
                                    <div className="form-group">
                                        <TextField
                                            value={this.state.simcard_number_old}
                                            label="IMEI OBD Number (OLD)"
                                            id="simcard_number_old"
                                            error={validation.simcard_number_old.isInvalid ? true : false}
                                            fullWidth={true}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.bootstrapRoot,
                                                    input: classes.bootstrapInput,
                                                },
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: classes.bootstrapFormLabel,
                                            }}
                                            // style={{marginTop: 20}}
                                            margin="none"
                                            onChange={(event) => this.setState({ simcard_number_old: event.target.value }) } 
                                            helperText={validation.simcard_number_old.message}
                                            disabled
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <div className="form-group">
                                        <TextField
                                            value={this.state.imei_obd_number_new}
                                            label="IMEI OBD Number (NEW)"
                                            id="imei_obd_number_new"
                                            error={validation.imei_obd_number_new.isInvalid ? true : false}
                                            fullWidth={true}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.bootstrapRoot,
                                                    input: classes.bootstrapInput,
                                                },
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: classes.bootstrapFormLabel,
                                            }}
                                            // style={{marginTop: 20}}
                                            margin="none"
                                            onChange={(event) => this.setState({ imei_obd_number_new: event.target.value }) } 
                                            helperText={validation.imei_obd_number_new.message}
                                        />
                                    </div>
                                    <div className="form-group">
                                        <TextField
                                            value={this.state.simcard_number_new}
                                            label="IMEI OBD Number (NEW)"
                                            id="simcard_number_new"
                                            error={validation.simcard_number_new.isInvalid ? true : false}
                                            fullWidth={true}
                                            InputProps={{
                                                disableUnderline: true,
                                                classes: {
                                                    root: classes.bootstrapRoot,
                                                    input: classes.bootstrapInput,
                                                },
                                            }}
                                            InputLabelProps={{
                                                shrink: true,
                                                className: classes.bootstrapFormLabel,
                                            }}
                                            // style={{marginTop: 20}}
                                            margin="none"
                                            onChange={(event) => this.setState({ simcard_number_new: event.target.value }) } 
                                            helperText={validation.simcard_number_new.message}
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <div className="form-group">
                                        <Fragment>
                                            <div className="rct-picker">
                                                <DatePicker
                                                    ref={"start_date"}
                                                    label="Choose Start Date"
                                                    value={this.state.start_date_maintenance}
                                                    onChange={(date) => this.setState({ start_date_maintenance: date })}
                                                    format={'DD/MM/YYYY'}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            root: classes.bootstrapRootPassword,
                                                            input: classes.bootstrapInputPassword,
                                                        },
                                                        endAdornment: (
                                                            <InputAdornment position="end" style={{
                                                                position: 'absolute',
                                                                right: '16px',
                                                                bottom: '7px',
                                                                color: '#999'
                                                            }}>
                                                                <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.start_date.open() }>
                                                                    <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                                </IconButton>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                        className: classes.bootstrapFormLabel,
                                                    }}
                                                    animateYearScrolling={false}
                                                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                                    fullWidth
                                                    keyboard
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                />
                                            </div>
                                        </Fragment>
                                    </div>
                                    <div className="form-group">
                                        <Fragment>
                                            <div className="rct-picker">
                                                <DatePicker
                                                    ref={"end_date"}
                                                    label="Choose End Date"
                                                    value={this.state.end_date_maintenance}
                                                    onChange={(date) => this.setState({ end_date_maintenance: date })}
                                                    format={'DD/MM/YYYY'}
                                                    InputProps={{
                                                        disableUnderline: true,
                                                        classes: {
                                                            root: classes.bootstrapRootPassword,
                                                            input: classes.bootstrapInputPassword,
                                                        },
                                                        endAdornment: (
                                                            <InputAdornment position="end" style={{
                                                                position: 'absolute',
                                                                right: '16px',
                                                                bottom: '7px',
                                                                color: '#999'
                                                            }}>
                                                                <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.end_date.open() }>
                                                                    <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                                </IconButton>
                                                            </InputAdornment>
                                                        ),
                                                    }}
                                                    InputLabelProps={{
                                                        shrink: true,
                                                        className: classes.bootstrapFormLabel,
                                                    }}
                                                    animateYearScrolling={false}
                                                    leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                                    rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                                    fullWidth
                                                    keyboard
                                                    mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                                />
                                            </div>
                                        </Fragment>
                                    </div>
                                </Grid>
                            </Grid> : null }
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormMaintenanceVehicle()} color="primary">Cancel</Button>
                    {this.props.searchVehicle !== null || data !== null ?
                        <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                        : null }
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        );
    }

}

const classFormMaintenanceVehicle = withStyles(styles)(FormMaintenanceVehicle);

const mapStateToProps = ({ newMaintenance, vehicles }) => {
    const {searchVehicle, loading} = vehicles;
    return { newMaintenance, searchVehicle, loading}
}

export default connect(mapStateToProps, {
    addMaintenanceVehicles,
    closeFormMaintenanceVehicle,
    updateMaintenanceVehicles,
    getVehiclesByCode
})(classFormMaintenanceVehicle);