/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 09:01:04 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 11:39:34
 */
// React
import React, { Component } from 'react';
// Redux
import { connect } from "react-redux";
// Material UI
import {withStyles, Tooltip, IconButton} from '@material-ui/core';
// Reactstrap
import { Pagination, PaginationItem, PaginationLink, Button, Input, InputGroup, InputGroupAddon, Alert} from 'reactstrap';
// React Paginating
import Paginating from 'react-paginating';
// Export Data
import ReactExport from "react-data-export";
import { Description as DescriptionIcon } from "@material-ui/icons";
// Moment
import moment from 'moment';
import 'moment/locale/id';
// page title bar
import PageTitleBar from 'Components/PageTitleBar/PageTitleBar';
// Coomponent RCT
import RctCollapsibleCard from 'Components/RctCollapsibleCard/RctCollapsibleCard';
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// intl messages
import IntlMessages from 'Util/IntlMessages';
// Form
import FormVehicleAndDriverPair from './form';
// Actions
import { getTransactionVehiclePair, openFormTransactionVehiclePair } from '../../../actions';
// Styles
import styles from '../../../lib/styles';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class HistoryDriver extends Component {

    state = {
        data: [],
        detail: {},
        current_page: 1,
        rowsPerPage: 25,
        last_page: 1,
        total: 25,
        search: this.props.onSearch
    }

    componentDidMount() {
        this.props.getTransactionVehiclePair({ 
            page: this.state.current_page,
            license_plate: this.state.search
        })
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.vehicle_pair !== prevProps.vehicle_pair) {
            this.setState({
                data: this.props.vehicle_pair.data,
                rowsPerPage: this.props.vehicle_pair.per_page,
                current_page: this.props.vehicle_pair.current_page,
                last_page: this.props.vehicle_pair.last_page,
                total: this.props.vehicle_pair.total
            });
        }
        if(this.props.newPairing !== prevProps.newPairing) {
            this.props.getTransactionVehiclePair({ page: this.state.current_page });
        }
    }

    onSearch = () => {
        this.props.callbackValue(this.state.search)
        this.props.getTransactionVehiclePair({ page: this.state.current_page, license_plate: this.state.search })
    }

    render() {
        const { loading, className, modal, formWithData, clearForm } = this.props;
        const { data, total, current_page, rowsPerPage, last_page } = this.state;
        const excelData = data.length > 0 ? data : [];

        return (
            <div className="table-responsive">
                <div className="d-flex justify-content-between py-20 px-10 border-bottom">
                    <div className="col-md-4">
                        <InputGroup>
                            <Input
                                value={this.state.search}
                                type={"text"}
                                name="search"
                                id="search"
                                className="input-lg"
                                placeholder="Search"
                                onChange={(event) => this.setState({ search: event.target.value })}
                            />
                            <InputGroupAddon addonType="append" style={{ cursor: 'pointer' }} onClick={this.onSearch}>
                                <span className="input-group-text">
                                    <i className="ti-search"></i>
                                </span>
                            </InputGroupAddon>
                        </InputGroup>
                    </div>
                    {/* <div className="col-md-1">
                        
                    </div> */}
                    <div>
                        <ExcelFile element={
                            <Button outline color="success" onClick={this.toExcel}>
                                Export Excel <i className="zmdi zmdi-cloud-download"></i>
                            </Button>
                        } filename="History_Driver">
                            <ExcelSheet data={excelData} name="History Driver">
                                <ExcelColumn label="Pairing Code" value="transaction_vehicle_pair_code"/>
                                <ExcelColumn label="Vehicle" value={ item => item.vehicle.license_plate } />
                                <ExcelColumn label="Driver Name" value={ item => item.driver.name }/>
                                <ExcelColumn label="Start Date Pair" value={ item => moment(item.start_date_pair).format('YYYY-MM-DD') } />
                                <ExcelColumn label="End Date Pair" value={ item => moment(item.end_date_pair).format('YYYY-MM-DD') } />
                            </ExcelSheet>
                        </ExcelFile>
                        <Button outline color="success" onClick={() => this.props.openFormTransactionVehiclePair(null) } style={{ marginLeft: 10 }}>
                            Add New Pairing <i className="zmdi zmdi-plus"></i>
                        </Button>
                    </div>
                </div>
                <table className="table table-middle table-hover mb-0">
                    <thead>
                        <tr style={{ height: 60 }}>
                            <th>Pairing Code</th>
                            <th>Vehicle</th>
                            <th>Driver Name</th>
                            <th>Start Date Pair</th>
                            <th>End Date Pair</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data && data.map((value, key) => (
                            <tr key={key}>
                                <td>{value.transaction_vehicle_pair_code}</td>
                                <td>{value.vehicle.license_plate}</td>
                                <td>{value.driver.name}</td>
                                <td>{moment(value.start_date_pair).format('LL')}</td>
                                <td>{moment(value.end_date_pair).format('LL')}</td>
                                <td className="list-action">
                                    <a href="javascript:void(0)" onClick={() => {}}><i className="ti-eye"></i></a>
                                    <a href="javascript:void(0)" onClick={() => this.props.openFormTransactionVehiclePair(value)}><i className="ti-pencil"></i></a>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                    <tfoot className="border-top">
                        <tr>
                            <td colSpan="100%">
                                <Paginating
                                    total={total}
                                    limit={rowsPerPage}
                                    pageCount={5}
                                    currentPage={current_page}>
                                    {({
                                        pages,
                                        hasNextPage,
                                        hasPreviousPage
                                    }) => (
                                        <Pagination className="mb-0 py-10 px-10">
                                            <PaginationItem disabled={hasPreviousPage ? false : true}>
                                                <PaginationLink previous href="javascript:void(0)" onClick={() => {
                                                    if(hasPreviousPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page - 1 }
                                                        }, () => this.props.getTransactionVehiclePair({ page: this.state.current_page }))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                            {pages.map(page => {
                                                return <PaginationItem key={page} active={page === current_page ? true : false}>
                                                    <PaginationLink href="javascript:void(0)" onClick={() => {
                                                        if(page !== current_page) {
                                                            this.setState(prevState => {
                                                                return { current_page: page }
                                                            }, () => this.props.getTransactionVehiclePair({ page: this.state.current_page }));
                                                        }
                                                    }} >{page}</PaginationLink>
                                                </PaginationItem>
                                            })}
                                            <PaginationItem disabled={hasNextPage ? false : true}>
                                                <PaginationLink next href="javascript:void(0)" onClick={() => {
                                                    if(hasNextPage) {
                                                        this.setState(prevState => {
                                                            return { current_page: prevState.current_page + 1 }
                                                        }, () => this.props.getTransactionVehiclePair({ page: this.state.current_page }))    
                                                    }
                                                }} />
                                            </PaginationItem>
                                        </Pagination>
                                    )}
                                </Paginating>
                            </td>
                        </tr>
                    </tfoot>
                </table>
                {loading && !modal &&
                    <RctSectionLoader />
                }
                {/* Modal */}
                <FormVehicleAndDriverPair 
                    title={'Add Vehicle And Driver Pair'}
                    isOpen={modal}
                    className={className}
                    data={formWithData}
                    isLoading={loading} 
                    clearForm={clearForm} />
            </div>
        );
    }
}

const classHistoryDriver = withStyles(styles)(HistoryDriver);

const mapStateToProps = ({ vehicle_pair }) => {
    return vehicle_pair;
}

export default connect(mapStateToProps, {
    getTransactionVehiclePair,
    openFormTransactionVehiclePair
})(classHistoryDriver)