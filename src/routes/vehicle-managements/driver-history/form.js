/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 22:05:15 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 22:16:33
 */
// FIXME: "ERROR PADA SAAT INSERT DATA => Error: Request failed with status code 500"
import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, List, ListItem, ListItemText, Divider, AppBar, Toolbar, IconButton, Slide, TextField, FormControl, InputLabel, Input, InputAdornment, Select, MenuItem, FormHelperText } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
// Moment
import moment from 'moment';
import 'moment/locale/id';
// Date Picker
import { DatePicker } from 'material-ui-pickers';
// Styles
import styles from '../../../lib/styles';
// Actions
import { addTransactionVehiclePair, closeFormTransactionVehiclePair, updateTransactionVehiclePair, getVehicles, getDrivers } from '../../../actions';
// Transition
import Transition from '../../../components/Transition';
// Validation
import FormValidator from '../../../helpers/FormValidation';

class FormVehicleAndDriverPair extends Component {

    validator = new FormValidator([
        {
            field: "vehicle_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Vehicle tidak boleh kosong'
        },
        {
            field: "driver_code",
            method: 'isEmpty',
            validWhen: false,
            message: 'Driver tidak boleh kosong'
        },
        {
            field: "start_date_pair",
            method: 'isEmpty',
            validWhen: false,
            message: 'Start Date tidak boleh kosong'
        },
        {
            field: "end_date_pair",
            method: 'isEmpty',
            validWhen: false,
            message: 'End Date tidak boleh kosong'
        },
    ]);

    submitted = false

    state = {
        vehicle_code: '',
        driver_code: '',
        start_date_pair: moment(),
        end_date_pair: moment(),

        dataVehicle: [],
        dataDriver: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getDrivers({ page: 1 });
        this.props.getVehicles({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.drivers.drivers !== prevProps.drivers.drivers) {
            let master_drivers = this.props.drivers.drivers.data;
            let dataSelect = this.state.dataDriver;
            master_drivers.map((value, key) => {
                dataSelect.push({ value: value.driver_code, label: value.name })
            });
        }
        if(this.props.vehicles.vehicles !== prevProps.vehicles.vehicles) {
            let master_vehicles = this.props.vehicles.vehicles.data;
            let dataSelect = this.state.dataVehicle;
            master_vehicles.map((value, key) => {
                dataSelect.push({ value: value.vehicle_code, label: value.license_plate })
            });
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    vehicle_code: '',
                    driver_code: '',
                    start_date_pair: moment(),
                    end_date_pair: moment(),
                    validation: this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    vehicle_code: this.props.data.vehicle_code,
                    driver_code: this.props.data.driver_code,
                    start_date_pair: moment(this.props.data.start_date_pair),
                    end_date_pair: moment(this.props.data.end_date_pair),
                });
            }
        }
    }

    onSubmit = () => {
        const validation = this.validator.validate(this.state);
        this.setState({ validation });
        this.submitted = true;
        if(this.props.data !== null) {
            this.props.updateTransactionVehiclePair({
                id: this.props.data.transaction_vehicle_pair_code,
                FormData: {
                    vehicle_code: this.state.vehicle_code,
                    driver_code: this.state.driver_code,
                    start_date_pair: moment(this.state.start_date_pair).format('YYYY-MM-DD'),
                    end_date_pair: moment(this.state.end_date_pair).format('YYYY-MM-DD'),
                }
            });
        } else {
            if(validation.isValid) {
                this.props.addTransactionVehiclePair({
                    FormData: {
                        "vehicle_code" : this.state.vehicle_code,
                        "driver_code" : this.state.driver_code,
                        "start_date_pair" : moment(this.state.start_date_pair).format('YYYY-MM-DD'),
                        "end_date_pair" : moment(this.state.end_date_pair).format('YYYY-MM-DD'),
                        "status" : "1"
                    }
                });
            }
        }
    }

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataVehicle, dataDriver } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan pairing antara vehicle dan driver. silahkan daftarkan pairing baru antara vehicle dan driver di sini.
                    </DialogContentText>
                    <form noValidate autoComplete="off">
                        <div className="form-group">
                            <InputLabel htmlFor="role">Vehicle</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.vehicle_code,
                                    onChange: val => this.setState({ vehicle_code: val }),
                                    placeholder: 'Vehicle',
                                    instanceId: 'select-vehicle',
                                    id: 'select-vehicle',
                                    name: 'select-vehicle',
                                    simpleValue: true,
                                    options: dataVehicle.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.vehicle_code.isInvalid && <FormHelperText error={true}>{validation.vehicle_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Driver</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.driver_code,
                                    onChange: val => this.setState({ driver_code: val }),
                                    placeholder: 'Driver',
                                    instanceId: 'select-driver',
                                    id: 'select-driver',
                                    name: 'select-driver',
                                    simpleValue: true,
                                    options: dataDriver.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.driver_code.isInvalid && <FormHelperText error={true}>{validation.driver_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <Fragment>
                                <div className="rct-picker">
                                    <DatePicker
                                        ref={"start_date"}
                                        label="Choose Start Date Pair"
                                        value={this.state.start_date_pair}
                                        onChange={(date) => this.setState({ start_date_pair: date })}
                                        format={'DD/MM/YYYY'}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.bootstrapRootPassword,
                                                input: classes.bootstrapInputPassword,
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end" style={{
                                                    position: 'absolute',
                                                    right: '16px',
                                                    bottom: '7px',
                                                    color: '#999'
                                                }}>
                                                    <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.start_date.open() }>
                                                        <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                        InputLabelProps={{
                                            shrink: true,
                                            className: classes.bootstrapFormLabel,
                                        }}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                        keyboard
                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                    />
                                </div>
                            </Fragment>
                        </div>
                        <div className="form-group">
                            <Fragment>
                                <div className="rct-picker">
                                    <DatePicker
                                        ref={"end_date"}
                                        label="Choose End Date Pair"
                                        value={this.state.end_date_pair}
                                        onChange={(date) => this.setState({ end_date_pair: date })}
                                        format={'DD/MM/YYYY'}
                                        InputProps={{
                                            disableUnderline: true,
                                            classes: {
                                                root: classes.bootstrapRootPassword,
                                                input: classes.bootstrapInputPassword,
                                            },
                                            endAdornment: (
                                                <InputAdornment position="end" style={{
                                                    position: 'absolute',
                                                    right: '16px',
                                                    bottom: '7px',
                                                    color: '#999'
                                                }}>
                                                    <IconButton style={{ marginRight: -16, marginTop: 3 }} onClick={ () => this.refs.end_date.open() }>
                                                        <i className="zmdi zmdi-calendar" style={{ fontSize: '24px', color: '#999999' }}></i>
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                        InputLabelProps={{
                                            shrink: true,
                                            className: classes.bootstrapFormLabel,
                                        }}
                                        animateYearScrolling={false}
                                        leftArrowIcon={<i className="zmdi zmdi-arrow-back" />}
                                        rightArrowIcon={<i className="zmdi zmdi-arrow-forward" />}
                                        fullWidth
                                        keyboard
                                        mask={[/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/]}
                                    />
                                </div>
                            </Fragment>
                        </div>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormTransactionVehiclePair()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit()} color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        )
    }

}

const classFormVehicleAndDriverPair =  withStyles(styles)(FormVehicleAndDriverPair);

const mapStateToProps = ({ newPairing, vehicles, drivers }) => {
    return { newPairing, vehicles, drivers };
}

export default connect(mapStateToProps, {
    addTransactionVehiclePair,
    closeFormTransactionVehiclePair,
    updateTransactionVehiclePair,
    getDrivers,
    getVehicles
})(classFormVehicleAndDriverPair);