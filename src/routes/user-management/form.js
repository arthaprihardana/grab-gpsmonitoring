/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-19 21:01:18 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-02 22:55:17
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, DialogContent, DialogContentText, DialogTitle, DialogActions, IconButton, TextField, InputLabel, Input, InputAdornment, FormHelperText, AppBar, Toolbar } from '@material-ui/core';
import { Close as CloseIcon} from '@material-ui/icons';
import { withStyles } from '@material-ui/core/styles';
// rct section loader
import RctSectionLoader from 'Components/RctSectionLoader/RctSectionLoader';
// Rct Select Wrapped
import RctSelect from 'Components/RctSelect/RctSelect';
import Select from 'react-select';
// styles
import styles from '../../lib/styles';
// Transition
import Transition from '../../components/Transition';
// Actions
import { addUserProfile, closeFormUserProfile, updateUserProfile, getRoles, getNotifications } from '../../actions';
// Validation
import FormValidator from '../../helpers/FormValidation';

class FormMasterUser extends Component {

    validator = new FormValidator([
        {
            field: 'email',
            method: 'isEmpty',
            validWhen: false,
            message: 'Email tidak boleh kosong'
        },
        {
            field: 'password',
            method: 'isEmpty',
            validWhen: false,
            message: 'Password tidak boleh kosong'
        },
        {
            field: 'first_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'First name tidak boleh kosong'
        },
        {
            field: 'last_name',
            method: 'isEmpty',
            validWhen: false,
            message: 'Last name tidak boleh kosong'
        },
        {
            field: 'no_telp',
            method: 'isEmpty',
            validWhen: false,
            message: 'No Telepon tidak boleh kosong'
        },
        {
            field: 'identity',
            method: 'isEmpty',
            validWhen: false,
            message: 'No Identitas / KTP tidak boleh kosong'
        },
        {
            field: 'telegram',
            method: 'isEmpty',
            validWhen: false,
            message: 'Telegram tidak boleh kosong'
        },
        {
            field: 'role_code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Role tidak boleh kosong'
        },
        {
            field: 'notification_code',
            method: 'isEmpty',
            validWhen: false,
            message: 'Notification code tidak boleh kosong'
        },
    ])

    submitted = false

    state = {
        "email" : '',
        "password" : '',
        "first_name" : '',
        "last_name" : '',
        "no_telp" : '',
        "identity" : '',
        "telegram" : '',
        "role_code" : '',
        "notification_code" : '',
        
        showPassword: false,
        dataRole: [],
        dataNotification: [],
        validation: this.validator.valid()
    }

    componentDidMount() {
        this.props.getNotifications({ page: 1 });
        this.props.getRoles({ page: 1 });
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.notifications.notifications !== prevProps.notifications.notifications) {
            let master_notifications = this.props.notifications.notifications.data;
            let dataSelect = this.state.dataNotification;
            master_notifications.map((value, key) => {
                dataSelect.push({value: value.notification_code, label: value.notification_name})
            });
        }
        if(this.props.roles.roles !== prevProps.roles.roles) {
            let master_roles = this.props.roles.roles.data;
            let dataSelect = this.state.dataRole;
            master_roles.map((value, key) => {
                dataSelect.push({value: value.role_code, label: value.role_name})
            });
        }
        if(this.props.clearForm !== prevProps.clearForm) {
            if(this.props.clearForm) {
                this.setState({
                    "email" : '',
                    "password" : '',
                    "first_name" : '',
                    "last_name" : '',
                    "no_telp" : '',
                    "identity" : '',
                    "telegram" : '',
                    "role_code" : '',
                    "notification_code" : '',
                    "validation": this.validator.valid()
                }, () => this.forceUpdate());
                this.submitted = false;
            }
        }
        if(this.props.data !== prevProps.data) {
            if(this.props.data !== null) {
                this.setState({
                    "email" : this.props.data.email,
                    "first_name" : this.props.data.first_name,
                    "last_name" : this.props.data.last_name,
                    "no_telp" : this.props.data.no_telp,
                    "identity" : this.props.data.identity,
                    "telegram" : this.props.data.telegram,
                    "role_code" : this.props.data.role_code,
                    "notification_code" : this.props.data.notification_code,
                })
            }
        }
    }

    onSubmit = () => {
        if(this.props.data !== null) {
            this.props.updateUserProfile({
                id: this.props.data.user_profile_code,
                FormData: {
                    "email" : this.state.email,
                    "password" : this.state.password,
                    "first_name" : this.state.first_name,
                    "last_name" : this.state.last_name,
                    "no_telp" : this.state.no_telp,
                    "identity" : this.state.identity,
                    "telegram" : this.state.telegram,
                    "role_code" : this.state.role_code,
                    "notification_code" : this.state.notification_code
                }
            })
        } else {
            const validation = this.validator.validate(this.state);
            this.setState({ validation });
            this.submitted = true;
            if(validation.isValid) {
                this.props.addUserProfile({
                    FormData: {
                        "email" : this.state.email,
                        "password" : this.state.password,
                        "first_name" : this.state.first_name,
                        "last_name" : this.state.last_name,
                        "no_telp" : this.state.no_telp,
                        "identity" : this.state.identity,
                        "telegram" : this.state.telegram,
                        "role_code" : this.state.role_code,
                        "notification_code" : this.state.notification_code,
                        "status": "1"
                    }
                })
            }
        }
    }

    handleChange = prop => event => {
		this.setState({ [prop]: event.target.value });
	};

	handleClickShowPasssword = () => {
		this.setState({ showPassword: !this.state.showPassword });
	};

    render() {
        const { isOpen, title, data, isLoading, classes } = this.props;
        const { dataNotification, dataRole } = this.state;
        let validation = this.submitted ? this.validator.validate(this.state) : this.state.validation;
        return (
            <Dialog
                open={isOpen}
                TransitionComponent={Transition}
                scroll={"body"}
                disableBackdropClick={true}>
                <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                <IconButton color="inherit" onClick={() => this.props.closeFormUserProfile()} aria-label="Close" style={{ position: 'absolute', top: 10, right: 10}}>
                    <CloseIcon style={{ color: '#999999'}} />
                </IconButton>
                <DialogContent>
                    <DialogContentText style={{ marginBottom: 20 }}>
                        Untuk menambahkan pengguna yang dapat mengakses aplikasi. silahkan daftarkan pengguna baru di sini.
                    </DialogContentText>
                    <form autoComplete="off">
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.email : ""}
                                label="Email Address"
                                id="email-address"
                                error={validation.email.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ email: event.target.value }) } 
                                helperText={validation.email.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.first_name : ""}
                                label="First Name"
                                id="first-name"
                                error={validation.first_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ first_name: event.target.value }) } 
                                helperText={validation.first_name.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.last_name : ""}
                                label="Last Name"
                                id="last-name"
                                error={validation.last_name.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ last_name: event.target.value }) } 
                                helperText={validation.last_name.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.no_telp : ""}
                                label="No Telephone"
                                id="no-telp"
                                error={validation.no_telp.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ no_telp: event.target.value }) } 
                                helperText={validation.no_telp.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.telegram : ""}
                                label="Telegram"
                                id="telegram"
                                error={validation.telegram.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ telegram: event.target.value }) } 
                                helperText={validation.telegram.message}
                            />
                        </div>
                        <div className="form-group">
                            <TextField
                                defaultValue={data !== null ? data.identity : ""}
                                label="No Identity"
                                id="no-identity"
                                error={validation.identity.isInvalid ? true : false}
                                fullWidth={true}
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRoot,
                                        input: classes.bootstrapInput,
                                    },
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                // style={{marginTop: 20}}
                                margin="none"
                                onChange={(event) => this.setState({ identity: event.target.value }) } 
                                helperText={validation.identity.message}
                            />
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Role</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.role_code,
                                    onChange: val => this.setState({ role_code: val }),
                                    placeholder: 'Select Role',
                                    instanceId: 'select-role',
                                    id: 'select-role',
                                    name: 'select-role',
                                    simpleValue: true,
                                    options: dataRole.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                }}
                            />
                            {validation.role_code.isInvalid && <FormHelperText error={true}>{validation.role_code.message}</FormHelperText> }
                        </div>
                        <div className="form-group">
                            <InputLabel htmlFor="role">Choose Notification</InputLabel>
                            <Input
                                fullWidth
                                inputComponent={RctSelect}
                                disableUnderline={true}
                                inputProps={{
                                    classes,
                                    value: this.state.notification_code,
                                    multi: true,
                                    onChange: val => {
                                        let notif = val.map((v, k) => {
                                            return v.value;
                                        });
                                        this.setState({ notification_code: notif.join(",") })
                                    },
                                    placeholder: 'Select Notification',
                                    instanceId: 'select-notification',
                                    id: 'select-notification',
                                    name: 'select-notification',
                                    // simpleValue: true,
                                    options: dataNotification.map(value => ({
                                        label: value.label,
                                        value: value.value
                                    })),
                                    style: {
                                        padding: this.state.notification_code.length > 0 ? '3px 12px' : '10px 12px'
                                    }
                                }}
                            />
                            {validation.notification_code.isInvalid && <FormHelperText error={true}>{validation.notification_code.message}</FormHelperText> }
                        </div>
                        {data !== null ? <div /> : 
                        <div className="form-group">
                            <TextField
                                fullWidth
                                type={this.state.showPassword ? 'text' : 'password'}
                                error={validation.password.isInvalid ? true : false}
                                id="password"
                                label="Password"
                                margin="none"
                                InputProps={{
                                    disableUnderline: true,
                                    classes: {
                                        root: classes.bootstrapRootPassword,
                                        input: classes.bootstrapInputPassword,
                                    },
                                    endAdornment: (
                                        <InputAdornment position="end" style={{ marginTop: '6px'}}>
                                            <IconButton onClick={this.handleClickShowPasssword}>
                                                {this.state.showPassword ? <i className="zmdi zmdi-eye-off"></i> : <i className="zmdi zmdi-eye"></i>}
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                                InputLabelProps={{
                                    shrink: true,
                                    className: classes.bootstrapFormLabel,
                                }}
                                onChange={(event) => this.setState({ password: event.target.value }) } 
                                helperText={validation.password.message}
                                />
                        </div>
                        }
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.props.closeFormUserProfile()} color="primary">Cancel</Button>
                    <Button onClick={() => this.onSubmit() } color="primary">{data !== null ? "Update" : "Save"}</Button>
                    {isLoading && isOpen &&
                        <RctSectionLoader /> }
                </DialogActions>
            </Dialog>
        )
    }
}

const classFormMasterUser = withStyles(styles)(FormMasterUser);

const mapStateToProps = ({ newUser, notifications, roles }) => {
    return { newUser, notifications, roles }
}

export default connect(mapStateToProps, {
    addUserProfile,
    closeFormUserProfile,
    updateUserProfile,
    getNotifications,
    getRoles
})(classFormMasterUser)