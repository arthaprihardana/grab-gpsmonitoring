/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 16:43:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-24 00:48:26
 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { LinearProgress } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Form, FormGroup, Input, Button } from 'reactstrap';

// app config
import AppConfig from 'Constants/AppConfig';

// redux action
import {
    signIn,
    generatePasswordToken
} from 'Actions';
import {GREEN_500} from '../assets/colors';

/**
 * @description Class Sign In
 *
 * @class Signin
 * @extends {Component}
 */
class Signin extends Component {
    
	state = {
        // email: 'artha.projects@gmail.com',
        // password: '12345',
        email: '',
        password: '',
        showPassword: false
	}

	/**
	 * On User Login
	 */
	onUserLogin() {
		if (this.state.email !== '' && this.state.password !== '') {
            this.props.signIn(this.state, this.props.history);
            this.props.generatePasswordToken("password");
		}
	}

    _handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            this.onUserLogin()
        }
    }

	render() {
		const { email, password } = this.state;
		const { loading } = this.props;
		return (
            <div className="rct-session-wrapper">
                {loading &&
                    <LinearProgress />
                }
                <div className="container-login">
                    <div className="row">
                        <div className="col-12 col-md-6 col-lg-8 bg">

                        </div>
                        <div className="col-12 col-md-6 col-lg-4 side">
                            <div className="content">
                                <div className="brand-header">
                                    <Link to="/" className="logo">
                                        <img src={AppConfig.appLogo} alt="session-logo" />
                                        <h2 style={{ letterSpacing: 2, fontWeight: 400, color: '#00BA51', lineHeight: 2.1 }}>GPS Monitoring</h2>
                                    </Link>
                                </div>
                                <Form autoComplete="off">
                                    <FormGroup className="has-wrapper">
                                        <Input
                                            type="mail"
                                            value={email}
                                            name="user-mail"
                                            id="user-mail"
                                            className="has-input input-lg"
                                            placeholder="Enter Email Address"
                                            onChange={(event) => this.setState({ email: event.target.value })}
                                        />
                                    </FormGroup>
                                    <FormGroup className="has-wrapper">
                                        <Input
                                            value={password}
                                            type={this.state.showPassword ? "text" : "password"}
                                            name="user-pwd"
                                            id="pwd"
                                            className="has-input input-lg"
                                            placeholder="Password"
                                            onChange={(event) => this.setState({ password: event.target.value })}
                                            onKeyPress={this._handleKeyPress}
                                        />
                                        <span className="has-icon" onClick={() => this.setState({ showPassword: !this.state.showPassword})}>
                                            {this.state.showPassword ? <i className="ti-unlock" style={{ color: GREEN_500 }}></i> : <i className="ti-lock"></i> }
                                        </span>
                                    </FormGroup>
                                    <FormGroup className="mb-15">
                                        <Button className="mr-10 mb-10" outline color="success" size="lg" block onClick={() => this.onUserLogin() }>
                                            Sign In
                                        </Button>
                                    </FormGroup>
                                </Form>
                            </div>
                            
                            <div className="content-bottom text-center">
                                <p className="margin-bottom0">
                                    Copyright © 2018 GRAB<br/>
                                    All rights reserved
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		);
	}
}

// map state to props
const mapStateToProps = ({ authUser }) => {
	const { user, loading, authMiddleware } = authUser;
	return { user, loading, authMiddleware }
}

export default connect(mapStateToProps, {
    signIn,
    generatePasswordToken
})(Signin);
