/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-16 09:12:02 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 07:43:02
 */
/**
 * AsyncComponent
 * Code Splitting Component / Server Side Rendering
 */
import React from 'react';
import Loadable from 'react-loadable';

// rct page loader
import RctPageLoader from 'Components/RctPageLoader/RctPageLoader';

// Start of Monitoring 
const AsyncTracking = Loadable({
	loader: () => import('Routes/monitoring/tracking'),
	loading: () => <div style={{ marginTop: 30 }}><RctPageLoader /></div>,
});

const AsyncPlayBack = Loadable({
	loader: () => import('Routes/monitoring/play-back'),
	loading: () => <div style={{ marginTop: 30 }}><RctPageLoader /></div>,
});
// End of Monitoring

// Start of Vehicle Management
const AsyncVehicleManagement = Loadable({
	loader: () => import('Routes/vehicle-managements'),
	loading: () => <RctPageLoader />,
});
// End of Vehicle Management

// Start of Master
const AsyncMasterRoles = Loadable({
	loader: () => import('Routes/master/master-roles'),
	loading: () => <RctPageLoader />,
});

const AsyncMasterArea = Loadable({
	loader: () => import('Routes/master/master-area'),
	loading: () => <RctPageLoader />,
});

const AsyncMasterBrandVehicle = Loadable({
	loader: () => import('Routes/master/master-brand-vehicle'),
	loading: () => <RctPageLoader />,
});

const AsyncMasterNotification = Loadable({
	loader: () => import('Routes/master/master-notification'),
	loading: () => <RctPageLoader />,
});

const AsyncMasterStatusVehicles = Loadable({
	loader: () => import('Routes/master/master-status-vehicle'),
	loading: () => <RctPageLoader />,
});

const AsyncRolePairArea = Loadable({
	loader: () => import('Routes/master/role-pair-area'),
	loading: () => <RctPageLoader />
})

const AsyncZoneManagement = Loadable({
	loader: () => import('Routes/master/zone-management'),
	loading: () => <RctPageLoader />,
});
// End of Master

// Start of Configuration
const AsyncAlertNotification = Loadable({
	loader: () => import('Routes/configuration/alert-notification'),
	loading: () => <RctPageLoader />,
});

const AsyncListsAlert = Loadable({
	loader: () => import('Routes/configuration/lists-alert'),
	loading: () => <RctPageLoader />,
});
// End of Configuration

// Start of Provisioning
const AsyncDevice = Loadable({
	loader: () => import('Routes/provisioning/device'),
	loading: () => <RctPageLoader />
});
const AsyncDeviceIOMapper = Loadable({
	loader: () => import('Routes/provisioning/deviceIOMapper'),
	loading: () => <RctPageLoader />
});
// End of Provisioning

// Start of Session Page 404
const AsyncSessionPage404Component = Loadable({
	loader: () => import("Routes/session/404"),
	loading: () => <RctPageLoader />,
});
// End of Session Page 404

// Start of Session Page 500
const AsyncSessionPage500Component = Loadable({
	loader: () => import("Routes/session/500"),
	loading: () => <RctPageLoader />,
});
// End of Session Page 500

export {
	AsyncZoneManagement,
	AsyncPlayBack,
	AsyncVehicleManagement,
	AsyncMasterRoles,
	AsyncMasterArea,
	AsyncRolePairArea,
	AsyncMasterBrandVehicle,
	AsyncMasterNotification,
	AsyncMasterStatusVehicles,
	AsyncAlertNotification,
	AsyncListsAlert,
	AsyncDevice,
	AsyncDeviceIOMapper,
	AsyncTracking,

	AsyncSessionPage404Component,
	AsyncSessionPage500Component
};
