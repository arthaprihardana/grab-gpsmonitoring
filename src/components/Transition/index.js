/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-29 00:58:37 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-29 00:59:43
 */
import React from 'react';
import { Slide } from '@material-ui/core';

const Transition = props => (
    <Slide direction="up" {...props} />
);

export default Transition;