/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-17 19:42:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-19 20:23:21
 */
import React, { Component } from 'react';
import {Typography, MenuItem, Chip} from '@material-ui/core';
import { ArrowDropDown as ArrowDropDownIcon, ArrowDropUp as ArrowDropUpIcon, Clear as ClearIcon, Cancel as CancelIcon } from '@material-ui/icons';
import Select from 'react-select';

class Option extends Component {
    handleClick = event => {
      this.props.onSelect(this.props.option, event);
    };
  
    render() {
        const { children, isFocused, isSelected, onFocus } = this.props;
        return (
            <MenuItem
                onFocus={onFocus}
                selected={isFocused}
                onClick={this.handleClick}
                component="div"
                style={{
                    fontWeight: isSelected ? 500 : 400,
                }}
                >
                {children}
            </MenuItem>
        );
    }
}
  
function SelectWrapped(props) {
    const { classes, isMulti, ...other } = props;
    return (
        <Select
            optionComponent={Option}
            noResultsText={<Typography>{'No results found'}</Typography>}
            arrowRenderer={arrowProps => {
                return arrowProps.isOpen ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />;
            }}
            clearRenderer={() => <ClearIcon style={{ fontSize: 18 }} />}
            valueComponent={valueProps => {
                const { value, children, onRemove } = valueProps;
                const onDelete = event => {
                    event.preventDefault();
                    event.stopPropagation();
                    onRemove(value);
                };
                if (onRemove) {
                    return (
                    <Chip
                        tabIndex={-1}
                        label={children}
                        className={classes.chip}
                        deleteIcon={<CancelIcon onTouchEnd={onDelete} />}
                        onDelete={onDelete}
                    />
                    );
                }
                return <div className="Select-value">{children}</div>;
            }}
            {...other}
        />
    );
}

export default SelectWrapped;