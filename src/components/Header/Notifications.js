/**
 * Notification Component
 */
import React, { Component } from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import Button from '@material-ui/core/Button';
import { Badge } from 'reactstrap';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

// intl messages
import IntlMessages from 'Util/IntlMessages';

class Notifications extends Component {

  state = {
    notifications: null
  }

  render() {
    const { notifications } = this.state;
    return (
      <UncontrolledDropdown nav className="list-inline-item notification-dropdown">
        <DropdownToggle nav className="p-0">
          <Tooltip title="Notifications" placement="bottom">
            <IconButton aria-label="bell">
              <i className="zmdi zmdi-notifications-active"></i>
              {/* <Badge color="danger" className="badge-xs badge-top-right rct-notify">2</Badge> */}
            </IconButton>
          </Tooltip>
        </DropdownToggle>
      </UncontrolledDropdown>
    );
  }
}

export default Notifications;
