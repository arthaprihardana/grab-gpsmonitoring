/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 17:12:03 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 07:51:05
 */
export default {
	category1: [
		{
			"menu_title": "sidebar.dashboard",
			"menu_icon": "zmdi zmdi-view-dashboard",
			"path": "/app/dashboard",
			"child_routes": null
		},
		{
			"menu_title": "sidebar.monitoring",
			"menu_icon": "zmdi zmdi-cast-connected",
			"child_routes": [
				{
					"menu_title": "sidebar.tracking",
					"path": "/app/monitoring/tracking"
				},
				{
					"menu_title": "sidebar.playBack",
					"path": "/app/monitoring/play-back"
				}
			]
		},
		{
			"menu_title": "sidebar.report",
			"menu_icon": "zmdi zmdi-chart",
			"path": "/app/report",
			"child_routes": null
		},
		{
			"menu_title": "sidebar.userManagement",
			"menu_icon": "zmdi zmdi-accounts",
			"path": "/app/user-management",
			"child_routes": null
		},
		{
			"menu_title": "sidebar.vehicleManagements",
			"menu_icon": "zmdi zmdi-directions-car",
			"path": "/app/vehicle-managements",
			"child_routes": null
		},
		// {
		// 	"menu_title": "sidebar.vehicleManagement",
		// 	"menu_icon": "zmdi zmdi-directions-car",
		// 	"child_routes": [
		// 		{
		// 			"menu_title": "sidebar.masterVehicle",
		// 			"path": "/app/vehicle-management/master-vehicle"
		// 		},
		// 		{
		// 			"menu_title": "sidebar.vehicleAndDriverPair",
		// 			"path": "/app/vehicle-management/vehicle-and-driver-pair"
		// 		},
		// 		{
		// 			"menu_title": "sidebar.maintenanceVehicle",
		// 			"path": "/app/vehicle-management/maintenance-vehicle"
		// 		}
		// 	]
		// },
		{
			"menu_title": "sidebar.driverManagement",
			"menu_icon": "zmdi zmdi-accounts-list",
			"path": "/app/driver-management",
			"child_routes": null
		},
		{
			"menu_title": "sidebar.master",
			"menu_icon": "zmdi zmdi-case",
			"child_routes": [
				{
					"menu_title": "sidebar.masterRoles",
					"path": "/app/master/master-roles"
				},
				{
					"menu_title": "sidebar.masterArea",
					"path": "/app/master/master-area"
				},
				{
					"menu_title": "sidebar.rolePairArea",
					"path": "/app/master/master-role-pair-area"
				},
				{
					"menu_title": "sidebar.masterBrandVehicle",
					"path": "/app/master/master-brand-vehicle"
				},
				{
					"menu_title": "sidebar.masterStatusVehicle",
					"path": "/app/master/master-status-vehicle"
				},
				{
					"menu_title": "sidebar.masterNotification",
					"path": "/app/master/master-notification"
				},
				{
					"menu_title": "sidebar.zoneManagement",
					"path": "/app/master/zone-management"
				},
			]
		},
		{
			"menu_title": "sidebar.configuration",
			"menu_icon": "zmdi zmdi-settings-square",
			"child_routes": [
				{
					"menu_title": "sidebar.alertNotification",
					"path": "/app/configuration/alert-notification"
				},
				// {
				// 	"menu_title": "sidebar.listsAlert",
				// 	"path": "/app/configuration/lists-alert"
				// }
			]
		},
		{
			"menu_title": "sidebar.provisioning",
			"menu_icon": "zmdi zmdi-devices",
			"child_routes": [
				{
					"menu_title": "sidebar.device",
					"path": "/app/provisioning/device"
				},
				{
					"menu_title": "sidebar.deviceIOMapper",
					"path": "/app/provisioning/device-io-mapper"
				}
			]
		}
	]
}