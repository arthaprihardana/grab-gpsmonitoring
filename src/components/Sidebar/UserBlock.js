/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-15 19:50:57 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-24 07:15:59
 */
/**
 * User Block Component
 */
import React, { Component } from 'react';
import { Dropdown, DropdownToggle, DropdownMenu } from 'reactstrap';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';

// redux action
import { logoutUser } from 'Actions';

// intl messages
import IntlMessages from 'Util/IntlMessages';
import { GREY_900 } from '../../assets/colors';

/**
 * @description UserBlock yang ditampilkan di atas menu sidebar
 *
 * @class UserBlock
 * @extends {Component}
 */
class UserBlock extends Component {

	state = {
		userDropdownMenu: false,
	}

	/**
	 * Logout User
	 */
	logoutUser() {
		this.props.logoutUser();
	}

	/**
	 * Toggle User Dropdown Menu
	 */
	toggleUserDropdownMenu() {
		this.setState({ userDropdownMenu: !this.state.userDropdownMenu });
	}

	render() {
		const { user } = this.props;
		return (
			<div className="top-sidebar">
				<div className="sidebar-user-block">
					<Dropdown
						isOpen={this.state.userDropdownMenu}
						toggle={() => this.toggleUserDropdownMenu()}
						className="rct-dropdown"
					>
						<DropdownToggle
							tag="div"
							className="d-flex align-items-center"
						>
							<div className="user-profile">
								<img
									src={require('Assets/avatars/default.png')}
									alt="user profile"
									className="img-fluid rounded-circle"
									width="50"
									height="100"
								/>
							</div>
							<div className="user-info">
								<span className="user-name ml-4">{_.capitalize(user.first_name)} {_.capitalize(user.last_name)}</span>
								<i className="zmdi zmdi-chevron-down dropdown-icon mx-4"></i>
							</div>
						</DropdownToggle>
						<DropdownMenu>
							<ul className="list-unstyled mb-0">
								<li>
									<Link to={{
										pathname: '/app/users/user-profile-1',
										state: { activeTab: 0 }
									}}>
										<i className="ti-user mr-3" style={{ color: GREY_900 }}></i>
										<IntlMessages id="widgets.profile" />
									</Link>
								</li>
								<li>
									<a href="javascript:void(0)" onClick={() => this.logoutUser()}>
										<i className="zmdi zmdi-power mr-3" style={{ color: GREY_900 }}></i>
										<IntlMessages id="widgets.logOut" />
									</a>
								</li>
							</ul>
						</DropdownMenu>
					</Dropdown>
				</div>
			</div>
		);
	}
}

// map state to props
const mapStateToProps = ({ settings, authUser }) => {
	const { user } = authUser;
	return { settings, user };
}

export default connect(mapStateToProps, {
	logoutUser
})(UserBlock);
