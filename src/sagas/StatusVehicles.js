/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:43:32 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 07:10:27
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {VEHICLE_STATUS, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getStatusVehiclesSuccess, getStatusVehiclesFailure, addStatusVehicleSuccess, addStatusVehicleFailure, updateStatusVehicleSuccess, updateStatusVehicleFailure} from '../actions/MsStatusVehiclesActions';
import {GET_STATUS_VEHICLES, ADD_STATUS_VEHICLES, UPDATE_STATUS_VEHICLES} from '../actions/types';

const getStatusVehiclesRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${VEHICLE_STATUS}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(response => response)
        .catch(error => error);
}
function* getStatusVehiclesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getStatusVehiclesRequest, pagination);
        if(response.data) {
            yield put(getStatusVehiclesSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_STATUS_VEHICLES, getStatusVehiclesFromServer);
        }
    } catch (error) {
        yield put(getStatusVehiclesFailure(error));
    }
}

export function* getStatusVehicles() {
    yield takeEvery(GET_STATUS_VEHICLES, getStatusVehiclesFromServer);
}

const addStatusVehicleRequest = async (FormData) => {
    console.log('updateStatusVehicle', FormData.status_vehicle_name);
    let token = localStorage.getItem('token');
    return await axios.post(VEHICLE_STATUS, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .then(error => error);
}

function* addStatusVehicleToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addStatusVehicleRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addStatusVehicleSuccess(data));
        } else {
            yield put(addStatusVehicleFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addStatusVehicleFailure(error));
    }
}

export function* addStatusVehicle() {
    yield takeEvery(ADD_STATUS_VEHICLES, addStatusVehicleToServer);
}

const updateStatusVehicleRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${VEHICLE_STATUS}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateStatusVehicleToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateStatusVehicleRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateStatusVehicleSuccess(data));
        } else {
            yield put(updateStatusVehicleFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateStatusVehicleFailure(error));
    }
}

export function* updateStatusVehicle() {
    yield takeEvery(UPDATE_STATUS_VEHICLES, updateStatusVehicleToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getStatusVehicles),
        fork(addStatusVehicle),
        fork(updateStatusVehicle)
    ])
}