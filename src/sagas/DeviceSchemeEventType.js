/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 15:34:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:41:00
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_SCHEME_EVENT_TYPE, API_GENERATE_REFRESH_TOKEN } from '../constants/LibraryAPI';
import { GET_DEVICE_SCHEME_EVENT_TYPE } from '../actions/types';
import { getDeviceSchemeEventTypesSuccess, getDeviceSchemeEventTypesFailure } from '../actions';

const getDeviceSchemeEventTypeRequest = async (pagination) => {
    const {page} = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_SCHEME_EVENT_TYPE}?query=xxxx`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDeviceSchemeEventTypeFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDeviceSchemeEventTypeRequest, pagination);
        if(response.data) {
            yield put(getDeviceSchemeEventTypesSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE_SCHEME_EVENT_TYPE, getDeviceSchemeEventTypeFromServer);
        }
    } catch (error) {
        yield put(getDeviceSchemeEventTypesFailure(error));
    }
}

export function* getDeviceSchemeEventType() {
    yield takeEvery(GET_DEVICE_SCHEME_EVENT_TYPE, getDeviceSchemeEventTypeFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDeviceSchemeEventType)
    ])
}