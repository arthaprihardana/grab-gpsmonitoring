/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-27 06:27:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-07 09:14:38
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {ZONE, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getZoneSuccess, getZoneFailure, addZoneSuccess, addZoneFailure, updateZoneSuccess, updateZoneFailure} from '../actions/MsZoneActions';
import {GET_MASTER_ZONE, ADD_MASTER_ZONE, UPDATE_MASTER_ZONE} from '../actions/types';

const getZoneRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${ZONE}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getZoneFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getZoneRequest, pagination);
        if(response.data) {
            yield put(getZoneSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_MASTER_ZONE, getZoneFromServer);
        }
    } catch (error) {
        yield put(getZoneFailure(error));
    }
}

export function* getZone() {
    yield takeEvery(GET_MASTER_ZONE, getZoneFromServer);
}

const addZoneRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(ZONE, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addZoneToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addZoneRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addZoneSuccess(data));
        } else {
            yield put(addZoneFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addZoneFailure(error));
    }
}

export function* addZone() {
    yield takeEvery(ADD_MASTER_ZONE, addZoneToServer);
}

const updateZoneRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${ZONE}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateZoneToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateZoneRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateZoneSuccess(data));
        } else {
            yield put(updateZoneFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateZoneFailure(error));
    }
}

export function* updateZone() {
    yield takeEvery(UPDATE_MASTER_ZONE, updateZoneToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getZone),
        fork(addZone),
        fork(updateZone)
    ])
}