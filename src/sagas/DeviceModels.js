/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 22:36:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:34:47
 */
import {all, call, fork, put, takeEvery} from 'redux-saga/effects';
import axios from 'axios';
import { getDeviceModelsSuccess, getDeviceModelsFailure, addDeviceModelsSuccess, addDeviceModelsFailure, updateDeviceModelsSuccess, updateDeviceModelsFailure } from '../actions/DeviceModelsActions';
import {API_DEVICE_MODELS, API_GENERATE_REFRESH_TOKEN} from '../constants/LibraryAPI';
import { GET_DEVICE_MODELS, ADD_DEVICE_MODELS, UPDATE_DEVICE_MODELS } from '../actions/types';

const getDeviceModalsRequest = async (pagination) => {
    const { page } = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_DEVICE_MODELS}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDeviceModalsFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDeviceModalsRequest, pagination);
        if(response.data) {
            yield put(getDeviceModelsSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE_MODELS, getDeviceModalsFromServer);
        }
    } catch (error) {
        yield put(getDeviceModelsFailure(error));
    }
}

export function* getDeviceModels() {
    yield takeEvery(GET_DEVICE_MODELS, getDeviceModalsFromServer);
}

const addDeviceModelsRequest = async (FormData) => {
    let token = localStorage.getItem('access_token');
    return await axios.post(API_DEVICE_MODELS, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addDeviceModelsToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addDeviceModelsRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addDeviceModelsSuccess(data));
        } else {
            yield put(addDeviceModelsFailure(add.toString()));
        }
    } catch (error) {
        yield put(addDeviceModelsFailure(error));
    }
}

export function* addDeviceModels() {
    yield takeEvery(ADD_DEVICE_MODELS, addDeviceModelsToServer);
}

const updateDeviceModelsRequest = async (id, FormData) => {
    let token = localStorage.getItem('access_token');
    return await axios.put(`${API_DEVICE_MODELS}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDeviceModelsToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateDeviceModelsRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateDeviceModelsSuccess(update));
        } else {
            yield put(updateDeviceModelsFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateDeviceModelsFailure(error));
    }
}

export function* updateDeviceModels() {
    yield takeEvery(UPDATE_DEVICE_MODELS, updateDeviceModelsToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDeviceModels),
        fork(addDeviceModels),
        fork(updateDeviceModels)
    ])
}