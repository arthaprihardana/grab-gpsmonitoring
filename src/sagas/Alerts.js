/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 13:24:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-27 07:51:58
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {ALERT, REFRESH_TOKEN, API_GET_DETAIL_ALERT_SUMMARY} from '../constants/LibraryAPI';
import {getAlertsSuccess, getAlertsFailure, addAlertSuccess, addAlertFailure, updateAlertSuccess, updateAlertFailure, getDetailAlertSummarySuccess, getDetailAlertSummaryFailure} from '../actions/MsAlertsActions';
import {GET_ALERTS, ADD_ALERTS, UPDATE_ALERTS, GET_DETAIL_ALERT_SUMMARY} from '../actions/types';

const getAlertsRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${ALERT}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getAlertsFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getAlertsRequest, pagination);
        if(response.data) {
            yield put(getAlertsSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_ALERTS, getAlertsFromServer);
        }
    } catch (error) {
        yield put(getAlertsFailure(error));
    }
}

export function* getAlerts() {
    yield takeEvery(GET_ALERTS, getAlertsFromServer);
}

const addAlertRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(ALERT, {
        alert_name: FormData.alert_name,
        notification_code: FormData.notification_code
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addAlertToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addAlertRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addAlertSuccess(data));
        } else {
            yield put(addAlertFailure(add.toString()))
        }
    } catch (error) {
        yield put(addAlertFailure(error))
    }
}

export function* addAlert() {
    yield takeEvery(ADD_ALERTS, addAlertToServer);
}

const updateAlertRequest = async(id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${ALERT}/${id}`, {
        alert_name: FormData.alert_name,
        notification_code: FormData.notification_code
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error)
}

function* updateAlertToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateAlertRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateAlertSuccess(data));
        } else {
            yield put(updateAlertFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateAlertFailure(error));
    }
}

export function* updateAlert() {
    yield takeEvery(UPDATE_ALERTS, updateAlertToServer);
}

const getDetailAlertSummaryRequest = async(alertPriority) => {
    return await axios.get(`${API_GET_DETAIL_ALERT_SUMMARY}?alertPriority=${alertPriority}`, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
    .then(response => response)
    .catch(error => error)
}

function* getDetailAlertSummaryFromServer({ payload }) {
    try {
        const response = yield call(getDetailAlertSummaryRequest, payload);
        yield put(getDetailAlertSummarySuccess(response));
    } catch (error) {
        yield put(getDetailAlertSummaryFailure(error));
    }
}

export function* getDetailAlertSummarySaga() {
    yield takeEvery(GET_DETAIL_ALERT_SUMMARY, getDetailAlertSummaryFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getAlerts),
        fork(addAlert),
        fork(updateAlert),
        fork(getDetailAlertSummarySaga)
    ])
}