/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-20 07:12:39 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 11:45:17
 */
/**
 * Auth Sagas
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import {
    LOGIN_USER,
    LOGOUT_USER,
    GENERATE_PASSWORD_TOKEN
} from 'Actions/types';

import {
    signinUserSuccess,
    signinUserFailure,
    logoutUserSuccess,
    logoutUserFailure,
    generatePasswordTokenSuccess, 
    generatePasswordTokenFailure
} from 'Actions';
import axios from 'axios';
import {LOGIN, API_GENERATE_PASSWORD_TOKEN, GPS_MIDDLEWARE_OAUTH_REQUEST} from '../constants/LibraryAPI';

/**
 * @description Sigin User With Email and Password Request
 * @const signInUserWithEmailPasswordRequest
 * @async
 * @param {string} email
 * @param {string} password
 * @returns
 */
const signInUserWithEmailPasswordRequest = async (email, password) => {
    return await axios.post(LOGIN, { email: email, password: password })
        .then(authUser => authUser)
        .catch(error => error)
}

/**
 * @description Signin User With Email & Password
 * @method signInUserWithEmailPassword
 * @generator
 * @param {object} payload
 */
function* signInUserWithEmailPassword({ payload }) {
    const { email, password } = payload.user;
    const { history } = payload;
    try {
        const signInUser  = yield call(signInUserWithEmailPasswordRequest, email, password);
        if(signInUser.statusText === "OK") {
            let data = signInUser.data.Data;
            localStorage.setItem('user_id', JSON.stringify(data.user_data));
            localStorage.setItem('token', data.token);
            yield put(signinUserSuccess(data.user_data));
            history.push('/');
        } else {
            yield put(signinUserFailure("Login Failed"));
        }
    } catch (error) {
        yield put(signinUserFailure(error));
    }
}

/**
 * @description Signin User
 * @method signInUser
 * @generator
 */
export function* signinUser() {
    yield takeEvery(LOGIN_USER, signInUserWithEmailPassword);
}

/**
 * @description Signout Request
 * @const signOutRequest
 * @async
 */
function* signOut() {
    try {
        localStorage.removeItem('user_id');
        localStorage.removeItem('token');
        localStorage.removeItem('access_token');
        localStorage.removeItem('refresh_token');
        yield put(logoutUserSuccess());
    } catch (error) {
        yield put(logoutUserFailure());
    }
}

/**
 * @description Signout User
 * @method signOutUser
 * @generator
 */
export function* signOutUser() {
    yield takeEvery(LOGOUT_USER, signOut);
}

/**
 * GPS MIDDLEWARE
 */
/**
 * @description Generate Password Token Middleware
 * @const generatePasswordTokenRequest
 * @async
 * @returns
 */
const generatePasswordTokenRequest = async (grant_type) => {
    GPS_MIDDLEWARE_OAUTH_REQUEST.grant_type = grant_type;
    return await axios.post(API_GENERATE_PASSWORD_TOKEN, GPS_MIDDLEWARE_OAUTH_REQUEST, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
    .then(response => response)
    .catch(error => error);
}

/**
 * @description Generate Password Token Middleware
 * @method generatePasswordTokenFromServer
 * @generator
 */
function* generatePasswordTokenFromServer({ payload }) {
    const grant_type = payload;
    try {
        const response = yield call(generatePasswordTokenRequest, grant_type);
        if(response.statusText === "OK") {
            localStorage.setItem('access_token', response.data.access_token);
            localStorage.setItem('refresh_token', response.data.refresh_token);
            yield put(generatePasswordTokenSuccess(response.data))
        } else {
            yield put(generatePasswordTokenFailure(response.error))
        }
    } catch (error) {
        yield put(generatePasswordTokenFailure(error));
    }
}

/**
 * @description Generate Password Token Middleware
 * @method generatePasswordToken
 * @generator
 * @export
 */
export function* generatePasswordToken() {
    yield takeEvery(GENERATE_PASSWORD_TOKEN, generatePasswordTokenFromServer);
}

/**
 * @description Auth Root Saga
 * @export
 * @method rootSaga
 */
export default function* rootSaga() {
    yield all([
        fork(signinUser),
        fork(signOutUser),
        fork(generatePasswordToken)
    ]);
}