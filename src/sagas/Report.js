/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 09:16:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-24 09:36:24
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {REPORT_HISTORICAL, REFRESH_TOKEN, REPORT_FLEET_UTILIZATION, REPORT_OVERSPEED, REPORT_KM_DRIVEN, REPORT_DRIVER_SCORE, REPORT_OUT_OF_GEOFENCE, REPORT_GPS_NOT_UPDATE, REPORT_NOTIFICATION, REPORT_UNPLUGGED} from '../constants/LibraryAPI';
import {getReportHistoricalSuccess, getReportHistoricalFailure, getReportFleetUtilSuccess, getReportFleetUtilFailure, getReportOverspeedSuccess, getReportOverspeedFailure, getReportKmDrivenSuccess, getReportKmDrivenFailure, getReportDriverScoreSuccess, getReportDriverScoreFailure, getReportOutOfGeofenceSuccess, getReportOutOfGeofenceFailure, getReportGpsNotUpdateSuccess, getReportGpsNotUpdateFailure, getReportNotificationSuccess, getReportNotificatonFailure, getReportUnpluggedSuccess, getReportUnpluggedFailure} from '../actions';
import { REPORT_HISTORICAL_REQUEST, REPORT_FLEET_UTILIZATION_REQUEST, REPORT_OVERSPEED_REQUEST, REPORT_KM_DRIVEN_REQUEST, REPORT_DRIVER_SCORE_REQUEST, REPORT_OUT_OF_GEOFENCE_REQUEST, REPORT_GPS_NOT_UPDATE_REQUEST, REPORT_NOTIFICATION_REQUEST, REPORT_UNPLUGGED_REQUEST } from '../actions/types';
import {objectToQueryString} from '../helpers/helpers';

const getReportHistoricalRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_HISTORICAL}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportHistoricalFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportHistoricalRequest, params);
        if(response.data) {
            yield put(getReportHistoricalSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_HISTORICAL_REQUEST, getReportHistoricalFromServer);
        }
    } catch (error) {
        yield put(getReportHistoricalFailure(error));
    }
}

export function* getReportHistorical() {
    yield takeEvery(REPORT_HISTORICAL_REQUEST, getReportHistoricalFromServer)
}

const getReportFleetUtilRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_FLEET_UTILIZATION}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportFleetUtilFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportFleetUtilRequest, params);
        if(response.data) {
            yield put(getReportFleetUtilSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_FLEET_UTILIZATION_REQUEST, getReportFleetUtilFromServer);
        }
    } catch (error) {
        yield put(getReportFleetUtilFailure(error));
    }
}

export function* getReportUtil() {
    yield takeEvery(REPORT_FLEET_UTILIZATION_REQUEST, getReportFleetUtilFromServer)
}

const getReportOverspeedRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_OVERSPEED}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportOverspeedFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportOverspeedRequest, params);
        if(response.data) {
            yield put(getReportOverspeedSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_OVERSPEED_REQUEST, getReportOverspeedFromServer);
        }
    } catch (error) {
        yield put(getReportOverspeedFailure(error));
    }
}

export function* getReportOverspeed() {
    yield takeEvery(REPORT_OVERSPEED_REQUEST, getReportOverspeedFromServer)
}

const getReportKmDrivenRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_KM_DRIVEN}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportKmDrivenFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportKmDrivenRequest, params);
        if(response.data) {
            yield put(getReportKmDrivenSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_KM_DRIVEN_REQUEST, getReportKmDrivenFromServer);
        }
    } catch (error) {
        yield put(getReportKmDrivenFailure(error));
    }
}

export function* getReportKmDriven() {
    yield takeEvery(REPORT_KM_DRIVEN_REQUEST, getReportKmDrivenFromServer)
}

const getReportDriverScoreRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_DRIVER_SCORE}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportDriverScoreFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportDriverScoreRequest, params);
        if(response.data) {
            yield put(getReportDriverScoreSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_DRIVER_SCORE_REQUEST, getReportDriverScoreFromServer);
        }
    } catch (error) {
        yield put(getReportDriverScoreFailure(error));
    }
}

export function* getReportDriverScore() {
    yield takeEvery(REPORT_DRIVER_SCORE_REQUEST, getReportDriverScoreFromServer);
}

const getReportOutOfGeofenceRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_OUT_OF_GEOFENCE}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportOutOfGeofenceFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportOutOfGeofenceRequest, params);
        if(response.data) {
            yield put(getReportOutOfGeofenceSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_OUT_OF_GEOFENCE_REQUEST, getReportOutOfGeofenceFromServer);
        }
    } catch (error) {
        yield put(getReportOutOfGeofenceFailure(error));
    }
}

export function* getReportOutOfGeofence() {
    yield takeEvery(REPORT_OUT_OF_GEOFENCE_REQUEST, getReportOutOfGeofenceFromServer);
}

const getReportGpsNotUpdateRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_GPS_NOT_UPDATE}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportGpsNotUpdateFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportGpsNotUpdateRequest, params);
        if(response.data) {
            yield put(getReportGpsNotUpdateSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_GPS_NOT_UPDATE_REQUEST, getReportGpsNotUpdateFromServer);
        }
    } catch (error) {
        yield put(getReportGpsNotUpdateFailure(error));
    }
}

export function* getReportGpsNotUpdate() {
    yield takeEvery(REPORT_GPS_NOT_UPDATE_REQUEST, getReportGpsNotUpdateFromServer);
}

const getReportNotificationRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_NOTIFICATION}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportNotificationFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getReportNotificationRequest, params);
        if(response.data) {
            yield put(getReportNotificationSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_NOTIFICATION_REQUEST, getReportNotificationFromServer);
        }
    } catch (error) {
        yield put(getReportNotificatonFailure(error));
    }
}

export function* getReportNotification() {
    yield takeEvery(REPORT_NOTIFICATION_REQUEST, getReportNotificationFromServer);
}

const getReportUnpluggedRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params)
    return await axios.get(`${REPORT_UNPLUGGED}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getReportUnpluggedFromServer({ payload }) {
    try {
        const params = payload;
        const response = yield call(getReportUnpluggedRequest, params);
        if(response.data) {
            yield put(getReportUnpluggedSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(REPORT_UNPLUGGED_REQUEST, getReportUnpluggedFromServer);
        }
    } catch (error) {
        yield put(getReportUnpluggedFailure(error));
    }
}

export function* getReportUnplugged() {
    yield takeEvery(REPORT_UNPLUGGED_REQUEST, getReportUnpluggedFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getReportHistorical),
        fork(getReportUtil),
        fork(getReportOverspeed),
        fork(getReportKmDriven),
        fork(getReportDriverScore),
        fork(getReportOutOfGeofence),
        fork(getReportGpsNotUpdate),
        fork(getReportNotification),
        fork(getReportUnplugged)
    ])
}