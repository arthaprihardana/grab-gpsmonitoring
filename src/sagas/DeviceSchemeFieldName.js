/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 15:02:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:10:58
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_SCHEME_FIELD_NAME, API_GENERATE_REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getDeviceSchemeFieldNameSuccess, getDeviceSchemeFieldNameFailure } from '../actions';
import { GET_DEVICE_SCHEME_FIELD_NAME } from '../actions/types';

const getDeviceSchemeFieldNameRequest = async (pagination) => {
    const {page} = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_SCHEME_FIELD_NAME}?query=xxxx&per_page=500`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDeviceSchemeFielNameFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDeviceSchemeFieldNameRequest, pagination);
        if(response.data) {
            yield put(getDeviceSchemeFieldNameSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE_SCHEME_FIELD_NAME, getDeviceSchemeFielNameFromServer);
        }
    } catch (error) {
        yield put(getDeviceSchemeFieldNameFailure(error));
    }
}

export function* getDeviceSchemeFielName() {
    yield takeEvery(GET_DEVICE_SCHEME_FIELD_NAME, getDeviceSchemeFielNameFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDeviceSchemeFielName)
    ])
}