/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 12:56:51 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 07:28:35
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {NOTIFICATION, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getNotificationsSuccess, getNotificationsFailure, addNotificationSuccess, addNotificationFailure, updateNotificationSuccess, updateNotificationFailure} from '../actions/MsNotificationsActions';
import {GET_NOTIFICATIONS, ADD_NOTIFICATIONS, UPDATE_NOTIFICATIONS} from '../actions/types';

const getNotificationsRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${NOTIFICATION}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(response => response)
        .catch(error => error);
}
function* getNotificationsFromServer({ payload }) {
    const pagination = payload
    try {
        const response = yield call(getNotificationsRequest, pagination);
        if(response.data) {
            yield put(getNotificationsSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_NOTIFICATIONS, getNotificationsFromServer);
        }
    } catch (error) {
        yield put(getNotificationsFailure(error));
    }
}

export function* getNotifications() {
    yield takeEvery(GET_NOTIFICATIONS, getNotificationsFromServer);
}

const addNotificationRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(NOTIFICATION, {
        notification_name: FormData.notification_name
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .then(error => error);
}

function* addNotificationToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addNotificationRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addNotificationSuccess(data));
        } else {
            yield put(addNotificationFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addNotificationFailure(error));
    }
}

export function* addNotification() {
    yield takeEvery(ADD_NOTIFICATIONS, addNotificationToServer);
}

const updateNotificationRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${NOTIFICATION}/${id}`, {
        notification_name: FormData.notification_name
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateNotificationToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateNotificationRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateNotificationSuccess(data));
        } else {
            yield put(updateNotificationFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateNotificationFailure(error));
    }
}

export function* updateNotification() {
    yield takeEvery(UPDATE_NOTIFICATIONS, updateNotificationToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getNotifications),
        fork(addNotification),
        fork(updateNotification)
    ])
}