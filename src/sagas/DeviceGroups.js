/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 23:12:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 01:06:55
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {API_DEVICE_GROUPS, API_GENERATE_REFRESH_TOKEN} from '../constants/LibraryAPI';
import { getDeviceGroupsSuccess, getDEviceGroupsFailure, addDeviceGroupsSuccess, addDeviceGroupsFailure, updateDeviceGroupsSuccess, updateDeviceGroupsFailure } from '../actions';
import { GET_DEVICE_GROUPS, ADD_DEVICE_GROUPS, UPDATE_DEVICE_GROUPS } from '../actions/types';

const getDeviceGroupsRequest = async (pagination) => {
    const {page} = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_DEVICE_GROUPS}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDeviceGroupsFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDeviceGroupsRequest, pagination);
        if(response.data) {
            yield put(getDeviceGroupsSuccess(response.data))
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE_GROUPS, getDeviceGroupsFromServer);
        }
    } catch (error) {
        yield put(getDEviceGroupsFailure(error));
    }
}

export function* getDeviceGroups() {
    yield takeEvery(GET_DEVICE_GROUPS, getDeviceGroupsFromServer);
}

const addDeviceGroupsRequest = async (FormData) => {
    let token = localStorage.getItem('access_token');
    return await axios.post(API_DEVICE_GROUPS, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addDeviceGroupsToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addDeviceGroupsRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addDeviceGroupsSuccess(data));
        } else {
            yield put(addDeviceGroupsFailure(add.toString()))
        }
    } catch (error) {
        yield put(addDeviceGroupsFailure(error));
    }
}

export function* addDeviceGroup() {
    yield takeEvery(ADD_DEVICE_GROUPS, addDeviceGroupsToServer);
}

const updateDeviceGroupsRequest = async (id, FormData) => {
    let token = localStorage.getItem('access_token');
    return await axios.put(`${API_DEVICE_GROUPS}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDeviceGroupsToServer({ payload }) {
    const {id, FormData } = payload;
    try {
        const update = yield call(updateDeviceGroupsRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateDeviceGroupsSuccess(update));
        } else {
            yield put(updateDeviceGroupsFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateDeviceGroupsFailure(error));
    }
}

export function* updateDeviceGroups() {
    yield takeEvery(UPDATE_DEVICE_GROUPS, updateDeviceGroupsToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDeviceGroups),
        fork(addDeviceGroup),
        fork(updateDeviceGroups)
    ])
}