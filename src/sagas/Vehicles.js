/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 17:38:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 11:28:00
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {VEHICLE, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getVehiclesSuccess, getVehiclesFailure, addVehiclesSuccess, addVehiclesFailure, updateVehiclesSuccess, updateVehiclesFailure, getVehiclesByCode, getVehiclesByCodeSuccess, getVehiclesByCodeFailure} from '../actions/MsVehiclesActions';
import {GET_VEHICLES, ADD_VEHICLES, UPDATE_VEHICLES, GET_VEHICLES_BY_CODE} from '../actions/types';
import objectToQueryString from '../helpers/ObjectToQueryString';

const getVehiclesRequest = async (params) => {
    // const { page } = params;
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params);
    return await axios.get(`${VEHICLE}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getVehiclesFromServer({ payload }) {
    const params = payload;
    try {
        const response = yield call(getVehiclesRequest, params);
        if(response.data) {
            yield put(getVehiclesSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_VEHICLES, getVehiclesFromServer);
        }
    } catch (error) {
        yield put(getVehiclesFailure(error));
    }
}

export function* getVehicles() {
    yield takeEvery(GET_VEHICLES, getVehiclesFromServer);
}

const getVehicleByCodeRequest = async (code) => {
    let token = localStorage.getItem('token');
    return await axios.get(`${VEHICLE}/${code}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getVehicleByCodeFromServer({ payload }) {
    const code = payload;
    try {
        const response = yield call(getVehicleByCodeRequest, code);
        if(response.data) {
            yield put(getVehiclesByCodeSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_VEHICLES_BY_CODE, getVehicleByCodeFromServer);
        }
    } catch (error) {
        yield put(getVehiclesByCodeFailure(error))
    }
}

export function* getVehicleByCode() {
    yield takeEvery(GET_VEHICLES_BY_CODE, getVehicleByCodeFromServer);
}

const addVehiclesRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    console.log('Form Data ==>', JSON.stringify(FormData));
    return await axios.post(VEHICLE, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => {
        console.log('response =>', response)
        return response;
    })
    .catch(error => {
        console.log('error ==>', error);
        return error;
    });
}

function* addVehiclesToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addVehiclesRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addVehiclesSuccess(data));
        } else {
            yield put(addVehiclesFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addVehiclesFailure(error));
    }
}

export function* addVehicles() {
    yield takeEvery(ADD_VEHICLES, addVehiclesToServer);
}

const updateVehiclesRequest = async(id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${VEHICLE}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateVehiclesToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        let update = yield call(updateVehiclesRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateVehiclesSuccess(data));
        } else {
            yield put(updateVehiclesFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateVehiclesFailure(error));
    }
}

export function* updateVehicles() {
    yield takeEvery(UPDATE_VEHICLES, updateVehiclesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getVehicles),
        fork(getVehicleByCode),
        fork(addVehicles),
        fork(updateVehicles)
    ])
}