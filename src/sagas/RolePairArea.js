/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 09:16:47 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-10-24 09:16:47 
 */
import { all, call, fork, put, takeEvery} from 'redux-saga/effects';
import axios from 'axios';
import { ROLE_PAIR_AREA, REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getRolePairAreaSuccess, getRolePairAreaFailure, addRolePairAreaSuccess, addRolePairAreaFailure, updateRolePairAreaSuccess, updateRolePairAreaFailure } from '../actions/MsRolePairAreaActions';
import {GET_ROLE_PAIR_AREA, ADD_ROLE_PAIR_AREA, UPDATE_ROLE_PAIR_AREA} from '../actions/types';

const getRolePairAreaRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${ROLE_PAIR_AREA}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getRolePairAreaFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getRolePairAreaRequest, pagination);
        if(response.data) {
            yield put(getRolePairAreaSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_ROLE_PAIR_AREA, getRolePairAreaFromServer);
        }
    } catch (error) {
        yield put(getRolePairAreaFailure(error));
    }
}

export function* getRolePairArea() {
    yield takeEvery(GET_ROLE_PAIR_AREA, getRolePairAreaFromServer);
}

const addRolePairAreaRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(ROLE_PAIR_AREA, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addRolePairAreaToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addRolePairAreaRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.Data;
            yield put(addRolePairAreaSuccess(data));
        } else {
            yield put(addRolePairAreaFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addRolePairAreaFailure(error))
    }
}

export function* addRolePairArea() {
    yield takeEvery(ADD_ROLE_PAIR_AREA, addRolePairAreaToServer);
}

const updateRolePairAreaRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${ROLE_PAIR_AREA}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateRolePairAreaToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateRolePairAreaRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateRolePairAreaSuccess(data));
        } else {
            yield put(updateRolePairAreaFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateRolePairAreaFailure(error))
    }
}

export function* updateRolePairArea() {
    yield takeEvery(UPDATE_ROLE_PAIR_AREA, updateRolePairAreaToServer)
}

export default function* rootSaga() {
    yield all([
        fork(getRolePairArea),
        fork(addRolePairArea),
        fork(updateRolePairArea)
    ])
}