/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 11:14:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 11:28:10
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { GET_TRACKING_SUMMARY, GET_TRACKING_VEHICLE, GET_DETAIL_ADDRESS } from '../actions/types';
import { getTrackingSummarySuccess, getTrackingSummaryFailure, getTrackingVehicleSuccess, getTrackingVehicleFailure , getDetailAddressSuccess, getDetailAddressFailure} from '../actions/TrackingActions';
import {MW_MAPPING_TOTAL, REFRESH_TOKEN, MW_MAPPING, API_GET_DETAIL_ADDRESS} from '../constants/LibraryAPI';

const getTrackingVehicleRequest = async (search) => {
    let token = localStorage.getItem('token');
    let url = search ? `${MW_MAPPING}?search=${search}` : `${MW_MAPPING}`;
    return await axios.get(url, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getTrackingVehicleFromServer({ payload }) {
    let search = payload && payload.search;
    try {
        const response = yield call(getTrackingVehicleRequest, search);
        if(response.data) {
            yield put(getTrackingVehicleSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_TRACKING_VEHICLE, getTrackingVehicleFromServer);
        }
    } catch (error) {
        yield put(getTrackingVehicleFailure(error));
    }
}

export function* getTrackingVehicle() {
    yield takeEvery(GET_TRACKING_VEHICLE, getTrackingVehicleFromServer);
}

const getTrackingSummaryRequest = async() => {
    let token = localStorage.getItem('token');
    return await axios.get(`${MW_MAPPING_TOTAL}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getTrackingSummaryFromServer({ payload }) {
    try {
        const response = yield call(getTrackingSummaryRequest);
        if(response.data) {
            yield put(getTrackingSummarySuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_TRACKING_SUMMARY, getTrackingSummaryFromServer);
        }
    } catch (error) {
        yield put(getTrackingSummaryFailure(error));
    }
}

export function* getTrackingSummary() {
    yield takeEvery(GET_TRACKING_SUMMARY, getTrackingSummaryFromServer);
}

const getDetailAddressRequest = async ({ longitude, latitude }) => {
    return await axios.get(`${API_GET_DETAIL_ADDRESS}?longitude=${longitude}&latitude=${latitude}`, {
        headers: {
            'Content-Type': 'application/json',
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDetailAddressFromServer({ payload }) {
    try {
        const response = yield call(getDetailAddressRequest, payload);
        yield put(getDetailAddressSuccess(response));
    } catch (error) {
        yield put(getDetailAddressFailure(error))
    }
}

export function* getDetailAddressSaga() {
    yield takeEvery(GET_DETAIL_ADDRESS, getDetailAddressFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getTrackingSummary),
        fork(getTrackingVehicle),
        fork(getDetailAddressSaga)
    ])
}
