/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 20:56:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-04 06:24:37
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {API_DEVICE, API_GENERATE_REFRESH_TOKEN} from '../constants/LibraryAPI';
import { getDevicesSuccess, getDevicesFailure, addDeviceSuccess , addDeviceFailure, updateDeviceSuccess, updateDeviceFailure} from '../actions/DeviceActions';
import { GET_DEVICE, ADD_DEVICE, UPDATE_DEVICE } from '../actions/types';

const getDevicesRequest = async (pagination) => {
    const { page } = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_DEVICE}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDevicesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDevicesRequest, pagination);
        if(response.data) {
            yield put(getDevicesSuccess(response.data))
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE, getDevicesFromServer);
        }
    } catch (error) {
        yield put(getDevicesFailure(error))
    }
}

export function* getDevices() {
    yield takeEvery(GET_DEVICE, getDevicesFromServer);
}

const addDeviceRequest = async (FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.post(API_DEVICE, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addDeviceToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addDeviceRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addDeviceSuccess(data));
        } else {
            yield put(addDeviceFailure(add.toString()));
        }
    } catch (error) {
        yield put(addDeviceFailure(error))
    }
}

export function* addDevice() {
    yield takeEvery(ADD_DEVICE, addDeviceToServer);
}

const updateDeviceRequest = async(id, FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.put(`${API_DEVICE}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDeviceToServer({ payload }) {
    const {id, FormData } = payload;
    try {
        let update = yield call(updateDeviceRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateDeviceSuccess(update));
        } else {
            yield put(updateDeviceFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateDeviceFailure(error));
    }
}

export function* updateDevice() {
    yield takeEvery(UPDATE_DEVICE, updateDeviceToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDevices),
        fork(addDevice),
        fork(updateDevice)
    ])
}