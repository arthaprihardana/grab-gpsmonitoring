/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 21:54:11 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:32:35
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {API_DEVICE_TYPES, API_GENERATE_REFRESH_TOKEN} from '../constants/LibraryAPI';
import { getDeviceTypesSuccess, getDeviceTypesFailure, addDeviceTypesSuccess, addDeviceTypesFailure, updateDeviceTypesSuccess, updateDeviceTypesFailure } from '../actions';
import { GET_DEVICE_TYPES, ADD_DEVICE_TYPES, UPDATE_DEVICE_TYPES } from '../actions/types';

const getDeviceTypesRequest = async (pagination) => {
    const { page } = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_DEVICE_TYPES}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDeviceTypesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDeviceTypesRequest, pagination);
        if(response.data) {
            yield put(getDeviceTypesSuccess(response.data))
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE_TYPES, getDeviceTypesFromServer);
        }
    } catch (error) {
        yield put(getDeviceTypesFailure(error))
    }
}

export function* getDeviceTypes() {
    yield takeEvery(GET_DEVICE_TYPES, getDeviceTypesFromServer)
}

const addDeviceTypesRequest = async (FormData) => {
    let token = localStorage.getItem('access_token');
    return await axios.post(API_DEVICE_TYPES, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addDeviceTypesToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addDeviceTypesRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addDeviceTypesSuccess(data));
        } else {
            yield put(addDeviceTypesFailure(add.toString()))
        }
    } catch (error) {
        yield put(addDeviceTypesFailure(error));
    }
}

export function* addDeviceTypes() {
    yield takeEvery(ADD_DEVICE_TYPES, addDeviceTypesToServer);
}

const updateDeviceTypesRequest = async (id, FormData) => {
    let token = localStorage.getItem('access_token');
    return await axios.put(`${API_DEVICE_TYPES}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDeviceTypesToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateDeviceTypesRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateDeviceTypesSuccess(update));
        } else {
            yield put(updateDeviceTypesFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateDeviceTypesFailure(error));
    }
}

export function* updateDeviceTypes() {
    yield takeEvery(UPDATE_DEVICE_TYPES, updateDeviceTypesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDeviceTypes),
        fork(addDeviceTypes),
        fork(updateDeviceTypes)
    ])
}