/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 13:57:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-27 07:54:53
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {DRIVER, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getDriversSuccess, getDriversFailure, addDriverSuccess, addDriverFailure, updateDriverSuccess, updateDriverFailure} from '../actions/MsDriversActions';
import {GET_DRIVERS, ADD_DRIVERS, UPDATE_DRIVERS} from '../actions/types';

const getDriversRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${DRIVER}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDriversFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDriversRequest, pagination);
        if(response.data) {
            yield put(getDriversSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_DRIVERS, getDriversFromServer);
        }
    } catch (error) {
        yield put(getDriversFailure(error));
    }
}

export function* getDrivers() {
    yield takeEvery(GET_DRIVERS, getDriversFromServer);
}

const addDriverRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(DRIVER, {
        "name" : FormData.name,
        "spk_number" : FormData.spk_number,
        "area_code" : FormData.area_code,
        "status" : "1"
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addDriverToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addDriverRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addDriverSuccess(data));
        } else {
            yield put(addDriverFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(addDriverFailure(error));
    }
}

export function* addDriver() {
    yield takeEvery(ADD_DRIVERS, addDriverToServer);
}

const updateDriverRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${DRIVER}/${id}`, {
        "name" : FormData.name,
        "spk_number" : FormData.spk_number,
        "area_code" : FormData.area_code,
        "status" : "1"
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDriverToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateDriverRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateDriverSuccess(data));
        } else {
            yield put(updateDriverFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateDriverFailure(error));
    }
}

export function* updateDriver() {
    yield takeEvery(UPDATE_DRIVERS, updateDriverToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDrivers),
        fork(addDriver),
        fork(updateDriver)
    ])
}