/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 16:34:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 16:43:53
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { ZONE_KOORDINAT, REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getZoneKoordinatSuccess, getZoneKoordinatFailure, addDrawZoneSuccess, addDrawZoneFailure, updateDrawZoneSuccess, updateDrawZoneFailure } from '../actions';
import { GET_ZONE_KOORDINAT, ADD_DRAW_ZONE, UPDATE_DRAW_ZONE } from '../actions/types';

const getZoneKoordinatRequest = async () => {
    let token = localStorage.getItem('token');
    return await axios.get(`${ZONE_KOORDINAT}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getZoneKoordinatFromServer({ payload }) {
    try {
        const response = yield call(getZoneKoordinatRequest);
        if(response.data) {
            yield put(getZoneKoordinatSuccess(response));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_ZONE_KOORDINAT, getZoneKoordinatFromServer);
        }
    } catch (error) {
        yield put(getZoneKoordinatFailure(error));
    }
}

export function* getZoneKoordinat() {
    yield takeEvery(GET_ZONE_KOORDINAT, getZoneKoordinatFromServer);
}

const addZoneKoordinatRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(ZONE_KOORDINAT, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getZoneKoordinatToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addZoneKoordinatRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addDrawZoneSuccess(data));
        } else {
            yield put(addDrawZoneFailure("Oops! something went wrong"))
        }
    } catch (error) {
        
    }
}

export function* addDrawZone() {
    yield takeEvery(ADD_DRAW_ZONE, getZoneKoordinatToServer);
}

const updateDrawZoneKoordinatRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(`${ZONE_KOORDINAT}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDrawZoneKoordinatToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateDrawZoneKoordinatRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateDrawZoneSuccess(data));
        } else {
            yield put(updateDrawZoneFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateDrawZoneFailure(error))
    }
}

export function* updateDrawZoneKoordinat() {
    yield takeEvery(UPDATE_DRAW_ZONE, updateDrawZoneKoordinatToServer);
}

export default function* root() {
    yield all([
        fork(getZoneKoordinat),
        fork(addDrawZone),
        fork(updateDrawZoneKoordinat)
    ])
}