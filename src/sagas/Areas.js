/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-24 16:57:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-29 05:12:44
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { AREAS , REFRESH_TOKEN} from '../constants/LibraryAPI';
import { GET_AREAS, ADD_AREA , UPDATE_AREA} from '../actions/types';
import { getAreasSuccess, getAreasFailure } from '../actions';
import {addAreasSuccess, addAreasFailure, updateAreaSuccess, updateAreaFailure} from '../actions/MsAreasActions';

/**
 * @async
 * @const getAreasRequest
 * @method getAreasRequest
 * @param {object} pagination
 * @returns {Promise} Get data area from server
 */
const getAreasRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${AREAS}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

/**
 * Generator untuk menghandle request get data Area from server
 * @generator
 * @method getAreasFromServer
 * @param {object} payload
 * @yields {object} Result / Response dari get data Area
 */
function* getAreasFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getAreasRequest, pagination);
        if(response.data) {
            yield put(getAreasSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_AREAS, getAreasFromServer);
        }
    } catch (error) {
        yield put(getAreasFailure(error));
    }
}

/**
 * Method yang dipanggil untuk memanggil data area dari server
 * @method getAreas
 * @generator
 * @yields {object} Result / Response dari get data Area
 * @export
 */
export function* getAreas() {
    yield takeEvery(GET_AREAS, getAreasFromServer);
}

/**
 * @async
 * @const addAreasRequest
 * @method addAreasRequest
 * @param {*} area_name 
 * @returns {Promise} Post data Master Area to server
 */
const addAreasRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(AREAS, {
        "area_code": "ARS-0001",
        "area_name" : FormData.area_name,
        "status" : 1
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

/**
 * Generator untuk menghandle post data Master Area
 * @method addAreasToServer
 * @generator
 * @param {object} payload
 * @yields {object} Result / Response dari post data Master Area
 */
function* addAreasToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addAreasRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addAreasSuccess(data));
        } else {
            yield put(addAreasFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addAreasFailure(error));
    }
}

/**
 * Method yang dipanggil untuk post data Master Area
 * @method addArea
 * @generator
 * @yields {object} Result / Response dari post data Master Area
 */
export function* addArea() {
    yield takeEvery(ADD_AREA, addAreasToServer);
}

const updateAreasRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${AREAS}/${id}`, {
        "area_name" : FormData.area_name,
        "status" : 1
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateAreasToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateAreasRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateAreaSuccess(data));
        } else {
            yield put(updateAreaFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateAreaFailure(error));
    }
}

export function* updateArea() {
    yield takeEvery(UPDATE_AREA, updateAreasToServer);
}

/**
 * @exports
 * @generator
 * @method rootSaga
 * @yields {array} Collect semua generator untuk menghandle Master Area di dalam saga middleware 
 */
export default function* rootSaga() {
    yield all([
        fork(getAreas),
        fork(addArea),
        fork(updateArea)
    ])
}