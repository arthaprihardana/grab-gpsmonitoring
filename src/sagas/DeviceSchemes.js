/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 06:36:31 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 12:35:07
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_DEVICE_SCHEMES, API_GENERATE_REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getDeviceSchemesSuccess, getDeviceSchemesFailure, addDeviceSchemeSuccess, addDeviceSchemeFailure, updateDeviceSchemeSuccess, updateDeviceSchemeFailure } from '../actions';
import { GET_DEVICE_SCHEME, ADD_DEVICE_SCHEME, UPDATE_DEVICE_SCHEME } from '../actions/types';

const getDeviceSchemeRequest = async (pagination) => {
    const { page } = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_DEVICE_SCHEMES}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getDeviceSchemesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getDeviceSchemeRequest, pagination);
        if(response.data) {
            yield put(getDeviceSchemesSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_DEVICE_SCHEME, getDeviceSchemesFromServer);
        }
    } catch (error) {
        yield put(getDeviceSchemesFailure(error))
    }
}

export function* getDeviceSchemes() {
    yield takeEvery(GET_DEVICE_SCHEME, getDeviceSchemesFromServer);
}

const addDeviceSchemeRequest = async (FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.post(API_DEVICE_SCHEMES, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addDeviceSchemeToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addDeviceSchemeRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addDeviceSchemeSuccess(data));
        } else {
            yield put(addDeviceSchemeFailure(add.toString()));
        }
    } catch (error) {
        yield put(addDeviceSchemeFailure(error));
    }
}

export function* addDeviceScheme() {
    yield takeEvery(ADD_DEVICE_SCHEME, addDeviceSchemeToServer);
}

const updateDeviceSchemeRequest = async (id, FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.put(`${API_DEVICE_SCHEMES}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateDeviceSchemeToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateDeviceSchemeRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateDeviceSchemeSuccess(update));
        } else {
            yield put(updateDeviceSchemeFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateDeviceSchemeFailure(error));
    }
}

export function* updateDeviceScheme() {
    yield takeEvery(UPDATE_DEVICE_SCHEME, updateDeviceSchemeToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getDeviceSchemes),
        fork(addDeviceScheme),
        fork(updateDeviceScheme)
    ])
}