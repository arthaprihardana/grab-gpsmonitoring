/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-20 07:12:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-24 09:36:50
 */
/**
 * Root Sagas
 */
import { all } from 'redux-saga/effects';

// sagas middleware
import authSagas from './Auth';

import areasSagas from './Areas';
import rolesSagas from './Roles';
import rolePairAreaSagas from './RolePairArea';
import brandVehiclesSagas from './BrandVehicles';
import modelVehiclesSagas from './ModelBrandVehicle';
import statusVehiclesSagas from './StatusVehicles';
import notificationsSagas from './Notifications';
import alertsSagas from './Alerts';
import driversSagas from './Drivers';
import vehiclesSagas from './Vehicles';
import maintenanceVehicleSagas from './MaintenanceVehicles';
import transactionVehiclePairSagas from './TransactionVehiclePair';
import userProfileSagas from './UserProfile';
import zoneSagas from './Zone';
import TrackingSagas from './Tracking';
import ZoneKoordinatSagas from './ZoneKoordinat';
import ReportSagas from './Report';

// GPS MIDDLEWARE
import DeviceSaga from './Devices';
import DeviceTypesSaga from './DeviceTypes';
import DeviceModelsSaga from './DeviceModels';
import DeviceGroupSaga from './DeviceGroups';
import DeviceSchemesSaga from './DeviceSchemes';
import EventTypesSaga from './EventTypes';
import FieldNamesSaga from './FieldNames';
import DeviceSchemeFieldNameSaga from './DeviceSchemeFieldName';
import DeviceSchemeEventTypeSaga from './DeviceSchemeEventType';
import EventTypeCondition from './EventTypeCondition';
import FilterDeviceMessageSaga from './FilterDeviceMessage';

export default function* rootSaga(getState) {
    yield all([
        authSagas(),
        areasSagas(),
        rolesSagas(),
        brandVehiclesSagas(),
        modelVehiclesSagas(),
        statusVehiclesSagas(),
        notificationsSagas(),
        alertsSagas(),
        driversSagas(),
        vehiclesSagas(),
        maintenanceVehicleSagas(),
        transactionVehiclePairSagas(),
        userProfileSagas(),
        zoneSagas(),
        rolePairAreaSagas(),
        TrackingSagas(),
        ReportSagas(),
        // GPS MIDDLEWARE
        DeviceSaga(),
        DeviceTypesSaga(),
        DeviceModelsSaga(),
        DeviceGroupSaga(),
        DeviceSchemesSaga(),
        EventTypesSaga(),
        FieldNamesSaga(),
        DeviceSchemeFieldNameSaga(),
        DeviceSchemeEventTypeSaga(),
        EventTypeCondition(),
        ZoneKoordinatSagas(),
        FilterDeviceMessageSaga()
    ]);
}