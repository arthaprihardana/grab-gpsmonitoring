/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-10 00:24:34 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:06:34
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {API_EVENT_TYPE_CONDITION, API_GENERATE_PASSWORD_TOKEN} from '../constants/LibraryAPI';
import { getEventTypeConditionSuccess, getEventTypeConditionFailure } from '../actions';
import { GET_EVENT_TYPE_CONDITION } from '../actions/types';

const getEventTypeConditionRequest = async (pagination) => {
    const { page } = pagination;
    const access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_EVENT_TYPE_CONDITION}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getEventTypeConditionFromServer({ payload }) {
    const pagination = payload;
    try {
        let response = yield call(getEventTypeConditionRequest, pagination);
        if(response.statusText === "OK") {
            yield put(getEventTypeConditionSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_PASSWORD_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_EVENT_TYPE_CONDITION, getEventTypeConditionFromServer);
        }
    } catch (error) {
        yield put(getEventTypeConditionFailure(error));
    }
}

export function* getEventType() {
    yield takeEvery(GET_EVENT_TYPE_CONDITION, getEventTypeConditionFromServer);
}

export default function* rootSaga() {
    yield all([
        fork(getEventType)
    ])
}