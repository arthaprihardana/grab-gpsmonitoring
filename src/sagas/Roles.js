/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 09:55:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 12:34:38
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {ROLES, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getRolesSuccess, getRolesFailure, addRolesSuccess, addRolesFailure, updateRolesSuccess, updateRolesFailure} from '../actions/MsRolesActions';
import {GET_ROLES, ADD_ROLES, UPDATE_ROLES} from '../actions/types';

const getRolesRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${ROLES}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(response => response)
        .catch(error => error);
}

function* getRolesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getRolesRequest, pagination);
        if(response.data) {
            yield put(getRolesSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_ROLES, getRolesFromServer);
        }
    } catch (error) {
        yield put(getRolesFailure(error));
    }
}

export function* getRoles() {
    yield takeEvery(GET_ROLES, getRolesFromServer);
}

const addRolesRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(ROLES, {
        "role_name" : FormData.role_name,
        "status" : 1
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addRolesToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addRolesRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addRolesSuccess(data));
        } else {
            yield put(addRolesFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addRolesFailure(error));
    }
}

export function* addRoles() {
    yield takeEvery(ADD_ROLES, addRolesToServer);
}

const updateRolesRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${ROLES}/${id}`, {
        "role_name" : FormData.role_name,
        "status" : 1
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateRolesToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateRolesRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateRolesSuccess(data));
        } else {
            yield put(updateRolesFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateRolesSuccess(error));
    }
}

export function* updateRoles() {
    yield takeEvery(UPDATE_ROLES, updateRolesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getRoles),
        fork(addRoles),
        fork(updateRoles)
    ])
};