/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-27 06:25:55 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-02 22:51:18
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {USER_PROFILE, REFRESH_TOKEN, REGISTER_USER} from '../constants/LibraryAPI';
import { getUserProfileSuccess, getUserProfileFailure , addUserProfileSuccess, addUserProfileFailure, updateUserProfileSuccess, updateUserProfileFailure} from '../actions/UserProfileActions';
import {GET_USER_PROFILE, ADD_USER_PROFILE, UPDATE_USER_PROFILE} from '../actions/types';

const getUserProfileRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${USER_PROFILE}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response )
    .catch(error => error);
}

function* getUserProfileFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getUserProfileRequest, pagination);
        if(response.data) {
            yield put(getUserProfileSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_USER_PROFILE, getUserProfileFromServer);
        }
    } catch (error) {
        yield put(getUserProfileFailure(error));
    }
}

export function* getUserProfile() {
    yield takeEvery(GET_USER_PROFILE, getUserProfileFromServer);
}

const addUserProfileRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    console.log('formdata ==>', FormData);
    return await axios.post(REGISTER_USER, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => {
        console.log('response')
        return response;
    })
    .catch(error => {
        console.log('error', error);
        return error;
    });
}

function* addUserProfileToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addUserProfileRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addUserProfileSuccess(data));
        } else {
            yield put(addUserProfileFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(addUserProfileFailure(error));
    }
}

export function* addUserProfile() {
    yield takeEvery(ADD_USER_PROFILE, addUserProfileToServer);
}

const updateUserProfileRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${USER_PROFILE}/${id}`, {
        "email" : FormData.email,
        "password" : FormData.password,
        "first_name" : FormData.first_name,
        "last_name" : FormData.last_name,
        "no_telp" : FormData.no_telp,
        "identity" : FormData.identity,
        "telegram" : FormData.telegram,
        "role_code" : FormData.role_code,
        "notification_code" : FormData.notification_code,
        "status" : "1"
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateUserProfileToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateUserProfileRequest, id, FormData);
        console.log('update ==>', update);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateUserProfileSuccess(data));
        } else {
            yield put(updateUserProfileFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateUserProfileFailure(error));
    }
}

export function* updateUserProfile() {
    yield takeEvery(UPDATE_USER_PROFILE, updateUserProfileToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getUserProfile),
        fork(addUserProfile),
        fork(updateUserProfile)
    ])
}