/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 10:28:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-27 07:54:07
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {VEHICLE_BRAND, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getBrandVehiclesSuccess, getBrandVehiclesFailure, addBrandVehiclesSuccess, addBrandVehiclesFailure, updateBrandVehiclesSuccess, updateBrandVehiclesFailure} from '../actions/MsBrandVehiclesActions';
import {GET_BRAND_VEHICLES, ADD_BRAND_VEHICLES, UPDATE_BRAND_VEHICLES} from '../actions/types';

const getBrandVehiclesRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${VEHICLE_BRAND}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getBrandVehiclesFromServer({ payload }) {
    const pagination = payload
    try {
        const response = yield call(getBrandVehiclesRequest, pagination);
        if(response.data) {
            yield put(getBrandVehiclesSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_BRAND_VEHICLES, getBrandVehiclesFromServer);
        }
    } catch (error) {
        yield put(getBrandVehiclesFailure(error));
    }
}

export function* getBrandVehicles() {
    yield takeEvery(GET_BRAND_VEHICLES, getBrandVehiclesFromServer);
}

const addBrandVehiclesRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(VEHICLE_BRAND, {
        "brand_vehicle_name" : FormData.brand_vehicle_name,
        "status" : 1
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addBrandVehiclesToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addBrandVehiclesRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addBrandVehiclesSuccess(data));
        } else {
            yield put(addBrandVehiclesFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addBrandVehiclesFailure(error));
    }
}

export function* addBrandVehicles() {
    yield takeEvery(ADD_BRAND_VEHICLES, addBrandVehiclesToServer);
}

const updateBrandVehiclesrequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${VEHICLE_BRAND}/${id}`, {
        "brand_vehicle_name" : FormData.brand_vehicle_name,
        "status" : 1
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateBrandVehiclesToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateBrandVehiclesrequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateBrandVehiclesSuccess(data));
        } else {
            yield put(updateBrandVehiclesFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateBrandVehiclesFailure(error));
    }
}

export function* updateBrandVehicles() {
    yield takeEvery(UPDATE_BRAND_VEHICLES, updateBrandVehiclesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getBrandVehicles),
        fork(addBrandVehicles),
        fork(updateBrandVehicles)
    ])
}