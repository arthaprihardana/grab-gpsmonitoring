/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 06:59:14 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 22:51:50
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_EVENT_TYPES, API_GENERATE_REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getEventTypesSuccess, getEventTypesFailure, addEventTypesSuccess, addEventTypesFailure, updateEventTypesSuccess, updateEventTypesFailure } from '../actions';
import { GET_EVENT_TYPES, ADD_EVENT_TYPES, UPDATE_EVENT_TYPES } from '../actions/types';

const getEventTypesRequest = async (pagination) => {
    const {page} = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_EVENT_TYPES}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getEventTypesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getEventTypesRequest, pagination);
        if(response.data) {
            yield put(getEventTypesSuccess(response.data))
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_EVENT_TYPES, getEventTypesFromServer);
        }
    } catch (error) {
        yield put(getEventTypesFailure(error))
    }
}

export function* getEventTypes() {
    yield takeEvery(GET_EVENT_TYPES, getEventTypesFromServer);
}

const addEventTypesRequest = async (FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.post(API_EVENT_TYPES, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addEventTypesToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addEventTypesRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addEventTypesSuccess(data));
        } else {
            yield put(addEventTypesFailure(add.toString()));
        }
    } catch (error) {
        yield put(addEventTypesFailure(error));
    }
}

export function* addEventTypes() {
    yield takeEvery(ADD_EVENT_TYPES, addEventTypesToServer);
}

const updateEventTypesRequest = async (id, FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.put(`${API_EVENT_TYPES}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateEventTypesToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateEventTypesRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateEventTypesSuccess(update));
        } else {
            yield put(updateEventTypesFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateEventTypesFailure(error));
    }
}

export function* updateEventTypes() {
    yield takeEvery(UPDATE_EVENT_TYPES, updateEventTypesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getEventTypes),
        fork(addEventTypes),
        fork(updateEventTypes)
    ])
}