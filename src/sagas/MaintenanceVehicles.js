/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 19:39:46 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 13:49:37
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {VEHICLE_MAINTENANCE, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getMaintenanceVehiclesSuccess, getMaintenanceVehiclesFailure, addMaintenanceVehiclesSuccess, addMaintenanceVehiclesFailure, updateMaintenanceVehiclesSuccess, updateMaintenanceVehiclesFailure} from '../actions/MaintenanceVehiclesActions';
import {GET_MAINTENANCE_VEHICLES, ADD_MAINTENANCE_VEHICLES, UPDATE_MAINTENANCE_VEHICLES} from '../actions/types';
import objectToQueryString from '../helpers/ObjectToQueryString';

const getMaintenanceVehiclesRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params);
    return await axios.get(`${VEHICLE_MAINTENANCE}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getMaintenanceVehiclesFromServer({ payload }) {
    const params = payload
    try {
        const response = yield call(getMaintenanceVehiclesRequest, params);
        if(response.data) {
            yield put(getMaintenanceVehiclesSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_MAINTENANCE_VEHICLES, getMaintenanceVehiclesFromServer);
        }
    } catch (error) {
        yield put(getMaintenanceVehiclesFailure(error));
    }
}

export function* getMaintenanceVehicles() {
    yield takeEvery(GET_MAINTENANCE_VEHICLES, getMaintenanceVehiclesFromServer);
}

const addMaintenanceVehicleRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(VEHICLE_MAINTENANCE, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addMaintenanceVehicleToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addMaintenanceVehicleRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addMaintenanceVehiclesSuccess(data));
        } else {
            yield put(addMaintenanceVehiclesFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(addMaintenanceVehiclesFailure(error));
    }
}

export function* addMaintenance() {
    yield takeEvery(ADD_MAINTENANCE_VEHICLES, addMaintenanceVehicleToServer);
}

const updateMaintenanceVehiclesRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${VEHICLE_MAINTENANCE}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateMaintenanceVehiclesToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateMaintenanceVehiclesRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateMaintenanceVehiclesSuccess(data));
        } else {
            yield put(updateMaintenanceVehiclesFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateMaintenanceVehiclesFailure(error));
    }
}

export function* updateMaintenance() {
    yield takeEvery(UPDATE_MAINTENANCE_VEHICLES, updateMaintenanceVehiclesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getMaintenanceVehicles),
        fork(addMaintenance),
        fork(updateMaintenance)
    ])
}