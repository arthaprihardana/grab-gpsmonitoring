/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 19:39:44 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-11-16 11:38:45
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {TRANSACTION_VEHICLE_PAIR, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getTransactionVehiclePairSuccess, getTransactionVehiclePairFailure, addTransactionVehiclePairSuccess, addTransactionVehiclePairFailure, updateTransactionVehiclePairSuccess, updateTransactionVehiclePairFailure} from '../actions/TransactionVehiclePairActions';
import {GET_VEHICLES_DRIVER_PAIR, ADD_VEHICLES_DRIVER_PAIR, UPDATE_VEHICLES_DRIVER_PAIR} from '../actions/types';
import objectToQueryString from '../helpers/ObjectToQueryString';

const getVehiclesPairingRequest = async (params) => {
    let token = localStorage.getItem('token');
    let prm = objectToQueryString(params);
    return await axios.get(`${TRANSACTION_VEHICLE_PAIR}?${prm}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(response => response)
        .catch(error => error);
}
function* getVehiclesPairingFromServer({ payload }) {
    const params = payload
    try {
        const response = yield call(getVehiclesPairingRequest, params);
        if(response.data) {
            yield put(getTransactionVehiclePairSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_VEHICLES_DRIVER_PAIR, getVehiclesPairingFromServer);
        }
    } catch (error) {
        yield put(getTransactionVehiclePairFailure(error));
    }
}

export function* getVehilcesPairing() {
    yield takeEvery(GET_VEHICLES_DRIVER_PAIR, getVehiclesPairingFromServer);
}

const addVehiclesPairingRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(TRANSACTION_VEHICLE_PAIR, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addVehiclesPairingToServer({ payload }) {
    const { FormData } = payload;
    console.log('formdata ==>', FormData);
    
    try {
        const add = yield call(addVehiclesPairingRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addTransactionVehiclePairSuccess(data));
        } else {
            yield put(addTransactionVehiclePairFailure(add.toString()))
        }
    } catch (error) {
        yield put(addTransactionVehiclePairFailure(error))
    }
}

export function* addVehiclesPairing() {
    yield takeEvery(ADD_VEHICLES_DRIVER_PAIR, addVehiclesPairingToServer);
}

const updateVehiclesPairingRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${TRANSACTION_VEHICLE_PAIR}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateVehiclesPairingToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateVehiclesPairingRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateTransactionVehiclePairSuccess(data));
        } else {
            yield put(updateTransactionVehiclePairFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(updateTransactionVehiclePairFailure(error));
    }
}

export function* updateVehiclesPairing() {
    yield takeEvery(UPDATE_VEHICLES_DRIVER_PAIR, updateVehiclesPairingToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getVehilcesPairing),
        fork(addVehiclesPairing),
        fork(updateVehiclesPairing)
    ])
}