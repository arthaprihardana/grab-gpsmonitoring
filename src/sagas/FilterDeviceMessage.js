/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-20 23:28:50 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-20 23:48:12
 */
import { all, call, put, fork, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_FILTER_DEVICE_MESSAGES, API_GENERATE_REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getFilterDeviceMessageSuccess, getFilterDeviceMessageFailure } from '../actions';
import { GET_FILTER_DEVICES_MESSAGES } from '../actions/types';

const getFilterDeviceMessageRequest = async (FilterData) => {
    const { imei, start_time, end_time } = FilterData;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_FILTER_DEVICE_MESSAGES}?filter[imei]=${imei}&start_time=${start_time}&end_time=${end_time}&order_by=device_time&sort=asc`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getFilterDeviceMessageFromServer({ payload }) {
    const { FilterData } = payload;
    try {
        const response = yield call(getFilterDeviceMessageRequest, FilterData);
        if(response.data) {
            yield put(getFilterDeviceMessageSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_FILTER_DEVICES_MESSAGES, getFilterDeviceMessageFromServer);
        }
    } catch (error) {
        yield put(getFilterDeviceMessageFailure(error));
    }
}

export function* getFilterDeviceMessage() {
    yield takeEvery(GET_FILTER_DEVICES_MESSAGES, getFilterDeviceMessageFromServer)
}

export default function* rootSaga() {
    yield all([
        fork(getFilterDeviceMessage)
    ])
}