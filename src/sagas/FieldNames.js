/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 07:22:10 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:28:47
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { API_FIELD_NAMES, API_GENERATE_REFRESH_TOKEN } from '../constants/LibraryAPI';
import { getFieldNamesSuccess, getFieldNamesFailure, addFieldNamesSuccess, addFieldNamesFailure, updateFieldNamesSuccess, updateFieldNamesFailure } from '../actions';
import { GET_FIELD_NAMES, ADD_FIELD_NAMES, UPDATE_FIELD_NAMES } from '../actions/types';

const getFieldNamesRequest = async (pagination) => {
    const { page } = pagination;
    let access_token = localStorage.getItem('access_token');
    return await axios.get(`${API_FIELD_NAMES}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* getFieldNamesFromServer({ payload }) {
    const pagination = payload;
    try {
        const response = yield call(getFieldNamesRequest, pagination);
        if(response.data) {
            yield put(getFieldNamesSuccess(response.data));
        } else {
            let refresh_token = yield call(API_GENERATE_REFRESH_TOKEN);
            localStorage.setItem('access_token', refresh_token.data.access_token);
            localStorage.setItem('refresh_token', refresh_token.data.refresh_token);
            yield takeEvery(GET_FIELD_NAMES, getFieldNamesFromServer);
        }
    } catch (error) {
        yield put(getFieldNamesFailure(error));
    }
}

export function* getFieldNames() {
    yield takeEvery(GET_FIELD_NAMES, getFieldNamesFromServer);
}

const addFieldNamesRequest = async (FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.post(API_FIELD_NAMES, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* addFieldNamesToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addFieldNamesRequest, FormData);
        if(add.statusText === "Created") {
            let data = add.data.data;
            yield put(addFieldNamesSuccess(data));
        } else {
            yield put(addFieldNamesFailure(add.toString()));
        }
    } catch (error) {
        yield put(addFieldNamesFailure(error));
    }
}

export function* addFieldNames() {
    yield takeEvery(ADD_FIELD_NAMES, addFieldNamesToServer);
}

const updateFieldNamesRequest = async (id, FormData) => {
    let access_token = localStorage.getItem('access_token');
    return await axios.put(`${API_FIELD_NAMES}/${id}`, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateFieldNamesToServer({ payload }) {
    const { id, FormData } = payload;
    try {
        const update = yield call(updateFieldNamesRequest, id, FormData);
        if(update.statusText === "OK") {
            yield put(updateFieldNamesSuccess(update));
        } else {
            yield put(updateFieldNamesFailure(update.toString()));
        }
    } catch (error) {
        yield put(updateFieldNamesFailure(error));
    }
}

export function* updateFieldNames() {
    yield takeEvery(UPDATE_FIELD_NAMES, updateFieldNamesToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getFieldNames),
        fork(addFieldNames),
        fork(updateFieldNames)
    ])
}