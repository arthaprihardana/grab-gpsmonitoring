/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:19:42 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 05:43:14
 */
import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import {VEHICLE_MODEL, REFRESH_TOKEN} from '../constants/LibraryAPI';
import {getModelBrandVehicleSuccess, getModelBrandVehicleFailure, addModelBrandVehicleSuccess, addModelBrandVehicleFailure, updateModelBrandVehicleSuccess, updateModelBrandVehicleFailure} from '../actions/MsModelBrandVehicleActions';
import {GET_MODEL_BRAND_VEHICLES, ADD_MODEL_BRAND_VEHICLES, UPDATE_MODEL_BRAND_VEHICLES} from '../actions/types';

const getVehicleModelsRequest = async (pagination) => {
    const { page } = pagination;
    let token = localStorage.getItem('token');
    return await axios.get(`${VEHICLE_MODEL}?page=${page}`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
        .then(response => response)
        .catch(error => error);
}
function* getVehicleModelsFromServer({ payload }) {
    const pagination = payload
    try {
        const response = yield call(getVehicleModelsRequest, pagination);
        if(response.data) {
            yield put(getModelBrandVehicleSuccess(response.data.Data));
        } else {
            let refresh = yield call(REFRESH_TOKEN);
            localStorage.setItem('token', refresh.data.Data.token);
            yield takeEvery(GET_MODEL_BRAND_VEHICLES, getVehicleModelsFromServer);
        }
    } catch (error) {
        yield put(getModelBrandVehicleFailure(error));
    }
}

export function* getVehicleModels() {
    yield takeEvery(GET_MODEL_BRAND_VEHICLES, getVehicleModelsFromServer);
}

const addVehicleModelRequest = async (FormData) => {
    let token = localStorage.getItem('token');
    return await axios.post(VEHICLE_MODEL, FormData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .then(error => error);
}

function* addVehicleModelToServer({ payload }) {
    const { FormData } = payload;
    try {
        const add = yield call(addVehicleModelRequest, FormData);
        if(add.statusText === "OK") {
            let data = add.data;
            yield put(addModelBrandVehicleSuccess(data));
        } else {
            yield put(addModelBrandVehicleFailure("Oops! something went wrong"))
        }
    } catch (error) {
        yield put(addModelBrandVehicleFailure(error));
    }
}

export function* addVehicleModel() {
    yield takeEvery(ADD_MODEL_BRAND_VEHICLES, addVehicleModelToServer);
}

const updateVehicleModelRequest = async (id, FormData) => {
    let token = localStorage.getItem('token');
    return await axios.put(`${VEHICLE_MODEL}/${id}`, {
        model_vehicle_name: FormData.model_vehicle_name,
        brand_vehicle_code: FormData.brand_vehicle_code
    }, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    })
    .then(response => response)
    .catch(error => error);
}

function* updateVehicleModelToServer({ payload }) {
    const {id, FormData} = payload;
    try {
        const update = yield call(updateVehicleModelRequest, id, FormData);
        if(update.statusText === "OK") {
            let data = update.data;
            yield put(updateModelBrandVehicleSuccess(data));
        } else {
            yield put(updateModelBrandVehicleFailure("Oops! something went wrong"));
        }
    } catch (error) {
        yield put(updateModelBrandVehicleFailure(error));
    }
}

export function* updateVehicleModel() {
    yield takeEvery(UPDATE_MODEL_BRAND_VEHICLES, updateVehicleModelToServer);
}

export default function* rootSaga() {
    yield all([
        fork(getVehicleModels),
        fork(addVehicleModel),
        fork(updateVehicleModel)
    ])
}