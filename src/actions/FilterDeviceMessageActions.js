/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-20 23:15:22 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-23 15:19:11
 */
import {GET_FILTER_DEVICES_MESSAGES, GET_FILTER_DEVICES_MESSAGES_SUCCESS, GET_FILTER_DEVICES_MESSAGES_FAILURE, CLEAR_FILTER_DEVICE_MESSAGES} from './types';

export const getFilterDeviceMessage = FilterData => ({
    type: GET_FILTER_DEVICES_MESSAGES,
    payload: FilterData
});

export const getFilterDeviceMessageSuccess = response => ({
    type: GET_FILTER_DEVICES_MESSAGES_SUCCESS,
    payload: response
});

export const getFilterDeviceMessageFailure = error => ({
    type: GET_FILTER_DEVICES_MESSAGES_FAILURE,
    payload: error
});

export const clearFilterDeviceMessage = () => ({
    type: CLEAR_FILTER_DEVICE_MESSAGES
})