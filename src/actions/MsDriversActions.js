/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 13:55:23 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 13:58:29
 */
import {GET_DRIVERS, GET_DRIVERS_SUCCESS, GET_DRIVERS_FAILURE, ADD_DRIVERS, ADD_DRIVERS_SUCCESS, ADD_DRIVERS_FAILURE, UPDATE_DRIVERS, UPDATE_DRIVERS_SUCCESS, UPDATE_DRIVERS_FAILURE, OPEN_FORM_DRIVERS, CLOSE_FORM_DRIVERS} from './types';

export const getDrivers = pagination => ({
    type: GET_DRIVERS,
    payload: pagination
});

export const getDriversSuccess = response => ({
    type: GET_DRIVERS_SUCCESS,
    payload: response
});

export const getDriversFailure = error => ({
    type: GET_DRIVERS_FAILURE,
    payload: error
});

export const addDriver = driver => ({
    type: ADD_DRIVERS,
    payload: driver
});

export const addDriverSuccess = response => ({
    type: ADD_DRIVERS_SUCCESS,
    payload: response
});

export const addDriverFailure = error => ({
    type: ADD_DRIVERS_FAILURE,
    payload: error
});

export const updateDriver = driver => ({
    type: UPDATE_DRIVERS,
    payload: driver
});

export const updateDriverSuccess = response => ({
    type: UPDATE_DRIVERS_SUCCESS,
    payload: response
});

export const updateDriverFailure = error => ({
    type: UPDATE_DRIVERS_FAILURE,
    payload: error
});

export const openFormDriver = modal => ({
    type: OPEN_FORM_DRIVERS,
    payload: modal
});

export const closeFormDriver = modal => ({
    type: CLOSE_FORM_DRIVERS,
    payload: modal
});