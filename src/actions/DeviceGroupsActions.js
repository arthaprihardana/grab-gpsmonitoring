/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 23:05:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:56:14
 */
import {GET_DEVICE_GROUPS, GET_DEVICE_GROUPS_SUCCESS, GET_DEVICE_GROUPS_FAILURE, ADD_DEVICE_GROUPS, ADD_DEVICE_GROUPS_SUCCESS, ADD_DEVICE_GROUPS_FAILURE, UPDATE_DEVICE_GROUPS, UPDATE_DEVICE_GROUPS_SUCCESS, UPDATE_DEVICE_GROUPS_FAILURE, OPEN_FORM_DEVICE_GROUPS, CLOSE_FORM_DEVICE_GROUPS} from './types';

export const getDeviceGroups = pagination => ({
    type: GET_DEVICE_GROUPS,
    payload: pagination
});

export const getDeviceGroupsSuccess = response => ({
    type: GET_DEVICE_GROUPS_SUCCESS,
    payload: response
});

export const getDEviceGroupsFailure = error => ({
    type: GET_DEVICE_GROUPS_FAILURE,
    payload: error
});

export const addDeviceGroups = deviceGroups => ({
    type: ADD_DEVICE_GROUPS,
    payload: deviceGroups
});

export const addDeviceGroupsSuccess = response => ({
    type: ADD_DEVICE_GROUPS_SUCCESS,
    payload: response
});

export const addDeviceGroupsFailure = error => ({
    type: ADD_DEVICE_GROUPS_FAILURE,
    payload: error
});

export const updateDeviceGroups = deviceGroups => ({
    type: UPDATE_DEVICE_GROUPS,
    payload: deviceGroups
});

export const updateDeviceGroupsSuccess = response => ({
    type: UPDATE_DEVICE_GROUPS_SUCCESS,
    payload: response
});

export const updateDeviceGroupsFailure = error => ({
    type: UPDATE_DEVICE_GROUPS_FAILURE,
    payload: error
});

export const openFormDeviceGroups = modal => ({
    type: OPEN_FORM_DEVICE_GROUPS,
    payload: modal
});

export const closeFormDeviceGroups = modal => ({
    type: CLOSE_FORM_DEVICE_GROUPS,
    payload: modal
});