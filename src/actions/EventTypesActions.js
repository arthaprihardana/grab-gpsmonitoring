/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 06:55:01 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 22:40:01
 */
import { GET_EVENT_TYPES, GET_EVENT_TYPES_SUCCESS, GET_EVENT_TYPES_FAILURE, ADD_EVENT_TYPES, ADD_EVENT_TYPES_SUCCESS, ADD_EVENT_TYPES_FAILURE, UPDATE_EVENT_TYPES, UPDATE_EVENT_TYPES_SUCCESS, UPDATE_EVENT_TYPES_FAILURE, OPEN_FORM_EVENT_TYPES, CLOSE_FORM_EVENT_TYPES } from "./types";

export const getEventTypes = pagination => ({
    type: GET_EVENT_TYPES,
    payload: pagination
});

export const getEventTypesSuccess = response => ({
    type: GET_EVENT_TYPES_SUCCESS,
    payload: response
});

export const getEventTypesFailure = error => ({
    type: GET_EVENT_TYPES_FAILURE,
    payload: error
});

export const addEventTypes = eventTypes => ({
    type: ADD_EVENT_TYPES,
    payload: eventTypes
});

export const addEventTypesSuccess = response => ({
    type: ADD_EVENT_TYPES_SUCCESS,
    payload: response
});

export const addEventTypesFailure = error => ({
    type: ADD_EVENT_TYPES_FAILURE,
    payload: error
});

export const updateEventTypes = eventTypes => ({
    type: UPDATE_EVENT_TYPES,
    payload: eventTypes
});

export const updateEventTypesSuccess = response => ({
    type: UPDATE_EVENT_TYPES_SUCCESS,
    payload: response
});

export const updateEventTypesFailure = error => ({
    type: UPDATE_EVENT_TYPES_FAILURE,
    payload: error
});

export const openFormEventTypes = modal => ({
    type: OPEN_FORM_EVENT_TYPES,
    payload: modal
});

export const closeFormEventTypes = modal => ({
    type: CLOSE_FORM_EVENT_TYPES,
    payload: modal
});