/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-27 06:23:30 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-07 09:08:08
 */
import {GET_MASTER_ZONE, GET_MASTER_ZONE_SUCCESS, GET_MASTER_ZONE_FAILURE, ADD_MASTER_ZONE, ADD_MASTER_ZONE_SUCCESS, ADD_MASTER_ZONE_FAILURE, UPDATE_MASTER_ZONE, UPDATE_MASTER_ZONE_SUCCESS, UPDATE_MASTER_ZONE_FAILURE, CLOSE_FORM_MASTER_ZONE, OPEN_FORM_MASTER_ZONE, OPEN_DRAW_ZONE, CLOSE_DRAW_ZONE, ADD_DRAW_ZONE, ADD_DRAW_ZONE_SUCCESS, ADD_DRAW_ZONE_FAILURE, UPDATE_DRAW_ZONE, UPDATE_DRAW_ZONE_SUCCESS, UPDATE_DRAW_ZONE_FAILURE} from './types';

export const getZone = pagination => ({
    type: GET_MASTER_ZONE,
    payload: pagination
});

export const getZoneSuccess = response => ({
    type: GET_MASTER_ZONE_SUCCESS,
    payload: response
});

export const getZoneFailure = error => ({
    type: GET_MASTER_ZONE_FAILURE,
    payload: error
});

export const addZone = zone => ({
    type: ADD_MASTER_ZONE,
    payload: zone
});

export const addZoneSuccess = response => ({
    type: ADD_MASTER_ZONE_SUCCESS,
    payload: response
});

export const addZoneFailure = error => ({
    type: ADD_MASTER_ZONE_FAILURE,
    payload: error
});

export const updateZone = zone => ({
    type: UPDATE_MASTER_ZONE,
    payload: zone
});

export const updateZoneSuccess = response => ({
    type: UPDATE_MASTER_ZONE_SUCCESS,
    payload: response
});

export const updateZoneFailure = error => ({
    type: UPDATE_MASTER_ZONE_FAILURE,
    payload: error
});

export const openFormZone = modal => ({
    type: OPEN_FORM_MASTER_ZONE,
    payload: modal
});

export const closeFormZone = modal => ({
    type: CLOSE_FORM_MASTER_ZONE,
    payload: modal
});

export const openDrawZone = modal => ({
    type: OPEN_DRAW_ZONE,
    payload: modal
});

export const closeDrawZone = modal => ({
    type: CLOSE_DRAW_ZONE,
    payload: modal
});

export const addDrawZone = drawZone => ({
    type: ADD_DRAW_ZONE,
    payload: drawZone
});

export const addDrawZoneSuccess = response => ({
    type: ADD_DRAW_ZONE_SUCCESS,
    payload: response
});

export const addDrawZoneFailure = error => ({
    type: ADD_DRAW_ZONE_FAILURE,
    payload: error
});

export const updateDrawZone = drawZone => ({
    type: UPDATE_DRAW_ZONE,
    payload: drawZone
});

export const updateDrawZoneSuccess = response => ({
    type: UPDATE_DRAW_ZONE_SUCCESS,
    payload: response
});

export const updateDrawZoneFailure = error => ({
    type: UPDATE_DRAW_ZONE_FAILURE,
    payload: error
});