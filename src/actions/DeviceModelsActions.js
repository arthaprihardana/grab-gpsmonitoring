/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 22:28:59 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 00:21:12
 */
import { GET_DEVICE_MODELS, GET_DEVICE_MODELS_FAILURE, GET_DEVICE_MODELS_SUCCESS, ADD_DEVICE_MODELS, ADD_DEVICE_MODELS_SUCCESS, ADD_DEVICE_MODELS_FAILURE, UPDATE_DEVICE_MODELS, UPDATE_DEVICE_MODELS_SUCCESS, UPDATE_DEVICE_MODELS_FAILURE, OPEN_FORM_DEVICE_MODELS, CLOSE_FORM_DEVICE_MODELS } from "./types";

export const getDeviceModels = pagination => ({
    type: GET_DEVICE_MODELS,
    payload: pagination
});

export const getDeviceModelsSuccess = response => ({
    type: GET_DEVICE_MODELS_SUCCESS,
    payload: response
});

export const getDeviceModelsFailure = error => ({
    type: GET_DEVICE_MODELS_FAILURE,
    payload: error
});

export const addDeviceModels = deviceModels => ({
    type: ADD_DEVICE_MODELS,
    payload: deviceModels
});

export const addDeviceModelsSuccess = response => ({
    type: ADD_DEVICE_MODELS_SUCCESS,
    payload: response
});

export const addDeviceModelsFailure = error => ({
    type: ADD_DEVICE_MODELS_FAILURE,
    payload: error
});

export const updateDeviceModels = deviceModels => ({
    type: UPDATE_DEVICE_MODELS,
    payload: deviceModels
});

export const updateDeviceModelsSuccess = response => ({
    type: UPDATE_DEVICE_MODELS_SUCCESS,
    payload: response
});

export const updateDeviceModelsFailure = error => ({
    type: UPDATE_DEVICE_MODELS_FAILURE,
    payload: error
});

export const openFormDeviceModels = modal => ({
    type: OPEN_FORM_DEVICE_MODELS,
    payload: modal
});

export const closeFormDeviceModels = modal => ({
    type: CLOSE_FORM_DEVICE_MODELS,
    payload: modal
});