/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 10:20:56 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 10:23:08
 */
import {GET_BRAND_VEHICLES, GET_BRAND_VEHICLES_SUCCESS, GET_BRAND_VEHICLES_FAILURE, ADD_BRAND_VEHICLES, ADD_BRAND_VEHICLES_SUCCESS, ADD_BRAND_VEHICLES_FAILURE, UPDATE_BRAND_VEHICLES, UPDATE_BRAND_VEHICLES_FAILURE, UPDATE_BRAND_VEHICLES_SUCCESS, OPEN_FORM_BRAND_VEHICLES, CLOSE_FORM_BRAND_VEHICLES} from './types';

export const getBrandVehicles = pagination => ({
    type: GET_BRAND_VEHICLES,
    payload: pagination
});

export const getBrandVehiclesSuccess = response => ({
    type: GET_BRAND_VEHICLES_SUCCESS,
    payload: response
});

export const getBrandVehiclesFailure = error => ({
    type: GET_BRAND_VEHICLES_FAILURE,
    payload: error
});

export const addBrandVehicles = brand => ({
    type: ADD_BRAND_VEHICLES,
    payload: brand
});

export const addBrandVehiclesSuccess = response => ({
    type: ADD_BRAND_VEHICLES_SUCCESS,
    payload: response
});

export const addBrandVehiclesFailure = error => ({
    type: ADD_BRAND_VEHICLES_FAILURE,
    payload: error
});

export const updateBrandVehicles = brand => ({
    type: UPDATE_BRAND_VEHICLES,
    payload: brand
});

export const updateBrandVehiclesSuccess = response => ({
    type: UPDATE_BRAND_VEHICLES_SUCCESS,
    payload: response
});

export const updateBrandVehiclesFailure = error => ({
    type: UPDATE_BRAND_VEHICLES_FAILURE,
    payload: error
});

export const openFormBrandVehicles = modal => ({
    type: OPEN_FORM_BRAND_VEHICLES,
    payload: modal
});

export const closeFormBrandVehicles = modal => ({
    type: CLOSE_FORM_BRAND_VEHICLES,
    payload: modal
});
