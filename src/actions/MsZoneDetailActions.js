/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-13 18:25:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-13 18:30:29
 */
import { GET_MASTER_ZONE_DETAIL , GET_MASTER_ZONE_DETAIL_SUCCESS, GET_MASTER_ZONE_DETAIL_FAILURE, ADD_MASTER_ZONE_DETAIL, ADD_MASTER_ZONE_DETAIL_SUCCESS, ADD_MASTER_ZONE_FAILURE, UPDATE_MASTER_ZONE_DETAIL, UPDATE_MASTER_ZONE_DETAIL_SUCCESS, UPDATE_MASTER_ZONE_DETAIL_FAILURE} from "./types";

export const getDetailZone = () => ({
    type: GET_MASTER_ZONE_DETAIL
});

export const getDetailZoneSuccess = response => ({
    type: GET_MASTER_ZONE_DETAIL_SUCCESS,
    payload: response
});

export const getDetailZoneFailure = error => ({
    type: GET_MASTER_ZONE_DETAIL_FAILURE,
    payload: error
});

export const addDetailZone = FormData => ({
    type: ADD_MASTER_ZONE_DETAIL,
    payload: FormData
});

export const addDetailZoneSuccess = response => ({
    type: ADD_MASTER_ZONE_DETAIL_SUCCESS,
    payload: response
});

export const addDetailZoneFailure = error => ({
    type: ADD_MASTER_ZONE_FAILURE,
    payload: error
});

export const updateDetailZone = FormData => ({
    type: UPDATE_MASTER_ZONE_DETAIL,
    payload: FormData
});

export const updateDetailZoneSuccess = response => ({
    type: UPDATE_MASTER_ZONE_DETAIL_SUCCESS,
    payload: response
});

export const updateDetailZoneFailure = error => ({
    type: UPDATE_MASTER_ZONE_DETAIL_FAILURE,
    payload: error
});