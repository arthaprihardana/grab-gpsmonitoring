/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:39:58 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 06:35:51
 */
import {GET_STATUS_VEHICLES, GET_STATUS_VEHICLES_SUCCESS, GET_STATUS_VEHICLES_FAILURE, ADD_STATUS_VEHICLES, ADD_STATUS_VEHICLES_SUCCESS, ADD_STATUS_VEHICLES_FAILURE, UPDATE_STATUS_VEHICLES, UPDATE_STATUS_VEHICLES_SUCCESS, UPDATE_STATUS_VEHICLES_FAILURE, OPEN_FORM_STATUS_VEHICLES, CLOSE_FORM_STATUS_VEHICLES} from './types';

export const getStatusVehicles = pagination => ({
    type: GET_STATUS_VEHICLES,
    payload: pagination
});

export const getStatusVehiclesSuccess = response => ({
    type: GET_STATUS_VEHICLES_SUCCESS,
    payload: response
});

export const getStatusVehiclesFailure = error => ({
    type: GET_STATUS_VEHICLES_FAILURE,
    payload: error
});

export const addStatusVehicle = status => ({
    type: ADD_STATUS_VEHICLES,
    payload: status
});

export const addStatusVehicleSuccess = response => ({
    type: ADD_STATUS_VEHICLES_SUCCESS,
    payload: response
});

export const addStatusVehicleFailure = error => ({
    type: ADD_STATUS_VEHICLES_FAILURE,
    payload: error
});

export const updateStatusVehicle = status => ({
    type: UPDATE_STATUS_VEHICLES,
    payload: status
});

export const updateStatusVehicleSuccess = response => ({
    type: UPDATE_STATUS_VEHICLES_SUCCESS,
    payload: response
});

export const updateStatusVehicleFailure = error => ({
    type: UPDATE_STATUS_VEHICLES_FAILURE,
    payload: error
});

export const openFormStatusVehicle = modal => ({
    type: OPEN_FORM_STATUS_VEHICLES,
    payload: modal
});

export const closeFormStatusVehicle = modal => ({
    type: CLOSE_FORM_STATUS_VEHICLES,
    payload: modal
});