/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 09:47:38 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 09:50:58
 */
import {GET_ROLES, GET_ROLES_SUCCESS, GET_ROLES_FAILURE, OPEN_FORM_ROLES, CLOSE_FORM_ROLES, ADD_ROLES, ADD_ROLES_SUCCESS, ADD_ROLES_FAILURE, UPDATE_ROLES, UPDATE_ROLES_SUCCESS, UPDATE_ROLES_FAILURE} from './types';

export const getRoles = pagination => ({
    type: GET_ROLES,
    payload: pagination
});

export const getRolesSuccess = response => ({
    type: GET_ROLES_SUCCESS,
    payload: response
});

export const getRolesFailure = error => ({
    type: GET_ROLES_FAILURE,
    payload: error
});

export const addRoles = role => ({
    type: ADD_ROLES,
    payload: role
});

export const addRolesSuccess = response => ({
    type: ADD_ROLES_SUCCESS,
    payload: response
});

export const addRolesFailure = error => ({
    type: ADD_ROLES_FAILURE,
    payload: error
});

export const updateRoles = role => ({
    type: UPDATE_ROLES,
    payload: role
});

export const updateRolesSuccess = response => ({
    type: UPDATE_ROLES_SUCCESS,
    payload: response
});

export const updateRolesFailure = error => ({
    type: UPDATE_ROLES_FAILURE,
    payload: error
});

export const openFormRoles = modal => ({
    type: OPEN_FORM_ROLES,
    payload: modal
});

export const closeFormRoles = modal => ({
    type: CLOSE_FORM_ROLES,
    payload: modal
});