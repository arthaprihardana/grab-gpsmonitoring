/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-10 00:19:48 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-09-10 00:19:48 
 */
import { GET_EVENT_TYPE_CONDITION , GET_EVENT_TYPE_CONDITION_SUCCESS, GET_EVENT_TYPE_CONDITION_FAILURE} from "./types";

export const getEventTypeCondition = pagination => ({
    type: GET_EVENT_TYPE_CONDITION,
    payload: pagination
});

export const getEventTypeConditionSuccess = response => ({
    type: GET_EVENT_TYPE_CONDITION_SUCCESS,
    payload: response
});

export const getEventTypeConditionFailure = error => ({
    type: GET_EVENT_TYPE_CONDITION_FAILURE,
    payload: error
});