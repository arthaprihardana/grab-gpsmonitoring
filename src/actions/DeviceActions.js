/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 20:50:52 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 20:52:33
 */
import { GET_DEVICE, GET_DEVICE_SUCCESS, GET_DEVICE_FAILURE, ADD_DEVICE, ADD_DEVICE_SUCCESS, ADD_DEVICE_FAILURE, UPDATE_DEVICE, UPDATE_DEVICE_SUCCESS, UPDATE_DEVICE_FAILURE, OPEN_FORM_DEVICE, CLOSE_FORM_DEVICE } from "./types";

export const getDevices = pagination => ({
    type: GET_DEVICE,
    payload: pagination
});

export const getDevicesSuccess = response => ({
    type: GET_DEVICE_SUCCESS,
    payload: response
});

export const getDevicesFailure = error => ({
    type: GET_DEVICE_FAILURE,
    payload: error
});

export const addDevice = device => ({
    type: ADD_DEVICE,
    payload: device
});

export const addDeviceSuccess = response => ({
    type: ADD_DEVICE_SUCCESS,
    payload: response
});

export const addDeviceFailure = error => ({
    type: ADD_DEVICE_FAILURE,
    payload: error
});

export const updateDevice = device => ({
    type: UPDATE_DEVICE,
    payload: device
});

export const updateDeviceSuccess = response => ({
    type: UPDATE_DEVICE_SUCCESS,
    payload: response
});

export const updateDeviceFailure = error => ({
    type: UPDATE_DEVICE_FAILURE,
    payload: error
});

export const openFormDevice = modal => ({
    type: OPEN_FORM_DEVICE,
    payload: modal
});

export const closeFormDevice = modal => ({
    type: CLOSE_FORM_DEVICE,
    payload: modal
});