/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 12:54:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 07:19:16
 */
import {GET_NOTIFICATIONS, GET_NOTIFICATIONS_SUCCESS, GET_NOTIFICATIONS_FAILURE, ADD_NOTIFICATIONS, ADD_NOTIFICATIONS_SUCCESS, ADD_NOTIFICATIONS_FAILURE, UPDATE_NOTIFICATIONS, UPDATE_NOTIFICATIONS_SUCCESS, UPDATE_NOTIFICATIONS_FAILURE, OPEN_FORM_NOTIFICATIONS, CLOSE_FORM_NOTIFICATIONS} from './types';

export const getNotifications = pagination => ({
    type: GET_NOTIFICATIONS,
    payload: pagination
});

export const getNotificationsSuccess = response => ({
    type: GET_NOTIFICATIONS_SUCCESS,
    payload: response
});

export const getNotificationsFailure = error => ({
    type: GET_NOTIFICATIONS_FAILURE,
    payload: error
});

export const addNotification = model => ({
    type: ADD_NOTIFICATIONS,
    payload: model
});

export const addNotificationSuccess = response => ({
    type: ADD_NOTIFICATIONS_SUCCESS,
    payload: response
});

export const addNotificationFailure = error => ({
    type: ADD_NOTIFICATIONS_FAILURE,
    payload: error
});

export const updateNotification = model => ({
    type: UPDATE_NOTIFICATIONS,
    payload: model
});

export const updateNotificationSuccess = response => ({
    type: UPDATE_NOTIFICATIONS_SUCCESS,
    payload: response
});

export const updateNotificationFailure = error => ({
    type: UPDATE_NOTIFICATIONS_FAILURE,
    payload: error
});

export const openFormNotification = modal => ({
    type: OPEN_FORM_NOTIFICATIONS,
    payload: modal
});

export const closeFormNotification = modal => ({
    type: CLOSE_FORM_NOTIFICATIONS,
    payload: modal
});