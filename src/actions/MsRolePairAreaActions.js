/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 06:48:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-03 06:53:52
 */
import {GET_ROLE_PAIR_AREA, GET_ROLE_PAIR_AREA_SUCCESS, GET_ROLE_PAIR_AREA_FAILED, ADD_ROLE_PAIR_AREA, ADD_ROLE_PAIR_AREA_SUCCESS, ADD_ROLE_PAIR_AREA_FAILURE, UPDATE_ROLE_PAIR_AREA, UPDATE_ROLE_PAIR_AREA_SUCCESS, UPDATE_ROLE_PAIR_AREA_FAILURE, OPEN_ROLE_PAIR_AREA, CLOSE_ROLE_PAIR_AREA} from './types';

export const getRolePairArea = pagination => ({
    type: GET_ROLE_PAIR_AREA,
    payload: pagination
});

export const getRolePairAreaSuccess = response => ({
    type: GET_ROLE_PAIR_AREA_SUCCESS,
    payload: response
});

export const getRolePairAreaFailure = error => ({
    type: GET_ROLE_PAIR_AREA_FAILED,
    payload: error
});

export const addRolePairArea = FormData => ({
    type: ADD_ROLE_PAIR_AREA,
    payload: FormData
});

export const addRolePairAreaSuccess = response => ({
    type: ADD_ROLE_PAIR_AREA_SUCCESS,
    payload: response
});

export const addRolePairAreaFailure = error => ({
    type: ADD_ROLE_PAIR_AREA_FAILURE,
    payload: error
});

export const updateRolePairArea = FormData => ({
    type: UPDATE_ROLE_PAIR_AREA,
    payload: FormData
});

export const updateRolePairAreaSuccess = response => ({
    type: UPDATE_ROLE_PAIR_AREA_SUCCESS,
    payload: response
});

export const updateRolePairAreaFailure = error => ({
    type: UPDATE_ROLE_PAIR_AREA_FAILURE,
    payload: error
});

export const openFormRolePairArea = modal => ({
    type: OPEN_ROLE_PAIR_AREA,
    payload: modal
});

export const closeFormRolePairArea = modal => ({
    type: CLOSE_ROLE_PAIR_AREA,
    payload: modal
});