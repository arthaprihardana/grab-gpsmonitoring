/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 15:27:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 15:28:52
 */
import { GET_DEVICE_SCHEME_EVENT_TYPE, GET_DEVICE_SCHEME_EVENT_TYPE_SUCCESS , GET_DEVICE_SCHEME_EVENT_TYPE_FAILURE} from "./types";

export const getDeviceSchemeEventTypes = pagination => ({
    type: GET_DEVICE_SCHEME_EVENT_TYPE,
    payload: pagination
});

export const getDeviceSchemeEventTypesSuccess = response => ({
    type: GET_DEVICE_SCHEME_EVENT_TYPE_SUCCESS,
    payload: response
});

export const getDeviceSchemeEventTypesFailure = error => ({
    type: GET_DEVICE_SCHEME_EVENT_TYPE_FAILURE,
    payload: error
});