/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 06:32:28 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 12:24:07
 */
import { GET_DEVICE_SCHEME, GET_DEVICE_SCHEME_SUCCESS, GET_DEVICE_SCHEME_FAILURE, ADD_DEVICE_SCHEME, ADD_DEVICE_SCHEME_SUCCESS, ADD_DEVICE_SCHEME_FAILURE, UPDATE_DEVICE_SCHEME, UPDATE_DEVICE_SCHEME_SUCCESS, UPDATE_DEVICE_SCHEME_FAILURE, OPEN_FORM_DEVICE_SCHEME, CLOSE_FORM_DEVICE_SCHEME } from "./types";

export const getDeviceShemes = pagination => ({
    type: GET_DEVICE_SCHEME,
    payload: pagination
});

export const getDeviceSchemesSuccess = response => ({
    type: GET_DEVICE_SCHEME_SUCCESS,
    payload: response
});

export const getDeviceSchemesFailure = error => ({
    type: GET_DEVICE_SCHEME_FAILURE,
    payload: error
});

export const addDeviceScheme = deviceScheme => ({
    type: ADD_DEVICE_SCHEME,
    payload: deviceScheme
});

export const addDeviceSchemeSuccess = response => ({
    type: ADD_DEVICE_SCHEME_SUCCESS,
    payload: response
});

export const addDeviceSchemeFailure = error => ({
    type: ADD_DEVICE_SCHEME_FAILURE,
    payload: error
});

export const updateDeviceScheme = deviceScheme => ({
    type: UPDATE_DEVICE_SCHEME,
    payload: deviceScheme
});

export const updateDeviceSchemeSuccess = response => ({
    type: UPDATE_DEVICE_SCHEME_SUCCESS,
    payload: response
});

export const updateDeviceSchemeFailure = error => ({
    type: UPDATE_DEVICE_SCHEME_FAILURE,
    payload: error
});

export const openFormDeviceScheme = modal => ({
    type: OPEN_FORM_DEVICE_SCHEME,
    payload: modal
});

export const closeFormDeviceScheme = modal => ({
    type: CLOSE_FORM_DEVICE_SCHEME,
    payload: modal
});