/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 16:16:26 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-30 05:42:20
 */
import {GET_MODEL_BRAND_VEHICLES, GET_MODEL_BRAND_VEHICLES_SUCCESS, GET_MODEL_BRAND_VEHICLES_FAILURE, ADD_MODEL_BRAND_VEHICLES, ADD_MODEL_BRAND_VEHICLES_SUCCESS, ADD_MODEL_BRAND_VEHICLES_FAILURE, UPDATE_MODEL_BRAND_VEHICLES, UPDATE_MODEL_BRAND_VEHICLES_SUCCESS, UPDATE_MODEL_BRAND_VEHICLES_FAILURE, OPEN_FORM_MODAL_BRAND_VEHICLES, CLOSE_FORM_MODAL_BRAND_VEHICLES} from './types';

export const getModelBrandVehicle = pagination => ({
    type: GET_MODEL_BRAND_VEHICLES,
    payload: pagination
});

export const getModelBrandVehicleSuccess = response => ({
    type: GET_MODEL_BRAND_VEHICLES_SUCCESS,
    payload: response
});

export const getModelBrandVehicleFailure = error => ({
    type: GET_MODEL_BRAND_VEHICLES_FAILURE,
    payload: error
});

export const addModelBrandVehicle = model => ({
    type: ADD_MODEL_BRAND_VEHICLES,
    payload: model
});

export const addModelBrandVehicleSuccess = response => ({
    type: ADD_MODEL_BRAND_VEHICLES_SUCCESS,
    payload: response
});

export const addModelBrandVehicleFailure = error => ({
    type: ADD_MODEL_BRAND_VEHICLES_FAILURE,
    payload: error
});

export const updateModelBrandVehicle = model => ({
    type: UPDATE_MODEL_BRAND_VEHICLES,
    payload: model
});

export const updateModelBrandVehicleSuccess = response => ({
    type: UPDATE_MODEL_BRAND_VEHICLES_SUCCESS,
    payload: response
});

export const updateModelBrandVehicleFailure = error => ({
    type: UPDATE_MODEL_BRAND_VEHICLES_FAILURE,
    payload: error
});

export const openFormModelBrandVehicle = modal => ({
    type: OPEN_FORM_MODAL_BRAND_VEHICLES,
    payload: modal
});

export const closeFormModelBrandVehicle = modal => ({
    type: CLOSE_FORM_MODAL_BRAND_VEHICLES,
    payload: modal
});