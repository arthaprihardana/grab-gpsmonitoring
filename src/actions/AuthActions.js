/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-20 06:55:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-24 00:54:23
 */
/**
 * Auth Actions
 */
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILURE,
    LOGOUT_USER,
    LOGOUT_USER_SUCCESS,
    LOGOUT_USER_FAILURE,
    // START TO ACCESS GPS MIDDLEWARE
    GENERATE_PASSWORD_TOKEN,
    GENERATE_PASSWORD_TOKEN_SUCCESS,
    GENERATE_PASSWORD_TOKEN_FAILURE,
    GENERATE_REFRESH_TOKEN,
    GENERATE_REFRESH_TOKEN_SUCCESS,
    GENERATE_REFRESH_TOKEN_FAILURE
    // END
} from './types';

/**
 * @description Redux Action to Sign In
 * @const signIn
 * 
 * @param {*} user 
 * @param {*} history 
 */
export const signIn = (user, history) => ({
    type: LOGIN_USER,
    payload: { user, history }
});

/**
 * @description Redux Action Signin User Success
 * @const signinUserSuccess
 * @param {*} string
 */
export const signinUserSuccess = (user) => ({
    type: LOGIN_USER_SUCCESS,
    payload: user
});

/**
 * @description Redux Action Signin User Failure
 * @const signinUserFailure
 * @param {*} error
 */
export const signinUserFailure = (error) => ({
    type: LOGIN_USER_FAILURE,
    payload: error
});

/**
 * @description Redux Action To Signout
 * @const logoutUser
 */
export const logoutUser = () => ({
    type: LOGOUT_USER
});

/**
 * @description Redux Action to Logout
 * @const logoutUserSuccess
 */
export const logoutUserSuccess = () => ({
    type: LOGOUT_USER_SUCCESS
});

/**
 * @description Redux Action Signout User Failure
 * @const logoutUserFailure
 */
export const logoutUserFailure = () => ({
    type: LOGOUT_USER_FAILURE
});

/**
 * @description Redux Action generate password token from GPS Middleware
 * @const generatePasswordToken
 */
export const generatePasswordToken = grant_type => ({
    type: GENERATE_PASSWORD_TOKEN,
    payload: grant_type
});

/**
 * @description Redux Action generate password token Success
 * @const generatePasswordTokenSuccess
 * @param {*} auth 
 */
export const generatePasswordTokenSuccess = auth => ({
    type: GENERATE_PASSWORD_TOKEN_SUCCESS,
    payload: auth
});

/**
 * @description Redux Action generate password token failure
 * @const generatePasswordTokenFailure
 * @param {*} error 
 */
export const generatePasswordTokenFailure = error => ({
    type: GENERATE_PASSWORD_TOKEN_FAILURE,
    payload: error
});

/**
 * @description Redux Action generate refresh token
 * @const generateRefreshToken
 * @param {*} refresh_token 
 */
export const generateRefreshToken = refresh_token => ({
    type: GENERATE_REFRESH_TOKEN,
    payload: refresh_token
});

/**
 * @description Redux Action generate refresh token success
 * @const generateRefreshTokenSuccess
 * @param {*} auth 
 */
export const generateRefreshTokenSuccess = auth => ({
    type: GENERATE_REFRESH_TOKEN_SUCCESS,
    payload: auth
});

/**
 * @description Redux Action generate refresh token failure
 * @const generateRefreshTokenFailure
 * @param {*} error 
 */
export const generateRefreshTokenFailure = error => ({
    type: GENERATE_REFRESH_TOKEN_FAILURE,
    payload: error
});