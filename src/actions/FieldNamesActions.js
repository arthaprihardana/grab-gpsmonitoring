/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-04 07:13:54 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-10 01:23:50
 */
import { GET_FIELD_NAMES, GET_FIELD_NAMES_SUCCESS, GET_FIELD_NAMES_FAILURE, ADD_FIELD_NAMES, ADD_FIELD_NAMES_SUCCESS, ADD_FIELD_NAMES_FAILURE, UPDATE_FIELD_NAMES, UPDATE_FIELD_NAMES_SUCCESS, UPDATE_FIELD_NAMES_FAILURE, OPEN_FORM_FIELD_NAMES, CLOSE_FORM_FIELD_NAMES } from "./types";

export const getFieldNames = pagination => ({
    type: GET_FIELD_NAMES,
    payload: pagination
});

export const getFieldNamesSuccess = response => ({
    type: GET_FIELD_NAMES_SUCCESS,
    payload: response
});

export const getFieldNamesFailure = error => ({
    type: GET_FIELD_NAMES_FAILURE,
    payload: error
});

export const addFieldNames = FieldNames => ({
    type: ADD_FIELD_NAMES,
    payload: FieldNames
});

export const addFieldNamesSuccess = response => ({
    type: ADD_FIELD_NAMES_SUCCESS,
    payload: response
});

export const addFieldNamesFailure = error => ({
    type: ADD_FIELD_NAMES_FAILURE,
    payload: error
});

export const updateFieldNames = FieldNames => ({
    type: UPDATE_FIELD_NAMES,
    payload: FieldNames
});

export const updateFieldNamesSuccess = response => ({
    type: UPDATE_FIELD_NAMES_SUCCESS,
    payload: response
});

export const updateFieldNamesFailure = error => ({
    type: UPDATE_FIELD_NAMES_FAILURE,
    payload: error
});

export const openFormFieldNames = modal => ({
    type: OPEN_FORM_FIELD_NAMES,
    payload: modal
});

export const closeFormFieldNames = modal => ({
    type: CLOSE_FORM_FIELD_NAMES,
    payload: modal
});