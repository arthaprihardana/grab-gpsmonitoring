/**
 * @author: Artha Prihardana 
 * @Date: 2018-10-24 08:41:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-10-24 08:48:27
 */
import {REPORT_HISTORICAL_REQUEST, REPORT_HISTORICAL_REQUEST_SUCCESS, REPORT_HISTORICAL_REQUEST_FAILURE, REPORT_FLEET_UTILIZATION_REQUEST, REPORT_FLEET_UTILIZATION_REQUEST_SUCCESS, REPORT_FLEET_UTILIZATION_REQUEST_FAILURE, REPORT_OVERSPEED_REQUEST, REPORT_OVERSPEED_REQUEST_SUCCESS, REPORT_OVERSPEED_REQUEST_FAILURE, REPORT_KM_DRIVEN_REQUEST, REPORT_KM_DRIVEN_REQUEST_SUCCESS, REPORT_KM_DRIVEN_REQUEST_FAILURE, RESET_ALL_REPORT, REPORT_DRIVER_SCORE_REQUEST, REPORT_DRIVER_SCORE_REQUEST_SUCCESS, REPORT_DRIVER_SCORE_REQUEST_FAILURE, REPORT_OUT_OF_GEOFENCE_REQUEST, REPORT_OUT_OF_GEOFENCE_REQUEST_SUCCESS, REPORT_OUT_OF_GEOFENCE_REQUEST_FAILURE, REPORT_GPS_NOT_UPDATE_REQUEST, REPORT_GPS_NOT_UPDATE_REQUEST_SUCCESS, REPORT_GPS_NOT_UPDATE_REQUEST_FAILURE, REPORT_NOTIFICATION_REQUEST, REPORT_NOTIFICATION_REQUEST_SUCCESS, REPORT_NOTIFICATION_REQUEST_FAILURE, REPORT_UNPLUGGED, REPORT_UNPLUGGED_SUCCESS, REPORT_UNPLUGGED_FAILURE, REPORT_UNPLUGGED_REQUEST, REPORT_UNPLUGGED_REQUEST_SUCCESS, REPORT_UNPLUGGED_REQUEST_FAILURE} from './types';

// HISTORICAL REPORT
export const getReportHistorical = params => ({
    type: REPORT_HISTORICAL_REQUEST,
    payload: params
});

export const getReportHistoricalSuccess = response => ({
    type: REPORT_HISTORICAL_REQUEST_SUCCESS,
    payload: response
});

export const getReportHistoricalFailure = error => ({
    type: REPORT_HISTORICAL_REQUEST_FAILURE,
    payload: error
});

// FLEET UTILIZATION REPORT
export const getReportFleetUtil = params => ({
    type: REPORT_FLEET_UTILIZATION_REQUEST,
    payload: params
});

export const getReportFleetUtilSuccess = response => ({
    type: REPORT_FLEET_UTILIZATION_REQUEST_SUCCESS,
    payload: response
});

export const getReportFleetUtilFailure = error => ({
    type: REPORT_FLEET_UTILIZATION_REQUEST_FAILURE,
    payload: error
});

// OVERSPEED REPORT
export const getReportOverspeed = params => ({
    type: REPORT_OVERSPEED_REQUEST,
    payload: params
});

export const getReportOverspeedSuccess = response => ({
    type: REPORT_OVERSPEED_REQUEST_SUCCESS,
    payload: response
});

export const getReportOverspeedFailure = error => ({
    type: REPORT_OVERSPEED_REQUEST_FAILURE,
    payload: error
});

// KM DRIVEN REPORT
export const getReportKmDriven = params => ({
    type: REPORT_KM_DRIVEN_REQUEST,
    payload: params
});

export const getReportKmDrivenSuccess = response => ({
    type: REPORT_KM_DRIVEN_REQUEST_SUCCESS,
    payload: response
});

export const getReportKmDrivenFailure = error => ({
    type: REPORT_KM_DRIVEN_REQUEST_FAILURE,
    payload: error
});

// DRIVER SCORE REPORT
export const getReportDriverScore = params => ({
    type: REPORT_DRIVER_SCORE_REQUEST,
    payload: params
});

export const getReportDriverScoreSuccess = response => ({
    type: REPORT_DRIVER_SCORE_REQUEST_SUCCESS,
    payload: response
});

export const getReportDriverScoreFailure = error => ({
    type: REPORT_DRIVER_SCORE_REQUEST_FAILURE,
    payload: error
});

// OUT OF GEOFENCE REPORT
export const getReportOutOfGeofence = params => ({
    type: REPORT_OUT_OF_GEOFENCE_REQUEST,
    payload: params
});

export const getReportOutOfGeofenceSuccess = response => ({
    type: REPORT_OUT_OF_GEOFENCE_REQUEST_SUCCESS,
    payload: response
});

export const getReportOutOfGeofenceFailure = error => ({
    type: REPORT_OUT_OF_GEOFENCE_REQUEST_FAILURE,
    payload: error
});

// GPS NOT UPDATE REPORT
export const getReportGpsNotUpdate = params => ({
    type: REPORT_GPS_NOT_UPDATE_REQUEST,
    payload: params
});

export const getReportGpsNotUpdateSuccess = response => ({
    type: REPORT_GPS_NOT_UPDATE_REQUEST_SUCCESS,
    payload: response
});

export const getReportGpsNotUpdateFailure = error => ({
    type: REPORT_GPS_NOT_UPDATE_REQUEST_FAILURE,
    payload: error
});

// NOTIFICATION REPORT
export const getReportNotification = params => ({
    type: REPORT_NOTIFICATION_REQUEST,
    payload: params
});

export const getReportNotificationSuccess = response => ({
    type: REPORT_NOTIFICATION_REQUEST_SUCCESS,
    payload: response
});

export const getReportNotificatonFailure = error => ({
    type: REPORT_NOTIFICATION_REQUEST_FAILURE,
    payload: error
});

// UNPLUGGED REPORT
export const getReportUnplugged = params => ({
    type: REPORT_UNPLUGGED_REQUEST,
    payload: params
});

export const getReportUnpluggedSuccess = response => ({
    type: REPORT_UNPLUGGED_REQUEST_SUCCESS,
    payload: response
});

export const getReportUnpluggedFailure = error => ({
    type: REPORT_UNPLUGGED_REQUEST_FAILURE,
    payload: error
});

// 
export const resetAllReport = () => ({
    type: RESET_ALL_REPORT
});