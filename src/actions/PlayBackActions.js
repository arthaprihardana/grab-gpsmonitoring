/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-22 17:24:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-22 22:30:57
 */
import { PLAYBACK_PLAY, PLAYBACK_STOP } from "./types";

export const playbackPlay = (start_time) => ({
    type: PLAYBACK_PLAY,
    payload: start_time
});

export const playbackStop = () => ({
    type: PLAYBACK_STOP
});