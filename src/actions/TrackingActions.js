/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-13 23:30:17 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-14 11:21:36
 */
import { OPEN_DETAIL_TRACKING_VEHICLE, CLOSE_DETAIL_TRACKING_VEHICLE, GET_TRACKING_VEHICLE, GET_TRACKING_VEHICLE_SUCCESS, GET_TRACKING_VEHICLE_FAILURE, GET_TRACKING_SUMMARY, GET_TRACKING_SUMMARY_SUCCESS, GET_TRACKING_SUMMARY_FAILURE, SEARCH_NOPOL_TRACKING, SEARCH_NOPOL_TRACKING_SUCCESS, SEARCH_NOPOL_TRACKING_FAILURE, GET_DETAIL_ADDRESS, GET_DETAIL_ADDRESS_SUCCESS, GET_DETAIL_ADDRESS_FAILURE} from "./types";

export const getTrackingVehicle = search => ({
    type: GET_TRACKING_VEHICLE,
    payload: search
});

export const getTrackingVehicleSuccess = response => ({
    type: GET_TRACKING_VEHICLE_SUCCESS,
    payload: response
});

export const getTrackingVehicleFailure = error => ({
    type: GET_TRACKING_VEHICLE_FAILURE,
    payload: error
});

export const getTrackingSummary = () => ({
    type: GET_TRACKING_SUMMARY
});

export const getTrackingSummarySuccess = response => ({
    type: GET_TRACKING_SUMMARY_SUCCESS,
    payload: response
});

export const getTrackingSummaryFailure = error => ({
    type: GET_TRACKING_SUMMARY_FAILURE,
    payload: error
});

export const searchNopolTracking = search => ({
    type: SEARCH_NOPOL_TRACKING,
    payload: search
});

export const searchNopolTrackingSuccess = response => ({
    type: SEARCH_NOPOL_TRACKING_SUCCESS,
    payload: response
});

export const searchNopolTrackingFailure = error => ({
    type: SEARCH_NOPOL_TRACKING_FAILURE,
    payload: error
});

export const openDetailTrackingVehicle = data => ({
    type: OPEN_DETAIL_TRACKING_VEHICLE,
    payload: data
});

export const closeDetailTrackingVehicle = () => ({
    type: CLOSE_DETAIL_TRACKING_VEHICLE
});

export const getDetailAddress = (koordinat) => ({
    type: GET_DETAIL_ADDRESS,
    payload: koordinat
});

export const getDetailAddressSuccess = response => ({
    type: GET_DETAIL_ADDRESS_SUCCESS,
    payload: response
});

export const getDetailAddressFailure = error => ({
    type: GET_DETAIL_ADDRESS_FAILURE,
    payload: error
});