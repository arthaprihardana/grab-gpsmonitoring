/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 17:36:19 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 17:36:19 
 */
import {GET_MAINTENANCE_VEHICLES, GET_MAINTENANCE_VEHICLES_SUCCESS, GET_MAINTENANCE_VEHICLES_FAILURE, ADD_MAINTENANCE_VEHICLES, ADD_MAINTENANCE_VEHICLES_SUCCESS, ADD_MAINTENANCE_VEHICLES_FAILURE, UPDATE_MAINTENANCE_VEHICLES, UPDATE_MAINTENANCE_VEHICLES_SUCCESS, UPDATE_MAINTENANCE_VEHICLES_FAILURE, OPEN_FORM_MAINTENANCE_VEHICLES, CLOSE_FORM_MAINTENANCE_VEHICLES} from './types';

export const getMaintenanceVehicles = pagination => ({
    type: GET_MAINTENANCE_VEHICLES,
    payload: pagination
});

export const getMaintenanceVehiclesSuccess = response => ({
    type: GET_MAINTENANCE_VEHICLES_SUCCESS,
    payload: response
});

export const getMaintenanceVehiclesFailure = error => ({
    type: GET_MAINTENANCE_VEHICLES_FAILURE,
    payload: error
});

export const addMaintenanceVehicles = maintenance => ({
    type: ADD_MAINTENANCE_VEHICLES,
    payload: maintenance
});

export const addMaintenanceVehiclesSuccess = response => ({
    type: ADD_MAINTENANCE_VEHICLES_SUCCESS,
    payload: response
});

export const addMaintenanceVehiclesFailure = error => ({
    type: ADD_MAINTENANCE_VEHICLES_FAILURE,
    payload: error
});

export const updateMaintenanceVehicles = maintenance => ({
    type: UPDATE_MAINTENANCE_VEHICLES,
    payload: maintenance
});

export const updateMaintenanceVehiclesSuccess = response => ({
    type: UPDATE_MAINTENANCE_VEHICLES_SUCCESS,
    payload: response
});

export const updateMaintenanceVehiclesFailure = error => ({
    type: UPDATE_MAINTENANCE_VEHICLES_FAILURE,
    payload: error
});

export const openFormMaintenanceVehicle = modal => ({
    type: OPEN_FORM_MAINTENANCE_VEHICLES,
    payload: modal
});

export const closeFormMaintenanceVehicle = modal => ({
    type: CLOSE_FORM_MAINTENANCE_VEHICLES,
    payload: modal
});