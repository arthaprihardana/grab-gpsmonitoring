/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 20:01:23 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 20:01:23 
 */
import {GET_USER_PROFILE, GET_USER_PROFILE_SUCCESS, GET_USER_PROFILE_FAILURE, ADD_USER_PROFILE, ADD_USER_PROFILE_SUCCESS, ADD_USER_PROFILE_FAILURE, UPDATE_USER_PROFILE, UPDATE_USER_PROFILE_SUCCESS, UPDATE_USER_PROFILE_FAILURE, OPEN_FORM_USER_PROFILE, CLOSE_FORM_USER_PROFILE} from './types';

export const getUserProfile = pagination => ({
    type: GET_USER_PROFILE,
    payload: pagination
});

export const getUserProfileSuccess = response => ({
    type: GET_USER_PROFILE_SUCCESS,
    payload: response
});

export const getUserProfileFailure = error => ({
    type: GET_USER_PROFILE_FAILURE,
    payload: error
});

export const addUserProfile = user => ({
    type: ADD_USER_PROFILE,
    payload: user
});

export const addUserProfileSuccess = response => ({
    type: ADD_USER_PROFILE_SUCCESS,
    payload: response
});

export const addUserProfileFailure = error => ({
    type: ADD_USER_PROFILE_FAILURE,
    payload: error
});

export const updateUserProfile = user => ({
    type: UPDATE_USER_PROFILE,
    payload: user
});

export const updateUserProfileSuccess = response => ({
    type: UPDATE_USER_PROFILE_SUCCESS,
    payload: response
});

export const updateUserProfileFailure = error => ({
    type: UPDATE_USER_PROFILE_FAILURE,
    payload: error
});

export const openFormUserProfile = modal => ({
    type: OPEN_FORM_USER_PROFILE,
    payload: modal
});

export const closeFormUserProfile = modal => ({
    type: CLOSE_FORM_USER_PROFILE,
    payload: modal
});