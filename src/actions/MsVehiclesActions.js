/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 17:16:07 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 17:18:48
 */
import {GET_VEHICLES, GET_VEHICLES_SUCCESS, GET_VEHICLES_FAILURE, ADD_VEHICLES, ADD_VEHICLES_SUCCESS, ADD_VEHICLES_FAILURE, UPDATE_VEHICLES, UPDATE_VEHICLES_SUCCESS, UPDATE_VEHICLES_FAILURE, OPEN_FORM_VEHICLES, CLOSE_FORM_VEHICLES, GET_VEHICLES_BY_CODE, GET_VEHICLES_BY_CODE_SUCCESS, GET_VEHICLES_BY_CODE_FAILURE} from './types';

export const getVehicles = pagination => ({
    type: GET_VEHICLES,
    payload: pagination
});

export const getVehiclesSuccess = response => ({
    type: GET_VEHICLES_SUCCESS,
    payload: response
});

export const getVehiclesFailure = error => ({
    type: GET_VEHICLES_FAILURE,
    payload: error
});

export const getVehiclesByCode = code => ({
    type: GET_VEHICLES_BY_CODE,
    payload: code
});

export const getVehiclesByCodeSuccess = response => ({
    type: GET_VEHICLES_BY_CODE_SUCCESS,
    payload: response
});

export const getVehiclesByCodeFailure = error => ({
    type: GET_VEHICLES_BY_CODE_FAILURE,
    payload: error
});

export const addVehicles = vehicles => ({
    type: ADD_VEHICLES,
    payload: vehicles
});

export const addVehiclesSuccess = response => ({
    type: ADD_VEHICLES_SUCCESS,
    payload: response
});

export const addVehiclesFailure = error => ({
    type: ADD_VEHICLES_FAILURE,
    payload: error
});

export const updateVehicles = vehicles => ({
    type: UPDATE_VEHICLES,
    payload: vehicles
});

export const updateVehiclesSuccess = response => ({
    type: UPDATE_VEHICLES_SUCCESS,
    payload: response
});

export const updateVehiclesFailure = error => ({
    type: UPDATE_VEHICLES_FAILURE,
    payload: error
});

export const openFormVehicles = modal => ({
    type: OPEN_FORM_VEHICLES,
    payload: modal
});

export const closeFormVehicles = modal => ({
    type: CLOSE_FORM_VEHICLES,
    payload: modal
});