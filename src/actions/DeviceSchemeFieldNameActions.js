/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-09 14:56:27 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-09 14:57:43
 */
import { GET_DEVICE_SCHEME_FIELD_NAME, GET_DEVICE_SCHEME_FIELD_NAME_SUCCESS, GET_DEVICE_SCHEME_FIELD_NAME_FAILURE } from "./types";

export const getDeviceShcemeFieldName = pagination => ({
    type: GET_DEVICE_SCHEME_FIELD_NAME,
    payload: pagination
});

export const getDeviceSchemeFieldNameSuccess = response => ({
    type: GET_DEVICE_SCHEME_FIELD_NAME_SUCCESS,
    payload: response
});

export const getDeviceSchemeFieldNameFailure = error => ({
    type: GET_DEVICE_SCHEME_FIELD_NAME_FAILURE,
    payload: error
});