/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 19:37:24 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-08-26 19:37:24 
 */
import {GET_VEHICLES_DRIVER_PAIR, GET_VEHICLES_DRIVER_PAIR_SUCCESS, GET_VEHICLES_DRIVER_PAIR_FAILURE, ADD_VEHICLES_DRIVER_PAIR, ADD_VEHICLES_DRIVER_PAIR_SUCCESS, ADD_VEHICLES_DRIVER_PAIR_FAILURE, UPDATE_VEHICLES_DRIVER_PAIR, UPDATE_VEHICLES_DRIVER_PAIR_SUCCESS, UPDATE_VEHICLES_DRIVER_PAIR_FAILURE, OPEN_FORM_VEHICLES_DRIVER_PAIR, CLOSE_FORM_VEHICLES_DRIVER_PAIR} from './types';

export const getTransactionVehiclePair = pagination => ({
    type: GET_VEHICLES_DRIVER_PAIR,
    payload: pagination
});

export const getTransactionVehiclePairSuccess = response => ({
    type: GET_VEHICLES_DRIVER_PAIR_SUCCESS,
    payload: response
});

export const getTransactionVehiclePairFailure = error => ({
    type: GET_VEHICLES_DRIVER_PAIR_FAILURE,
    payload: error
});

export const addTransactionVehiclePair = pairing => ({
    type: ADD_VEHICLES_DRIVER_PAIR,
    payload: pairing
});

export const addTransactionVehiclePairSuccess = response => ({
    type: ADD_VEHICLES_DRIVER_PAIR_SUCCESS,
    payload: response
});

export const addTransactionVehiclePairFailure = error => ({
    type: ADD_VEHICLES_DRIVER_PAIR_FAILURE,
    payload: error
});

export const updateTransactionVehiclePair = pairing => ({
    type: UPDATE_VEHICLES_DRIVER_PAIR,
    payload: pairing
});

export const updateTransactionVehiclePairSuccess = response => ({
    type: UPDATE_VEHICLES_DRIVER_PAIR_SUCCESS,
    payload: response
});

export const updateTransactionVehiclePairFailure = error => ({
    type: UPDATE_VEHICLES_DRIVER_PAIR_FAILURE,
    payload: error
});

export const openFormTransactionVehiclePair = modal => ({
    type: OPEN_FORM_VEHICLES_DRIVER_PAIR,
    payload: modal
});

export const closeFormTransactionVehiclePair = modal => ({
    type: CLOSE_FORM_VEHICLES_DRIVER_PAIR,
    payload: modal
});