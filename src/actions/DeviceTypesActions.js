/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-03 21:50:20 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-09-08 23:30:54
 */
import { GET_DEVICE_TYPES, GET_DEVICE_TYPES_SUCCESS, GET_DEVICE_TYPES_FAILURE , ADD_DEVICE_TYPES, ADD_DEVICE_TYPES_SUCCESS, ADD_DEVICE_TYPES_FAILURE, UPDATE_DEVICE_TYPES, UPDATE_DEVICE_TYPES_SUCCESS, UPDATE_DEVICE_TYPES_FAILURE, OPEN_FORM_DEVICE_TYPES, CLOSE_FORM_DEVICE_TYPES} from "./types";

export const getDeviceTypes = pagination => ({
    type: GET_DEVICE_TYPES,
    payload: pagination
});

export const getDeviceTypesSuccess = response => ({
    type: GET_DEVICE_TYPES_SUCCESS,
    payload: response
});

export const getDeviceTypesFailure = error => ({
    type: GET_DEVICE_TYPES_FAILURE,
    payload: error
});

export const addDeviceTypes = deviceTypes => ({
    type: ADD_DEVICE_TYPES,
    payload: deviceTypes
});

export const addDeviceTypesSuccess = response => ({
    type: ADD_DEVICE_TYPES_SUCCESS,
    payload: response
});

export const addDeviceTypesFailure = error => ({
    type: ADD_DEVICE_TYPES_FAILURE,
    payload: error
});

export const updateDeviceTypes = deviceTypes => ({
    type: UPDATE_DEVICE_TYPES,
    payload: deviceTypes
});

export const updateDeviceTypesSuccess = response => ({
    type: UPDATE_DEVICE_TYPES_SUCCESS,
    payload: response
});

export const updateDeviceTypesFailure = error => ({
    type: UPDATE_DEVICE_TYPES_FAILURE,
    payload: error
});

export const openFormDeviceTypes = modal => ({
    type: OPEN_FORM_DEVICE_TYPES,
    payload: modal
});

export const closeFormDeviceTypes = modal => ({
    type: CLOSE_FORM_DEVICE_TYPES,
    payload: modal
});