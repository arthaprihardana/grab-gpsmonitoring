/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-26 13:22:33 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-26 13:26:03
 */
import {GET_ALERTS, GET_ALERTS_SUCCESS, GET_ALERTS_FAILURE, ADD_ALERTS, ADD_ALERTS_SUCCESS, ADD_ALERTS_FAILURE, UPDATE_ALERTS, UPDATE_ALERTS_SUCCESS, UPDATE_ALERTS_FAILURE, OPEN_FORM_ALERTS, CLOSE_FORM_ALERTS, GET_DETAIL_ALERT_SUMMARY, GET_DETAIL_ALERT_SUMMARY_SUCCESS, GET_DETAIL_ALERT_SUMMARY_FAILURE} from './types';

export const getAlerts = pagination => ({
    type: GET_ALERTS,
    payload: pagination
});

export const getAlertsSuccess = response => ({
    type: GET_ALERTS_SUCCESS,
    payload: response
});

export const getAlertsFailure = error => ({
    type: GET_ALERTS_FAILURE,
    payload: error
});

export const addAlert = alert => ({
    type: ADD_ALERTS,
    payload: alert
});

export const addAlertSuccess = response => ({
    type: ADD_ALERTS_SUCCESS,
    payload: response
});

export const addAlertFailure = error => ({
    type: ADD_ALERTS_FAILURE,
    payload: error
});

export const updateAlert = alert => ({
    type: UPDATE_ALERTS,
    payload: alert
});

export const updateAlertSuccess = response => ({
    type: UPDATE_ALERTS_SUCCESS,
    payload: response
});

export const updateAlertFailure = error => ({
    type: UPDATE_ALERTS_FAILURE,
    payload: error
});

export const openFormAlert = modal => ({
    type: OPEN_FORM_ALERTS,
    payload: modal
});

export const closeFormAlert = modal => ({
    type: CLOSE_FORM_ALERTS,
    payload: modal
});

export const getDetailAlertSummary = alertPriority => ({
    type: GET_DETAIL_ALERT_SUMMARY,
    payload: alertPriority
});

export const getDetailAlertSummarySuccess = response => ({
    type: GET_DETAIL_ALERT_SUMMARY_SUCCESS,
    payload: response
});

export const getDetailAlertSummaryFailure = error => ({
    type: GET_DETAIL_ALERT_SUMMARY_FAILURE,
    payload: error
});