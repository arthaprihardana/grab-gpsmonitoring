/**
 * @author: Artha Prihardana 
 * @Date: 2018-09-14 16:31:50 
 * @Last Modified by:   Artha Prihardana 
 * @Last Modified time: 2018-09-14 16:31:50 
 */
import { GET_ZONE_KOORDINAT, GET_ZONE_KOORDINAT_SUCCESS, GET_ZONE_KOORDINAT_FAILURE } from "./types";

export const getZoneKoordinat = () => ({
    type: GET_ZONE_KOORDINAT
});

export const getZoneKoordinatSuccess = response => ({
    type: GET_ZONE_KOORDINAT_SUCCESS,
    payload: response
});

export const getZoneKoordinatFailure = error => ({
    type: GET_ZONE_KOORDINAT_FAILURE,
    payload: error
});