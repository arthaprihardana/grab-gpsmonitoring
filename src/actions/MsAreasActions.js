/**
 * @author: Artha Prihardana 
 * @Date: 2018-08-24 16:48:13 
 * @Last Modified by: Artha Prihardana
 * @Last Modified time: 2018-08-29 05:06:51
 */
import {
    GET_AREAS,
    GET_AREAS_SUCCESS,
    GET_AREAS_FAILURE,
    ADD_AREA,
    ADD_AREA_SUCCESS,
    ADD_AREA_FAILURE, 
    OPEN_FORM_AREA, 
    CLOSE_FORM_AREA, 
    UPDATE_AREA, 
    UPDATE_AREA_SUCCESS, 
    UPDATE_AREA_FAILURE
} from './types';
/**
 * @exports getArea
 */
export const getAreas = pagination => ({
    type: GET_AREAS,
    payload: pagination
});
/**
 * @exports getAreasSuccess
 * @param {*} response 
 */
export const getAreasSuccess = response => ({
    type: GET_AREAS_SUCCESS,
    payload: response
});
/**
 * @export getAreasFailure
 * @param {*} error 
 */
export const getAreasFailure = error => ({
    type: GET_AREAS_FAILURE,
    payload: error
});
/**
 * @exports addAreas
 * @param {*} area 
 */
export const addAreas = area => ({
    type: ADD_AREA,
    payload: area
});
/**
 * @exports addAreasSuccess
 * @param {*} response 
 */
export const addAreasSuccess = response => ({
    type: ADD_AREA_SUCCESS,
    payload: response
});
/**
 * @export addAreasFailure
 * @param {*} error 
 */
export const addAreasFailure = error => ({
    type: ADD_AREA_FAILURE,
    payload: error
});
export const updateAreas = area => ({
    type: UPDATE_AREA,
    payload: area
});
export const updateAreaSuccess = response => ({
    type: UPDATE_AREA_SUCCESS,
    payload: response
});
export const updateAreaFailure = error => ({
    type: UPDATE_AREA_FAILURE,
    payload: error
});
/**
 * @exports openFormArea
 * @param {*} modal 
 */
export const openFormArea = modal => ({
    type: OPEN_FORM_AREA,
    payload: modal
});
/**
 * @exports closeFormArea
 * @param {*} modal 
 */
export const closeFormArea = modal => ({
    type: CLOSE_FORM_AREA,
    payload: modal
});